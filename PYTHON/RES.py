import cv2
import numpy as np
import multiprocessing as mp
from multiprocessing import Process
from threading import Thread
import datetime
import numba
from termcolor import colored as color
from termcolor import cprint
from ssh2.session import Session
import socket
from pyModbusTCP.client import ModbusClient
import time
from ftplib import FTP
import msoffcrypto
import os
import shutil
import glob
from pyxlsb import open_workbook as open_xlsb
import psycopg2
from psycopg2.extras import NamedTupleCursor
import traceback
from statistics import median

import NET.NET_from_image as NET
from  MODULS.Sort import RECIPE as RECEPIES

# =====================================================================================================================
RTSP = dict(cam1='rtsp://admin:z1x2c3v4b5@192.168.0.101:554', cam2='rtsp://admin:z1x2c3v4b5@192.168.0.102:554',
            cam3='rtsp://admin:z1x2c3v4b5@192.168.0.103:554', cam4='rtsp://admin:z1x2c3v4b5@192.168.0.104:554',
            cam5='rtsp://admin:z1x2c3v4b5@192.168.0.105:554', cam6='rtsp://admin:z1x2c3v4b5@192.168.0.106:554',
            cam7='rtsp://admin:z1x2c3v4b5@192.168.0.107:554', cam8='rtsp://admin:z1x2c3v4b5@192.168.0.108:554')

scp_port = dict(cam1=4000, cam2=4001, cam3=4002, cam4=4003, cam5=4004, cam6=4005, cam7=4006, cam8=4007,
                cam1_b=4010, cam2_b=4011, cam3_b=4012, cam4_b=4013, cam5_b=4014, cam6_b=4015, cam7_b=4016, cam8_b=4016)

Frame = dict(cam1=None, cam2=None, cam3=None, cam4=None, cam5=None, cam6=None, cam7=None, cam8=None,
             cam1_b=None, cam2_b=None, cam3_b=None, cam4_b=None, cam5_b=None, cam6_b=None, cam7_b=None, cam8_b=None,
             flag_err=None)

Frame_del_background = dict(cam1=None, cam2=None, cam3=None, cam4=None, cam5=None, cam6=None, cam7=None, cam8=None)

# положения доски на изображении
Y_min = dict(cam1=None, cam2=None, cam3=None, cam4=None, cam5=None, cam6=None, cam7=None, cam8=None)
Y_max = dict(cam1=None, cam2=None, cam3=None, cam4=None, cam5=None, cam6=None, cam7=None, cam8=None)
# ширина доски
W_board = dict(cam1=None, cam2=None, cam3=None, cam4=None, cam5=None, cam6=None, cam7=None, cam8=None)

# in % (0.0-1.0)
Roi = dict(
    cam1=[0.1, 0.5, 0, -17], cam2=[0.1, 0.5, 17, -17], cam3=[0.1, 0.5, 17, -17], cam4=[0.1, 0.5, 17, -1],
    cam1_b=[0.1, 0.5, 0, -17], cam2_b=[0.1, 0.5, 17, -17], cam3_b=[0.1, 0.5, 17, -17], cam4_b=[0.1, 0.5, 17, -1],

    cam5_b=[0.1, 0.5, 30, -1], cam6_b=[0.1, 0.5, 20, -1], cam7_b=[0.1, 0.5, 25, -260], cam8_b=[0.1, 0.5, 40, -30],
    cam5=[0.1, 0.5, 30, -1], cam6=[0.1, 0.5, 20, -1], cam7=[0.1, 0.5, 25, -260], cam8=[0.1, 0.5, 40, -30],
)

# Data exchange process
# --------------------------------------------------------------------------------------------------------------------
lock = mp.Array('i', [1, 0, 0, 0, 0, 0, 0, 0, 0, 0])
# lock[0]  - create snapshot camera, start value for initialisation. this signal most be from optical sensor sick
arr = mp.Array('i', [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
# arr[0]  - start_object_detection
# arr[1]  - cod for Net
# arr[2]  -
# arr[3]  -
# arr[4]  -
# arr[5]  -
# arr[6]  -
# arr[7]  -
# arr[8]  -
# arr[9]  - write data to reset flags rw
# arr[10] - cod sord board
flag = mp.Array('i', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
# flag[0] - sensor
# flag[1] - rw_board
# flag[2] - rw_layer
# flag[3] - rw_palet
# flag[4] - rw_input
# flag[5] - start_net
# flag[6] - TX-to write sort in plc
# flag[7] - image ready
# flag[11] - cam1 done
# flag[12] - cam2 done

ftp = mp.Array('d', [0, 0, 0])
# ftp[0]  - flow1
# ftp[1]  - flow2
# ftp[2]  - summ volum

flag_process = mp.Array('i', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
# flag_process[1] - cam1          - ready
# flag_process[2] - cam1          - ready
# flag_process[3] - cam1          - ready
# flag_process[4] - cam1          - ready
# flag_process[5] - tcp           - ready
# flag_process[6] - db            - ready
# flag_process[7] - net           - ready
# flag_process[8] - startServer   - ready
# flag_process[9] - ftp           - ready
# flag_process[10] - CREATE IMAGE - ready

tcp = mp.Array('i', [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ])

board_param = mp.Array('i', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
# board_param['x_min'] = [3]
# board_param['x_max'] = [4]
# board_param['y_min'] = [5]
# board_param['y_max'] = [6]
# board_param['width'] = [7]
# board_param['lenght']= [8]

# PATH
PATH_TO_CAM_IMAGE = "/home/ast/Desktop/_IMAGE/CAM"

# PATH_TO_SAVE_IMAGE = '/home/ast/Desktop/'
PATH_TO_SAVE_IMAGE = '/home/ast/Desktop/_IMAGE/CAM/'

_CAM_PATH_1 = "/home/ast/Desktop/_IMAGE/CAM/cam1_b.jpg"
_CAM_PATH_2 = "/home/ast/Desktop/_IMAGE/CAM/cam2_b.jpg"
_CAM_PATH_3 = "/home/ast/Desktop/_IMAGE/CAM/cam3_b.jpg"
_CAM_PATH_4 = "/home/ast/Desktop/_IMAGE/CAM/cam4_b.jpg"
_CAM_PATH_5 = "/home/ast/Desktop/_IMAGE/CAM/cam5_b.jpg"
_CAM_PATH_6 = "/home/ast/Desktop/_IMAGE/CAM/cam6_b.jpg"
_CAM_PATH_7 = "/home/ast/Desktop/_IMAGE/CAM/cam7_b.jpg"
_CAM_PATH_8 = "/home/ast/Desktop/_IMAGE/CAM/cam8_b.jpg"

CAM_PATH_1 = "/home/ast/Desktop/_IMAGE/CAM/cam1.jpg"
CAM_PATH_2 = "/home/ast/Desktop/_IMAGE/CAM/cam2.jpg"
CAM_PATH_3 = "/home/ast/Desktop/_IMAGE/CAM/cam3.jpg"
CAM_PATH_4 = "/home/ast/Desktop/_IMAGE/CAM/cam4.jpg"
CAM_PATH_5 = "/home/ast/Desktop/_IMAGE/CAM/cam5.jpg"
CAM_PATH_6 = "/home/ast/Desktop/_IMAGE/CAM/cam6.jpg"
CAM_PATH_7 = "/home/ast/Desktop/_IMAGE/CAM/cam7.jpg"
CAM_PATH_8 = "/home/ast/Desktop/_IMAGE/CAM/cam8.jpg"

TOP_IMAGE_PATH = "/home/ast/Desktop/_IMAGE/NET/top.jpg"
BOTTOM_IMAGE_PATH = "/home/ast/Desktop/_IMAGE/NET/bottom.jpg"

_PATH_TO_WRITE_TOP = "/var/www/html/your-project/storage/app/public/board/top.jpg"
_PATH_TO_WRITE_BOTTOM = "/var/www/html/your-project/storage/app/public/board/bottom.jpg"

_PATH_TO_WRITE_TOP_BOARD = "/var/www/html/your-project/storage/app/public/board/top-board.jpg"
_PATH_TO_WRITE_BOTTOM_BOARD = "/var/www/html/your-project/storage/app/public/board/bottom-board.jpg"

_PATH_TO_WRITE_TOP_RAW = "/var/www/html/your-project/storage/app/public/board/top-raw.jpg"
_PATH_TO_WRITE_BOTTOM_RAW = "/var/www/html/your-project/storage/app/public/board/bottom-raw.jpg"

FTP_TEMP_PATH = '/home/ast/Documents/ftpFiles/'


# =====================================================================================================================
# =====================================================================================================================

def Cam(url, flag, path, path_b, flag_process):
    position = int(path[-5:-4]) + 10
    positionStart = int(path[-5:-4]) + 50
    try:
        cam = cv2.VideoCapture(url)
        cprint('start process: %s, time: %s' % (path.split('/')[-1], datetime.datetime.now()), 'yellow')
        flag_cam = 0
        flag_process[position - 10] = 1
        while True:
            cam.grab()
            if flag[positionStart] == 1 or flag_cam > 0:
                flag_cam += 1
                # зона над экраном
                if flag_cam == 1:
                    ts = time.time()
                    res, frame = cam.read()
                    # saveIMG(frame, '/home/ast/Pictures/')
                    # frame = fishEyeCam(cv2.resize(frame, (1920, 1080)))[350:800, :]
                    frame1 = fishEyeCam(cv2.resize(frame, (1280, 720)))[220:450, :]
                    cv2.imwrite(path, frame1)
                    # flag[position] = 1
                # зона после экрана
                if flag_cam == 10:
                    flag_cam = 0
                    res, frame = cam.read()
                    frame = fishEyeCam(cv2.resize(frame, (1280, 720)))[220:450, :]
                    # frame = fishEyeCam(cv2.resize(frame, (1920, 1080)))[350:800, :]
                    cv2.imwrite(path_b, frame)
                    # сохраним данные для исследований
                    # saveIMG(frame)
                    flag[position] = 1
                    flag[positionStart] = 0
                    # print((time.time() - ts).__round__(4), datetime.datetime.now())
    except Exception as e:
        flag_process[position - 10] = 0
        cprint('ERR %s' % path, 'red', attrs=['reverse'])
        os.system('killall -s 9 python3')

# =====================================================================================================================

def saveIMG(image, path='/home/ast/Desktop'):
    PATH_TO_FOLDER = path
    FOLDER_NAME = time.strftime("%Y-%m-%d")
    _PATH = os.path.join(PATH_TO_FOLDER, FOLDER_NAME)
    # проверка наличия пути, если нет, то создаем папку
    if os.path.exists(_PATH):
        pass
    # print('OK')
    else:
        try:
            os.makedirs(_PATH)
        except OSError:
            print("Creation of the directory %s failed" % _PATH)
        else:
            print("Successfully created the directory %s " % _PATH)

    # формируем имя файла
    FILE_NAME = '{}.jpg'.format(len(os.listdir(_PATH)) + 1)
    # print(FILE_NAME)
    # сохраняем файл в папку
    cv2.imwrite(_PATH + '/' + FILE_NAME, image)


def fishEyeCam(frame):
    K = np.array(
        [[1987.2902013412322, 0.0, 1565.7521175856489], [0.0, 1914.5965846929532, 865.8461627100188], [0.0, 0.0, 1.0]])
    D = np.array([[-0.061326402914289105], [0.2306815273667478], [-1.002826819903716], [1.0982644076261452]])
    DIM = (3072, 1728)

    # img = cv2.imread(PATH_ROOT)
    img_dim = frame.shape[:2][::-1]
    balance = 0.0
    scaled_K = K * img_dim[0] / DIM[0]
    scaled_K[2][2] = 1.0
    new_K = cv2.fisheye.estimateNewCameraMatrixForUndistortRectify(scaled_K, D,
                                                                   img_dim, np.eye(3), balance=balance)

    map1, map2 = cv2.fisheye.initUndistortRectifyMap(scaled_K, D, np.eye(3),
                                                     new_K, img_dim, cv2.CV_16SC2)
    undist_image = cv2.remap(frame, map1, map2, interpolation=cv2.INTER_LINEAR,
                             borderMode=cv2.BORDER_CONSTANT)
    return undist_image


def grab_image(filename):
    try:
        key = filename[:-4]
        # os.system('nc -vl %s >/home/ast/Desktop/test/%s' % (scp_port[key], filename))
        frame = cv2.imread(os.path.join(PATH_TO_CAM_IMAGE, filename))
        if frame is not None:
            # frame = get_image(PATH_TO_CAM_IMAGE, filename)
            frame = cv2.resize(frame, (1280, 720), interpolation=cv2.INTER_LINEAR)
            frame = fishEyeCam(frame)
            y, x = frame.shape[:-1]
            # frame = frame[int(Roi[key][0]*y):int(Roi[key][1]*y), int(Roi[key][2]*x):int(Roi[key][3]*x)]
            frame = frame[int(Roi[key][0] * y):int(Roi[key][1] * y), Roi[key][2]:Roi[key][3]]
            # frame, _, __ = IMG.find_board_region(frame)
            Frame[key] = frame
        else:
            Frame['flag_err'] = True
            raise Exception('No image')
    except Exception as e:
        pass
        # print('grab_image ', filename, ': ', e)


def grab_image_from_server(filename):
    try:
        key = filename[:-4]
        frame = cv2.imread(os.path.join(PATH_TO_CAM_IMAGE, filename))
        if frame is not None:
            frame = frame[:, Roi[key][2]:Roi[key][3]]
            Frame[key] = frame
            # saveIMG(frame)
        else:
            Frame['flag_err'] = True
            raise Exception('No image')
    except Exception as e:
        pass
        print('grab_image ', filename, ': ', e)


def grab_img_thread():
    try:
        tr1 = Thread(target=grab_image_from_server, args=('cam1.jpg',))
        tr2 = Thread(target=grab_image_from_server, args=('cam2.jpg',))
        tr3 = Thread(target=grab_image_from_server, args=('cam3.jpg',))
        tr4 = Thread(target=grab_image_from_server, args=('cam4.jpg',))
        # tr5 = Thread(target=grab_image_from_server, args=('cam5.jpg',))
        # tr6 = Thread(target=grab_image_from_server, args=('cam6.jpg',))
        # tr7 = Thread(target=grab_image_from_server, args=('cam7.jpg',))
        # tr8 = Thread(target=grab_image_from_server, args=('cam8.jpg',))

        tr1_b = Thread(target=grab_image_from_server, args=('cam1_b.jpg',))
        tr2_b = Thread(target=grab_image_from_server, args=('cam2_b.jpg',))
        tr3_b = Thread(target=grab_image_from_server, args=('cam3_b.jpg',))
        tr4_b = Thread(target=grab_image_from_server, args=('cam4_b.jpg',))
        # tr5_b = Thread(target=grab_image_from_server, args=('cam5_b.jpg',))
        # tr6_b = Thread(target=grab_image_from_server, args=('cam6_b.jpg',))
        # tr7_b = Thread(target=grab_image_from_server, args=('cam7_b.jpg',))
        # tr8_b = Thread(target=grab_image_from_server, args=('cam8_b.jpg',))

        tr1.start()
        tr2.start()
        tr3.start()
        tr4.start()
        # tr5.start()
        # tr6.start()
        # tr7.start()
        # tr8.start()

        tr1_b.start()
        tr2_b.start()
        tr3_b.start()
        tr4_b.start()
        # tr5_b.start()
        # tr6_b.start()
        # tr7_b.start()
        # tr8_b.start()

        tr1.join()
        tr2.join()
        tr3.join()
        tr4.join()
        # tr5.join()
        # tr6.join()
        # tr7.join()
        # tr8.join()

        tr1_b.join()
        tr2_b.join()
        tr3_b.join()
        tr4_b.join()
        # tr5_b.join()
        # tr6_b.join()
        # tr7_b.join()
        # tr8_b.join()
    except Exception as e:
        print('grab_img_thread: ', e)


def grab_background(image_key, mask_key):
    path = '/home/ast/Desktop/_IMAGE/CAM/'
    key = image_key[:-4]
    try:
        origin = cv2.imread(path + image_key)
        mask = cv2.imread(path + mask_key, 0)
        if origin is not None and mask is not None:
            Frame_del_background[key] = [mask, origin]
            Y_min[key], Y_max[key], W_board[key] = find_concat_param(mask)
    except Exception as e:
        print('ERR - grab_background: %s' % e)


def delete_background():
    #
    # tr1 = Thread(target=deleteBackground_v02, args=('cam1', 'cam1_b'))
    # tr2 = Thread(target=deleteBackground_v02, args=('cam2', 'cam2_b'))
    # tr3 = Thread(target=deleteBackground_v02, args=('cam3', 'cam3_b'))
    # tr4 = Thread(target=deleteBackground_v02, args=('cam4', 'cam4_b'))

    tr1 = Thread(target=deleteBackground_v03, args=('cam1', 'cam1_b'))
    tr2 = Thread(target=deleteBackground_v03, args=('cam2', 'cam2_b'))
    tr3 = Thread(target=deleteBackground_v03, args=('cam3', 'cam3_b'))
    tr4 = Thread(target=deleteBackground_v03, args=('cam4', 'cam4_b'))

    # tr5 = Thread(target=grab_background, args=('cam5.jpg', 'cam5_b.jpg'))
    # tr6 = Thread(target=grab_background, args=('cam6.jpg', 'cam6_b.jpg'))
    # tr7 = Thread(target=grab_background, args=('cam7.jpg', 'cam7_b.jpg'))
    # tr8 = Thread(target=grab_background, args=('cam8.jpg', 'cam8_b.jpg'))
    #
    tr1.start()
    tr2.start()
    tr3.start()
    tr4.start()
    # tr5.start()
    # tr6.start()
    # tr7.start()
    # tr8.start()
    #
    tr1.join()
    tr2.join()
    tr3.join()
    tr4.join()
    # tr5.join()
    # tr6.join()
    # tr7.join()
    # tr8.join()


def concat_image_top():
    try:
        y1a, y1b = Y_min['cam1'][0], Y_min['cam1'][-1]
        y2a, y2b = Y_min['cam2'][0], Y_min['cam2'][-1]
        y3a, y3b = Y_min['cam3'][0], Y_min['cam3'][-1]
        y4a, y4b = Y_min['cam4'][0], Y_min['cam4'][-1]
        # проверим есть ли доска на изображении
        if Y_min['cam1'] and any(Y_min['cam1']) == 0:
            cam1 = False
        else:
            cam1 = True

        if Y_min['cam2'] and any(Y_min['cam2']) == 0:
            cam2 = False
        else:
            cam2 = True

        if Y_min['cam3'] and any(Y_min['cam3']) == 0:
            cam3 = False
        else:
            cam3 = True

        if Y_min['cam4'] and any(Y_min['cam4']) == 0:
            cam4 = False
        else:
            cam4 = True
        # =========================================
        dY = 0
        # -------------------------------------------------------------------------------
        a_cam = []
        a_cam_mask = []
        a_cam_raw = []

        a_cam.clear()
        a_cam_mask.clear()
        a_cam_raw.clear()
        # -------------------------------------------------------------------------------
        Y2 = []
        Y3 = []
        Y4 = []
        # -------------------------------------------------------------------------------
        if cam1:
            dY = 0
            a_cam.append(Frame_del_background['cam1'][1])
            a_cam_mask.append(Frame_del_background['cam1'][0])
            a_cam_raw.append(Frame['cam1'])
        # 1 - 2
        if cam2:
            dY = y1b - y2a
            a_cam.append(lineUp(Frame_del_background['cam2'][1], dY))
            a_cam_mask.append(lineUp(Frame_del_background['cam2'][0], dY))
            a_cam_raw.append(lineUp(Frame['cam2'], dY))
            Y2 = [i + dY for i in Y_min['cam2'] if i != 0]
        # 2 - 3
        if cam3:
            dY = y2b - y3a + dY
            a_cam.append(lineUp(Frame_del_background['cam3'][1], dY))
            a_cam_mask.append(lineUp(Frame_del_background['cam3'][0], dY))
            a_cam_raw.append(lineUp(Frame['cam3'], dY))
            Y3 = [i + dY for i in Y_min['cam3'] if i != 0]
        # 3 - 4
        if cam4:
            dY = y3b - y4a + dY
            a_cam.append(lineUp(Frame_del_background['cam4'][1], dY))
            a_cam_mask.append(lineUp(Frame_del_background['cam4'][0], dY))
            a_cam_raw.append(lineUp(Frame['cam4'], dY))
            Y4 = [i + dY for i in Y_min['cam4'] if i != 0]

        # -------------------------------------------------------------------------------
        top_image = cv2.hconcat(a_cam)
        top_image_mask = cv2.hconcat(a_cam_mask)
        top_image_raw = cv2.hconcat(a_cam_raw)
        # -------------------------------------------------------------------------------
        # fix top roll
        # y_roll = []
        # y = 0
        # y_roll = y_roll + Y_min['cam1'] + Y2 + Y3 + Y4
        # y_roll.sort(reverse=True)
        # # print(y_roll)
        # for i in range(len(y_roll)):
        #     yi = y_roll.pop()
        #     if yi != 0:
        #         y = yi
        #         break
        # top_image[:y, :] = 0
        # top_image_mask[:y, :] = 0

        # yc = int((Y_min['cam1'][1] + Y_max['cam1'][1]) / 2)
        # top_image[yc:yc + 5, :] = 255
        # top_image_mask[yc:yc + 5, :] = 255
        #

        return top_image, top_image_mask, top_image_raw


    except Exception as e:
        print('concat_image:', e)


def concat_image_bottom():
    try:
        y1a, y1b = Y_min['cam5'][0], Y_min['cam5'][-1]
        y2a, y2b = Y_min['cam6'][0], Y_min['cam6'][-1]
        y3a, y3b = Y_min['cam7'][0], Y_min['cam7'][-1]
        y4a, y4b = Y_min['cam8'][0], Y_min['cam8'][-1]

        # y1c, y1d = Y_min['cam5'][1], Y_min['cam5'][1]

        # проверим есть ли доска на изображении
        if Y_min['cam5'] and any(Y_min['cam5']) == 0:
            cam1 = False
        else:
            cam1 = True

        if Y_min['cam6'] and any(Y_min['cam6']) == 0:
            cam2 = False
        else:
            cam2 = True

        if Y_min['cam7'] and any(Y_min['cam7']) == 0:
            cam3 = False
        else:
            cam3 = True

        if Y_min['cam8'] and any(Y_min['cam8']) == 0:
            cam4 = False
        else:
            cam4 = True

        # cam1 = cam2 = cam3 = cam4 = True

        # =========================================
        dY = 0
        a_cam = []
        a_cam_mask = []
        a_cam_raw = []

        # W = Frame['cam5'].shape[0]
        pos = 1
        a_cam.clear()
        if cam1:
            # dY = int(y1c + (y1d-y1c)/2 - W/2)
            # dY = 0

            a_cam.append(Frame_del_background['cam5'][pos])
            a_cam_mask.append(Frame_del_background['cam5'][0])
            a_cam_raw.append(Frame['cam5'])
            # a_cam.append(lineUp(Frame_del_background['cam5'][pos], dY))
            # a_cam_mask.append(lineUp(Frame_del_background['cam5'][0], dY))
            # a_cam_raw.append(lineUp(Frame['cam5'], dY))

        # 1 - 2
        if cam2:
            dY = y1b - y2a
            a_cam.append(lineUp(Frame_del_background['cam6'][pos], dY))
            a_cam_mask.append(lineUp(Frame_del_background['cam6'][0], dY))
            a_cam_raw.append(lineUp(Frame['cam6'], dY))

        # 2 - 3
        if cam3:
            dY = y2b - y3a + dY
            a_cam.append(lineUp(Frame_del_background['cam7'][pos], dY))
            a_cam_mask.append(lineUp(Frame_del_background['cam7'][0], dY))
            a_cam_raw.append(lineUp(Frame['cam7'], dY))

        # 3 - 4
        if cam4:
            dY = y3b - y4a + dY
            a_cam.append(lineUp(Frame_del_background['cam8'][pos], dY))
            a_cam_mask.append(lineUp(Frame_del_background['cam8'][0], dY))
            a_cam_raw.append(lineUp(Frame['cam8'], dY))

        btm_image = cv2.hconcat(a_cam)
        btm_image_mask = cv2.hconcat(a_cam_mask)
        btm_image_raw = cv2.hconcat(a_cam_raw)

        # --------------------------------------------------------------------------------------------
        # иправим пробелы из-за рельс:
        yc = int((Y_min['cam5'][1] + Y_max['cam5'][1]) / 2)
        btm_image_mask[yc:yc + 5, :] = 255

        # cv2.imwrite('/home/ast/Desktop/_IMAGE/btm_raw.jpg', btm_image)

        # --------------------------------------------------------------------------------------------
        return btm_image, btm_image_mask, btm_image_raw

    except Exception as e:
        traceback.print_exc()
        print('concat_image:', e)


def histogramm(res):
    try:
        res = cv2.cvtColor(res, cv2.COLOR_BGR2YUV)
        res[0, :, :] = cv2.equalizeHist(res[0, :, :])
        res[:, 0, :] = cv2.equalizeHist(res[:, 0, :])
        return cv2.cvtColor(res, cv2.COLOR_YUV2BGR)
    except Exception as e:
        print('histogramm ', e)
        return None


def sepatare_btm_image():
    image = cv2.imread(_PATH_TO_WRITE_BOTTOM_RAW)
    Frame['cam5'] = image[:, :1280 - Roi['cam5'][3]]
    image = image[:, 1280 - Roi['cam5'][3]:]
    Frame['cam6'] = image[:, :1280 - Roi['cam6'][3]]
    image = image[:, 1280 - Roi['cam6'][3]:]
    Frame['cam7'] = image[:, :1280 - Roi['cam7'][3]]
    image = image[:, 1280 - Roi['cam7'][3]:]
    Frame['cam8'] = image


def CREATE_IMAGE_SENSOR(flag, arr, flag_process, board_param):
    # flag[0] - sensor
    # flag[5] - start_net
    # flag[11] - cam1 done
    # flag[12] - cam2 done
    # arr
    # arr[1] - cod for nerwork detection
    global x_max
    cprint('start process: CREATE IMAGE, time: %s' % (datetime.datetime.now()), 'yellow')
    cod = 77
    cod_b = 77
    cnt = 0
    x_max = 0
    # IMAGE_READY = False
    init = True
    IMAGE_READY = False
    # ---------------------------------------------------------------------------------------------------------
    while True:
        # -----------------------------------------------------------------------------------------------------
        flag_process[10] = 1
        # -----------------------------------------------------------------------------------------------------
        try:
            SENSOR = True
            # --------------------------------------------------------------------------------------------------
            # если камеры сделали снимок, то запускаем скрипт
            # if flag[11] and flag[12] and flag[13] and flag[14]:
            if flag[11] and flag[12] and flag[13] and flag[14] and flag[30] and flag[31] and flag[32]:
                IMAGE_READY = True
            # --------------------------------------------------------------------------------------------------
            if SENSOR or init:
                # print('start')
                if IMAGE_READY or init:
                    start_time = time.time()
                    IMAGE_READY = False
                    cnt += 1
                    # --------------------------------------------------------------------------------------------------
                    # подготовка изображения
                    grab_img_thread()
                    # сохраняем исходные изображения для вывода на экран вехрней пласти
                    # top_raw = cv2.hconcat([Frame['cam1'], Frame['cam2'], Frame['cam3'], Frame['cam4']])
                    # cv2.imwrite(_PATH_TO_WRITE_TOP_RAW, histogramm(top_raw))
                    # saveIMG(top_raw, '/home/ast/Desktop/')
                    # --------------------------------------------------------------------------------------------------
                    delete_background()
                    # --------------------------------------------------------------------------------------------------
                    if Frame['flag_err'] is None:
                        # TOP
                        try:
                            top_image_mask_gray, top_image_mask_hsv, top_original_raw = concat_image_top()
                            # --------------------------------------------------------------------------------------
                            # TESTING
                            # cv2.imwrite('/home/ast/Desktop/_IMAGE/top_mask_gray.jpg', top_image_mask_gray)
                            # cv2.imwrite('/home/ast/Desktop/_IMAGE/top_mask_hsv.jpg', top_image_mask_hsv)
                            msk1 = top_image_mask_gray == 0
                            t1 = top_original_raw.copy()
                            t1[msk1] = 0
                            # cv2.imwrite('/home/ast/Desktop/_IMAGE/top_orig_g.jpg', t1)
                            saveIMG(t1, '/home/ast/Desktop/_IMAGE/')
                            # msk = top_image_mask_hsv == 0
                            # t2 = top_original_raw.copy()
                            # t2[msk] = 0
                            # cv2.imwrite('/home/ast/Desktop/_IMAGE/top_orig.jpg', t2)
                            # saveIMG(t2, '/home/ast/Desktop/_IMAGE/')
                            # --------------------------------------------------------------------------------------

                            if top_image_mask_gray is not None:
                                # сохраняем изображение для нейросети
                                # cv2.imwrite(TOP_IMAGE_PATH, histogramm(top_original_raw))
                                # cv2.imwrite(TOP_IMAGE_PATH, histogramm(t1))
                                cv2.imwrite(TOP_IMAGE_PATH, t1)
                                # сохраняем изображение в архив
                                saveIMG(top_original_raw, '/home/ast/Pictures/')
                                # --------------------------------------------------------------------------------------
                                # сохраняем исходные изображения для вывода на экран вехрней пласти (raw)
                                cv2.imwrite(_PATH_TO_WRITE_TOP_RAW, histogramm(top_original_raw))
                                # --------------------------------------------------------------------------------------
                                # определяем дефекты
                                cod, proc_def, top_image_defects, x_max, len_board, width_board = \
                                    defekt_mech(top_image_mask_gray, top_image_mask_hsv, top_original_raw, cut=True)
                                # --------------------------------------------------------------------------------------
                                # добавим текст
                                # создадим рамку изображения добавив вверх
                                header = np.zeros([50, top_image_defects.shape[1], 3], dtype=np.uint8)
                                img = cv2.vconcat([header, top_image_defects])
                                cv2.putText(img,
                                            'COD: %s, size: %sx%s, Defekt mech: %s'
                                            % (cod, len_board, width_board, proc_def.__round__(1)),
                                            (50, 45), cv2.FONT_HERSHEY_SIMPLEX, 1.5, (255, 255, 0), 2)
                                # сохраняем изображение в зону board
                                cv2.imwrite(_PATH_TO_WRITE_TOP_BOARD, histogramm(img))
                                # --------------------------------------------------------------------------------------
                                # saveIMG(top_image_defects, '/home/ast/Desktop/')
                                # saveIMG(img, '/home/ast/Pictures/')
                                # saveIMG(top_original_raw, '/home/ast/Pictures/')
                                # --------------------------------------------------------------------------------------
                                # заполним данные по физическим размерам доски
                                board_param[3] = 0
                                board_param[4] = int(x_max)
                                board_param[5] = int(Y_min['cam1'][1])
                                board_param[6] = int(Y_max['cam1'][1])
                                board_param[7] = int(width_board)
                                board_param[8] = int(len_board)
                                # --------------------------------------------------------------------------------------
                        except Exception as e:
                            print(color('ERR defekt_top: %s' % e, attrs=['reverse']))
                            cod = 77

                        # BOTTOM
                        # ----------------------------------------------------------------------------------------------
                        # ожидаем изображения с нижних камер
                        # if not init:
                        #     while True:
                        #         if flag[30] and flag[31] and flag[32]:
                        #             # flag[30] = flag[31] = flag[32] = 0
                        #             break
                        # ----------------------------------------------------------------------------------------------
                        try:
                            # print(flag[30], flag[31], flag[32])
                            flag[30] = flag[31] = flag[32] = 0
                            btm_image_mask_g = cv2.imread('/home/ast/Desktop/_IMAGE/CAM/bottom_mask_g.jpg', 0)
                            btm_image_mask_h = cv2.imread('/home/ast/Desktop/_IMAGE/CAM/bottom_mask_h.jpg', 0)
                            btm_image_raw = cv2.imread('/home/ast/Desktop/_IMAGE/CAM/bottom-raw.jpg')
                            # ------------------------------------------------------------------------------------------

                            msk2 = btm_image_mask_h == 0
                            t2 = btm_image_raw.copy()
                            t2[msk2] = 0
                            # cv2.imwrite('/home/ast/Desktop/_IMAGE/btm_orig_g.jpg', t2)
                            saveIMG(t2, '/home/ast/Desktop/_IMAGE/')


                            # ------------------------------------------------------------------------------------------
                            if btm_image_raw is not None:
                                # сохраняем исходные изображения для вывода на экран вехрней пласти (raw)
                                cv2.imwrite(_PATH_TO_WRITE_BOTTOM_RAW, histogramm(btm_image_raw))
                                # cv2.imwrite(_PATH_TO_WRITE_BOTTOM_RAW, histogramm(t2))
                                saveIMG(btm_image_raw, '/home/ast/Pictures/')
                                # --------------------------------------------------------------------------------------
                                k = 0.92
                                cut_border = int(x_max * k)
                                # --------------------------------------------------------------------------------------
                                # сохраняем изображение для нейросети
                                # cv2.imwrite(BOTTOM_IMAGE_PATH, histogramm(btm_image_raw.copy()[:, :]))
                                # cv2.imwrite(BOTTOM_IMAGE_PATH, histogramm(t2))
                                cv2.imwrite(BOTTOM_IMAGE_PATH, t2)
                                # --------------------------------------------------------------------------------------
                                cod_b, proc_def, btm_image_defects, x_max_btm, len_board, width_board = \
                                    defekt_mech(btm_image_mask_g[:, :cut_border],
                                                btm_image_mask_h[:, :cut_border],
                                                btm_image_raw[:, :cut_border],
                                                add=True)
                                # --------------------------------------------------------------------------------------
                                header = np.zeros([50, btm_image_defects.shape[1], 3], dtype=np.uint8)
                                img = cv2.vconcat([header, btm_image_defects])
                                cv2.putText(img, 'COD: %s, size: %sx%s, Defekt mech: %s'
                                            % (cod_b, len_board, width_board, proc_def.__round__(1)),
                                            (50, 45), cv2.FONT_HERSHEY_SIMPLEX, 1.5, (255, 255, 0), 2)
                                cv2.imwrite(_PATH_TO_WRITE_BOTTOM_BOARD, histogramm(img))
                                # --------------------------------------------------------------------------------------
                                # saveIMG(img, '/home/ast/Pictures/')
                                # saveIMG(btm_image_raw, '/home/ast/Pictures/')
                        except Exception as e:
                            traceback.print_exc()
                            print(color('ERR mech_btm: %s' % e, attrs=['reverse']))
                            cod_b = 77
                        # ----------------------------------------------------------------------------------------------
                    else:
                        Frame['flag_err'] = None
                        cod = 77
                        cod_b = 77

                    # cod_b = 100
                    if cod_b == 77 or cod == 77:
                        arr[8] = 77
                    elif cod_b == 100:
                        arr[8] = cod
                    elif cod == 100:
                        arr[8] = cod_b
                    else:
                        arr[8] = max(cod, cod_b)
                    # --------------------------------------------------------------------------------------------------
                    print(color('COD: %s, %s, %s' % (cod, cod_b, arr[8]), 'blue', attrs=['reverse']))
                    # --------------------------------------------------------------------------------------------------
                    cprint('%s CREATE IMAGE | time: %s, %s'
                           % (cnt, (time.time() - start_time).__round__(4), datetime.datetime.now()), attrs=['reverse'])
                    # --------------------------------------------------------------------------------------------------
                    # запускаем нейронку
                    flag[5] = 1
                    # --------------------------------------------------------------------------------------------------
                    flag[11] = flag[12] = flag[13] = flag[14] = 0
                    flag[15] = flag[16] = flag[17] = flag[18] = 0
                    # flag[21] = flag[22] = flag[23] = flag[24] = 0
                    flag[25] = flag[26] = flag[27] = flag[28] = 0
                    # --------------------------------------------------------------------------------------------------
                    init = False
                    # --------------------------------------------------------------------------------------------------
        except Exception as e:
            flag_process[10] = 0
            print('create object', e)
            print('Process: CREATE IMAGE STOP ', '| time: ', datetime.datetime.now())
            os.system('killall -s9 python3')
        finally:
            pass


def filter(mask):
    """
    фильтр помех при удалении фона
    :param mask:
    :return:
    """
    black_line = np.linspace(0, 0, mask.shape[1])
    # для каждой продолной линии:
    for i in range(mask.shape[0]):
        # если колличество вхождений 0 (черного цвета) больше заданного, то стираем
        if np.bincount(mask[i])[0] > 0.8 * mask.shape[1]:
            mask[i] = black_line
    return mask


def deleteBackground_v03(key_original_image, key_background_image):
    """
    Функиция позволяет сделать сравнение 2 изображений для удаления фона путем создания маски
    """
    try:
        delta_gray = 40
        delta_hsv = 40

        key = key_original_image
        original_image = Frame[key_original_image].copy()
        background_image = Frame[key_background_image].copy()
        # ---------------------------------------------------------------------------------
        gray_original = cv2.cvtColor(original_image.copy(), cv2.COLOR_BGR2GRAY)
        gray_background = cv2.cvtColor(background_image.copy(), cv2.COLOR_BGR2GRAY)

        hsv_original = cv2.cvtColor(original_image.copy(), cv2.COLOR_BGR2HSV)
        hsv_background = cv2.cvtColor(background_image.copy(), cv2.COLOR_BGR2HSV)
        gray_hsv_original = cv2.cvtColor(hsv_original, cv2.COLOR_BGR2GRAY)
        gray_hsv_background = cv2.cvtColor(hsv_background, cv2.COLOR_BGR2GRAY)
        # ---------------------------------------------------------------------------------
        original = cv2.medianBlur(gray_original, 11)
        background = cv2.medianBlur(gray_background, 11)
        original_hsv = cv2.medianBlur(gray_hsv_original, 11)
        background_hsv = cv2.medianBlur(gray_hsv_background, 11)
        # ---------------------------------------------------------------------------------
        msk1 = original >= background + delta_gray
        msk2 = original_hsv >= background_hsv + delta_gray
        msk1b = original < background + delta_hsv
        msk2b = original_hsv < background_hsv + delta_hsv
        # ---------------------------------------------------------------------------------
        original[msk1] = 255
        original_hsv[msk2] = 255
        original[msk1b] = 0
        original = filter(original)
        original_hsv[msk2b] = 0
        original_hsv = filter(original_hsv)
        msk3 = original_hsv != original
        original_hsv[msk3] = 255
        # ---------------------------------------------------------------------------------
        # получим параметры изображения (доски) для соединения изображений и/или геометрических параметров доски:
        # ширина, площадь, длинна окружной линии и т.д.
        Y_min[key], Y_max[key], W_board[key] = find_concat_param(original_hsv)
        # ---------------------------------------------------------------------------------
        Frame_del_background[key] = [original, original_hsv]
        # ---------------------------------------------------------------------------------

    except Exception as e:
        print('ERR delete background', e)
        Y_min[key], Y_max[key], W_board[key] = 0, 0, 0


def find_concat_param(image):
    """
    :param image: подаем на вход бинарную маску
    """
    fr_copy = image.copy()
    k = 3
    y, x = image.shape
    frame = cv2.resize(fr_copy, (int(x / k), int(y / k)), interpolation=cv2.INTER_LINEAR)
    Ymin = []
    Ymax = []
    W = []
    # зададим точки в которых хотим делать проверку
    X = [0.0, 0.5, 0.97]
    # X = np.linspace(0, 0.95, 11)
    roi_hsv = frame
    for i in range(len(X)):
        y_min = 0
        y_max = frame.shape[0]

        x_00 = int(roi_hsv.shape[1] * X[i])
        x_01 = int(roi_hsv.shape[1] * (X[i] + 0.02))

        roi = roi_hsv[:, x_00:x_01]
        # mask = cv2.inRange(roi, np.array([0, 35, 90]), np.array([30, 200, 255]))
        mask = roi
        mask = cv2.medianBlur(mask, 5)

        # cv2.imshow('mask', mask)
        # cv2.waitKey()
        # cv2.destroyAllWindows()

        white_line = list(np.linspace(255, 255, roi.shape[1], dtype=np.int16))
        black_line = list(np.linspace(0, 0, roi.shape[1], dtype=np.int16))
        for y in range(roi.shape[0]):
            # if all(mask[y] == white_line) and y_min == 0:
            if all(mask[y] != black_line) and y_min == 0:
                y_min = y
            elif all(mask[y] != white_line) and y_min != 0 and y_max == frame.shape[0]:
                y_max = y
                # если все нашли, то выходим из цикла
                break
        Ymin.append(y_min * k)
        Ymax.append(y_max * k)
        W.append(y_max * k - y_min * k)

    return Ymin, Ymax, W


def lineUp(Frame, dY):
    """
    :param Frame: изображение которое нужно поднять или опустить
    :param dY:  на сколько нужно передвинуть изображение + - вверх, - - вниз
    :return: изображение
    """
    try:
        rows, cols = Frame.shape[0], Frame.shape[1]
        M = np.float32([[1, 0, 0], [0, 1, dY]])
        return cv2.warpAffine(Frame, M, (cols, rows))
    except Exception as e:
        cprint('ERROR line up: %s' % (e), 'red')
        return Frame

# =====================================================================================================================

def roi_map_v5(img_g, img_h, original_image, delta_y=0, add=False):
    delta_width = delta_y
    # --------------------------------------------------------------------------------
    # КОНТУР
    # найдем контур доски
    edge_g, _ = cv2.findContours(img_g, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    edge_h, _ = cv2.findContours(img_h, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    # --------------------------------------------------------------------------------
    # отсортируем контуры для выявления наибольшего
    edge_g.sort(key=lambda i: len(i), reverse=True)
    edge_h.sort(key=lambda i: len(i), reverse=True)
    # --------------------------------------------------------------------------------
    # нанесем контур
    # gray
    cv2.drawContours(original_image, edge_g, 0, (0, 255, 255), 2)
    # hsv
    cv2.drawContours(original_image, edge_h, 0, (255, 255, 255), 2)
    # --------------------------------------------------------------------------------
    # cv2.imwrite('/home/ast/Desktop/_IMAGE/t1.jpg', original_image)
    # --------------------------------------------------------------------------------
    # найдем обзол
    # obzol = obzol_find(img_g, img_h, w_min=5, w_max=300, l_min=100, l_max=3000)
    # if len(obzol) != 0:
    #     for obz in obzol:
    #         x1, y1, x2, y2 = obz[0], obz[1], obz[2], obz[3]
    #         cv2.rectangle(original_image, (x1, y1), (x2, y2), (255, 100, 100))
    # cv2.imwrite('/home/ast/Desktop/_IMAGE/t1.jpg', original_image)
    # --------------------------------------------------------------------------------
    # определим если ли несоответствие по геометрии доски.
    # для этого проверим ширину доски через заданное расстояние
    D_POINT = 20
    top_point = []
    btm_point = []
    width = []
    width_max = []
    i = 0
    end_point_x = 0
    while i < original_image.shape[1]:
        # на каждом шаге находим точки которые имеют кординату х, соответствующую шагу
        # если мы контролируем нижнюю доску, то для нее определены мертвые зоны в которых не проводится контроль
        if add:
            # if 0 <= i <= 50 or 250 <= i <= 360 or 1330 <= i <= 1420 or 2230 <= i <= 2330 or 2880 <= i <= 2950:
            # if 0 <= i <= 10 or 210 <= i <= 310 or 1280 <= i <= 1380 or 2190 <= i <= 2290 or 2840 <= i <= 2910:
            if 0 <= i <= 310 or 1280 <= i <= 1380 or 2180 <= i <= 2290 or 2840 <= i <= 2910:
                i += D_POINT
                continue
        # индекс точки
        index = np.where(edge_h[0] == i)
        # для верхней пласти
        # if not add:
        #     index = np.where(edge_h[0] == i)
        # else:
        #     # для нижней пласти
        #     index = np.where(edge_g[0] == i)
        if len(index[0]) == 2:
            # координата х:
            end_point_x = i
            y1, y2 = index[0][0], index[0][1]
            # print(y1, y2)
            p0 = edge_h[0][y1][0][1]
            p1 = edge_h[0][y2][0][1]
            # линии в середине доски, показывающие хорошую доску
            # cv2.line(original_image, (i, p0), (i, p1), (0, 255, 0), 1)
            top = p0 if p0 < p1 else p1
            btm = p0 if p0 > p1 else p1
            top_point.append([i, top])
            btm_point.append([i, btm])
            w = abs(p0 - p1)
            # width_max = w if w > width_max else width_max
            width_max.append(w)
            width.append([i, w])
            # линии от края до контура
            # cv2.line(original_image, (i, 0), (i, top), (0, 0, 255), 1)
            # cv2.line(original_image, (i, btm), (i, original_image.shape[0]), (255, 0, 255), 1)
        else:
            # переберем коодринаты в которых другое кол-во точек
            # print(index)
            # cprint([index[0][i] for i in range(len(index[0])) if index[2][i] == 0], 'blue')
            _index = [index[0][i] for i in range(len(index[0])) if index[2][i] == 0]
            if len(_index) != 0:
                # координата х:
                end_point_x = i
                y1, y2 = max(_index), min(_index)
                p0 = edge_h[0][y1][0][1]
                p1 = edge_h[0][y2][0][1]
                # линии в середине доски, показывающие хорошую доску
                # cv2.line(original_image, (i, p0), (i, p1), (0, 255, 0), 1)
                top = p0 if p0 < p1 else p1
                btm = p0 if p0 > p1 else p1
                top_point.append([i, top])
                btm_point.append([i, btm])
                w = abs(p0 - p1)
                width_max.append(w)
                # width_max = w if w > width_max else width_max
                width.append([i, w])
                # линии от края до контура
                # cv2.line(original_image, (i, 0), (i, top), (0, 0, 255), 1)
                # cv2.line(original_image, (i, btm), (i, original_image.shape[0]), (255, 0, 255), 1)
        # ШАГ ИЗМЕРЕНИЙ
        i += D_POINT
    # --------------------------------------------------------------------------------
    # нарисуем линии
    # for i in range(len(top_point) - 1):
    #     x1, y1 = top_point[i][0], top_point[i][1]
    #     x2, y2 = top_point[i + 1][0], top_point[i + 1][1]
    #     cv2.line(original_image, (x1, y1), (x2, y2), (255, 0, 0), 1)
    # for i in range(len(btm_point) - 1):
    #     x1, y1 = btm_point[i][0], btm_point[i][1]
    #     x2, y2 = btm_point[i + 1][0], btm_point[i + 1][1]
    #     cv2.line(original_image, (x1, y1), (x2, y2), (255, 0, 0), 1)
    # --------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------
    # выделим дефекты, которые будут учитываться
    # вариант 1 - основываясь на ширине пласти доски:
    DEFEKTS = []
    key = 1
    x1 = 0
    x2 = 0
    if len(width_max) > 2:
        w_max = median(width_max)
    else:
        w_max = 100
    # для каждой точки:
    for point in width:
        # если ширина меньша на заданную величину, то считаем точку началом дефекта:
        if (w_max - point[1]) > delta_width and key:
            key = False
            x1 = point[0]
        elif (w_max - point[1]) < delta_width and x1 != 0:
            key = True
            x2 = point[0]
            DEFEKTS.append([x1, x2])
            x1 = 0
            x2 = 0
    # если доска закончилась дефектом, то нужно закрыть массив добавив последнюю часть
    if not key:
        x2 = width[-1][0]
        DEFEKTS.append([x1, x2])
    # --------------------------------------------------------------------------------
    # для верхней пласти "закроем" до конца контур
    # длинна изображения в пикселях
    len_board_px = img_g.shape[1]
    # print('len_board_px', len_board_px, end_point_x)
    if (len_board_px - 100) > end_point_x:
        DEFEKTS.append([end_point_x, len_board_px])
    # print(DEFEKTS)
    # --------------------------------------------------------------------------------
    # линии дефектов
    # for line in DEFEKTS:
    #     x1, x2 = line
    #     # cv2.line(img_t, (x1, y1 - 20), (x2, y2 - 20), (0, 0, 255), 5)
    #     # cv2.line(original_image, (x1, 50), (x2, 50), (0, 0, 255), 5)
    #     draw_area_defect(original_image, x1, x2)
    # --------------------------------------------------------------------------------
    cv2.imwrite('/home/ast/Desktop/_IMAGE/t1.jpg', original_image)
    # print('width ', width)
    # print('width ', width_max)
    # print('DEFEKTS: ', DEFEKTS)
    # --------------------------------------------------------------------------------
    return DEFEKTS, original_image, w_max


def draw_area_defect(img, X1, X2, step=20):
    # сетка с квадратом step x spep
    # for i in range(int((x2 - x1)/step)):
    # red
    # color = (0, 0, 255)
    # green
    color = (0, 255, 0)
    y = 1
    x = X1
    while y < img.shape[0]:
        # ----горизонтальные линии------------------------------
        x1, y1, x2, y2 = X1, y, X2, y
        cv2.line(img, (x1, y1), (x2, y2), color, 1)
        y += step
    while x <= X2:
        # ----вертикальные линии------------------------------
        x1, y1, x2, y2 = x, 0, x, img.shape[0]
        cv2.line(img, (x1, y1), (x2, y2), color, 1)
        x += step



def obzol_find(mask_h, mask_g, w_min=5, w_max=100, l_min=50, l_max=100):
    """
    :param mask_h: маска hsv
    :param mask_g: маска gray
    :param w_min: ширина минимум
    :param w_max: ширина максимум
    :param l_min: длинна минимум
    :param l_max: длинна максимум
    :return: массив с контурами описанных не повернутых прямоугольников
    """
    obzol_array = []
    #---------------------------------------------------------------
    # find mask obzol
    obzol = mask_h - mask_g
    # cv2.imwrite('/home/ast/Desktop/_IMAGE/t2.jpg', obzol)
    #---------------------------------------------------------------
    # find contour obzol
    try:
        cont, hier = cv2.findContours(obzol, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        #-----------------------------------------------------------
        # filter obzol
        for _cont in cont:
            # опишем прямоугольник вокруг контура
            c = cv2.boundingRect(_cont)
            # преобразуем в кординаты
            x1, y1, x2, y2 = c[0], c[1], c[0]+c[2], c[0]+c[3]
            # длинна описанного прямоугольника
            _len = abs(x1-x2)
            # ширина описанного прямоугольника
            width = abs(y1-y2)
            # фильтруем полученные данные
            if w_max >= width >= w_min and l_max >= _len >= l_min:
                obzol_array.append([x1, y1, x2, y2])
        #-----------------------------------------------------------
    except:
        return obzol_array
    #---------------------------------------------------------------
    return obzol_array
    #---------------------------------------------------------------


def defekt_mech(res, mask, original_image, cut=False, add=False):
    try:
        # -----------------------------------------------------------------------------
        if cut:
            image, x_min, x_max = cut_lenght(mask, res)
            original_image = original_image[:, :x_max+30]
        else:
            image = res
        # -----------------------------------------------------------------------------
        border, image, w_board = roi_map_v5(image, mask, original_image, delta_y=8, add=add)
        # border, image, w_board = roi_map_v5(res, mask, original_image, delta_y=7, add=add)
        # -----------------------------------------------------------------------------
        if border is None:
            return 77, 0, res, res.shape[1], res.shape[1], 100
            # return 77, res, res.shape[1]
        # -----------------------------------------------------------------------------
        if cut:
            # дгоним ширину для верхней пласти
            W_board = (w_board * 1.28).__round__(1)
        else:
            # если мы исследуем нижнюю доску, то вводим поправочный коэффициент, для того, чтобы длинна была одинаковой
            image, x_min, x_max = image, 0, int(image.shape[1] * 1.0875)
            # дгоним ширину для нижней пласти
            W_board = (w_board * 1.2).__round__(1)
        # -----------------------------------------------------------------------------
        # переведем длинну в мм
        len_board = ((x_max - x_min) * 1.21).__round__(1)
        # print('len_board: ', len_board)
        # -----------------------------------------------------------------------------
        # подсчитаем брак
        cod, proc, defekt_area = check_brak(border, len_board, x_max)
        # -----------------------------------------------------------------------------
        # нарисуем дефекты, которые учитываются
        for line in defekt_area:
            x1, x2 = line
            draw_area_defect(image, x1, x2)
        # -----------------------------------------------------------------------------
        return cod, proc, image, x_max, len_board, W_board
        # -----------------------------------------------------------------------------
    except Exception as e:
        traceback.print_exc()
        print('check defekts: ', e)
        return 77, 0, res, res.shape[1], res.shape[1], 100


def cut_lenght(mask, image):
    # mask = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)
    # обрезка по длинне:
    # print(mask.shape)
    x_min = 0
    x_max = mask.shape[1]
    # black_line = [0 for 0 in range(mask.shape[0])]
    black_line = np.linspace(0, 0, mask.shape[0], dtype=np.uint8)
    for i in range(mask.shape[1]):
        # x = mask.shape[1] - i
        if all(mask[:, -i] == black_line):
            pass
        else:
            x_max = mask.shape[1] - i
            break
    # print(x_min, x_max)
    image = image[:, x_min:x_max]

    return image, x_min, x_max


def check_brak(defekt, len_board, x_max):
    """
    :return:
        166 - optim 20%
        266 - optim 40%
        7 - Brak
        100 - sorting by neural netvork

    """
    defeck_top = []
    defeck_bottom = []

    # допуск по длинне линий в %
    DOPUSK = 3
    # ------------------------------------------------------------------------------------------------------------------
    # print('defekt:', defekt)
    def filter_defekts(border, defekt_arr):
        if len(border) >= 1:
            for i in range(len(border)):
                x1, x2 = border[i]
                if int((abs(x2 - x1) * 100) / len_board) >= DOPUSK:
                    defekt_arr.append([x1, x2])

    filter_defekts(defekt, defeck_top)
    # print('defeck_top:', defeck_top)
    # ------------------------------------------------------------------------------------------------------------------
    # draw defect
    if len(defeck_top) != 0 or len(defeck_bottom) != 0:
        # BRAK
        sum_length_defekt_top = 0
        # sum_length_defekt_bottom = 0
        # просуммируем общую длинну дефектов по кромке
        for d in defeck_top:
            sum_length_defekt_top += abs(d[1] - d[0])
        # for d in defeck_bottom:
        #     sum_length_defekt_bottom += abs(d[1] - d[0])
        # # print(sum_length_defekt_top*1.21, sum_length_defekt_bottom*1.21)
        # выбираем максимальную сторону дефекта и приводим к длинне доски
        # sum_defekt = max(sum_length_defekt_bottom, sum_length_defekt_top) * 1.21

        sum_defekt = sum_length_defekt_top
        proc_def = (sum_defekt / x_max) * 100

        # print('sum_defekt: ', sum_defekt,' len_board:',len_board, ' proc_def: ', proc_def)

        if (sum_defekt / x_max) * 100 > 40:
            return 77, proc_def, defeck_top
        else:
            for d in defeck_top:
                if d[0] < x_max * 0.6:
                    return 77, proc_def, defeck_top
            for d in defeck_bottom:
                if d[0] < x_max * 0.6:
                    return 77, proc_def, defeck_top

        # =================================================================================
        # optimizacija
        opt = []
        for d in defeck_top:
            if d[0] > x_max * 0.8:
                opt.append(166)
            else:
                opt.append(266)
        for d in defeck_bottom:
            if d[0] > x_max * 0.8:
                opt.append(166)
            else:
                opt.append(266)
        if len(opt) != 0:
            opt.sort()
            return opt[0], proc_def, defeck_top

    else:
        return 100, 0, [[0, 0]]

# =====================================================================================================================

# сервер клиент
def send_file_scp(file):
    host = '192.168.0.121'
    user = 'ast'

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, 22))

    session = Session()
    session.handshake(sock)
    session.agent_auth(user)

    name = file.split('/').pop()

    fileinfo = os.stat(file)

    start_time = datetime.datetime.now()

    chan = session.scp_send('/home/ast/Desktop/%s' % name, fileinfo.st_mode & 0o777, fileinfo.st_size)

    with open(file, 'rb') as local_fh:
        for data in local_fh:
            chan.write(data)
    local_fh.close()

    # # read
    # channel, info = session.scp_recv('/home/ast/Documents/ServClient/cam3.jpg')
    # now = datetime.now()
    # buf = 1024
    # size, data = channel.read(buf)
    # f = open('/home/ast/Desktop/cam3.jpg', 'wb')
    # cnt = 0
    # while size == buf:
    #     cnt += 1
    #     f.write(data)
    #     # print('data %s, size: %s' % (cnt, size))
    #     size, data = channel.read(buf)
    # print('end')
    # f.close()
    # channel.close()

    res_time = datetime.datetime.now() - start_time
    cprint("copy file program on client server: %s" % res_time, 'yellow', attrs=['reverse'])


def start_Server_Client():
    cprint('start process: Server_Client, time: %s' % (datetime.datetime.now()), 'yellow')

    t1 = time.time()

    # start script
    try:
        os.system('ssh ast@192.168.0.121 killall python3')
        time.sleep(0.2)
        # os.system('ssh ast@192.168.0.121 python3 /home/ast/Documents/ServClient/ServClient.py')
        os.system('ssh ast@192.168.0.121 python3 /home/ast/Desktop/ServClient.py')
    except Exception as e:
        os.system('ssh ast@192.168.0.121 killall python3')
        print('ERROR Server_Client ', e)
    finally:
        # stop script
        # os.system('ssh ast@192.168.0.121 cp -a /home/ast/Documents/CAM_RAW/. /home/ast/Documents/ServClient/')
        os.system('ssh ast@192.168.0.121 killall -s 9 python3')
    # os.system(
    #     'cp -a /run/user/1000/gvfs/smb-share:server=ast-b360m-d3p.local,share=servclient/. /home/ast/Desktop/test/')
    # print(time.time() - t1)


# =====================================================================================================================
# =====================================================================================================================
# =====================================================================================================================
# =====================================================================================================================
# TCP

def Client(SERVER_HOST='192.168.0.100', SERVER_PORT='502'):
    client = ModbusClient()
    client.host(SERVER_HOST)
    client.port(int(SERVER_PORT))
    client.open()

    return [client, client.is_open()]


def read_03(client, adrFrom=0, adrTo=10):
    # open or reconnect TCP to server
    if not client.is_open():
        if not client.open():
            raise Exception('ERROR CONNECT TO HOST')

    if client.is_open():
        regs = client.read_holding_registers(adrFrom, adrTo)
        if regs:
            return regs


def write_06(client, adrReg, data):
    t = time.time()
    while True:
        # # open or reconnect TCP to server
        if not client.is_open():
            if not client.open():
                # pass
                print('err')
                # return 'unable to connect'
        if client.is_open():
            regs = client.write_single_register(adrReg, data)
            if regs:
                return regs


def write_01(client, adrCoil, data):
    # # open or reconnect TCP to server
    if not client.is_open():
        if not client.open():
            return 'unable to connect'

    if client.is_open():
        regs = client.write_single_coil(adrCoil, data)
        if regs:
            return regs


def MB_SIEMENS(arr, tcp, flag, flag_process):
    # flag[0] - optical sensor
    global flag_01
    flag_01 = True
    plc, status = Client('192.168.0.90', '502')
    cprint('Start process: TCP, time %s' % datetime.datetime.now(), 'yellow')
    t1 = time.time()
    cnt = 0
    flag_process[5] = 1
    while True:
        start_time = time.time()
        life_bit = 1
        bit_board = flag[1]
        bit_layer = flag[2]
        bit_palet = flag[3]
        bit_sort = flag[6]

        try:
            # чтение регистров
            buff = read_03(plc, 0, 29)
            for i in range(len(buff)):
                tcp[i] = buff[i]
            #   ======================================================================================================
            write_data = create_msk(bit0=bit_board, bit1=bit_layer, bit2=life_bit, bit3=bit_sort, bit4=bit_palet,
                                    bit5=0)
            #   ======================================================================================================
            # # записываем определенный сорт в ПЛК
            write_06(plc, adrReg=4, data=arr[10])
            # write
            write_06(plc, 30, write_data)

            # сброс флага о записи сорта в ПЛК
            if buff[8] == 1 and flag[6] == 1:
                # print('reset')
                flag[6] = 0

        except Exception as e:
            print(e)
            flag_process[5] = 1
            time.sleep(10)
            plc, status = Client('192.168.0.90', '502')
            print(plc, status)
        finally:
            pass
            # time.sleep(0.1)
            # reset Tx


def create_msk(bit0=0, bit1=0, bit2=0, bit3=0, bit4=0, bit5=0, bit6=0, bit7=0):
    mask1 = set_bit(0, 0, bit0)
    mask2 = set_bit(mask1, 1, bit1)
    mask3 = set_bit(mask2, 2, bit2)
    mask4 = set_bit(mask3, 3, bit3)
    mask5 = set_bit(mask4, 4, bit4)
    mask6 = set_bit(mask5, 5, bit5)
    # mask7 = set_bit(mask6, 6, bit6)
    # mask8 = set_bit(mask7, 7, bit7)
    return mask6


def set_bit(v, index, x):
    mask = 1 << index
    v &= ~mask
    if x:
        v |= mask
        return v
    else:
        return v


# =====================================================================================================================
# =====================================================================================================================
# =====================================================================================================================
# =====================================================================================================================
# FTP
# ftpInputVolume v3.0
# Функция забирает входной объем бревен с 2х линий
# dirpath - путь где храняться скаченные файлы
def ftpInputVolume(dirpath, flag, ftp_data, flag_process):
    flag_process[9] = 1
    while True:
        try:
            # удаляем старые файлы
            for filename in os.listdir(dirpath):
                filepath = os.path.join(dirpath, filename)
                try:
                    shutil.rmtree(filepath)
                except OSError:
                    os.remove(filepath)

            path1file = dirpath
            path2file = dirpath + 'm1.xlsb'
            # подключаемся к FTP
            ftp = FTP('177.168.3.11')
            ftp.login('root', 'z1x2c3v4b5')

            # функция сохранения файлов входного объема на компьютере
            def loadXlsx(direct_listPath, pathLoad, rash):
                direct_list = ftp.nlst(direct_listPath)
                for dirr in direct_list:
                    inName = 'merjenje' in dirr
                    inExt = rash in dirr
                    nameXlsx = dirr[dirr.rfind('merjenje'):len(dirr)]
                    pathDir = pathLoad + nameXlsx
                    if inName and inExt:
                        with open(pathDir, 'wb') as f:
                            ftp.retrbinary('RETR ' + dirr, f.write)

            loadXlsx('/doNotTouch/save1', path1file, '.xlsx')

            direct_list = ftp.nlst("/doNotTouch/save1/auto")

            # докачиваем остальные файлы
            for p in direct_list:
                if 'merjenje' in p:
                    file_list = ftp.nlst(p)
                    loadXlsx(p, path1file, '.xlsb')

            fileFolderXlsb = glob.glob(dirpath + '*.xlsb')
            fileFolderXlsb.sort(reverse=True)
            finder = fileFolderXlsb[0]

            file = msoffcrypto.OfficeFile(open(finder, "rb"))
            file.load_key(password="a444cda")
            file.decrypt(open(path2file, "wb"))

            df = []

            with open_xlsb(path2file) as wb:
                s = wb.get_sheet(11)
                cnt = 0
                for r in s.rows():
                    cnt += 1
                    potok = [r[1].v, r[13].v]
                    if cnt >= 1:
                        break

            flag[4] = 1
            ftp_data[0] = potok[0]
            ftp_data[1] = potok[1]
            ftp_data[2] = potok[0] + potok[1]
            # print(ftp_data[:], flag[4])
            time.sleep(300)
        except Exception as e:
            flag_process[9] = 0
            cprint('FTP error: %s, time: %s' % (e, datetime.datetime.now()), 'red', attrs=['reverse'])
            time.sleep(3600)


# =====================================================================================================================
# =====================================================================================================================
# =====================================================================================================================
# =====================================================================================================================

Data_layer_from_plc = dict(Length=0, Width=0, Height=0, Sort=0, Volume=0, karman=0)
Data_board_from_plc = dict(Length=0, Width=0, Height=0, Sort=0, karman=0, id_man=0)
Data_palet_from_plc = dict(Length=0, Width=0, Height=0, Sort=0, Volume=0, karman=0)


def start_connection():
    global connection, cursor
    try:
        connection = psycopg2.connect(user="admin",
                                      password="z1x2c3v4b5",
                                      host="127.0.0.1",
                                      port="5432",
                                      database="ast")
        cursor = connection.cursor(cursor_factory=NamedTupleCursor)
        # cursor = connection.cursor()
        # Print PostgreSQL Connection properties
        # print(connection.get_dsn_parameters(), "\n")
        # logging.debug(connection.get_dsn_parameters())
        # Print PostgreSQL version
        cursor.execute("SELECT version();")
        record = cursor.fetchone()
        # print("You are connected to - ", record, "\n")
    except (Exception, psycopg2.Error) as error:
        print("Error while connecting to PostgreSQL", error)

    return connection, cursor


def close_connection(connection, cursor):
    # #closing database connection.
    if connection:
        cursor.close()
        connection.close()
        # print("PostgreSQL connection is closed")


def status(connection, cursor):
    # write status in data base
    postgres_insert_query = """ UPDATE machine_infos SET status_connect=True WHERE status_connect=False"""
    cursor.execute(postgres_insert_query)
    try:
        connection.commit()
    except (Exception, psycopg2.Error) as error:
        print("Error status", error)


def insert_input_volum(connection, cursor, flow1=0, flow2=0, sum=0, time=datetime.datetime.now()):
    postgres_insert_query = """ INSERT INTO input_volume ("flow1", "flow2", "SumVolume","Time") 
        VALUES (%s,%s,%s,%s)"""
    record_to_insert = (flow1, flow2, sum, time)
    cursor.execute(postgres_insert_query, record_to_insert)
    try:
        connection.commit()
        count = cursor.rowcount
        # print(count, "DB - Record insert_input_volum: ", datetime.datetime.now())
        # logging.debug("DB - Record insert_input_volum")
        result = 1
    except (Exception, psycopg2.Error) as error:
        print("Error DB - Record insert_input_volum", error)
        result = 0
    return result


def insert_board(connection, cursor, length=0, width=0, height=0, time=datetime.datetime.now(), volume=0, sort=0,
                 type='Хвоя', suchki=0, treschiny=0, svievatist=0, prorost=0, zabolon=0, plesen=0, gnil=0,
                 sineva=0, obzol=0, mech=0, piatna=0, smena=0, priznak_size=0, id_man=0):
    postgres_insert_query = """ INSERT INTO data_boards ("Length", "Width", "Height","Time","Sort","Volume",\
        "Type","Suchki","Treschiny","Svievatist", "Prorost","Zabolon","Plesen","Gnil",\
        "Sineva","Obzol","Mech","Piatna", "Smena", "Priznak", "IdManual") 
        VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
    record_to_insert = (length, width, height, time, sort, volume, type, suchki,
                        treschiny, svievatist, prorost, zabolon, plesen, gnil, sineva, obzol, mech, piatna,
                        smena, priznak_size, id_man)
    cursor.execute(postgres_insert_query, record_to_insert)
    try:
        connection.commit()
        count = cursor.rowcount
        # print(count, "DB - Record data_boards: ", datetime.datetime.now())
        # logging.debug("DB - Record data_boards")
        result = 1
    except (Exception, psycopg2.Error) as error:
        print("Error Record in data_boards", error)
        result = 0
    return result


def insert_layer(connection, cursor, length=0, width=0, height=0, time=datetime.datetime.now(),
                 sort=0, ColBoard=0, karman=0, priznak_size=0):
    postgres_insert_query = """ INSERT INTO statistic_politers
    ("Length", "Width", "Height","Time","sort","ColBoard", "karman", "Priznak") VALUES (%s,%s,%s,%s,%s,%s,%s, %s)"""
    record_to_insert = (length, width, height, time, sort, ColBoard, karman, priznak_size)
    cursor.execute(postgres_insert_query, record_to_insert)
    try:
        connection.commit()
        count = cursor.rowcount
        # print(count, "DB - Record politers: ", datetime.datetime.now())
        # logging.debug("DB - Record politers")
        result = 1
    except (Exception, psycopg2.Error) as error:
        print("Error Record in statistic_politers", error)
        result = 0
    return result


def insert_polet(connection, cursor, length=0, width=0, height=0, time=datetime.datetime.now(), ColBoard=0):
    postgres_insert_query = """ INSERT INTO paletka_board
    ("Length", "Width", "Height","Time", "ColBoard") VALUES (%s,%s,%s,%s,%s)"""
    record_to_insert = (length, width, height, time, ColBoard)
    cursor.execute(postgres_insert_query, record_to_insert)
    try:
        connection.commit()
        count = cursor.rowcount
        # print(count, "DB - Record palet")
        # logging.debug("DB - Record palet")
        return 1
    except (Exception, psycopg2.Error) as error:
        print("Error Record in palet_board", error)
        return 0


def smena():
    """
    :return:
        1 если 1 смена. работа с 8:00 до 16:30
        2 если 2 смена. работа с 16:30 до 01:00
    """
    t_hour = datetime.datetime.now().time().hour
    t_minute = datetime.datetime.now().time().minute

    if t_hour >= 16 and t_minute >= 30:
        return 2
    else:
        return 1


def priznak(h, w, l, sort):
    """
    Функция возвращает признак, относящий доску к стандартному ряду типоразмеров
    :param h: толщина
    :param w: ширина
    :param l: длинна
    :return: признак
    """
    global _h, _w

    _h = 0
    _w = 0
    H = [16, 19, 22, 25, 32, 40, 44, 50, 60]
    W = [50, 75, 100, 125, 150, 175, 200, 225]
    L = [3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0]
    S = [0, 1, 2, 3, 4]
    try:
        for i in range(len(H)):
            if h >= H[i] and h < H[i + 1]:
                _h = H[i]
        for j in range(len(W)):
            if w >= W[j] and h < W[j + 1]:
                _w = W[j]
    except Exception as e:
        print(e)
    K = [_h, _w, l, sort]

    # create array
    KEY = []
    for q in range(len(H)):
        for w in range(len(W)):
            for e in range(len(L)):
                for s in range(len(S)):
                    KEY.append([H[q], W[w], L[e], S[s]])

    # print(K, KEY[0])
    #
    for i in range(len(KEY)):
        if KEY[i] == K:
            return i
    return 999


def parse_date_layer(TCP_BUFER):
    Data_layer_from_plc['Length'] = TCP_BUFER[21] / 100
    Data_layer_from_plc['Width'] = TCP_BUFER[20] / 100
    Data_layer_from_plc['Height'] = TCP_BUFER[22] / 100
    Data_layer_from_plc['Sort'] = TCP_BUFER[19]
    Data_layer_from_plc['Volume'] = TCP_BUFER[23]
    Data_layer_from_plc['karman'] = TCP_BUFER[18]


def parse_date_board(TCP_BUFER):
    Data_board_from_plc['Length'] = TCP_BUFER[13] / 10
    Data_board_from_plc['Height'] = TCP_BUFER[14] / 100
    Data_board_from_plc['Width'] = TCP_BUFER[15] / 100
    Data_board_from_plc['Sort'] = TCP_BUFER[16]
    Data_board_from_plc['karman'] = TCP_BUFER[17]
    Data_board_from_plc['id_man'] = TCP_BUFER[12]



def parse_date_palet(TCP_BUFER):
    Data_palet_from_plc['Length'] = TCP_BUFER[26] / 100
    Data_palet_from_plc['Height'] = TCP_BUFER[28] / 100
    Data_palet_from_plc['Width'] = TCP_BUFER[27] / 100
    Data_palet_from_plc['Sort'] = TCP_BUFER[25]
    Data_palet_from_plc['Volume'] = TCP_BUFER[29]
    Data_palet_from_plc['karman'] = TCP_BUFER[24]


def rw_data_base(tcp, flag, ftp, flag_process):
    # tcp[1], flag[1] - rw_board
    # tcp[2], flag[2] - rw_layout
    # tcp[3], flag[3] - rw_palet
    #         flag[4] - rw_input
    global connection, cursor, rw_board, rw_layer, rw_palet, rw_input
    rw_board, rw_layer, rw_palet, rw_input = 0, 0, 0, 0

    cprint('start process: DataBase, time: %s' % (datetime.datetime.now()), 'yellow')
    flag_process[6] = 1
    while True:
        # print('write1', rw_board, tcp[1], tcp[2], tcp[3])
        try:
            start_time = time.time()
            connection, cursor = start_connection()
            #  записываем значение доски
            if tcp[1] == 1 and rw_board == 0:
                # parse data
                parse_date_board(tcp)
                retValB = insert_board(
                    connection, cursor,
                    length=Data_board_from_plc['Length'],
                    width=Data_board_from_plc['Width'],
                    height=Data_board_from_plc['Height'],
                    id_man=Data_board_from_plc['id_man'],
                    time=datetime.datetime.now(),
                    sort=Data_board_from_plc['Sort'],
                    smena=smena(),
                    priznak_size=priznak(Data_board_from_plc['Height'],
                                         Data_board_from_plc['Width'],
                                         Data_board_from_plc['Length'],
                                         Data_board_from_plc['Sort']))
                if retValB:
                    rw_board = 1

            # записываем данные слоев
            if tcp[2] == 1 and rw_layer == 0:
                parse_date_layer(tcp)
                retVal = insert_layer(
                    connection, cursor,
                    length=Data_layer_from_plc['Length'],
                    width=Data_layer_from_plc['Width'],
                    height=Data_layer_from_plc['Height'],
                    time=datetime.datetime.now(),
                    sort=Data_layer_from_plc['Sort'],
                    ColBoard=Data_layer_from_plc['Volume'],
                    karman=Data_layer_from_plc['karman'],
                    priznak_size=priznak(Data_layer_from_plc['Height'],
                                         Data_layer_from_plc['Width'],
                                         Data_layer_from_plc['Length'] / 10,
                                         Data_layer_from_plc['Sort'])
                )
                if retVal != 0:
                    rw_layer = 1
            # write data polet
            if tcp[7] == 1 and rw_palet == 0:
                parse_date_palet(tcp)
                retValP = insert_polet(
                    connection, cursor,
                    length=Data_palet_from_plc['Length'],
                    width=Data_palet_from_plc['Width'],
                    height=Data_palet_from_plc['Height'],
                    time=datetime.datetime.now(),
                    # sort=Data_palet_from_plc['Sort'],
                    ColBoard=Data_palet_from_plc['Volume'],
                )
                if retValP != 0:
                    rw_palet = 1
            # ========================================================================================================
            # reset flag
            # ========================================================================================================
            if tcp[1] == 0:
                rw_board = 0
            if tcp[2] == 0:
                rw_layer = 0
            if tcp[7] == 0:
                rw_palet = 0
            # ========================================================================================================
            # input volum
            if flag[4]:
                result = insert_input_volum(connection, cursor,
                                            flow1=ftp[0],
                                            flow2=ftp[1],
                                            sum=ftp[2],
                                            time=datetime.datetime.now())
                if result != 0:
                    flag[4] = 0

            # ========================================================================================================
            # write status
            # ========================================================================================================
            status(connection, cursor)
            # ========================================================================================================
            # sensor(connection, cursor)
            # ========================================================================================================
            flag[1] = rw_board
            flag[2] = rw_layer
            flag[3] = rw_palet
            # print(flag[3])
            # ========================================================================================================
            # print('Cycle DB', time.time()-start_time)
            # ========================================================================================================
        except Exception as e:
            flag_process[6] = 1
            print('rw_data base_error: ', e, '|time: ', datetime.datetime.now())
        finally:
            time.sleep(0.1)
            # закрываем соединение
            close_connection(connection, cursor)

# =====================================================================================================================
# =====================================================================================================================
# TCP SERVER

def server(name, port, flag, pos):
    file = PATH_TO_SAVE_IMAGE + name
    host = '0.0.0.0'
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    s.listen()
    cprint('wait connect %s' % name, 'yellow')
    cnt = 0
    while True:
        conn, addr = s.accept()
        ts = time.time()
        LEN = 0
        f = open(file, 'wb')
        while True:
            data = conn.recv(4096)
            LEN += len(data)
            f.write(data)
            if not data:
                # cnt += 1
                f.close()
                # print(cnt, ' ', name, 'LEN:', LEN, ': time: ', (time.time() - ts).__round__(4))
                flag[pos] = 1
                break
        # print(data)
        # time.sleep(0.5)


def server_image(name, port, file, flag, pos):
    # file = _PATH_TO_WRITE_BOTTOM_RAW
    host = '0.0.0.0'
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    s.listen()
    cprint('wait connect %s' % name, 'yellow')
    cnt = 0
    while True:
        conn, addr = s.accept()
        LEN = 0
        f = open(file, 'wb')
        while True:
            # ts = time.time()
            data = conn.recv(2048)
            # LEN += len(data)
            f.write(data)
            if not data:
                # cnt += 1
                f.close()
                flag[pos] = True
                # print(cnt, ' ', name, 'LEN:', LEN, ': time: ', (time.time() - ts).__round__(4))
                break
        # time.sleep(0.5)


def server_sensor(port, flag):
    host = '0.0.0.0'
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    s.listen()
    # s.settimeout(3)
    print('wait connect SENSOR')
    cnt = 0
    ts = time.time()
    while True:
        try:
            # if flag[0] == 0:
            if True:
                conn, addr = s.accept()
                dt = None
                while True:
                    message = dt
                    dt = conn.recv(1024)
                    if not dt:
                        cnt += 1
                        flag[0] = 1
                        flag[51] = flag[52] = flag[53] = flag[54] = 1
                        cprint('%s SENSOR_mess: %s : time: %s, %s' %
                               (cnt, message, (time.time() - ts).__round__(3), datetime.datetime.now()), 'magenta',
                               attrs=['reverse'])
                        ts = time.time()
                        break
        except Exception as e:
            cprint('ERROR server sensor: %s' % e, 'red', attrs=['reverse'])


def client_vis(host, port, flag_process):
    while True:
        try:
            while True:
                data = bytes(flag_process)
                s = socket.create_connection((host, port), timeout=30)
                s.sendall(data)
                s.close()
                # print('DATA VIS: ', data)
                time.sleep(10)
        except Exception as e:
            # pass
            print('VISUALISATION TCP', e)
            time.sleep(5)


def server_tcp(data):
    try:
        print('start.....')
        # --------------------------------------------------------------------------------------------------------------
        path_btm_raw = '/home/ast/Desktop/_IMAGE/CAM/bottom-raw.jpg'
        path_btm_mask_g = '/home/ast/Desktop/_IMAGE/CAM/bottom_mask_g.jpg'
        path_btm_mask_h = '/home/ast/Desktop/_IMAGE/CAM/bottom_mask_h.jpg'
        # --------------------------------------------------------------------------------------------------------------
        t00 = Thread(target=server_sensor, args=(10000, data))
        t_vis = Thread(target=client_vis, args=('192.168.0.145', 20040, flag_process))
        # t_irw = Thread(target=server_image, args=('bottom-raw.jpg', 10100, _PATH_TO_WRITE_BOTTOM_RAW))
        t_irw = Thread(target=server_image, args=('bottom-raw.jpg', 10100, path_btm_raw, data, 30))
        t_mg = Thread(target=server_image, args=('bottom_mask_g.jpg', 10101, path_btm_mask_g, data, 31))
        t_mh = Thread(target=server_image, args=('bottom_mask_h.jpg', 10102, path_btm_mask_h, data, 32))
        # t1 = Thread(target=server, args=('cam1.jpg', 10001, data, 11))
        # t2 = Thread(target=server, args=('cam2.jpg', 10002, data, 12))
        # t3 = Thread(target=server, args=('cam3.jpg', 10003, data, 13))
        # t4 = Thread(target=server, args=('cam4.jpg', 10004, data, 14))
        # t5 = Thread(target=server, args=('cam5.jpg', 10005, data, 15))
        # t6 = Thread(target=server, args=('cam6.jpg', 10006, data, 16))
        # t7 = Thread(target=server, args=('cam7.jpg', 10007, data, 17))
        # t8 = Thread(target=server, args=('cam8.jpg', 10008, data, 18))
        # t11 = Thread(target=server, args=('cam1_b.jpg', 10011, data, 21))
        # t12 = Thread(target=server, args=('cam2_b.jpg', 10012, data, 22))
        # t13 = Thread(target=server, args=('cam3_b.jpg', 10013, data, 23))
        # t14 = Thread(target=server, args=('cam4_b.jpg', 10014, data, 24))
        # t15 = Thread(target=server, args=('cam5_b.jpg', 10015, data, 25))
        # t16 = Thread(target=server, args=('cam6_b.jpg', 10016, data, 26))
        # t17 = Thread(target=server, args=('cam7_b.jpg', 10017, data, 27))
        # t18 = Thread(target=server, args=('cam8_b.jpg', 10018, data, 28))

        t00.start()
        # t1.start()
        # t2.start()
        # t3.start()
        # t4.start()
        # t5.start()
        # t6.start()
        # t7.start()
        # t8.start()
        # t11.start()
        # t12.start()
        # t13.start()
        # t14.start()
        # t15.start()
        # t16.start()
        # t17.start()
        # t18.start()
        t_irw.start()
        t_vis.start()
        t_mg.start()
        t_mh.start()

        t00.join()
        # t1.join()
        # t2.join()
        # t3.join()
        # t4.join()
        # t5.join()
        # t6.join()
        # t7.join()
        # t8.join()
        # t11.join()
        # t12.join()
        # t13.join()
        # t14.join()
        # t15.join()
        # t16.join()
        # t17.join()
        # t18.join()
        t_irw.join()
        t_vis.join()
        t_mg.join()
        t_mh.join()

        print('end.....')

    finally:
        os.system('killall -s 9 python3')
        print('fin def')


# =====================================================================================================================
# =====================================================================================================================
# MAIN


if __name__ == '__main__':

    print('*************************************************************************')
    print('*************************************************************************')
    print('********                                                         ********')
    print('********                      START                              ********')
    print('********                                                         ********')
    print('*************************************************************************')
    print('*************************************************************************')
    try:
        file = '/home/ast/Desktop/GIT/PYTHON/ServClient.py'
        # передадим скрипт на сервер
        # send_file_scp(file)

        cam1 = Process(target=Cam, args=(RTSP['cam1'], flag, CAM_PATH_1, _CAM_PATH_1, flag_process))
        cam2 = Process(target=Cam, args=(RTSP['cam2'], flag, CAM_PATH_2, _CAM_PATH_2, flag_process))
        cam3 = Process(target=Cam, args=(RTSP['cam3'], flag, CAM_PATH_3, _CAM_PATH_3, flag_process))
        cam4 = Process(target=Cam, args=(RTSP['cam4'], flag, CAM_PATH_4, _CAM_PATH_4, flag_process))
        cam5 = Process(target=Cam, args=(RTSP['cam5'], flag, CAM_PATH_5, _CAM_PATH_5, flag_process))
        cam6 = Process(target=Cam, args=(RTSP['cam6'], flag, CAM_PATH_6, _CAM_PATH_6, flag_process))
        cam7 = Process(target=Cam, args=(RTSP['cam7'], flag, CAM_PATH_7, _CAM_PATH_7, flag_process))
        cam8 = Process(target=Cam, args=(RTSP['cam8'], flag, CAM_PATH_8, _CAM_PATH_8, flag_process))

        _tcp = Process(target=MB_SIEMENS, args=(arr, tcp, flag,flag_process))
        _db = Process(target=rw_data_base, args=(tcp, flag, ftp, flag_process))
        _ftp = Process(target=ftpInputVolume, args=(FTP_TEMP_PATH, flag, ftp, flag_process))
        create_image = Process(target=CREATE_IMAGE_SENSOR, args=(flag, arr, flag_process, board_param))
        net = Process(target=NET.object_detection, args=(flag, arr, flag_process, board_param))
        server_TCP = Process(target=server_tcp, args=(flag,))

        # симуляция
        # create_image = Process(target=CREATE_IMAGE_SENSOR, args=([1, 0], flag))
        # net = Process(target=NET.object_detection, args=(flag, arr))

        # Cервер клиент
        serv_client = Process(target=start_Server_Client)
        # ====================================
        cam1.start()
        cam2.start()
        cam3.start()
        cam4.start()
        # cam5.start()
        # cam6.start()
        # cam7.start()
        # cam8.start()
        create_image.start()
        net.start()
        _db.start()
        _ftp.start()

        server_TCP.start()

        time.sleep(10)

        _tcp.start()
        # serv_client.start()
        flag_process[8] = 1
        # ====================================
        print('*************************************************************************')
        print('*************************************************************************')
        cam1.join()
        cam2.join()
        cam3.join()
        cam4.join()
        # cam5.join()
        # cam6.join()
        # cam7.join()
        # cam8.join()
        _tcp.join()
        print('tcp end')
        create_image.join()
        print('create image end')
        net.join()
        _db.join()
        print('db end')
        _ftp.join()
        print('ftp end')
        serv_client.join()
        print('client server end')
        server_TCP.start()

    finally:
        flag_process[8] = 0
        # убиваем все процессы
        os.system('killall -s 9 python3')
