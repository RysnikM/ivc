import time
from multiprocessing import Process, Array, Value
import PYTHON.MAIN as MAIN
import PYTHON.NET.NET_from_image as NET
import PYTHON.MODULS.TCP as TCP
# ===========================================================


# DATA = Dict(dat)
num = Value('i', 0)
arr = Array('i', [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
tcp = Array('i', [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,])
# arr[0]  - start object detection
# arr[1]  - Tx, send data to PLC
# arr[2]  - lock_001
# arr[3]  - board_param['x_min']
# arr[4]  - board_param['x_max']
# arr[5]  - board_param['y_min']
# arr[6]  - board_param['y_max']
# arr[7]  - board_param['wiedth']
# arr[8]  - board_param['lenght']
# arr[9]  - write_data
# arr[10] - result sort

# ===========================================================
main = Process(target=MAIN.main, args=(num, arr, tcp))
net = Process(target=NET.object_detection, args=(num, arr))
tcp = Process(target=TCP.MB_SIEMENS, args=(arr, tcp))
# ===========================================================
net.start()
time.sleep(5)
main.start()
time.sleep(5)
tcp.start()
# ===========================================================
main.join()
net.join()
tcp.join()
