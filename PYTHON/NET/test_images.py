
######## Webcam Object Detection Using Tensorflow-trained Classifier #########
# Import packages
import os
import cv2
import numpy as np
import tensorflow as tf
import sys
import time
import cv2
import glob
from lxml import etree
from NET.util import label_map_util
from NET.util import visualization_utils as vis_util

# данные для ввода =============================================================
MODEL_NAME = 'net_v05'

# MODEL_NAME_2 = 'net_v05'


# files = glob.glob('/home/ast/Pictures/2019-11-21/*.jpg')
files = glob.glob('/home/ast/Desktop/_IMAGE/2019-12-10/*.jpg')
# files = glob.glob('/home/ast/Pictures/2019-12-10/*.jpg')

# ==============================================================================

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")
CWD_PATH = os.getcwd()
PATH_TO_CKPT = os.path.join(CWD_PATH, MODEL_NAME, 'frozen_inference_graph.pb')
PATH_TO_LABELS = os.path.join(CWD_PATH, 'defects_class.pbtxt')

# PATH_TO_CKPT_2 = os.path.join(CWD_PATH, MODEL_NAME_2, 'frozen_inference_graph.pb')


def load_data_into_memory():
    global sess, detection_boxes, detection_scores, detection_classes, \
        num_detections, frame_expanded, image_tensor, category_index
    # print(PATH_TO_LABELS)
    # Load the label map.
    category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS,
                                                                        use_display_name=True)
    # print(category_index)
    #
    # # Load the Tensorflow model into memory.
    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')
        sess = tf.Session(graph=detection_graph)

    # Define input and output tensors (i.e. data) for the object detection classifier
    # Input tensor is the image
    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
    # Output tensors are the detection boxes, scores, and classes
    # Each box represents a part of the image where a particular object was detected
    detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
    # Each score represents level of confidence for each of the objects.
    # The score is shown on the result image, together with the class label.
    detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
    detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
    # Number of objects detected
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')
    # print('net ready ===============================')



def fishEyeCam(frame):
    K = np.array(
        [[1987.2902013412322, 0.0, 1540.7521175856489], [0.0, 1914.5965846929532, 843.8461627100188], [0.0, 0.0, 1.0]])
    D = np.array([[-0.061326402914289105], [0.2306815273667478], [-1.002826819903716], [1.0982644076261452]])
    DIM = (3072, 1728)

    # img = cv2.imread(PATH_ROOT)
    img_dim = frame.shape[:2][::-1]
    balance = 0.0
    scaled_K = K * img_dim[0] / DIM[0]
    scaled_K[2][2] = 1.0
    new_K = cv2.fisheye.estimateNewCameraMatrixForUndistortRectify(scaled_K, D,
                                                                   img_dim, np.eye(3), balance=balance)

    map1, map2 = cv2.fisheye.initUndistortRectifyMap(scaled_K, D, np.eye(3),
                                                     new_K, img_dim, cv2.CV_16SC2)
    undist_image = cv2.remap(frame, map1, map2, interpolation=cv2.INTER_LINEAR,
                             borderMode=cv2.BORDER_CONSTANT)
    return undist_image


def saveIMG(image, name, path='/home/ast/Desktop', folder=time.strftime("%Y-%m-%d")):
    PATH_TO_FOLDER = path
    FOLDER_NAME = 'DataSet/' + folder
    _PATH = os.path.join(PATH_TO_FOLDER, FOLDER_NAME)
    # проверка наличия пути, если нет, то создаем папку
    if os.path.exists(_PATH):
        pass
    # print('OK')
    else:
        try:
            os.makedirs(_PATH)
        except OSError:
            print("Creation of the directory %s failed" % _PATH)
        else:
            print("Successfully created the directory %s " % _PATH)

    # формируем имя файла
    FILE_NAME = '{}.jpg'.format(name)
    # print(FILE_NAME)
    # сохраняем файл в папку
    cv2.imwrite(_PATH + '/' + FILE_NAME, image)


def pars_data_from_net(image, boxes=[], scores=[], classes=[], min_score_thresh=0.5, num=300,
                       _folder='', _filename='', _path='',
                       category_index={}):
    page = etree.Element('annotation')
    doc = etree.ElementTree(page)

    folder = etree.SubElement(page, 'folder')
    folder.text = _folder

    filename = etree.SubElement(page, 'filename')
    filename.text = _filename + '.jpg'

    path = etree.SubElement(page, 'path')
    path.text = _path

    source = etree.SubElement(page, 'source')
    database = etree.SubElement(source, 'database')
    database.text = 'Unknown'

    _height, _width, _channels = frame.shape

    size = etree.SubElement(page, 'size')
    width = etree.SubElement(size, 'width')
    width.text = str(_width)
    height = etree.SubElement(size, 'height')
    height.text = str(_height)
    depth = etree.SubElement(size, 'depth')
    depth.text = str(_channels)

    # переберем очки, для того, чтобы не работать с ненужными данными
    for i in range(num):

        if scores[0][i] > min_score_thresh:
            obj = etree.SubElement(page, 'object')
            name = etree.SubElement(obj, 'name')
            name.text = category_index[np.squeeze(classes).astype(np.int32)[i]].get('name')
            pose = etree.SubElement(obj, 'pose')
            pose.text = 'Unspecified'
            truncated = etree.SubElement(obj, 'truncated')
            truncated.text = '0'
            difficult = etree.SubElement(obj, 'difficult')
            difficult.text = '0'
            bndbox = etree.SubElement(obj, 'bndbox')
            xmin = etree.SubElement(bndbox, 'xmin')
            xmin.text = str((boxes[i][1] * _width).__round__(0))
            ymin = etree.SubElement(bndbox, 'ymin')
            ymin.text = str((boxes[i][0] * _height).__round__(0))
            xmax = etree.SubElement(bndbox, 'xmax')
            xmax.text = str((boxes[i][3] * _width).__round__(0))
            ymax = etree.SubElement(bndbox, 'ymax')
            ymax.text = str((boxes[i][2] * _height).__round__(0))
        else:
            break

    # print (etree.tostring(page, pretty_print=True).decode("utf-8"))
    # path_to_save = os.path.join(_path, _folder, '/')
    path_to_save = _path + "/"
    doc.write(path_to_save + _filename + '.xml', pretty_print=True)


i = []
# for f in files:
#     start_time = time.time()
#     frame = cv2.imread(f)
#     if frame is not None:
#         frame = fishEyeCam(frame)
#         frame = cv2.resize(frame, (1280, 720), interpolation=cv2.INTER_LINEAR)
#         # frame = frame[30:130, :]
#         i.append(frame)
#
# res = cv2.hconcat(i)


STANDART_COLORS = [
    (255, 0, 100),    (0, 255, 0),      (0, 100, 255),
    (255, 50, 50),    (50, 255, 50),    (50, 50, 255),
    (255, 50, 50),    (50, 255, 50),    (50, 50, 255),
    (255, 50, 50),    (50, 255, 50),    (50, 50, 255),
    (255, 50, 50),    (50, 255, 50),    (50, 50, 255)
]

def draw_obj(frame, boxes, scores, classes, num=300, min_score_thresh=0.5):
    try:
        # print(classes)
        _height, _width, _channels = frame.shape
        for i in range(num):
            if scores[0][i] > min_score_thresh:
                # name.text = category_index[np.squeeze(classes).astype(np.int32)[i]].get('name')
                # print(boxes[i][1])
                xmin = int(boxes[i][1] * _width)
                ymin = int(boxes[i][0] * _height)
                xmax = int(boxes[i][3] * _width)
                ymax = int(boxes[i][2] * _height)

                # cv2.rectangle(frame, (xmin, ymin), (xmax, ymax), (0, 200, 0), 2)
                cv2.rectangle(frame, (xmin, ymin), (xmax, ymax), STANDART_COLORS[int(classes[i])], 2)
            else:
                break
        return frame
    except Exception as e:
        print('draw obj: ', e,)


load_data_into_memory()
cnt = 0
for f in files:
    start_time = time.time()
    frame = cv2.imread(f)
    # frame = res
    if frame is not None:
        try:
            cnt += 1
            # frame = fishEyeCam(frame)
            y, x, _ = frame.shape

            # frame = cv2.resize(frame, (1280, int(y/(1280/x))), interpolation=cv2.INTER_LINEAR)
            # y, x, _ = frame.shape
            # mp = np.zeros([int(x/1.778-y), x, 3], dtype=np.uint8)
            # frame = cv2.vconcat([frame, mp])

            # frame = frame[:, :1024]



            res = frame.copy()
            # frame = cv2.resize(frame, (1024, int(y/(x/1024))), interpolation=cv2.INTER_LINEAR)
            frame = cv2.resize(frame, (1280, 720), interpolation=cv2.INTER_LINEAR)
            # frame = cv2.resize(frame, (1024, 640), interpolation=cv2.INTER_LINEAR)
            data_frame = frame.copy()
            # frame = cv2.resize(frame, (535, 300), interpolation=cv2.INTER_LINEAR)
            # frame = frame[30:130, :]
            frame_expanded = np.expand_dims(frame, axis=0)

            (boxes, scores, classes, num) = sess.run(
                [detection_boxes, detection_scores, detection_classes, num_detections],
                feed_dict={image_tensor: frame_expanded})

            # Draw the results of the detection (aka 'visulaize the results')
            # res = cv2.resize(res, (1280, 720), interpolation=cv2.INTER_LINEAR)
            vis_util.visualize_boxes_and_labels_on_image_array(
                res,
                np.squeeze(boxes),
                np.squeeze(classes).astype(np.int32),
                np.squeeze(scores),
                category_index,
                use_normalized_coordinates=True,
                line_thickness=1,
                min_score_thresh=0.1)

            # res = draw_obj(res,
            #                  boxes=np.squeeze(boxes),
            #                  classes=np.squeeze(classes),
            #                  scores=scores, min_score_thresh=0.05)

            # All the results have been drawn on the frame, so it's time to display it.

            data = f.split('/')

            # name = data[-1][:-4]
            folder = data[-2]
            path = '/home/ast/Documents/DataSet/'+folder
            if os.path.exists(path):
                pass
            else:
                os.makedirs(path)
            name = 'img#{}#{}.jpg'.format(folder, len(os.listdir(path)) + 1)
            saveIMG(data_frame, path='/home/ast/Documents/', folder=folder, name=name)
            pars_data_from_net(image=frame, boxes=np.squeeze(boxes), scores=scores, classes=classes, min_score_thresh=0.3, num=300,
                            _folder=folder, _filename=name, _path=path,
                            category_index=category_index)


            print(cnt,' / name: ', name,  ' / time: ', (time.time() - start_time).__round__(4), '-------')

            # frame = cv2.imread(f)
            # frame = fishEyeCam(frame)

            # frame = cv2.resize(frame, (x, y), interpolation=cv2.INTER_LINEAR)
            # frame = cv2.resize(frame, (1280, 720), interpolation=cv2.INTER_LINEAR)
            # cv2.imshow('RES', frame)

            # res = frame[30:130, :]
            # cv2.imshow('Object detector', cv2.resize(res, (1280, 720), interpolation=cv2.INTER_LINEAR))
            cv2.imshow('RES', res)

            key = cv2.waitKey()
            if key == 27:
                cv2.destroyAllWindows()
                break


        except Exception as e:
            print('err', e)
            break
    
