import os
import sys
import time
import logging
import datetime

import cv2
import numpy as np
import tensorflow as tf
from termcolor import cprint

from skimage import feature
from scipy import ndimage as ndi
from PIL import Image

# import PYTHON.NET.util.label_map_util as label_map_util

# import PYTHON.NET.util.label_map_util as label_map_util
# import PYTHON.MODULS.Sort as Sort
import NET.util.label_map_util as label_map_util
import MODULS.Sort as Sort

# import util.visualization_utils as vis_util
# import PYTHON.NET.util.visualization_utils as vis_util

# import util.label_map_util as label_map_util
# import util.visualization_utils as vis_util

logging.basicConfig(format=u'%(levelname)-8s [%(asctime)s] %(message)s', level=logging.INFO, filename="log_net.log")

STANDART_COLORS = [
    (255, 0, 100), (0, 255, 0), (0, 100, 255),
    (255, 50, 50), (50, 255, 50), (50, 50, 255),
    (255, 100, 50), (100, 255, 50), (100, 50, 255),
    (255, 150, 50), (150, 255, 50), (150, 50, 255),
    (255, 200, 50), (200, 255, 50), (200, 50, 255),
]

DATA_NET = {
    'start': True,
}
# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")
# Grab path to current working directory
# CWD_PATH = os.getcwd()
# Name of the directory containing the object detection module we're using
MODEL_NAME = 'net_v05'

# Path to frozen detection graph .pb file, which contains the model that is used
# for object detection.
# PATH_TO_CKPT = os.path.join(CWD_PATH, MODEL_NAME, 'frozen_inference_graph.pb')
PATH_TO_CKPT = '/home/ast/Desktop/GIT/PYTHON/NET/' + MODEL_NAME + '/frozen_inference_graph.pb'
# Path to label map file
# PATH_TO_LABELS = os.path.join(CWD_PATH, MODEL_NAME, 'defects_class.pbtxt')
PATH_TO_LABELS = '/home/ast/Desktop/GIT/PYTHON/NET/defects_class.pbtxt'

_PATH_TO_READ_TOP = "/home/ast/Desktop/_IMAGE/NET/top.jpg"
_PATH_TO_READ_BOTTOM = "/home/ast/Desktop/_IMAGE/NET/bottom.jpg"
_CAM1 = "/home/ast/Desktop/_IMAGE/CAM/cam1.jpg"
_CAM2 = "/home/ast/Desktop/_IMAGE/CAM/cam2.jpg"
_CAM3 = "/home/ast/Desktop/_IMAGE/CAM/cam3.jpg"

_PATH_TO_WRITE_TOP = "/var/www/html/your-project/storage/app/public/board/top.jpg"
_PATH_TO_WRITE_BOTTOM = "/var/www/html/your-project/storage/app/public/board/bottom.jpg"
CAM1 = "/home/ast/Desktop/_IMAGE/CAM/cam1.jpg"
CAM2 = "/home/ast/Desktop/_IMAGE/CAM/cam2.jpg"
CAM3 = "/home/ast/Desktop/_IMAGE/CAM/cam3.jpg"

list_image_save = [_PATH_TO_WRITE_TOP, _PATH_TO_WRITE_BOTTOM]
list_image_read = [_PATH_TO_READ_TOP, _PATH_TO_READ_BOTTOM]

sort_list = []
# для симуляции
# list_image_read = [_PATH_TO_READ_TOP, _PATH_TO_READ_BOTTOM, _CAM1, _CAM2, _CAM3]
# list_image_save = [_PATH_TO_WRITE_TOP, _PATH_TO_WRITE_BOTTOM, CAM1, CAM2, CAM3]


def load_data_into_memory():
    global sess, detection_boxes, detection_scores, detection_classes, \
        num_detections, frame_expanded, image_tensor, category_index
    # print(PATH_TO_LABELS)
    # Load the label map.
    category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS,
                                                                        use_display_name=True)
    # print(category_index)
    #
    # # Load the Tensorflow model into memory.
    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')
        sess = tf.Session(graph=detection_graph)

    # Define input and output tensors (i.e. data) for the object detection classifier
    # Input tensor is the image
    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
    # Output tensors are the detection boxes, scores, and classes
    # Each box represents a part of the image where a particular object was detected
    detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
    # Each score represents level of confidence for each of the objects.
    # The score is shown on the result image, together with the class label.
    detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
    detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
    # Number of objects detected
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')
    logging.info('load data into memory')
    # print('net ready ===============================')


def draw_obj(frame, boxes, scores, classes, num=300, min_score_thresh=0.5):
    DELTA = 7
    try:
        _height, _width, _channels = frame.shape
        for i in range(num):
            if scores[0][i] > min_score_thresh:
                # name.text = category_index[np.squeeze(classes).astype(np.int32)[i]].get('name')
                # print(boxes[i][1])

                xmin = int(boxes[i][1] * _width) + DELTA
                ymin = int(boxes[i][0] * _height)
                xmax = int(boxes[i][3] * _width) - DELTA
                ymax = int(boxes[i][2] * _height)

                # cv2.rectangle(frame, (xmin, ymin), (xmax, ymax), (0, 200, 0), 2)
                cv2.rectangle(frame, (xmin, ymin), (xmax, ymax), STANDART_COLORS[int(classes[i])], 4)

            else:
                break
        return frame
    except Exception as e:
        print('draw obj: ', e, ' | time:', datetime.datetime.now())


def check_serdcevina(img):
    try:
        img_g = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        edg1 = cv2.Canny(img_g.copy(), 100, 350)
        board = edg1.copy()
        edg2 = cv2.Canny(img_g.copy(), 0, 500)
        # --------------------------------------------------------------------------------------------------------------
        msk01 = edg2 == 255
        res = edg1.copy()
        res[msk01] = 0
        # --------------------------------------------------------------------------------------------------------------
        res = cv2.dilate(res, (3, 3), iterations=3)
        # cv2.imwrite('/home/ast/Desktop/_IMAGE/s01.jpg', edg1)
        # cv2.imwrite('/home/ast/Desktop/_IMAGE/s02.jpg', edg2)

        # --------------------------------------------------------------------------------------------------------------
        edge_top, _ = cv2.findContours(res, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        # --------------------------------------------------------------------------------------------------------------
        for i in edge_top:
            x, y, w, h = cv2.boundingRect(i)
            # r = cv2.minAreaRect(i)
            # box = cv2.boxPoints(r)
            # box = np.int0(box)
            # x11, y11 = box[1][0], box[1][1]
            # x1, y1 = box[0][0], box[0][1]
            # x2, y2 = box[2][0], box[2][1]
            # ----------------------------------------------------------------------------------------------------------
            if 10 > h >= 1 and w/h >= 20 and w > 100:
                line = board[:, x]
                pos = np.where(line != 0)
                y_min = pos[0][0]
                y_max = pos[0][-1]
                # print(box, y_min, y_max, pos)
                if y_min+10 > y > y_min-10:
                    continue
                if y_max+10 > y > y_max-10:
                    continue

                return [True, [x, y, w, h]]
            # ----------------------------------------------------------------------------------------------------------
        return [False]
    except:
        return [False]


def find_defekts(image, knot, type_knot, obzol, serdcevina):
    # ----------------------------------------------------------------------------
    im = image.copy()
    # im = cv2.resize(im, (int(im.shape[1] / 1.3), int(im.shape[0] / 1.3)))
    img = cv2.cvtColor(im.copy(), cv2.COLOR_BGR2GRAY)
    x, y = img.shape
    img = Image.fromarray(img)
    img = ndi.gaussian_filter(img, 1)
    # ----------------------------------------------------------------------------
    # edg_contour = feature.canny(img, sigma=5, low_threshold=0, high_threshold=80)
    edg_knot_t3 = feature.canny(img, sigma=3, low_threshold=50, high_threshold=55)
    edg_knot_t2 = feature.canny(img, sigma=4, low_threshold=30, high_threshold=40)
    edg_knot_t1 = feature.canny(img, low_threshold=40, high_threshold=90)
    # ----------------------------------------------------------------------------
    m_knot_t3 = np.zeros([x, y], dtype=np.uint8)
    m_knot_t2 = np.zeros([x, y], dtype=np.uint8)
    m_knot_t1 = np.zeros([x, y], dtype=np.uint8)
    # ----------------------------------------------------------------------------
    m_knot_t3[edg_knot_t3] = 255
    m_knot_t3 = cv2.dilate(m_knot_t3, (3, 3), iterations=2)
    cont_knot_t3, _ = cv2.findContours(m_knot_t3, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # ----------------------------------------------------------------------------
    m_knot_t1[edg_knot_t1] = 255
    # ----------------------------------------------------------------------------
    m_knot_t2[edg_knot_t2] = 255
    # --------------------------------------------------------------------------------------------------------------
    # img = im
    # --------------------------------------------------------------------------------------------------------------
    # сучки 3 типа
    for i in cont_knot_t3:
        x, y, w, h = cv2.boundingRect(i)
        if 25 >= h >= 6 and 1.8 >= w / h >= 0.8:
            # print(x, y, w, h)
            # проверим есть ли уже выделение сучка
            if m_knot_t3[int(y + h / 2), int(x + w / 2)] == 255:
                continue
            # cv2.rectangle(img, (x, y), (x + w, y + h), (150, 0, 150), 2)
            # удалим контуры для 3 типа для того, чтобы небло дублирования сучков
            d = 5
            m_knot_t2[y - d:y + h + d, x - d:x + w + d] = 0
            m_knot_t1[y - d:y + h + d, x - d:x + w + d] = 0
            # -------------------
            # избавимся от повторов выделения сучков одного типа
            D = 10
            m_knot_t3[y - D:y + h + D, x - D:x + w + D] = 255
            # -------------------
            knot.append([x, y, w, h])
            type_knot.append(3)
    # -------------------
    # сучки 2 типа
    m_knot_t2 = cv2.dilate(m_knot_t2, (3, 3), iterations=2)
    cont_knot_t2, _ = cv2.findContours(m_knot_t2, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # -------------------
    for i in cont_knot_t2:
        x, y, w, h = cv2.boundingRect(i)
        if 30 >= h >= 4 and 30 >= w >= 4 and 5 >= w / h >= 0.2:
            # проверим есть ли уже выделение сучка
            if m_knot_t2[int(y + h / 2), int(x + w / 2)] == 255:
                continue
            # --------------------------------------
            # cv2.rectangle(img, (x, y), (x + w, y + h), (10, 250, 50), 2)
            # удалим контуры для 2 и 3 типа для того, чтобы небло дублирования сучков
            d = 5
            m_knot_t1[y - d:y + h + d, x - d:x + w + d] = 0
            # -------------------
            # избавимся от повторов выделения сучков одного типа
            D = 10
            m_knot_t2[y - D:y + h + D, x - D:x + w + D] = 255
            # ------------------
            knot.append([x, y, w, h])
            type_knot.append(2)
    # --------------------------------------------------------------------------------------------------------------
    # контур доски:
    cont_01 = cv2.cvtColor(im.copy(), cv2.COLOR_BGR2GRAY)
    mask1 = cont_01 >= 25
    im[mask1] = 255
    cont_02 = cv2.Canny(im, 150, 500)
    # fix border --------------------------------------------------------------------------------------------------
    cont_02[:10, :10] = 0
    # cv2.imwrite('/home/ast/Desktop/_IMAGE/t1.jpg', m_knot_t1)
    # --------------------------------------------------------------------------------------------------------------
    m_knot_cont = cv2.morphologyEx(cont_02, cv2.MORPH_DILATE, np.ones([5, 5], np.uint8))
    edg = m_knot_cont == 255
    m_knot_t1[edg] = 0
    cv2.imwrite('/home/ast/Desktop/_IMAGE/t1.jpg', m_knot_t1)
    cv2.imwrite('/home/ast/Desktop/_IMAGE/t2.jpg', m_knot_cont)
    # --------------------------------------------------------------------------------------------------------------
    m_knot_t1 = cv2.dilate(m_knot_t1, (3, 3), iterations=2)
    # ------------------
    cont_knot_t1, _ = cv2.findContours(m_knot_t1, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for i in cont_knot_t1:
        x, y, w, h = cv2.boundingRect(i)
        # ----------------------------------------------------------------------------------------------------------
        # обзол:
        if w > 40 and 3 < h < 15:
            line = m_knot_cont[:, x]
            pos = np.where(line != 0)
            try:
                y_min = pos[0][0]
                y_max = pos[0][-1]
                # cv2.rectangle(im, (x, y), (x + w, y + h), (255, 50, 255), 2)
                # если верхний край или нижний край прямоугольника находится в зоне границы доски
                delta = 10
                if h < 20 and (y_min + delta > y > y_min - delta or y_max + delta > y + h > y_max - delta):
                    obzol.append([x, y, w, h])
                    continue
                    # print(y_min, y_max, x, y, w, h)
                    # cv2.rectangle(im, (x, y), (x + w, y + h), (255, 50, 255), 2)
            except: print('err_1')
        # ----------------------------------------------------------------------------------------------------------
        # сердцевина
        if 10 >= h >= 1 and w / h >= 10 and w > 50:
            # line = m_knot_t1[:, x]
            line = m_knot_cont[:, x]
            pos = np.where(line != 0)
            try:
                y_min = pos[0][0]
                y_max = pos[0][-1]
                delta = 15
                if y_max - delta > y > y_min + delta:
                    serdcevina.append([x, y, w, h])
                    continue
                    # print(y_min, y_max, x, y, w, h)
                    # cv2.rectangle(im, (x, y), (x + w, y + h), (50, 0, 150), 1)
            except:
                print('err_2')
        # ----------------------------------------------------------------------------------------------------------
        # сучки 1 типа
        if 30 >= h >= 8 and 30 >= w >= 8 and 8 >= w / h >= 0.1:
            # проверим есть ли уже выделение сучка
            if m_knot_t1[int(y + h / 2), int(x + w / 2)] == 255 and m_knot_t1[int(y + 2), int(x + 2)] == 255:
                continue
            # cv2.rectangle(img, (x, y), (x + w, y + h), (100, 100, 100), 2)
            # -------------------
            # избавимся от повторов выделения сучков одного типа
            D = 10
            m_knot_t1[y - D:y + h + D, x - D:x + w + D] = 255
            # ------------------
            knot.append([x, y, w, h])
            type_knot.append(1)


def object_detection(flag, arr, flag_process, board_param):
    # flag
    # flag[5] - start_net
    # flag[6] - Tx to write data in PLC

    # arr
    # arr[8] - cod for network detection
    # arr[10] - cod sord board

    print('Start thread NET, time', datetime.datetime.now())
    # print(data)
    global frame, sort_cod
    sort_cod = 0
    # load_data_into_memory()
    category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS,
                                                                        use_display_name=True)

    # флаг инициализации сети
    init_flag = 0
    while True:
        if flag[5] == 1:
            time_start = time.time()
            if True:
                init_flag += 1
                for ii in range(len(list_image_read)):
                    frame_original = cv2.imread(list_image_read[ii])
                    try:
                        # если мы удачно считали изображение, то выполняем цикл
                        if frame_original is not None:
                            frame = frame_original.copy()
                            # ------------------------------------------------------------------------------------------
                            img = frame
                            img = cv2.resize(img, (int(img.shape[1] / 2), int(img.shape[0] / 2)))
                            # ------------------------------------------------------------------------------------------
                            knot = []
                            type_knot = []
                            obzol = []
                            serdcevina = []
                            find_defekts(img, knot, type_knot, obzol, serdcevina)
                            # ------------------------------------------------------------------------------------------
                            clr = [
                                (100, 100, 100),
                                (10, 250, 50),
                                (150, 0, 150)]
                            for i in range(len(knot)):
                                x, y, w, h = knot[i]
                                # print(type_knot[i])
                                cv2.rectangle(img, (x, y), (x + w, y + h), clr[type_knot[i] - 1], 2)
                            for obz in obzol:
                                x, y, w, h = obz
                                cv2.rectangle(img, (x, y), (x + w, y + h), (0, 50, 250), 1)
                            for src in serdcevina:
                                x, y, w, h = src
                                cv2.rectangle(img, (x, y), (x + w, y + h), (255, 30, 0), 1)
                            # ------------------------------------------------------------------------------------------
                            Sort.sort_knots(Sort.DEFECTS, knot, type_knot, board_param, category_index)
                            sort_list.append(Sort.SORT['result'])
                            # --------------------
                            if len(serdcevina) > 0:
                                sort_list.append(3)
                            # --------------------
                            sort_list.append(
                                Sort.sort_obzol(obzol, board_param))
                            # ------------------------------------------------------------------------------------------
                            # cv2.imshow('frame', frame)
                            # key = cv2.waitKey(10)
                            # if key == 27:
                            #     cv2.destroyAllWindows()
                            #     break
                            # ------------------------------------------------------------------------------------------
                            frame_original = img
                            header = np.zeros([50, frame_original.shape[1], 3], dtype=np.uint8)
                            frame_original_header = cv2.vconcat([header, frame_original])
                            cv2.putText(frame_original_header, 's_COD: %s, COD_BOARD: %s'
                                        % (max(sort_list) - 1, arr[8]),
                                        (45, 45 ), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 255, 0), 1)
                            # print(ii)
                            cv2.imwrite(list_image_save[ii], frame_original_header)
                            # ------------------------------------------------------------------------------------------
                            # visualisation
                            flag_process[7] = 1
                            # ------------------------------------------------------------------------------------------
                    except Exception as e:
                        import traceback
                        flag_process[7] = 0
                        traceback.print_exc()
                        print('neural networt: ', e, ' | time: ', datetime.datetime.now())
            # ----------------------------------------------------------------------------------------------------------
            # выведем время выполнения операции
            # сообщаем, что распознование прошло
            flag[5] = 0
            # необходимо передать в ПЛК информацию о определенном сорте
            # ----------------------------------------------------------------------------------------------------------
            if init_flag > 1:
                flag[6] = 1
            # ----------------------------------------------------------------------------------------------------------
            if init_flag >= 5000:
                init_flag = 2
            # ----------------------------------------------------------------------------------------------------------
            sort = max(sort_list)
            sort_cod = create_sort_data_to_plc(sort, sort, 0)
            if arr[8] == 100:
                arr[10] = sort_cod
            else:
                arr[10] = arr[8]
            # ----------------------------------------------------------------------------------------------------------
            print('sort_list: ', sort_list, ' sort: ', sort, ' sort_cod: ', sort_cod)
            cprint('SORT COD: %s, time: %s' % (arr[10], (time.time() - time_start).__round__(4)), attrs=['reverse'])
            # print('SORT COD: ', arr[10])
            # очистим массив сортов
            sort_list.clear()
            # cprint('SORT COD: %s, time: %s' % (arr[10], (time.time() - time_start).__round__(4)), attrs=['reverse'])
            # print('SORT COD: ', arr[10])
            # очистим массив сортов
            sort_list.clear()


def create_sort_data_to_plc(x0=0, x1=0, x2=0):
    """
    XXX - код, передаваемый на ПЛК для далнейшей сортировки
    _ _ Х - сорт доски для толщины до 40мм
        1 - отборный сорт
        2 - 1-й сорт
        3 - 2-й сорт
        4 - 3-й сорт
        5 - 4-й сорт
        6 - доска для оптимизации
        7 - брак
    _ Х _ - сорт доски для толщины свыше 40мм
        код такой же как и для предыдущей позиции
    Х _ _ - положение обзола на доске:
        0 - нет обзола
        1 - обзол сконцентрировал слева в объеме 20% от длинны доски
        2 - обзол сконцентрировал слева в объеме 40% от длинны доски
    :return: код
    """
    return x0 + 10 * x1 + 100 * x2


if __name__ == '__main__':

    object_detection()
