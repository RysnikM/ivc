import cv2
import numpy as np
import multiprocessing as mp
from multiprocessing import Process
from threading import Thread
import time
import datetime
import os
import socket
from termcolor import cprint
from pyModbusTCP.client import ModbusClient
import traceback
# =====================================================================================================================
RTSP = dict(cam1='rtsp://admin:z1x2c3v4b5@192.168.0.101:554', cam2='rtsp://admin:z1x2c3v4b5@192.168.0.102:554',
            cam3='rtsp://admin:z1x2c3v4b5@192.168.0.103:554', cam4='rtsp://admin:z1x2c3v4b5@192.168.0.104:554',
            cam5='rtsp://admin:z1x2c3v4b5@192.168.0.105:554', cam6='rtsp://admin:z1x2c3v4b5@192.168.0.106:554',
            cam7='rtsp://admin:z1x2c3v4b5@192.168.0.107:554', cam8='rtsp://admin:z1x2c3v4b5@192.168.0.108:554')
# ----------------------------------------------------------------------------------------------------------------------
Frame = dict(cam1=None, cam2=None, cam3=None, cam4=None, cam5=None, cam6=None, cam7=None, cam8=None,
             cam1_b=None, cam2_b=None, cam3_b=None, cam4_b=None, cam5_b=None, cam6_b=None, cam7_b=None, cam8_b=None,
             flag_err=None)
Frame_del_background = dict(cam1=None, cam2=None, cam3=None, cam4=None, cam5=None, cam6=None, cam7=None, cam8=None)
# ----------------------------------------------------------------------------------------------------------------------
Roi = dict(
    cam1=[0.1, 0.5, 0, -17], cam2=[0.1, 0.5, 17, -17], cam3=[0.1, 0.5, 17, -17], cam4=[0.1, 0.5, 17, -1],
    cam1_b=[0.1, 0.5, 0, -17], cam2_b=[0.1, 0.5, 17, -17], cam3_b=[0.1, 0.5, 17, -17], cam4_b=[0.1, 0.5, 17, -1],
    cam5_b=[0.1, 0.5, 30, -1], cam6_b=[0.1, 0.5, 20, -1], cam7_b=[0.1, 0.5, 25, -260], cam8_b=[0.1, 0.5, 40, -30],
    cam5=[0.1, 0.5, 30, -1], cam6=[0.1, 0.5, 20, -1], cam7=[0.1, 0.5, 25, -260], cam8=[0.1, 0.5, 40, -30],)
# ----------------------------------------------------------------------------------------------------------------------
# положения доски на изображении
Y_min = dict(cam1=None, cam2=None, cam3=None, cam4=None, cam5=None, cam6=None, cam7=None, cam8=None)
Y_max = dict(cam1=None, cam2=None, cam3=None, cam4=None, cam5=None, cam6=None, cam7=None, cam8=None)
# ширина доски
W_board = dict(cam1=None, cam2=None, cam3=None, cam4=None, cam5=None, cam6=None, cam7=None, cam8=None)
# ----------------------------------------------------------------------------------------------------------------------
# Data exchange process
data = mp.Array('i', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
# data[0] - оптический сенсор на прохождение доски
# data[1] - cam1 - done
# data[2] - cam2 - done
# data[3] - cam3 - done
# data[4] - cam4 - done
# data[5] - cam5 - done
# data[6] - cam6 - done
# data[7] - cam7 - done
# data[8] - cam8 - done
# data[9] - get image done
# ----------------------------------------------------------------------------------------------------------------------
flag_process = mp.Array('i', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
# flag_process[5] - cam5 - ready
# flag_process[6] - cam6 - ready
# flag_process[7] - cam7 - ready
# flag_process[8] - cam8 - ready
# flag_process[1] - tcp - ready
# ----------------------------------------------------------------------------------------------------------------------
_CAM_PATH_1 = "/home/ast/Documents/CAM_RAW/cam1_b.jpg"
_CAM_PATH_2 = "/home/ast/Documents/CAM_RAW/cam2_b.jpg"
_CAM_PATH_3 = "/home/ast/Documents/CAM_RAW/cam3_b.jpg"
_CAM_PATH_4 = "/home/ast/Documents/CAM_RAW/cam4_b.jpg"
_CAM_PATH_5 = "/home/ast/Documents/CAM_RAW/cam5_b.jpg"
_CAM_PATH_6 = "/home/ast/Documents/CAM_RAW/cam6_b.jpg"
_CAM_PATH_7 = "/home/ast/Documents/CAM_RAW/cam7_b.jpg"
_CAM_PATH_8 = "/home/ast/Documents/CAM_RAW/cam8_b.jpg"

CAM_PATH_1 = "/home/ast/Documents/CAM_RAW/cam1.jpg"
CAM_PATH_2 = "/home/ast/Documents/CAM_RAW/cam2.jpg"
CAM_PATH_3 = "/home/ast/Documents/CAM_RAW/cam3.jpg"
CAM_PATH_4 = "/home/ast/Documents/CAM_RAW/cam4.jpg"
CAM_PATH_5 = "/home/ast/Documents/CAM_RAW/cam5.jpg"
CAM_PATH_6 = "/home/ast/Documents/CAM_RAW/cam6.jpg"
CAM_PATH_7 = "/home/ast/Documents/CAM_RAW/cam7.jpg"
CAM_PATH_8 = "/home/ast/Documents/CAM_RAW/cam8.jpg"
# ----------------------------------------------------------------------------------------------------------------------
# TOP_IMAGE_PATH = "/home/ast/Desktop/_IMAGE/NET/top.jpg"
# BOTTOM_IMAGE_PATH = "/home/ast/Desktop/_IMAGE/NET/bottom.jpg"
# FTP_TEMP_PATH = '/home/ast/Documents/ftpFiles/'
PATH_TO_WRITE = '/home/ast/Documents/CAM/'
# PATH_TO_IMAGE = '/home/ast/Documents/CAM_RAW/'
PATH_TO_IMAGE = '/home/ast/Documents/CAM/'
# =====================================================================================================================


def Cam(url, data, path, path_b, port_1, port_2, flag_process):
    global ts
    # номер камеры
    position = int(path.split('/').pop()[3:4])
    # позиция бита для старта получения изображения
    positionStart = position + 10
    # ------------------------------------------------------------------------------------------------------------------
    try:
        cam = cv2.VideoCapture(url)
        cprint('start process: %s, time: %s' % (path.split('/')[-1], datetime.datetime.now()), 'yellow')
        # --------------------------------------------------------------------------------------------------------------
        # для визуализации
        flag_process[position] = 1
        # --------------------------------------------------------------------------------------------------------------
        # номер кадра в буфере
        num_frame = 0
        # cnt = 0
        while True:
            cam.grab()
            # ----------------------------------------------------------------------------------------------------------
            # старт проходит от функции обработки TCP
            if data[positionStart] == 1 or num_frame > 0:
                data[position] = 0
                num_frame += 1
                # ------------------------------------------------------------------------------------------------------
                # зона над экраном
                if num_frame == 1:
                    ts = time.time()
                    res, frame = cam.read()
                    cv2.imwrite(path_b, fishEyeCam(cv2.resize(frame, (1280, 720)))[250:500, :])
                    # cv2.imwrite(path, frame)
                    # сохраним данные для исследований
                    # saveIMG(frame)
                # ------------------------------------------------------------------------------------------------------
                # зона после экрана
                if num_frame == 12:
                    num_frame = 0
                    res, frame = cam.read()
                    cv2.imwrite(path, fishEyeCam(cv2.resize(frame, (1280, 720)))[250:500, :])
                    # saveIMG(frame)
                    print((time.time() - ts).__round__(5))
                    # --------------------------------------------------------------------------------------------------
                    # установим флаги для начала наботы других частей программы
                    data[positionStart] = 0
                    data[position] = 1
    except Exception as e:
        cprint('ERR %s' % path, 'red', attrs=['reverse'])
        flag_process[position] = 0
        os.system('killall -s9 python3')
        exit(159)


def saveIMG(image, path='/home/ast/Desktop'):
    PATH_TO_FOLDER = path
    FOLDER_NAME = '_IMAGE/' + time.strftime("%Y-%m-%d")
    _PATH = os.path.join(PATH_TO_FOLDER, FOLDER_NAME)
    # проверка наличия пути, если нет, то создаем папку
    if os.path.exists(_PATH):
        pass
    # print('OK')
    else:
        try:
            os.makedirs(_PATH)
        except OSError:
            print("Creation of the directory %s failed" % _PATH)
        else:
            print("Successfully created the directory %s " % _PATH)

    # формируем имя файла
    FILE_NAME = '{}.jpg'.format(len(os.listdir(_PATH)) + 1)
    # print(FILE_NAME)
    # сохраняем файл в папку
    cv2.imwrite(_PATH + '/' + FILE_NAME, image)


def fishEyeCam(frame):
    K = np.array(
        [[1987.2902013412322, 0.0, 1580.7521175856489], [0.0, 1914.5965846929532, 865.8461627100188], [0.0, 0.0, 1.0]])
    D = np.array([[-0.061326402914289105], [0.2306815273667478], [-1.002826819903716], [1.0982644076261452]])
    DIM = (3072, 1728)

    # img = cv2.imread(PATH_ROOT)
    img_dim = frame.shape[:2][::-1]
    balance = 0.0
    scaled_K = K * img_dim[0] / DIM[0]
    scaled_K[2][2] = 1.0
    new_K = cv2.fisheye.estimateNewCameraMatrixForUndistortRectify(scaled_K, D,
                                                                   img_dim, np.eye(3), balance=balance)

    map1, map2 = cv2.fisheye.initUndistortRectifyMap(scaled_K, D, np.eye(3),
                                                     new_K, img_dim, cv2.CV_16SC2)
    undist_image = cv2.remap(frame, map1, map2, interpolation=cv2.INTER_LINEAR,
                             borderMode=cv2.BORDER_CONSTANT)
    return undist_image

# =====================================================================================================================
# TCP
# =====================================================================================================================
def Client(SERVER_HOST='192.168.0.100', SERVER_PORT='502'):
    client = ModbusClient()
    client.host(SERVER_HOST)
    client.port(int(SERVER_PORT))
    client.open()

    return [client, client.is_open()]


def read_03(client, adrFrom=0, adrTo=10):
    # open or reconnect TCP to server
    if not client.is_open():
        if not client.open():
            raise Exception('NO CONNECT TO HOST')

    if client.is_open():
        regs = client.read_holding_registers(adrFrom, adrTo)
        if regs:
            return regs


def write_06(client, adrReg, data):
    while True:
        if not client.is_open():
            if not client.open():
                print('err')
        #                return 'ERROR WRITE TCP'
        if client.is_open():
            regs = client.write_single_register(adrReg, data)
            if regs:
                return regs


def tcpClient(data, flag_process):
    plc, status = Client('192.168.0.90', '503')
    cprint('Start process: TCP, time: %s' % datetime.datetime.now(), 'blue', attrs=['reverse'])
    flag_01 = 1
    flag_02 = 0
    tsens = time.time()
    cnt = 0
    flag_process[1] = 1
    while True:
        t1 = time.time()
        try:
            # чтение регистров
            buff = read_03(plc, 0, 10)
            #   ======================================================================================================
            # сенсор переднего фронта
            if buff[0] == 1 and flag_01:
                cnt += 1
                cprint('%s, sensor, time: %s' % (cnt, (time.time() - tsens).__round__(4)), 'magenta', attrs=['reverse'])
                # нижние камеры
                data[15] = data[16] = data[17] = data[18] = 1
                # sensor ===============================
                # для передачи на другой сервер
                data[0] = 1
                # ======================================
                # установим флаги, сообщая о том, что доска зашла под датчик
                flag_01 = False
                flag_02 = True
                tsens = time.time()
                reset = 1
                time.sleep(0.3)
            # ---------------------------------------------------------------------------------------------------------
            if buff[2] == 1 and flag_02:
                cprint('Sensor btm', 'yellow', attrs=['reverse'])

                # нижние камеры
                # data[15] = data[16] = data[17] = data[18] = 1
                # сбросим флаги
                flag_02 = False
                # установим сброс в True
                # reset = 1
                # time.sleep(0.3)
            # ---------------------------------------------------------------------------------------------------------
            # если бит сенсора сброшев в ПЛК, то продолжаем работать
            if buff[0] == 0:
                flag_01 = True
                data[0] = 0
                reset = 0
            # ---------------------------------------------------------------------------------------------------------
            # сброс датчика
            write_06(plc, 1, reset)
            #   ======================================================================================================
        except Exception as e:
            print('TCP: ', e)
            print('RESTART')
            flag_process[1] = 0
            time.sleep(5)
            plc, status = Client('192.168.0.90', '503')
            print(plc, status)
        finally:
            pass

# =====================================================================================================================

def client_vis(host, port, flag_process):
    while True:
        try:
            while True:
                data = bytes(flag_process)
                s = socket.create_connection((host, port), timeout=30)
                s.sendall(data)
                s.close()
                # print('DATA VIS: ', data)
                time.sleep(10)
        except Exception as e:
            # pass
            print('VISUALISATION TCP', e)
            time.sleep(5)


def client(host, port, name, flag, position):
    file = PATH_TO_IMAGE + name
    # cnt = 0
    while True:
        try:
            # s = socket.create_connection((host, port), timeout=30)
            while True:
                if flag[position] == 1:
                    # time.sleep(0.1)
                    # cnt += 1
                    # print(cnt, 'start send ', name, " & ", position)
                    s = socket.create_connection((host, port), timeout=30)
                    _file = open(file, 'rb')
                    data = _file.read()
                    size = len(data)
                    s.sendall(data)
                    s.close()
                    flag[position] = 0
                    print(name, ' ', size,  " & ", position)
        except Exception as e:
            pass
            # print('/////', e)
        finally:
            pass
            # flag[position] = 0


def client_sensor(host, port, flag):
    cnt = 0
    while True:
        try:
            if flag[0] == 1:
                time.sleep(0.1)
                s = socket.create_connection((host, port), timeout=30)
                s.sendall(b'SENSOR')
                # s.send(b'SENSOR')
                cnt += 1
                # cprint('%s sensor ' % cnt, attrs=['reverse'])
                time.sleep(0.1)
                s.close()
                flag[0] = 0
        except Exception as e:
            pass
            # cprint('ERROR client sensor: %s' % e, 'red', attrs=['reverse'])


def client_tcp(data):
    # data[0] - оптический сенсор на прохождение доски
    # data[1] - cam1 - done
    # data[2] - cam2 - done
    # data[3] - cam3 - done
    # data[4] - cam4 - done
    # data[5] - cam5 - done
    # data[6] - cam6 - done
    # data[7] - cam7 - done
    # data[8] - cam8 - done

    try:
        # сенсор о прохождении доски
        t00 = Thread(target=client_sensor, args=('192.168.0.40', 10000, data))
        # --------------------------------------------------------------------------------------------------------------
        # визуализация
        t_vis = Thread(target=client_vis, args=('192.168.0.145', 20121, flag_process))
        # --------------------------------------------------------------------------------------------------------------
        # подготовленные изображения
        t_irw = Thread(target=client, args=('192.168.0.40', 10100, 'btm9_raw.jpg', data, 41))
        t_mask_g = Thread(target=client, args=('192.168.0.40', 10101, 'mask_g.jpg', data, 39))
        t_mask_h = Thread(target=client, args=('192.168.0.40', 10102, 'mask_h.jpg', data, 40))
        # --------------------------------------------------------------------------------------------------------------
        # t1 = Thread(target=client, args=('192.168.0.40', 10001, 'cam1.jpg', data))
        # t2 = Thread(target=client, args=('192.168.0.40', 10002, 'cam2.jpg', data))
        # t3 = Thread(target=client, args=('192.168.0.40', 10003, 'cam3.jpg', data))
        # t4 = Thread(target=client, args=('192.168.0.40', 10004, 'cam4.jpg', data))
        # t5 = Thread(target=client, args=('192.168.0.40', 10005, 'cam5_b.jpg', data))
        # t6 = Thread(target=client, args=('192.168.0.40', 10006, 'cam6_b.jpg', data))
        # t7 = Thread(target=client, args=('192.168.0.40', 10007, 'cam7_b.jpg', data))
        # t8 = Thread(target=client, args=('192.168.0.40', 10008, 'cam8_b.jpg', data))

        # t5 = Thread(target=client, args=('192.168.0.40', 10005, 'cam5_m.jpg', data, 31))
        # t6 = Thread(target=client, args=('192.168.0.40', 10006, 'cam6_m.jpg', data, 32))
        # t7 = Thread(target=client, args=('192.168.0.40', 10007, 'cam7_m.jpg', data, 33))
        # t8 = Thread(target=client, args=('192.168.0.40', 10008, 'cam8_m.jpg', data, 34))
        #
        # # t11 = Thread(target=client, args=('192.168.0.40', 10011, 'cam1_b.jpg', data))
        # # t12 = Thread(target=client, args=('192.168.0.40', 10012, 'cam2_b.jpg', data))
        # # t13 = Thread(target=client, args=('192.168.0.40', 10013, 'cam3_b.jpg', data))
        # # t14 = Thread(target=client, args=('192.168.0.40', 10014, 'cam4_b.jpg', data))
        #
        # t15 = Thread(target=client, args=('192.168.0.40', 10015, 'cam5.jpg', data, 35))
        # t16 = Thread(target=client, args=('192.168.0.40', 10016, 'cam6.jpg', data, 36))
        # t17 = Thread(target=client, args=('192.168.0.40', 10017, 'cam7.jpg', data, 37))
        # t18 = Thread(target=client, args=('192.168.0.40', 10018, 'cam8.jpg', data, 38))

        t00.start()
        # t1.start()
        # t2.start()
        # t3.start()
        # t4.start()
        # t5.start()
        # t6.start()
        # t7.start()
        # t8.start()
        # # t11.start()
        # # t12.start()
        # # t13.start()
        # # t14.start()
        # t15.start()
        # t16.start()
        # t17.start()
        # t18.start()

        t_irw.start()
        t_vis.start()
        t_mask_g.start()
        t_mask_h.start()

        t00.join()
        # t1.join()
        # t2.join()
        # t3.join()
        # t4.join()
        # t5.join()
        # t6.join()
        # t7.join()
        # t8.join()
        # # t11.join()
        # # t12.join()
        # # t13.join()
        # # t14.join()
        # t15.join()
        # t16.join()
        # t17.join()
        # t18.join()

        t_irw.join()
        t_vis.join()
        t_mask_g.join()
        t_mask_h.join()

    finally:
        print('FINALLY SCRIPT')
        os.system('killall -s 9 python3')



def CREATE_IMAGE(data):
    init = True
    cnt = 0
    CAM_READY = False
    IMAGE_SEND_READY = False
    while True:
        try:
            # если от всех камер пришел флаг о готовности кадров, ставим флаг готовности камер в true
            if data[5] and data[6] and data[7] and data[8]:
                CAM_READY = True
            # ----------------------------------------------------------------------------------------------------------
            if not data[39] and not data[40] and not data[41]:
                IMAGE_SEND_READY = True
            else:
                IMAGE_SEND_READY = False
            # ----------------------------------------------------------------------------------------------------------
            if (CAM_READY and IMAGE_SEND_READY) or init:
                t1 = time.time()
                # ------------------------------------------------------------------------------------------------------
                cnt += 1
                # сбросим флаги, для того, чтобы камеры не ждали окончания цикла и могли получать новые кадры
                data[5] = data[6] = data[7] = data[8] = 0
                CAM_READY = False
                IMAGE_SEND_READY = False
                # ------------------------------------------------------------------------------------------------------
                # ts = time.time()
                grab_img_thread()
                # ------------------------------------------------------------------------------------------------------
                # bottom_raw = cv2.hconcat([Frame['cam5'], Frame['cam6'], Frame['cam7'], Frame['cam8']])
                # ------------------------------------------------------------------------------------------------------
                delete_background()
                # ------------------------------------------------------------------------------------------------------
                btm_mask_g, btm_mask_h, bottom_raw = concat_image_bottom()
                # ------------------------------------------------------------------------------------------------------
                # cv2.imwrite('/home/ast/Documents/CAM/cam5.jpg', Frame_del_background['cam5'][0])
                # cv2.imwrite('/home/ast/Documents/CAM/cam5_m.jpg', Frame_del_background['cam5'][1])
                # cv2.imwrite('/home/ast/Documents/CAM/cam6.jpg', Frame_del_background['cam6'][0])
                # cv2.imwrite('/home/ast/Documents/CAM/cam6_m.jpg', Frame_del_background['cam6'][1])
                # cv2.imwrite('/home/ast/Documents/CAM/cam7.jpg', Frame_del_background['cam7'][0])
                # cv2.imwrite('/home/ast/Documents/CAM/cam7_m.jpg', Frame_del_background['cam7'][1])
                # cv2.imwrite('/home/ast/Documents/CAM/cam8.jpg', Frame_del_background['cam8'][0])
                # cv2.imwrite('/home/ast/Documents/CAM/cam8_m.jpg', Frame_del_background['cam8'][1])
                # ------------------------------------------------------------------------------------------------------
                # tr = time.time() - ts
                # cv2.imwrite('/home/ast/Documents/CAM/mask_h.jpg', btm_mask_h)
                data[39] = 1
                # cv2.imwrite('/home/ast/Documents/CAM/mask_g.jpg', btm_mask_g)
                data[40] = 1
                # cv2.imwrite('/home/ast/Documents/CAM/btm9_raw.jpg', bottom_raw)
                data[41] = 1
                # ------------------------------------------------------------------------------------------------------
                # флаги готовности изображений с камер
                # data[31] = data[32] = data[33]= data[34] =data[35] = data[36] = data[37] = data[38] = True
                # флаг готовности обработанного и склеенного изображения
                # data[9] = 1
                # data[39] = data[40] = data[41] = 1
                # ------------------------------------------------------------------------------------------------------
                cprint("%s CREATE IMAGE: %s" % (cnt, (time.time() - t1).__round__(3)), 'blue')
                init = False
        except Exception as e:
            print('ERROR:', e)


def concat_image_bottom():
    try:
        y1a, y1b = Y_min['cam5'][0], Y_min['cam5'][-1]
        y2a, y2b = Y_min['cam6'][0], Y_min['cam6'][-1]
        y3a, y3b = Y_min['cam7'][0], Y_min['cam7'][-1]
        y4a, y4b = Y_min['cam8'][0], Y_min['cam8'][-1]
        # --------------------------------------------------------------------------------------------------------------
        # проверим есть ли доска на изображении
        if Y_min['cam5'] and any(Y_min['cam5']) == 0:
            cam1 = False
        else:
            cam1 = True

        if Y_min['cam6'] and any(Y_min['cam6']) == 0:
            cam2 = False
        else:
            cam2 = True

        if Y_min['cam7'] and any(Y_min['cam7']) == 0:
            cam3 = False
        else:
            cam3 = True

        if Y_min['cam8'] and any(Y_min['cam8']) == 0:
            cam4 = False
        else:
            cam4 = True
        # --------------------------------------------------------------------------------------------------------------
        # cam1 = cam2 = cam3 = cam4 = True
        dY = 0
        a_cam = []
        a_cam_mask = []
        a_cam_raw = []
        # --------------------------------------------------------------------------------------------------------------
        pos = 1
        a_cam.clear()
        if cam1:
            a_cam.append(Frame_del_background['cam5'][pos])
            a_cam_mask.append(Frame_del_background['cam5'][0])
            a_cam_raw.append(Frame['cam5'])
        # 1 - 2
        if cam2:
            dY = y1b - y2a
            a_cam.append(lineUp(Frame_del_background['cam6'][pos], dY))
            a_cam_mask.append(lineUp(Frame_del_background['cam6'][0], dY))
            a_cam_raw.append(lineUp(Frame['cam6'], dY))
        # 2 - 3
        if cam3:
            dY = y2b - y3a + dY
            a_cam.append(lineUp(Frame_del_background['cam7'][pos], dY))
            a_cam_mask.append(lineUp(Frame_del_background['cam7'][0], dY))
            a_cam_raw.append(lineUp(Frame['cam7'], dY))
        # 3 - 4
        if cam4:
            dY = y3b - y4a + dY
            a_cam.append(lineUp(Frame_del_background['cam8'][pos], dY))
            a_cam_mask.append(lineUp(Frame_del_background['cam8'][0], dY))
            a_cam_raw.append(lineUp(Frame['cam8'], dY))
        # --------------------------------------------------------------------------------------------------------------
        btm_image = cv2.hconcat(a_cam)
        btm_image_mask = cv2.hconcat(a_cam_mask)
        btm_image_raw = cv2.hconcat(a_cam_raw)
        # --------------------------------------------------------------------------------------------
        # иправим пробелы из-за рельс:
        yc = int((Y_min['cam5'][1] + Y_max['cam5'][1]) / 2)
        btm_image[yc:yc + 5, :] = 255
        btm_image_mask[yc:yc + 5, :] = 255
        # --------------------------------------------------------------------------------------------
        cv2.imwrite('/home/ast/Documents/CAM/mask_h.jpg', btm_image_mask)
        cv2.imwrite('/home/ast/Documents/CAM/mask_g.jpg', btm_image)
        cv2.imwrite('/home/ast/Documents/CAM/btm9_raw.jpg', btm_image_raw)
        # --------------------------------------------------------------------------------------------
        return btm_image_mask, btm_image, btm_image_raw

    except Exception as e:
        traceback.print_exc()
        print('concat_image:', e)


def lineUp(Frame, dY):
    """
    :param Frame: изображение которое нужно поднять или опустить
    :param dY:  на сколько нужно передвинуть изображение + - вверх, - - вниз
    :return: изображение
    """
    try:
        rows, cols = Frame.shape[0], Frame.shape[1]
        M = np.float32([[1, 0, 0], [0, 1, dY]])
        return cv2.warpAffine(Frame, M, (cols, rows))
    except:
        return Frame


def grab_image_from_server(filename):
    PATH_TO_CAM_IMAGE = '/home/ast/Documents/CAM_RAW/'
    try:
        key = filename[:-4]
        frame = cv2.imread(os.path.join(PATH_TO_CAM_IMAGE, filename))
        if frame is not None:
            frame = frame[:, Roi[key][2]:Roi[key][3]]
            Frame[key] = frame
            # saveIMG(frame)
        else:
            Frame['flag_err'] = True
            raise Exception('No image')
    except Exception as e:
        pass
        print('grab_image ', filename, ': ', e)


def grab_img_thread():
    try:
        tr1_b = Thread(target=grab_image_from_server, args=('cam5.jpg',))
        tr2_b = Thread(target=grab_image_from_server, args=('cam6.jpg',))
        tr3_b = Thread(target=grab_image_from_server, args=('cam7.jpg',))
        tr4_b = Thread(target=grab_image_from_server, args=('cam8.jpg',))
        tr5_b = Thread(target=grab_image_from_server, args=('cam5_b.jpg',))
        tr6_b = Thread(target=grab_image_from_server, args=('cam6_b.jpg',))
        tr7_b = Thread(target=grab_image_from_server, args=('cam7_b.jpg',))
        tr8_b = Thread(target=grab_image_from_server, args=('cam8_b.jpg',))

        tr1_b.start()
        tr2_b.start()
        tr3_b.start()
        tr4_b.start()
        tr5_b.start()
        tr6_b.start()
        tr7_b.start()
        tr8_b.start()

        tr1_b.join()
        tr2_b.join()
        tr3_b.join()
        tr4_b.join()
        tr5_b.join()
        tr6_b.join()
        tr7_b.join()
        tr8_b.join()
    except Exception as e:
        print('grab_img_thread: ', e)


def delete_background():
    #
    # tr5 = Thread(target=deleteBackground_v02, args=('cam5', 'cam5_b'))
    # tr6 = Thread(target=deleteBackground_v02, args=('cam6', 'cam6_b'))
    # tr7 = Thread(target=deleteBackground_v02, args=('cam7', 'cam7_b'))
    # tr8 = Thread(target=deleteBackground_v02, args=('cam8', 'cam8_b'))
    tr5 = Thread(target=deleteBackground_v03, args=('cam5', 'cam5_b'))
    tr6 = Thread(target=deleteBackground_v03, args=('cam6', 'cam6_b'))
    tr7 = Thread(target=deleteBackground_v03, args=('cam7', 'cam7_b'))
    tr8 = Thread(target=deleteBackground_v03, args=('cam8', 'cam8_b'))
    #
    tr5.start()
    tr6.start()
    tr7.start()
    tr8.start()
    #
    tr5.join()
    tr6.join()
    tr7.join()
    tr8.join()


# def deleteBackground_v02(key_original_image, key_background_image):
#     """
#     Функиция позволяет сделать сравнение 2 изображений для удаления фона путем создания маски
#     """
#     try:
#         original_image = Frame[key_original_image].copy()
#         background_image = Frame[key_background_image].copy()
#
#         key = key_original_image
#
#         if original_image is not None and background_image is not None:
#             gray_original = cv2.cvtColor(original_image, cv2.COLOR_BGR2GRAY)
#             gray_background = cv2.cvtColor(background_image, cv2.COLOR_BGR2GRAY)
#
#             original = cv2.medianBlur(gray_original, 17)
#             background = cv2.medianBlur(gray_background, 17)
#             dlt = 40
#             white_line_3 = np.linspace([0, 0, 0], 0, original.shape[1], dtype=np.uint8)
#             black_line_1 = np.linspace(0, 0, original.shape[1], dtype=np.uint8)
#             # на выходе: маска, изображение
#             mask, orig_img = applyMask_v02(original, background, original_image, dlt,
#                                            white_line_3, black_line_1)
#             Frame_del_background[key] = [mask, orig_img]
#         else:
#             print('no image', key_original_image)
#             # Frame_del_bachground[key] = [np.zeros([20, 20], dtype=np.uint8), np.zeros([20, 20], dtype=np.uint8)]
#     except Exception as e:
#         print('deleteBackground_v02: ', e)
#         print(key_original_image, key_background_image)
#
#
# @numba.jit
# def applyMask_v02(t1, t2, original_image, dlt, white_line, black_line):
#     """
#     :param t1: gray original image
#     :param t2: gray background image
#     :param original_image: origimal image
#     :param dlt: delta
#     :return:
#     """
#     mask = t1
#     for i in range(t1.shape[0]):
#         cnt = 0
#         for j in range(t1.shape[1]):
#             if (t1[i][j] <= t2[i][j] + dlt) and (t1[i][j] >= t2[i][j] - dlt):
#                 mask[i][j] = 0
#                 original_image[i][j] = [0, 0, 0]
#             else:
#                 mask[i][j] = 255
#                 cnt += 1
#         if cnt < 0.20 * t1.shape[1]:
#             mask[i] = black_line
#             original_image[i] = white_line
#
#     return mask, original_image
#

def deleteBackground_v03(key_original_image, key_background_image):
    """
    Функиция позволяет сделать сравнение 2 изображений для удаления фона путем создания маски
    """
    try:
        delta_gray = 40
        delta_hsv = 50

        key = key_original_image
        original_image = Frame[key_original_image].copy()
        background_image = Frame[key_background_image].copy()
        # ---------------------------------------------------------------------------------
        gray_original = cv2.cvtColor(original_image.copy(), cv2.COLOR_BGR2GRAY)
        gray_background = cv2.cvtColor(background_image.copy(), cv2.COLOR_BGR2GRAY)

        hsv_original = cv2.cvtColor(original_image.copy(), cv2.COLOR_BGR2HSV)
        hsv_background = cv2.cvtColor(background_image.copy(), cv2.COLOR_BGR2HSV)
        gray_hsv_original = cv2.cvtColor(hsv_original, cv2.COLOR_BGR2GRAY)
        gray_hsv_background = cv2.cvtColor(hsv_background, cv2.COLOR_BGR2GRAY)
        # ---------------------------------------------------------------------------------
        original = cv2.medianBlur(gray_original, 11)
        background = cv2.medianBlur(gray_background, 11)
        original_hsv = cv2.medianBlur(gray_hsv_original, 11)
        background_hsv = cv2.medianBlur(gray_hsv_background, 11)
        # ---------------------------------------------------------------------------------
        msk1 = original >= background + delta_gray
        msk2 = original_hsv >= background_hsv + delta_gray
        msk1b = original < background + delta_hsv
        msk2b = original_hsv < background_hsv + delta_hsv
        # ---------------------------------------------------------------------------------
        original[msk1] = 255
        original_hsv[msk2] = 255
        original[msk1b] = 0
        original = _filter(original)
        original_hsv[msk2b] = 0
        original_hsv = _filter(original_hsv)
        msk3 = original_hsv != original
        original_hsv[msk3] = 255
        # ---------------------------------------------------------------------------------
        # получим параметры изображения (доски) для соединения изображений и/или геометрических параметров доски:
        # ширина, площадь, длинна окружной линии и т.д.
        Y_min[key], Y_max[key], W_board[key] = find_concat_param(original_hsv)
        # ---------------------------------------------------------------------------------
        Frame_del_background[key] = [original, original_hsv]
        # ---------------------------------------------------------------------------------

    except Exception as e:
        print('ERR delete background', e)
        Y_min[key], Y_max[key], W_board[key] = 0, 0, 0


def _filter(mask):
    """
    фильтр помех при удалении фона
    :param mask:
    :return:
    """
    black_line = np.linspace(0, 0, mask.shape[1])
    # для каждой продолной линии:
    for i in range(mask.shape[0]):
        # если колличество вхождений 0 (черного цвета) больше заданного, то стираем
        if np.bincount(mask[i])[0] > 0.8 * mask.shape[1]:
            mask[i] = black_line
    return mask


def find_concat_param(image):
    """
    :param image: подаем на вход бинарную маску
    """
    fr_copy = image.copy()
    k = 3
    y, x = image.shape
    frame = cv2.resize(fr_copy, (int(x / k), int(y / k)), interpolation=cv2.INTER_LINEAR)
    Ymin = []
    Ymax = []
    W = []
    # зададим точки в которых хотим делать проверку
    X = [0.0, 0.5, 0.97]
    # X = np.linspace(0, 0.95, 11)
    roi_hsv = frame
    for i in range(len(X)):
        y_min = 0
        y_max = frame.shape[0]

        x_00 = int(roi_hsv.shape[1] * X[i])
        x_01 = int(roi_hsv.shape[1] * (X[i] + 0.02))

        roi = roi_hsv[:, x_00:x_01]
        # mask = cv2.inRange(roi, np.array([0, 35, 90]), np.array([30, 200, 255]))
        mask = roi
        mask = cv2.medianBlur(mask, 5)

        # cv2.imshow('mask', mask)
        # cv2.waitKey()
        # cv2.destroyAllWindows()

        white_line = list(np.linspace(255, 255, roi.shape[1], dtype=np.int16))
        black_line = list(np.linspace(0, 0, roi.shape[1], dtype=np.int16))
        for y in range(roi.shape[0]):
            # if all(mask[y] == white_line) and y_min == 0:
            if all(mask[y] != black_line) and y_min == 0:
                y_min = y
            elif all(mask[y] != white_line) and y_min != 0 and y_max == frame.shape[0]:
                y_max = y
                # если все нашли, то выходим из цикла
                break
        Ymin.append(y_min * k)
        Ymax.append(y_max * k)
        W.append(y_max * k - y_min * k)

    return Ymin, Ymax, W


# =====================================================================================================================
# =====================================================================================================================
# =====================================================================================================================
def simulation(data):
    cnt = 0
    while True:
        cnt += 1
        ts = time.time()
        # сенсор для главного сервера
        data[0] = 1
        # старт камер
        data[15] = data[16] = data[17] = data[18] = 1
        # цикл работы в секундах
        time.sleep(1)
        print(cnt, ': SIMULATION, ', time.time() - ts)

# =====================================================================================================================
if __name__ == '__main__':

    print('*************************************************************************')
    print('********                 START PROGRAM                           ********')
    print('*************************************************************************')
# ----------------------------------------------------------------------------------------------------------------------
    try:
        flag_process[2] = 1
        # --------------------------------------------------------------------------------------------------------------
        # cam1 = Process(target=Cam, args=(RTSP['cam1'], data, CAM_PATH_1, _CAM_PATH_1, 4000, 4010, flag_process))
        # cam2 = Process(target=Cam, args=(RTSP['cam2'], data, CAM_PATH_2, _CAM_PATH_2, 4001, 4011, flag_process))
        # cam3 = Process(target=Cam, args=(RTSP['cam3'], data, CAM_PATH_3, _CAM_PATH_3, 4002, 4012, flag_process))
        # cam4 = Process(target=Cam, args=(RTSP['cam4'], data, CAM_PATH_4, _CAM_PATH_4, 4003, 4013, flag_process))
        cam5 = Process(target=Cam, args=(RTSP['cam5'], data, CAM_PATH_5, _CAM_PATH_5, 4004, 4014, flag_process))
        cam6 = Process(target=Cam, args=(RTSP['cam6'], data, CAM_PATH_6, _CAM_PATH_6, 4005, 4015, flag_process))
        cam7 = Process(target=Cam, args=(RTSP['cam7'], data, CAM_PATH_7, _CAM_PATH_7, 4006, 4016, flag_process))
        cam8 = Process(target=Cam, args=(RTSP['cam8'], data, CAM_PATH_8, _CAM_PATH_8, 4007, 4017, flag_process))
        # --------------------------------------------------------------------------------------------------------------
        sim = Process(target=simulation, args=(data,))
        # --------------------------------------------------------------------------------------------------------------
        _tcp = Process(target=tcpClient, args=(data, flag_process))
        client_tcp = Process(target=client_tcp, args=(data,))
        # --------------------------------------------------------------------------------------------------------------
        CREAT_IMG = Process(target=CREATE_IMAGE, args=(data,))
        # ====================================
        # cam1.start()
        # cam2.start()
        # cam3.start()
        # cam4.start()
        cam5.start()
        cam6.start()
        cam7.start()
        cam8.start()
        # --------------------------------------------------------------------------------------------------------------
        time.sleep(5)
        print('READY TO WORK')
        # --------------------------------------------------------------------------------------------------------------
        flag_process[2] = 0
        flag_process[3] = 1
        # --------------------------------------------------------------------------------------------------------------
        # sim.start()
        client_tcp.start()
        _tcp.start()
        CREAT_IMG.start()
        # ====================================
        # cam1.join()
        # cam2.join()
        # cam3.join()
        # cam4.join()
        cam5.join()
        cam6.join()
        cam7.join()
        cam8.join()
        _tcp.join()
        # sim.join()
        client_tcp.join()
        # ====================================
        flag_process[3] = 0
        os.system('killall -s 9 python3')
    finally:
        print('FINNALY MAIN')
        os.system('killall -s 9 python3')
