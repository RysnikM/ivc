import tkinter as tk
from PIL import ImageTk, Image
import os
import paramiko
import time
from multiprocessing import Process
from threading import Thread
import subprocess as sp
import socket

# База состоит из следующих полей: сервер1и2 - запущен/остановлен, запускается, перезапускается
# ТСР1и2 - вкл/выкл
# База данных и FTP - вкл/выкл
# Камеры1-8 - вкл/выкл
# Подготовка изображения и нейронка - вкл/выкл
#
Server1Run, Server2Run, Server1Started, Server2Started, Server1Restarted, Server2Restarted = 0, 0, 0, 0, 0, 0
sTCP1, sTCP2, sDB, sFTP, sCrImg, sNet = 0, 0, 0, 0, 0, 0
sCam1, sCam2, sCam3, sCam4, sCam5, sCam6, sCam7, sCam8 = 0, 0, 0, 0, 0, 0, 0, 0
timerStarted1, timerStarted2 = 0, 0
stTimerStarted1, stTimerStarted2 = 0, 0
stTimerRun1 = stTimerRun2 = 0
timerRun1 = timerRun2 = time.time()


def main():
    def connect_to_host(ip, user, password, port):
        try:
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(hostname=ip, username=user, password=password, port=port, timeout=2)
            # print('Connected to host', ip)
            ssh.close()
            return True
        except Exception as e:
            # print("Authentication failed when connecting to", ip)
            # print('Host marked as bad.')
            return False

    def connect_to_hostPING(ip):
        ip_v4 = ip
        status, result = sp.getstatusoutput("ping -c1 -w2 " + ip_v4)

        if status == 0:
            return True
        else:
            return False

    def command_to_ssh(ip, user, password, port, command):
        try:
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(hostname=ip, username=user, password=password, port=port, timeout=2)
            # stdin, stdout, stderr = ssh.exec_command(command)
            # for line in stdout.readlines():
            #     print(line)
            # # print('Connected to host', ip)
            ssh.exec_command(command)
            # time.sleep(1)
            # ssh.close()

        except Exception as e:
            print(e)
        # print('Host marked as bad.')

    c_folder = os.getcwd()
    path1 = c_folder + '/on.png'
    path2 = c_folder + '/off.png'
    path3 = c_folder + '/warnDB.png'
    myRed = '#FF4444'
    myGreen = '#92D8BC'
    myGrey = '#D9D9D9'
    mySeparate = '#C0C0C0'

    enSshServer1 = False
    enSshServer2 = False

    sCrImg, sNet = 0, 0

    window = tk.Tk()

    offPNG = ImageTk.PhotoImage(Image.open(path2))
    onPNG = ImageTk.PhotoImage(Image.open(path1))
    warnDB = ImageTk.PhotoImage(Image.open(path3))

    def clc_btnServer1():
        global Server1Started, Server1Run, Server1Restarted
        if Server1Run:
            command_to_ssh('192.168.0.40', 'ast', 'admin', 22, 'nohup killall -s9 python3')
            Server1Run = 0
            Server1Started = 0
            Server1Restarted = 0

        else:
            command_to_ssh('192.168.0.40', 'ast', 'admin', 22, 'nohup python3 /home/ast/Desktop/GIT/PYTHON/RES.py & '
                                                               'disown')
            Server1Started = 1
            Server1Restarted = 0
            Server1Run = 0

    def clc_btnServerReset1():
        global Server1Restarted, Server1Started, Server1Run
        command_to_ssh('192.168.0.40', 'ast', 'admin', 22, 'sudo reboot')
        Server1Restarted = 1
        Server1Started = 0
        Server1Run = 0

    def clc_btnServer2():
        global Server2Started, Server2Run, Server2Restarted
        if Server2Run:
            command_to_ssh('192.168.0.121', 'ast', 'z1x2c3v4b5', 22, 'nohup killall -s9 python3')
            Server2Run = 0
            # print('reset3')
            Server2Started = 0
            Server2Restarted = 0

        else:
            command_to_ssh('192.168.0.121', 'ast', 'z1x2c3v4b5', 22, 'nohup python3 /home/ast/Desktop/ServClient.py & '
                                                                     'disown')
            Server2Started = 1
            Server2Restarted = 0
            Server2Run = 0

    def clc_btnServerReset2():
        global Server2Restarted, Server2Started, Server2Run
        command_to_ssh('192.168.0.121', 'ast', 'z1x2c3v4b5', 22, 'sudo reboot')
        Server2Restarted = 1
        Server2Started = 0
        Server2Run = 0

    def clc_bntVisualisation():
        os.popen(c_folder + '/descktopApp/nw')

    window.resizable(False, False)
    window.title('Окно управления MachineVision')
    window.geometry('380x600')

    # Server1
    frameServer1 = tk.Frame(window)
    frameServer1 = tk.LabelFrame(text='Сервер 1', font=("Montserrat", 13))
    frameServer1.pack(pady=5, padx=5, fill=tk.X)
    frameBtnS1 = tk.Frame(frameServer1)
    frameBtnS1.pack()
    btnServer1 = tk.Button(frameBtnS1, text='Запустить',
                           bg=myRed, fg='white',
                           font=("Montserrat", 14),
                           border="0", command=clc_btnServer1,
                           width='13', state=tk.DISABLED,
                           activebackground=myGreen, activeforeground='black')
    btnServer1.pack(side=tk.LEFT, padx=3, pady=3)
    btnServerReset1 = tk.Button(frameBtnS1, text='Перезагрузить',
                                bg=myGrey, fg='black',
                                font=("Montserrat", 14),
                                border="0", command=clc_btnServerReset1,
                                width='13', state=tk.DISABLED,
                                activebackground=myGrey, activeforeground='white'
                                )
    btnServerReset1.pack(side=tk.RIGHT, padx=3, pady=3)

    # Server2
    frameServer2 = tk.Frame(window)
    frameServer2 = tk.LabelFrame(text='Сервер 2', font=("Montserrat", 13))
    frameServer2.pack(pady=5, padx=5, fill=tk.X)
    btnServer2 = tk.Button(frameServer2, text='Запустить',
                           bg=myRed, fg='white',
                           font=("Montserrat", 14),
                           border="0", command=clc_btnServer2,
                           width='13', state=tk.DISABLED,
                           activebackground=myGreen, activeforeground='black')
    btnServer2.pack(side=tk.LEFT, padx=3, pady=3)
    btnServerReset2 = tk.Button(frameServer2, text='Перезагрузить',
                                bg=myGrey, fg='black',
                                font=("Montserrat", 14),
                                border="0", command=clc_btnServerReset2,
                                width='13', state=tk.DISABLED,
                                activebackground=myGrey, activeforeground='white'
                                )
    btnServerReset2.pack(side=tk.RIGHT, padx=3, pady=3)

    # Control process
    frameControl = tk.Frame(window)
    frameControl = tk.LabelFrame(text='Контроль выполняемых процессов',
                                 font=("Montserrat", 13))
    frameControl.pack(pady=2, padx=5, fill=tk.X)

    # TCP server 1 and 2
    frameTCP = tk.Frame(frameControl)
    frameTCP.pack(anchor=tk.NW, fill=tk.X, padx=15, pady=1)

    txtTCP1 = tk.Label(frameTCP, text='TCP сервер1', font=("Montserrat", 12))
    txtTCP1.pack(side=tk.LEFT, padx=6)
    imgTCP1 = tk.Label(frameTCP, image=offPNG,
                       width='40', height='40')
    imgTCP1.pack(side=tk.LEFT, padx=4)
    imgTCP2 = tk.Label(frameTCP, image=offPNG,
                       width='40', height='40')
    imgTCP2.pack(side=tk.RIGHT, padx=5)
    txtTCP2 = tk.Label(frameTCP, text='TCP сервер2', font=("Montserrat", 12))
    txtTCP2.pack(side=tk.RIGHT, padx=5)
    #
    frSeparator1 = tk.Frame(frameControl)
    frSeparator1.pack(anchor=tk.NW)
    lineSeparator1 = tk.Canvas(frSeparator1, height=3)
    lineSeparator1.create_line(12, 2, 350, 2, fill=mySeparate)
    lineSeparator1.pack()
    #  DB and FTP

    frameDbFtp = tk.Frame(frameControl)
    frameDbFtp.pack(anchor=tk.NW, fill=tk.X, padx=15, pady=0)
    txtDB = tk.Label(frameDbFtp, text='База данных', font=("Montserrat", 12))
    txtDB.pack(side=tk.LEFT, padx=4)
    imgDB = tk.Label(frameDbFtp, image=offPNG,
                     width='40', height='40')
    imgDB.pack(side=tk.LEFT, padx=5)
    imgFTP = tk.Label(frameDbFtp, image=offPNG,
                      width='40', height='40')
    imgFTP.pack(side=tk.RIGHT, padx=5)
    txtFTP = tk.Label(frameDbFtp, text='FTP сервер', font=("Montserrat", 12))
    txtFTP.pack(side=tk.RIGHT, padx=5)
    #
    frSeparator2 = tk.Frame(frameControl)
    frSeparator2.pack(anchor=tk.NW)
    lineSeparator2 = tk.Canvas(frSeparator2, height=3)
    lineSeparator2.create_line(12, 2, 350, 2, fill=mySeparate)
    lineSeparator2.pack()

    # Frame cam 1 5
    frameCam1 = tk.Frame(frameControl)
    frameCam1.pack(anchor=tk.NW, fill=tk.X, padx=15)

    txtCam1 = tk.Label(frameCam1, text='Камера 1', font=("Montserrat", 12))
    txtCam1.pack(side=tk.LEFT, padx=19)
    imgCam1 = tk.Label(frameCam1, image=offPNG,
                       width='40', height='40')
    imgCam1.pack(side=tk.LEFT, padx=5)

    imgCam5 = tk.Label(frameCam1, image=offPNG,
                       width='40', height='40')
    imgCam5.pack(side=tk.RIGHT, padx=5)
    txtCam5 = tk.Label(frameCam1, text='Камера 5', font=("Montserrat", 12))
    txtCam5.pack(side=tk.RIGHT, padx=4)

    # Frame cam 2 6
    frameCam2 = tk.Frame(frameControl)
    frameCam2.pack(anchor=tk.NW, fill=tk.X, padx=15)
    txtCam2 = tk.Label(frameCam2, text='Камера 2', font=("Montserrat", 12))
    txtCam2.pack(side=tk.LEFT, padx=19)
    imgCam2 = tk.Label(frameCam2, image=offPNG,
                       width='40', height='40')
    imgCam2.pack(side=tk.LEFT, padx=5)

    imgCam6 = tk.Label(frameCam2, image=offPNG,
                       width='40', height='40')
    imgCam6.pack(side=tk.RIGHT, padx=5)
    txtCam6 = tk.Label(frameCam2, text='Камера 6', font=("Montserrat", 12))
    txtCam6.pack(side=tk.RIGHT, padx=4)

    # Frame cam 3 7
    frameCam3 = tk.Frame(frameControl)
    frameCam3.pack(anchor=tk.NW, fill=tk.X, padx=15)
    txtCam3 = tk.Label(frameCam3, text='Камера 3', font=("Montserrat", 12))
    txtCam3.pack(side=tk.LEFT, padx=19)
    imgCam3 = tk.Label(frameCam3, image=offPNG,
                       width='40', height='40')
    imgCam3.pack(side=tk.LEFT, padx=5)
    imgCam7 = tk.Label(frameCam3, image=offPNG,
                       width='40', height='40')
    imgCam7.pack(side=tk.RIGHT, padx=5)
    txtCam7 = tk.Label(frameCam3, text='Камера 7', font=("Montserrat", 12))
    txtCam7.pack(side=tk.RIGHT, padx=4)

    # Frame cam 4 8
    frameCam4 = tk.Frame(frameControl)
    frameCam4.pack(anchor=tk.NW, fill=tk.X, padx=15)
    txtCam4 = tk.Label(frameCam4, text='Камера 4', font=("Montserrat", 12))
    txtCam4.pack(side=tk.LEFT, padx=19)
    imgCam4 = tk.Label(frameCam4, image=offPNG,
                       width='40', height='40')
    imgCam4.pack(side=tk.LEFT, padx=5)
    imgCam8 = tk.Label(frameCam4, image=offPNG,
                       width='40', height='40')
    imgCam8.pack(side=tk.RIGHT, padx=5)
    txtCam8 = tk.Label(frameCam4, text='Камера 8', font=("Montserrat", 12))
    txtCam8.pack(side=tk.RIGHT, padx=4)
    #
    #
    frSeparator3 = tk.Frame(frameControl)
    frSeparator3.pack(anchor=tk.NW)
    lineSeparator3 = tk.Canvas(frSeparator3, height=3)
    lineSeparator3.create_line(12, 2, 350, 2, fill=mySeparate)
    lineSeparator3.pack()

    # Frame Create Image
    frameCrImg = tk.Frame(frameControl)
    frameCrImg.pack(anchor=tk.NW, fill=tk.X, padx=15, pady=0)
    imgCrImg = tk.Label(frameCrImg, image=offPNG,
                        width='40', height='40')
    imgCrImg.pack(side=tk.RIGHT, padx=5)
    txtCrImg = tk.Label(frameCrImg, text='Подготовка изображения', font=("Montserrat", 12))
    txtCrImg.pack(anchor=tk.E, side=tk.RIGHT, padx=5)
    #
    frSeparator4 = tk.Frame(frameControl)
    frSeparator4.pack(anchor=tk.NW)
    lineSeparator4 = tk.Canvas(frSeparator4, height=3)
    lineSeparator4.create_line(12, 2, 350, 2, fill=mySeparate)
    lineSeparator4.pack()

    # Frame Net
    frameNET = tk.Frame(frameControl)
    frameNET.pack(anchor=tk.NW, fill=tk.X, padx=15, pady=0)
    imgNET = tk.Label(frameNET, image=offPNG,
                      width='40', height='40')
    imgNET.pack(side=tk.RIGHT, padx=5)
    txtNET = tk.Label(frameNET, text='Нейронная сеть', font=("Montserrat", 12))
    txtNET.pack(anchor=tk.E, side=tk.RIGHT, padx=5)

    # StartVisualisation
    btnVisualisation = tk.Button(window, text='Запустить визуализацию',
                                 bg=myGreen, fg='black',
                                 font=("Montserrat", 18), height='2',
                                 border=0, command=clc_bntVisualisation, state=tk.DISABLED,
                                 activebackground=myGreen, activeforeground='white')
    btnVisualisation.pack(side=tk.BOTTOM, fill=tk.X, padx=5, pady=5)

    def checkEntry():
        global Server1Run, Server2Run, Server1Started, Server2Started, Server1Restarted, Server2Restarted, \
            sTCP1, sTCP2, sDB, sFTP, sCam1, sCam2, sCam3, sCam4, sCam5, sCam6, sCam7, sCam8, enSshServer1, enSshServer2, \
            sNet, sCrImg, timerStarted1, timerStarted2, stTimerStarted1, stTimerStarted2, stTimerRun2, timerRun2, stTimerRun1, timerRun1


        enSshServer1 = connect_to_host('192.168.0.40', 'ast', 'admin', 22)
        enSshServer2 = connect_to_host('192.168.0.121', 'ast', 'z1x2c3v4b5', 22)
        #
        # enSshServer1 = connect_to_hostPING('192.168.0.40')
        # enSshServer2 = connect_to_hostPING('192.168.0.121')

        # print(time.time() - timerRun2, '  ', stTimerRun2)
        if stTimerRun2 and (time.time() - timerRun2 > 15):
            stTimerRun2 = 0
            Server2Run = 0
        # print(time.time() - timerRun1, '  ', stTimerRun1)
        if stTimerRun1 and (time.time() - timerRun1 > 15):
            stTimerRun1 = 0
            Server1Run = 0
        #     Dinamic text
        if Server1Restarted:
            frameServer1.configure(text='Сервер 1 ...... перезапуск')
            btnServer1.configure(state=tk.DISABLED)
            btnServerReset1.configure(state=tk.DISABLED)
        else:
            btnServerReset1.configure(state=tk.NORMAL)

        if Server1Started and enSshServer1:
            frameServer1.configure(text='Сервер 1 ...... запуск')
            btnServer1.configure(state=tk.DISABLED)
        else:
            btnServer1.configure(state=tk.NORMAL)

        if Server1Run and enSshServer1:
            Server1Started = 0

        if Server1Restarted == 0 and Server1Started == 0 and enSshServer1:
            frameServer1.configure(text='Сервер 1')
            if Server1Run:
                btnServer1.configure(bg=myGreen, fg='black', text='Остановить', activebackground=myRed,
                                     activeforeground='white')
            else:
                btnServer1.configure(bg=myRed, fg='white', text='Запустить', activebackground=myGreen,
                                     activeforeground='black')
                sTCP1 = 0
                sCam1 = 0
                sCam2 = 0
                sCam3 = 0
                sCam4 = 0
                sFTP = 0
                sDB = 0
                sCrImg = 0
                sNet = 0

        if Server2Restarted:
            frameServer2.configure(text='Сервер 2 ...... перезапуск')
            btnServer2.configure(state=tk.DISABLED)
            btnServerReset2.configure(state=tk.DISABLED)
        else:
            btnServerReset2.configure(state=tk.NORMAL)

        if Server2Started and enSshServer2:
            frameServer2.configure(text='Сервер 2 ...... запуск')
            btnServer2.configure(state=tk.DISABLED)
        else:
            btnServer2.configure(state=tk.NORMAL)

        if Server2Run and enSshServer2:
            Server2Started = 0

        if Server2Restarted == 0 and Server2Started == 0 and enSshServer2:
            frameServer2.configure(text='Сервер 2')
            if Server2Run:
                btnServer2.configure(bg=myGreen, fg='black', text='Остановить', activebackground=myRed,
                                     activeforeground='white')
            else:
                btnServer2.configure(bg=myRed, fg='white', text='Запустить', activebackground=myGreen,
                                     activeforeground='black')
                sTCP2 = 0
                sCam5 = 0
                sCam6 = 0
                sCam7 = 0
                sCam8 = 0

        if enSshServer1:
            Server1Restarted = 0
            btnVisualisation.configure(state=tk.NORMAL)
            if sCam1:
                imgCam1.configure(image=onPNG)
            else:
                imgCam1.configure(image=offPNG)

            if sCam2:
                imgCam2.configure(image=onPNG)
            else:
                imgCam2.configure(image=offPNG)

            if sCam2:
                imgCam2.configure(image=onPNG)
            else:
                imgCam2.configure(image=offPNG)

            if sCam3:
                imgCam3.configure(image=onPNG)
            else:
                imgCam3.configure(image=offPNG)

            if sCam4:
                imgCam4.configure(image=onPNG)
            else:
                imgCam4.configure(image=offPNG)

            if sCrImg:
                imgCrImg.configure(image=onPNG)
            else:
                imgCrImg.configure(image=offPNG)

            if sDB:
                imgDB.configure(image=onPNG)
            else:
                imgDB.configure(image=offPNG)

            if sFTP:
                imgFTP.configure(image=onPNG)
            else:
                imgFTP.configure(image=offPNG)

            if sNet:
                imgNET.configure(image=onPNG)
            else:
                imgNET.configure(image=offPNG)

            if sTCP1:
                imgTCP1.configure(image=onPNG)
            else:
                imgTCP1.configure(image=offPNG)

        else:
            if Server1Restarted == 0:
                frameServer1.configure(text='Сервер 1 недоступен')
            Server1Run = 0
            btnServer1.configure(state=tk.DISABLED)
            btnVisualisation.configure(state=tk.DISABLED)
            btnServerReset1.configure(state=tk.DISABLED)

            imgDB.configure(image=warnDB)
            imgDB.image = warnDB
            imgTCP1.configure(image=warnDB)
            imgTCP1.image = warnDB
            imgCam1.configure(image=warnDB)
            imgCam1.image = warnDB
            imgCam2.configure(image=warnDB)
            imgCam2.image = warnDB
            imgCam3.configure(image=warnDB)
            imgCam3.image = warnDB
            imgCam4.configure(image=warnDB)
            imgCam4.image = warnDB
            imgFTP.configure(image=warnDB)
            imgFTP.image = warnDB
            imgNET.configure(image=warnDB)
            imgNET.image = warnDB
            imgCrImg.configure(image=warnDB)
            imgCrImg.image = warnDB

        if enSshServer2:
            Server2Restarted = 0
            if sTCP2:
                imgTCP2.configure(image=onPNG)
            else:
                imgTCP2.configure(image=offPNG)

            if sCam5:
                imgCam5.configure(image=onPNG)
            else:
                imgCam5.configure(image=offPNG)

            if sCam6:
                imgCam6.configure(image=onPNG)
            else:
                imgCam6.configure(image=offPNG)

            if sCam7:
                imgCam7.configure(image=onPNG)
            else:
                imgCam7.configure(image=offPNG)

            if sCam8:
                imgCam8.configure(image=onPNG)
            else:
                imgCam8.configure(image=offPNG)

        else:
            if Server2Restarted == 0:
                frameServer2.configure(text='Сервер 2 недоступен')
            Server2Run = 0
            btnServer2.configure(state=tk.DISABLED)
            btnServerReset2.configure(state=tk.DISABLED)
            imgTCP2.configure(image=warnDB)
            imgTCP2.image = warnDB
            imgCam5.configure(image=warnDB)
            imgCam5.image = warnDB
            imgCam6.configure(image=warnDB)
            imgCam6.image = warnDB
            imgCam7.configure(image=warnDB)
            imgCam7.image = warnDB
            imgCam8.configure(image=warnDB)
            imgCam8.image = warnDB

        # waiting timers
        if Server1Started and stTimerStarted1 == 0:
            stTimerStarted1 = 1
            timerStarted1 = time.time()
        if stTimerStarted1 and (time.time() - timerStarted1 > 20):
            # print('reset2')
            Server1Started = 0
            stTimerStarted1 = 0
        if Server2Started and stTimerStarted2 == 0:
            stTimerStarted2 = 1
            timerStarted2 = time.time()
        if stTimerStarted2 and (time.time() - timerStarted2 > 20):
            # print('reset1', time.time() - timerStarted1)
            Server2Started = 0
            stTimerStarted2 = 0

        window.after(500, checkEntry)

    window.after(500, checkEntry)

    imgicon = tk.PhotoImage(file=c_folder + '/iconF.gif')
    window.tk.call('wm', 'iconphoto', window._w, imgicon)

    window.mainloop()


def serv_tcp_s2(port):
    global sCam1, sCam2, sCam3, sCam4, sCam5, sCam6, sCam7, sCam8, sTCP2, Server2Run, stTimerRun2, timerRun2
    # time.sleep(1)
    # sCam5 = 1
    # time.sleep(1)
    # sCam5 = 0

    host = '0.0.0.0'
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    s.listen()
    # s.settimeout(3)
    print('wait connect SERVER 2')
    while True:

        try:
            if True:
                conn, addr = s.accept()
                dt = None
                while True:
                    dt = conn.recv(1024)
                    if dt:
                        message = dt
                    if not dt:
                        # pars data
                        sCam5 = 1 if message[5] == 1 else 0
                        sCam6 = 1 if message[6] == 1 else 0
                        sCam7 = 1 if message[7] == 1 else 0
                        sCam8 = 1 if message[8] == 1 else 0
                        sTCP2 = 1 if message[1] == 1 else 0

                        Server2Run = 1 if message[3] == 1 else 0

                        if Server2Run:
                            stTimerRun2 = 1
                            timerRun2 = time.time()

                        print('Server2 - 121: ', message)
                        break
        except Exception as e:
            print('ERROR no connect to server client: %s' % e)
            # time.sleep(5)



def serv_tcp_s1(port):
    global sCam1, sCam2, sCam3, sCam4, sTCP1, Server1Run, stTimerRun1, timerRun1, sFTP, sDB, sCrImg, sNet
    # time.sleep(1)
    # sCam5 = 1
    # time.sleep(1)
    # sCam5 = 0


    host = '0.0.0.0'
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    s.listen()
    # s.settimeout(3)
    print('wait connect SERVER 1')
    while True:

        try:
            if True:
                conn, addr = s.accept()
                dt = None
                while True:
                    dt = conn.recv(1024)
                    if dt:
                        message = dt
                    if not dt:
                        # pars data
                        sCam1 = 1 if message[1] == 1 else 0
                        sCam2 = 1 if message[2] == 1 else 0
                        sCam3 = 1 if message[3] == 1 else 0
                        sCam4 = 1 if message[4] == 1 else 0
                        sTCP1 = 1 if message[5] == 1 else 0
                        sDB = 1 if message[6] == 1 else 0
                        sNet = 1 if message[7] == 1 else 0
                        Server1Run = 1 if message[8] == 1 else 0
                        sFTP = 1 if message[9] == 1 else 0
                        sCrImg = 1 if message[10] == 1 else 0
                        # reset timer RunServer if connection established and server1Run and isMassage
                        if Server1Run:
                            stTimerRun1 = 1
                            timerRun1 = time.time()

                        print('Server1 -  40: ', message)
                        break
        except Exception as e:
            print('ERROR no connect to server client: %s' % e)
            # time.sleep(5)


if __name__ == '__main__':
    p1 = Thread(target=main)
    p2 = Thread(target=serv_tcp_s2, args=(20121,))
    p3 = Thread(target=serv_tcp_s1, args=(20040,))
    p1.start()
    p2.start()
    p3.start()
    p1.join()

    os.system('killall -s9 python3')
