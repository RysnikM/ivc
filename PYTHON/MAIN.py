# =====================================================================================================================
# Release 0.8
#
#
# =====================================================================================================================
from __future__ import with_statement

import datetime
import threading
import logging
import time
import uuid

from MODULS.cam import camPreviewFromObject, creatCam, URL, createImgSensorTopButtom, FrameTop, FrameBottom
from MODULS.TCP import MB_SIEMENS
import MODULS.TCP as TCP
from MODULS.PGsqlConnect import rw_data_base
from NET.NET_from_image import object_detection
import MODULS.IMPORT as IMPORT


# =====================================================================================================================
# CREATE LOGS
# logging.basicConfig(format=u'%(levelname)-8s [%(asctime)s] %(message)s', level=logging.DEBUG, filename='log.log')
# logging.basicConfig(format=u'%(levelname)-8s [%(asctime)s] %(message)s', level=logging.ERROR, filename='log.log')
# logging.basicConfig(format=u'%(levelname)-8s [%(asctime)s] %(message)s', level=logging.WARNING, filename='log.log')
# logging.basicConfig(format=u'%(levelname)-8s [%(asctime)s] %(message)s', level=logging.INFO, filename='log_info')


# =====================================================================================================================
# CREATE VAR
mes = IMPORT.COLOR_TEXT_PRINT_MESS3

DATA = {}

# =====================================================================================================================
# FUNCTIONS
def guard_dog():
    mac_list = [
        0xac1f6bd1188e,
        0xac1f6bd1188f,
        0xac1f6ba5e20e,
        0xac1f6ba5e20f
    ]
    res = []
    main_mac = uuid.getnode()
    for mac in mac_list:
        res.append(main_mac == mac)

    if not any(res):
        raise NameError("You mother fucker, use chrack programm, will fuck you")
    else:
        print('check_license - OK', datetime.datetime.now().time())
# =====================================================================================================================
# MAIN
def main(val, data, tcp):
    try:
        guard_dog()
        # =============================================================
        # time.sleep(5)
        # прототипы потоков ===========================================
        # cam_1
        cam1 = threading.Thread(target=camPreviewFromObject, args=('cam1', creatCam(URL.get('cam1')), 'top', FrameTop))
        cam1.start()
        logging.info('start thread: cam1')
        # cam_2
        cam2 = threading.Thread(target=camPreviewFromObject, args=('cam2', creatCam(URL.get('cam2')), 'top', FrameTop))
        cam2.start()
        logging.info('start thread: cam2')
        # cam_3
        cam3 = threading.Thread(target=camPreviewFromObject, args=('cam3', creatCam(URL.get('cam3')), 'top', FrameTop))
        cam3.start()
        logging.info('start thread: cam3')
        # cam_4
        cam4 = threading.Thread(target=camPreviewFromObject, args=('cam4', creatCam(URL.get('cam4')), 'top', FrameTop))
        cam4.start()
        logging.info('start thread: cam4')
        # cam_5
        cam5 = threading.Thread(target=camPreviewFromObject, args=('cam5', creatCam(URL.get('cam5')), 'bottom', FrameBottom))
        cam5.start()
        logging.info('start thread: cam5')
        # cam_6
        cam6 = threading.Thread(target=camPreviewFromObject, args=('cam6', creatCam(URL.get('cam6')), 'bottom', FrameBottom))
        cam6.start()
        logging.info('start thread: cam6')
        # cam_7
        cam7 = threading.Thread(target=camPreviewFromObject, args=('cam7', creatCam(URL.get('cam7')), 'bottom', FrameBottom))
        cam7.start()
        logging.info('start thread: cam7')
        # cam_8
        cam8 = threading.Thread(target=camPreviewFromObject, args=('cam8', creatCam(URL.get('cam8')), 'bottom', FrameBottom))
        cam8.start()
        logging.info('start thread: cam8')
        # обмен данными с контроллером siemens s7-1200
        tcp = threading.Thread(target=TCP.create_data, args=(data, tcp))
        tcp.start()
        logging.info('start thread: TCP')
        # поток работы с базой данных
        db = threading.Thread(target=rw_data_base)
        db.start()
        logging.info('start thread: DB')
        # поток генерирующий изображения
        # верх
        create_image_top = threading.Thread(target=createImgSensorTopButtom, args=('top', 0, FrameTop, data))
        create_image_top.start()
        logging.info('start thread: create_image_top')
        # низ
        create_image_bottom = threading.Thread(target=createImgSensorTopButtom, args=('bottom', 0, FrameBottom, data))
        create_image_bottom.start()
        logging.info('start thread: create_image_bottom')
        #  поток neural network
        # net = threading.Thread(target=object_detection)
        # net.start()
        # остановка потоково и завершение работы программы ===============================================
        # net.join()
        create_image_top.join()

        logging.info('END PROGRAMM')
    except Exception as e:
        pass
    finally:
        pass

if __name__ == "__main__":
    main()








