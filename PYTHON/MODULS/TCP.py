from pyModbusTCP.client import ModbusClient
import time
import datetime
from termcolor import colored as color
from termcolor import cprint

# ===================================================================================================================
# CREATE VAR
TCP_BUFER = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

# global Data_from_plc_siemens
Data_from_plc_siemens = dict(sensor=None, rw_board=None, rw_layer=None, rw_palet=None,
                             write_to_db_board_succesfull=None, write_to_db_layer_succesfull=None,
                             write_to_db_palet_succesfull=None,
                             sensor_for_top=None, sensor_for_bottom=None,
                             Tx_write_sort=0)
Data_layer_from_plc = dict(Length=None, Width=None, Height=None, sort=None, Volume=None, karman=None)
Data_board_from_plc = dict(Length=None, Width=None, Height=None, Sort=None, karman=None)
Data_palet_from_plc = dict(Length=None, Width=None, Height=None, Sort=None, Volume=None, karman=None)


# ===================================================================================================================
# FUNCTION

def Client(
        SERVER_HOST='192.168.0.100',
        SERVER_PORT='502'):
    client = ModbusClient()
    client.host(SERVER_HOST)
    client.port(int(SERVER_PORT))
    client.open()

    return [client, client.is_open()]


def read_03(client, adrFrom=0, adrTo=10):
    # open or reconnect TCP to server
    if not client.is_open():
        if not client.open():
            raise Exception('ERROR CONNECT TO HOST')

    if client.is_open():
        regs = client.read_holding_registers(adrFrom, adrTo)
        if regs:
            return regs


def write_06(client, adrReg, data):
    t = time.time()
    while True:
        # # open or reconnect TCP to server
        if not client.is_open():
            if not client.open():
                # pass
                print('err')
                # return 'unable to connect'
        if client.is_open():
            regs = client.write_single_register(adrReg, data)
            if regs:
                return regs



def write_01(client, adrCoil, data):
    # # open or reconnect TCP to server
    if not client.is_open():
        if not client.open():
            return 'unable to connect'

    if client.is_open():
        regs = client.write_single_coil(adrCoil, data)
        if regs:
            return regs


def MB_SIEMENS(arr, tcp, flag):
    # flag[0] - optical sensor
    global flag_01
    flag_01 = True
    plc, status = Client('192.168.0.90', '502')
    print('Start process: TCP, time', datetime.datetime.now())
    t1 = time.time()
    cnt = 0
    while True:
        start_time = time.time()
        life_bit = 1
        bit_board = flag[1]
        bit_layer = flag[2]
        bit_palet = flag[3]
        bit_sort = flag[6]

        try:
            # чтение регистров
            buff = read_03(plc, 0, 29)
            for i in range(len(buff)):
                tcp[i] = buff[i]
            # print('TCP', tcp[:])
            #   ======================================================================================================
            # if buff[0] == 1 and flag_01:
            #     # print(write_06(plc, 9, 1))
            #     cnt += 1
            #     # cprint('%s, sensor, time: %s' % (cnt, time.time() - t1), 'magenta', attrs=['reverse'])
            #     t1 = time.time()
            #     flag[0] = 1
            #     flag_01 = False
            #     reset = 1
            # if buff[0] == 0:
            #     flag_01 = True
            #     reset = 0
            #   ======================================================================================================
            write_data = create_msk(bit0=bit_board, bit1=bit_layer, bit2=life_bit, bit3=bit_sort, bit4=bit_palet,
                                    bit5=0)
            # if buff[0] == 1:
            #     write_06(plc, 9, 1)
            # time.sleep(0.5)

            #   ======================================================================================================
            # # записываем определенный сорт в ПЛК
            write_06(plc, adrReg=4, data=arr[10])
            # write
            write_06(plc, 30, write_data)

            # сброс флага о записи сорта в ПЛК
            if buff[8] == 1 and flag[6] == 1:
                # print('reset')
                flag[6] = 0

            # print(time.time() - ts)
            # print(buff[0], '   ', flag_01, 'time: ', time.time() - start_time)
        except Exception as e:
            print(e)
            time.sleep(10)
            plc, status = Client('192.168.0.90', '502')
            print(plc, status)
        finally:
            pass
            # time.sleep(0.1)
            # reset Tx


def create_msk(bit0=0, bit1=0, bit2=0, bit3=0, bit4=0, bit5=0, bit6=0, bit7=0):
    mask1 = set_bit(0, 0, bit0)
    mask2 = set_bit(mask1, 1, bit1)
    mask3 = set_bit(mask2, 2, bit2)
    mask4 = set_bit(mask3, 3, bit3)
    mask5 = set_bit(mask4, 4, bit4)
    mask6 = set_bit(mask5, 5, bit5)
    # mask7 = set_bit(mask6, 6, bit6)
    # mask8 = set_bit(mask7, 7, bit7)
    return mask6


def set_bit(v, index, x):
    mask = 1 << index
    v &= ~mask
    if x:
        v |= mask
        return v
    else:
        return v


def parse_date_layer():
    Data_layer_from_plc['Length'] = TCP_BUFER[21] / 100
    Data_layer_from_plc['Width'] = TCP_BUFER[20] / 100
    Data_layer_from_plc['Height'] = TCP_BUFER[22] / 100
    Data_layer_from_plc['sort'] = TCP_BUFER[19]
    Data_layer_from_plc['Volume'] = TCP_BUFER[23]
    Data_layer_from_plc['karman'] = TCP_BUFER[18]


def parse_date_board():
    Data_board_from_plc['Length'] = TCP_BUFER[13] / 10
    Data_board_from_plc['Height'] = TCP_BUFER[14] / 100
    Data_board_from_plc['Width'] = TCP_BUFER[15] / 100
    Data_board_from_plc['Sort'] = TCP_BUFER[16]
    Data_board_from_plc['karman'] = TCP_BUFER[17]


def parse_date_palet():
    Data_palet_from_plc['Length'] = TCP_BUFER[26] / 100
    Data_palet_from_plc['Height'] = TCP_BUFER[28] / 100
    Data_palet_from_plc['Width'] = TCP_BUFER[27] / 100
    Data_palet_from_plc['Sort'] = TCP_BUFER[25]
    Data_palet_from_plc['Volume'] = TCP_BUFER[29]
    Data_palet_from_plc['karman'] = TCP_BUFER[24]


def testConnectClientServer():
    import os
    plc, status = Client('192.168.0.90', '502')
    cprint('Start process: TCP, time: %s'%datetime.datetime.now(), 'blue', attrs=['reverse'])
    flag_01 = False
    while True:
        try:

            # чтение регистров
            buff = read_03(plc, 0, 30)
            # cprint(buff, 'yellow')
            #   ======================================================================================================
            if buff[0] == 1 and flag_01:
                cprint('sensor', 'magenta',  attrs=['reverse'])
                # os.system('scp -r ast@192.168.0.121:/home/ast/Documents/ServClient/ /home/ast/Desktop/')
                # flag[0] = 1
                flag_01 = False
            if buff[0] == 0:
                # pass
                flag_01 = True
            #   ======================================================================================================
        except Exception as e:

            print('TCP: ', e)
            print('RESTART')
            time.sleep(10)
            plc.close()
            plc, status = Client('192.168.0.90', '502')
            print(plc, status)
        finally:
            pass

if __name__ == '__main__':
    # plc, status = Client('192.168.0.90', '502')
    # while True:
    #     cprint(read_03(plc, 0, 2), 'yellow')
    #     time.sleep(1)
    testConnectClientServer()

