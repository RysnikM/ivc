import cv2
import numpy as np
import os
import glob
import time

PATH_ROOT = '/home/ast/Desktop/F_eye/chech_map/'

def param():
    '''
    метод возвращает параметры камеры по фотографиям шахматной доски
    :return:
    '''
    CHECKERBOARD = (6, 9)
    subpix_criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.1)
    calibration_flags = cv2.fisheye.CALIB_RECOMPUTE_EXTRINSIC + cv2.fisheye.CALIB_CHECK_COND + cv2.fisheye.CALIB_FIX_SKEW
    objp = np.zeros((1, CHECKERBOARD[0] * CHECKERBOARD[1], 3), np.float32)
    objp[0, :, :2] = np.mgrid[0:CHECKERBOARD[0], 0:CHECKERBOARD[1]].T.reshape(-1, 2)
    _img_shape = None
    objpoints = []  # 3d point in real world space
    imgpoints = []  # 2d points in image plane.
    images = glob.glob(PATH_ROOT+'*.png')
    print(images)
    for fname in images:
        img = cv2.imread(fname)
        if _img_shape == None:
            _img_shape = img.shape[:2]
        else:
            assert _img_shape == img.shape[:2], "All images must share the same size."
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # Find the chess board corners
        ret, corners = cv2.findChessboardCorners(gray, CHECKERBOARD,
                                                 cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_FAST_CHECK + cv2.CALIB_CB_NORMALIZE_IMAGE)
        # If found, add object points, image points (after refining them)
        if ret == True:
            objpoints.append(objp)
            cv2.cornerSubPix(gray, corners, (3, 3), (-1, -1), subpix_criteria)
            imgpoints.append(corners)
    N_OK = len(objpoints)
    K = np.zeros((3, 3))
    D = np.zeros((4, 1))
    rvecs = [np.zeros((1, 1, 3), dtype=np.float64) for i in range(N_OK)]
    tvecs = [np.zeros((1, 1, 3), dtype=np.float64) for i in range(N_OK)]
    rms, _, _, _, _ = \
        cv2.fisheye.calibrate(
            objpoints,
            imgpoints,
            gray.shape[::-1],
            K,
            D,
            rvecs,
            tvecs,
            calibration_flags,
            (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 1e-6)
        )
    print("Found " + str(N_OK) + " valid images for calibration")
    print("DIM=" + str(_img_shape[::-1]))
    print("K=np.array(" + str(K.tolist()) + ")")
    print("D=np.array(" + str(D.tolist()) + ")")


K=np.array([[1987.2902013412322, 0.0, 1540.7521175856489], [0.0, 1914.5965846929532, 843.8461627100188], [0.0, 0.0, 1.0]])
D=np.array([[-0.061326402914289105], [0.2306815273667478], [-1.002826819903716], [1.0982644076261452]])
DIM=(3072, 1728)

def look_1(img):
    K = np.array(
        [[1987.2902013412322, 0.0, 1540.7521175856489], [0.0, 1914.5965846929532, 843.8461627100188], [0.0, 0.0, 1.0]])
    D = np.array([[-0.061326402914289105], [0.2306815273667478], [-1.002826819903716], [1.0982644076261452]])
    DIM = (3072, 1728)

    # img = cv2.imread(PATH_ROOT)
    img_dim = img.shape[:2][::-1]
    balance = 0.0
    scaled_K = K * img_dim[0] / DIM[0]
    scaled_K[2][2] = 1.0
    new_K = cv2.fisheye.estimateNewCameraMatrixForUndistortRectify(scaled_K, D,
        img_dim, np.eye(3), balance=balance)

    map1, map2 = cv2.fisheye.initUndistortRectifyMap(scaled_K, D, np.eye(3),
        new_K, img_dim, cv2.CV_16SC2)
    undist_image = cv2.remap(img, map1, map2, interpolation=cv2.INTER_LINEAR,
        borderMode=cv2.BORDER_CONSTANT)
    return undist_image



def creat():
    cnt = 0
    file = glob.glob('/home/ast/Desktop/F_eye/*.bmp')
    for f in file:
        cnt += 1
        img = cv2.imread(f)
        cv2.imwrite('/home/ast/Desktop/F_eye/chech_map/{}.png'.format(cnt), cv2.resize(img, (3072, 1728)))

if __name__ == '__main__':
    # creat()
    # param()
    images = glob.glob('/home/ast/Desktop/_IMAGE/2019-10-23/' + '*.jpg')
    for i in images:
        im = cv2.imread(i)
        img = look_1(im)
        k = np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])
        # k = np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]])

        # k = np.ones((5, 5), np.float32) / 25
        img = cv2.filter2D(img, -1, kernel=k)
        # cv2.imshow('img', cv2.resize(img, (800, 500)))
        cv2.imshow('img', img)

        print(img.shape)

        key = cv2.waitKey()
        if key == 27:
            cv2.destroyAllWindows()
            break
        elif key == 49:
            print('save')




