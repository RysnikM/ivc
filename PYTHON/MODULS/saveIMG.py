import cv2
import os
import time

def saveIMG(image, path='c:/Users/Mikalai/Desktop'):
	PATH_TO_FOLDER = path
	FOLDER_NAME = '_IMAGE/' + time.strftime("%Y-%m-%d")
	_PATH = os.path.join(PATH_TO_FOLDER, FOLDER_NAME)
	# проверка наличия пути, если нет, то создаем папку
	if os.path.exists(_PATH):
		pass
		# print('OK')
	else:
		try:
			os.makedirs(_PATH)
		except OSError:
			print ("Creation of the directory %s failed" % _PATH)
		else:
			print ("Successfully created the directory %s " % _PATH)

	# формируем имя файла
	FILE_NAME = '{}.jpg'.format(len(os.listdir(_PATH)) + 1)
	# print(FILE_NAME)
	# сохраняем файл в папку
	cv2.imwrite(_PATH + '/'+ FILE_NAME, image)
if __name__ == '__main__':
	img = cv2.imread("c:/3.jpg")
	# print('hello')
	saveIMG(img)
