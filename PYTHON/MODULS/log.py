import logging
# logging.basicConfig(format = u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', level = logging.DEBUG)
logging.basicConfig(format = u'%(levelname)-8s [%(asctime)s] %(message)s', level = logging.DEBUG, filename = 'log.log')
logging.basicConfig(format = u'%(levelname)-8s [%(asctime)s] %(message)s', level = logging.ERROR, filename = 'log.log')
logging.basicConfig(format = u'%(levelname)-8s [%(asctime)s] %(message)s', level = logging.WARNING, filename = 'log.log')
logging.basicConfig(format = u'%(levelname)-8s [%(asctime)s] %(message)s', level = logging.INFO, filename = 'log.log')

for i in range(1000):   
    logging.debug(  'This is a debug message')
    logging.error(  'This is an error message')
    logging.info(   'info')
    logging.warning('warning message')