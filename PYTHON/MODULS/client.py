import socket
import time
from threading import Thread
import os

PATH_TO_IMAGE = '/home/ast/Documents/CAM_RAW/'
# PATH_TO_IMAGE = '/home/ast/Desktop/CAM/CAMR/'


def client(host, port, name, flag):
    cnt = 0
    file = PATH_TO_IMAGE + name
    while True:
        try:
            # s = socket.create_connection((host, port), timeout=30)
            while True:
                s = socket.create_connection((host, port), timeout=30)
                _file = open(file, 'rb')
                data = _file.read()
                size = len(data)
                s.sendall(data)
                cnt += 1
                print(cnt, '_', size)
                s.close()
                time.sleep(1)
        except Exception as e:
            print('/////', e)


# client('192.168.0.40', 9001, '/home/ast/Desktop/CAM/CAMR/cam5.jpg', True)
# client_1('192.168.0.40', 9001, '/home/ast/Documents/CAM_RAW/cam5.jpg', True)

if __name__ == '__main__':
    try:
        # t1 = Thread(target=client, args=('192.168.0.40', 9001, 'cam5.jpg', True))
        # t2 = Thread(target=client, args=('192.168.0.40', 9002, 'cam6.jpg', True))
        # t3 = Thread(target=client, args=('192.168.0.40', 9003, 'cam7.jpg', True))
        # t4 = Thread(target=client, args=('192.168.0.40', 9004, 'cam8.jpg', True))
        t1 = Thread(target=client, args=('192.168.0.40', 10001, 'cam5.jpg', True))
        t2 = Thread(target=client, args=('192.168.0.40', 10002, 'cam6.jpg', True))
        t3 = Thread(target=client, args=('192.168.0.40', 10003, 'cam7.jpg', True))
        t4 = Thread(target=client, args=('192.168.0.40', 10004, 'cam8.jpg', True))


        t1.start()
        t2.start()
        t3.start()
        t4.start()

        t1.join()
        t2.join()
        t3.join()
        t4.join()

    finally:
        print('FINALLY SCRIPT')
        os.system('killall -s 9 python3')