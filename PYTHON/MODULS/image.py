from PIL import Image
import cv2
import numpy as np
from numba import jit
import time

from statistics import median
import MODULS.Sort as SORT


def createIMG(imgDict):
    '''
    imgArray - image list. 
    format images - numpy array

    need library:
    cv2, PIL, numpy

    out:
    (image_CV (format numpy arra), image_P (firmat image PIL))
    '''
    imgArray = []
    for img in imgDict.values():
        imgArray.append(img)

    img_P = Image.new('RGB', size=(1468, 489), color='BLACK')
    for i in range(8):
        try:
            # convert image to PIL format
            img = cv2.cvtColor(imgArray[i], cv2.COLOR_BGR2RGB)
            img = Image.fromarray(img)
            # paste image in image form
            if i <= 3:
                img_P.paste(img, (i * 367, 0))
            else:
                img_P.paste(img, ((i - 4) * 367, 240))
        except:
            # if we have exceptin, create new img, and paste in image form
            img_box = Image.new('RGB', size=(365, 240), color=(200, 200, 200))
            if i <= 3:
                img_P.paste(img_box, (i * 367, 0))
            else:
                img_P.paste(img_box, ((i - 4) * 367, 240))
    image_P = img_P
    image_CV = np.array(img_P)
    image_CV = cv2.cvtColor(image_CV, cv2.COLOR_RGB2BGR)

    return (image_CV, image_P)


def cut_board(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    canny = cv2.Canny(gray, 150, 255, 10)
    kernel = np.ones((3, 3), np.uint8)
    canny = cv2.dilate(canny, kernel, iterations=10)

    # cv2.imshow('qwe', gray)
    cv2.imshow('qwe', canny)
    cv2.waitKey(6000)
    cv2.destroyAllWindows()

    lines = cv2.HoughLines(canny, 1, np.pi / 2, 200)

    # print(lines)

    y_1 = 0
    y_2 = 10000

    for line in lines:
        for rho, theta in line:
            a = np.cos(theta)
            b = np.sin(theta)
            x0 = a * rho
            y0 = b * rho
            x1 = int(x0 + 1000 * (-b))
            y1 = int(y0 + 1000 * (a))
            x2 = int(x0 - 1000 * (-b))
            y2 = int(y0 - 1000 * (a))
            y_1 = max(y_1, y1)
            y_2 = min(y_2, y2)
            img = img[y_2 + 10:y_1 - 10, :]
            print(img)

    return img


def cutImg(img):
    inputImg = img

    canny = cv2.Canny(img, 255, 255, 5)
    # kernel = np.ones((3, 2), np.uint8)
    # kernel = np.array([ [2, 4, 5, 4, 2], 
    #                     [4, 9, 12, 9, 4],
    #                     [5, 12, 15, 12, 5], 
    #                     [4, 9, 12, 9, 4], 
    #                     [2, 4, 5, 4, 2]], np.uint8)/159
    kernel = np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]], np.uint8)
    canny = cv2.dilate(canny, kernel, iterations=5)

    roi_copy = inputImg.copy()
    roi_hsv = cv2.cvtColor(inputImg, cv2.COLOR_RGB2HSV)

    # filter black color
    mask1 = cv2.inRange(roi_hsv, np.array([80, 100, 80]), np.array([255, 255, 255]))
    # return mask1
    mask1 = cv2.morphologyEx(mask1, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3)))
    mask1 = cv2.Canny(mask1, 100, 300)
    mask1 = cv2.GaussianBlur(mask1, (1, 1), 0)
    mask1 = cv2.Canny(mask1, 100, 300)
    # mask1 = cv2.morphologyEx(mask1, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3)))

    lines = cv2.HoughLines(mask1, 1, np.pi / 2, 200)

    y_1 = 0
    y_2 = 10000
    try:
        for line in lines:
            for rho, theta in line:
                a = np.cos(theta)
                b = np.sin(theta)
                x0 = a * rho
                y0 = b * rho
                x1 = int(x0 + 1000 * (-b))
                y1 = int(y0 + 1000 * (a))
                x2 = int(x0 - 1000 * (-b))
                y2 = int(y0 - 1000 * (a))
                # if x1==x2:
                #     continue
                y_1 = max(y_1, y1)
                y_2 = min(y_2, y2)
                cv2.line(inputImg, (x1, y1), (x2, y2), (0, 255, 255), 1)
    except:
        # pass
        print('err')

    return inputImg


def cutImgMask(inputImg, mask):
    """
    :param inputImg:
    :param mask:
    :return:
    image, y_min, y_max
    """
    y_1 = 0
    y_2 = 10000
    delta = 10
    h, w = inputImg.shape[:2]

    y_min = 0
    y_max = h

    # c = int(h / 2)
    # cv2.line(inputImg, (0, c), (w, c), (0, 0, 200), 1)

    lines = cv2.HoughLines(mask, 1, np.pi / 2, 200)

    try:
        for line in lines:
            for rho, theta in line:
                a = np.cos(theta)
                b = np.sin(theta)
                x0 = a * rho
                y0 = b * rho
                x1 = int(x0 + w * (-b))
                y1 = int(y0 + w * (a))
                x2 = int(x0 - w * (-b))
                y2 = int(y0 - w * (a))
                if x1 == x2:
                    continue
                y_1 = max(y_1, y1)
                y_2 = min(y_2, y2)

                # print('x1: ', x1, ' x2: ', x2)
                # cv2.line(inputImg, (x1, 0), (x1, h), (255, 255, 0), 2)

        y_min = y_2 - delta
        if y_min < 0: y_min = 0

        y_max = y_1 + delta

        # print('area: ', y_min, y_max)

        cv2.line(inputImg, (0, y_min), (w, y_min), (0, 255, 255), 1)
        cv2.line(inputImg, (0, y_max), (w, y_max), (0, 255, 255), 1)
    except:
        # y_min = 150
        # y_max = 400
        # print('err: ',y_min, y_max)
        pass
    return inputImg, y_min, y_max


def cutImgMaskDict(inputImgDict, maskDict, Ymin, Ymax):
    for key in inputImgDict.keys():
        try:
            inputImgDict[key], Ymin[key], Ymax[key] = cutImgMask(inputImgDict[key], maskDict[key])
        except:
            # print('err')
            pass
    # return inputImgDict, Ymin, Ymax
    # return inputImgDict


def getMask(roi):
    roi_hsv = cv2.cvtColor(roi, cv2.COLOR_RGB2HSV)
    # filter black color
    mask1 = cv2.inRange(roi_hsv, np.array([80, 100, 80]), np.array([255, 255, 255]))
    # mask1 = cv2.morphologyEx(mask1, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3)))
    mask1 = cv2.Canny(mask1, 100, 300)
    mask1 = cv2.GaussianBlur(mask1, (1, 1), 0)
    mask1 = cv2.Canny(mask1, 100, 300)
    mask1 = cv2.morphologyEx(mask1, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3)))
    return mask1


def getMaskDict(Roi, Mask):
    for key in Roi.keys():
        try:
            Mask[key] = getMask(Roi[key])
        except:
            # print('err')
            pass
    # return Roi


def __bound_contours_set(roi, h1, h2, s1, s2, v1, v2):
    """
        returns modified roi(non-destructive) and rectangles that founded by the algorithm.
        @roi region of interest to find contours
        @return (roi, rects)
    """

    roi_copy = roi.copy()
    roi_hsv = cv2.cvtColor(roi, cv2.COLOR_RGB2HSV)
    # filter black color
    mask1 = cv2.inRange(roi_hsv, np.array([h1, s1, v1]), np.array([h2, s2, v2]))
    return mask1
    # mask1 = cv2.morphologyEx(mask1, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3)))
    mask1 = cv2.Canny(mask1, 100, 300)
    mask1 = cv2.GaussianBlur(mask1, (1, 1), 0)
    mask1 = cv2.Canny(mask1, 100, 300)
    mask1 = cv2.morphologyEx(mask1, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3)))
    # Find contours for detected portion of the image

    contours, _ = cv2.findContours(mask1.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    for cnt in contours:
        rect = cv2.minAreaRect(cnt)
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        center = (int(rect[0][0]), int(rect[0][1]))
        area = int(rect[1][0] * rect[1][1])  # вычисление площади
        if area > 8000:
            cv2.drawContours(roi_copy, [box], 0, (0, 255, 255), 1)

    return roi_hsv


def settingsMask(img):
    cv2.namedWindow('q')

    def nothing(x):
        pass

    cv2.createTrackbar('H1', 'q', 0, 255, nothing)
    cv2.createTrackbar('H2', 'q', 0, 255, nothing)
    cv2.createTrackbar('S1', 'q', 0, 255, nothing)
    cv2.createTrackbar('S2', 'q', 0, 255, nothing)
    cv2.createTrackbar('V1', 'q', 0, 255, nothing)
    cv2.createTrackbar('V2', 'q', 0, 255, nothing)

    while (True):
        h1 = cv2.getTrackbarPos('H1', 'q')
        h2 = cv2.getTrackbarPos('H2', 'q')
        s1 = cv2.getTrackbarPos('S1', 'q')
        s2 = cv2.getTrackbarPos('S2', 'q')
        v1 = cv2.getTrackbarPos('V1', 'q')
        v2 = cv2.getTrackbarPos('V2', 'q')

        # show = cv2.resize(img, (1000, 600))
        show = img
        # mask = __bound_contours_set(show, h1, h2, s1, s2, v1, v2)

        roi_copy = img.copy()
        # roi_hsv = cv2.cvtColor(roi_copy, cv2.COLOR_BGR2XYZ)
        # filter black color
        roi_hsv = roi_copy
        mask = cv2.inRange(roi_hsv, np.array([h1, s1, v1]), np.array([h2, s2, v2]))

        # image_CV = np.array(mask)
        mask = cv2.resize(mask, (1920, 200))
        roi_hsv = cv2.resize(roi_hsv, (1920, 200))

        # print(pr.shape, show.shape)
        # print(type(pr), type(mask))

        # show = cv2.bitwise_or(pr, pr, mask=image_CV)

        # show = cutImg(show)
        cv2.imshow('f', roi_hsv)
        cv2.imshow('q', mask)
        if cv2.waitKey(1) & 0xFF == 27:
            break


def fishEyeCam(frame):
    # вариант 1
    # коэффициенты камеры
    camera_matrix = np.array([[832.46839736, 0.0, 644.95754444],
                              [0., 801.40931086, 344.10344623],
                              [0.00000000e+00, 0.00000000e+00, 1.00000000e+00]])
    # поправочные коэффициенты
    dist_coefs = np.array([-0.35883063, 0.13040111, 0.00144063, -0.00088184, -0.01557561])

    # camera_matrix = np.array([
    #     [832 + 102.0, 0, 644 + 50.0],
    #     [0, 801 + 32.6, 344 + 46.5],
    #     [0, 0, 1]]
    # )
    # dist_coefs = np.array(
    #     [-0.35883063 + 0.00099, 0.13040111, 0.00144063 + 0.00781,
    #      -0.00088184, -0.01557561 - 0.00063]
    # )

    size = frame.shape
    newcameramtx, roi = cv2.getOptimalNewCameraMatrix(camera_matrix, dist_coefs, (size[0], size[1]), 1,
                                                      (size[0], size[1]))
    frame = cv2.undistort(frame, camera_matrix, dist_coefs, None, newcameramtx)
    h, w, c = frame.shape
    frame = frame[int(h / 6):int(5 * h / 6), int(w / 12):int(11 * w / 12)]
    # pass
    return frame


def fishEyeThread(Frame):
    from threading import Thread
    start_time = time.time()
    try:
        t1 = Thread(target=fishEyeCam, args=(Frame['cam1'], ))
        t2 = Thread(target=fishEyeCam, args=(Frame['cam1'], ))
        t3 = Thread(target=fishEyeCam, args=(Frame['cam1'], ))
        t4 = Thread(target=fishEyeCam, args=(Frame['cam1'], ))
        t5 = Thread(target=fishEyeCam, args=(Frame['cam1'], ))
        t6 = Thread(target=fishEyeCam, args=(Frame['cam1'], ))
        t7 = Thread(target=fishEyeCam, args=(Frame['cam1'], ))
        t8 = Thread(target=fishEyeCam, args=(Frame['cam1'], ))

        t1.start()
        t2.start()
        t3.start()
        t4.start()
        t5.start()
        t6.start()
        t7.start()
        t8.start()

        t1.join()
        t2.join()
        t3.join()
        t4.join()
        t5.join()
        t6.join()
        t7.join()
        t8.join()
    except Exception as e:
        print('fish eye:', e)
    finally:
        print(time.time() - start_time)





def color(img):
    Z = img.reshape((-1, 3))
    Z = np.float32(Z)
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)

    K = 2

    ret, label, center = cv2.kmeans(Z, K, None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)
    center = np.uint8(center)
    res = center[label.flatten()]
    res2 = res.reshape((img.shape))

    return res2


def deleteBackground(original_image, background_image):
    """
    Функиция позволяет сделать сравнение 2 изображений для удаления фона путем создания маски
    """
    gray_original = cv2.cvtColor(original_image, cv2.COLOR_BGR2GRAY)
    gray_background = cv2.cvtColor(background_image, cv2.COLOR_BGR2GRAY)
    k = 30
    y, x = gray_original.shape[:2]
    original = cv2.resize(gray_original, (int(x/k), int(y/k)))
    background = cv2.resize(gray_background, (int(x/k), int(y/k)))
    or_image = cv2.resize(original_image, (int(x / k), int(y / k)))

    dlt = 40
    # num_point = 1
    # X = list(np.linspace(0, 1, num_point+1))
    # Y_min = []
    # Y_max = []
    # IM = []
    # Xp=[]
    # print(or_image.shape, original.shape)
    # for i in range(len(X)-1):
    #     x_00 = int(original.shape[1] * X[i])
    #     dx = X[i+1] - X[i]
    #     x_01 = int(original.shape[1] * (X[i]+dx))
    #     print(x_00, x_01)
    #     _original = original[:, x_00:x_01]
    #     _background = background[:, x_00:x_01]
    #     _or_image = or_image[:, x_00:x_01]

        # t1, original_image = applyMask(gray_original, gray_background, original_image, dlt)
    y_min, y_max, _original_image = applyMask(original, background, dlt, or_image)
        # Y_min.append(y_min*k)
        # Y_max.append(y_max*k)
        # Xp.append([x_01*k, y_max*k])
        # IM.append(ooo)
        # _, t1 = cv2.threshold(t1, thresh=50, maxval=500, type=cv2.THRESH_BINARY)
    # print(Y_min)
    # print(Y_max)
    # print(Xp)


    # print(Ymin, '\n', Ymax, '\n', X)
    # points = np.array([Xp], dtype=np.int32)
    # cv2.polylines(original_image, points, False, (255, 255, 255), thickness=5)
    # points = np.array([Y_max], dtype=np.int32)
    # cv2.polylines(original_image,points, False, (0, 255, 0), thickness=1)
    # print(y_min*k, y_max*k, original_image.shape, x)
    # cv2.line(original_image, (0, y_max*k), (x, y_max*k), (255, 255, 255), 5)
    # cv2.line(original_image, (0, y_min*k), (x, y_min*k), (255, 255, 255), 5)

    return _original_image, original_image[int(y_min*k*0.8):int(y_max*k*1.2), :]


def deleteBackgroundDict(original_image, background_image, mask):
    for key in original_image.keys():
        try:
            mask[key], original_image[key] = deleteBackground(original_image.get(key), background_image.get(key))
        except:
            pass
    # return original_image

import numba
from numba import cuda

# @cuda.jit()
# @numba.njit()
def applyMask(t1, t2, dlt, original_image):
    """
    :param t1: gray original image
    :param t2: gray background image
    :param original_image: origimal image
    :param dlt: delta
    :return:
    """
    y_min, y_max = 0, 0
    t1 = cv2.medianBlur(t1, 5)
    t2 = cv2.medianBlur(t2, 5)
    for i in range(t1.shape[0]):
        cnt = 0
        for j in range(t1.shape[1]):
            if (t1[i][j] <= t2[i][j] + dlt) and (t1[i][j] >= t2[i][j] - dlt):
                # print(t1[i][j])
                t1[i][j] = 255
                original_image[i][j][0] = 255
                original_image[i][j][1] = 255
                original_image[i][j][2] = 255
                # cnt = 0
            else:
                cnt += 1
                # if cnt == 1:
                #     first = j
                #     end = j
            # print(cnt, first, end)

        # print(cnt, t1.shape[1]*0.90)
        if cnt < t1.shape[1]*0.40:
            # pass
            # original_image[i] = np.linspace([255, 255, 255], 255, t1.shape[1], dtype=np.uint8)
            if y_min != 0 and y_max == 0:
                y_max = i
        # y_min, y_max
        # ===============================================================================
        elif y_min == 0:
            y_min = i
        # ===============================================================================
    # return y_min, y_max
    return y_min, y_max, original_image





def roiCam(Cam, Roi):
    for key in Cam.keys():
        try:
            Cam[key] = Cam[key][Roi[key][0]:Roi[key][1], Roi[key][2]:Roi[key][3]]
        except:
            pass
    # return Cam


def lineUp(Frame, Ymin, Ymax):
    Tx = dict(
        cam1=0, cam2=(Ymin['cam1'] - Ymin['cam2']), cam3=(Ymin['cam1'] - Ymin['cam3']),
        cam4=(Ymin['cam1'] - Ymin['cam4']),
        cam5=0, cam6=0, cam7=0, cam8=0)
    # Tx = dict(
    #     cam1=0, cam2=50, cam3=100, cam4=-50,
    #     cam5=0, cam6=0, cam7=0, cam8=0)

    # print(Tx)
    for key in Frame.keys():
        try:
            rows, cols, _ = Frame[key].shape
            M = np.float32([[1, 0, 0], [0, 1, Tx[key]]])
            Frame[key] = cv2.warpAffine(Frame[key], M, (cols, rows))
        except:
            pass


def blacked(frame, y_min, y_max):
    """
    заполняем изображение черным цветом, исключая зону y_min:y_max
    :param frame:
    :param y_min:
    :param y_max:
    :return:
    """
    zero_line = np.zeros([frame.shape[1], frame.shape[2]])
    for y in range(frame.shape[0]):
        if y <= y_min or y >= y_max:
            frame[y] = zero_line
    return frame


def blacked_dict(Frame, Ymin, Ymax):
    for key in Frame.keys():
        try:
            Frame[key] = blacked(Frame[key], Ymin[key], Ymax[key])
        except Exception as e:
            print(e)
            print('blacked')

import numba


def find_region(roi, mask, black_line, frame):
    y_min = 0
    y_max = frame.shape[0]

    for y in range(roi.shape[0]):
        if all(mask == black_line) and y_min == 0:
            y_min = y
        elif all(mask != black_line) and y_min != 0 and y_max == frame.shape[0]:
            y_max = y
            break


def find_board_region(image):
    global y_min, y_max
    fr_copy = image.copy()
    k = 15
    y, x = image.shape[:-1]
    frame = cv2.resize(fr_copy, (int(x/k), int(y/k)), interpolation=cv2.INTER_LINEAR)
    Ymin = []
    Ymax = []
    # зададим точки в которых хотим делать проверку
    # X = [0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95]
    # X = [0, 0.5, 0.95]
    X = []
    num_point = 100
    for i in range(num_point):
        X.append(i/num_point)
    X = X[:-2]
    point_min = []
    point_max = []
    roi_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    for x in X:
        y_min = 0
        y_max = frame.shape[0]

        x_00 = int(roi_hsv.shape[1] * x)
        x_01 = int(roi_hsv.shape[1] * (x + 0.03))

        roi = roi_hsv[:, x_00:x_01]

        mask = cv2.inRange(roi, np.array([0, 35, 90]), np.array([30, 200, 255]))
        black_line = list(np.linspace(255, 255, roi.shape[1], dtype=np.int16))

        # try:
        for y in range(roi.shape[0]):
            if all(mask[y] == black_line) and y_min == 0:
                y_min = y*k
            elif all(mask[y] != black_line) and y_min != 0 and y_max == frame.shape[0]:
                y_max = y*k
                break
        # except Exception as e:
        #     print(e)

        if y_min != 0 and y_min != y_max:
            Ymin.append(y_min)
            # add point
            point_min.append([x*image.shape[1], y_min])
        if y_max != frame.shape[0] and y_min != y_max:
            Ymax.append(y_max)
            point_max.append([x*image.shape[1], y_max])

    if Ymin.__len__() != 0:
        y_min = median(Ymin)
        y_min = int(y_min*1)
    if Ymax.__len__() != 0:
        y_max = int(median(Ymax)*1)

    if y_min == 0 and y_max == frame.shape[0]:
        y_min = 0
        y_max = 0

    # frame = blacked(image, y_min, y_max)
    # y, x, c = frame.shape
    #
    # Tx = int((y/2 - y_max)+(y_max - y_min)/2)
    # M = np.float32([[1, 0, 0], [0, 1, Tx]])
    # frame = cv2.warpAffine(frame, M, (x, y))

    return image[y_min:y_max, :], y_min, y_max
    # for i in range(len(X)):
    #     X[i] = (X[i]+0.05)*image.shape[1]

    # print(Ymin, '\n', Ymax, '\n', X)
    # points = np.array([point_min], dtype=np.int32)
    # cv2.polylines(image, points, False, (0, 255, 0), thickness=1)
    # points = np.array([point_max], dtype=np.int32)
    # cv2.polylines(image,points, False, (0, 255, 0), thickness=1)

    return image, y_min, y_max



def find_board_region_dict(Frame, Ymin, Ymax):
    from threading import Thread
    for key in Frame.keys():
        try:
            Frame[key], Ymin[key], Ymax[key] = find_board_region(Frame[key])
        except Exception as e:
            print('find_board:', e)


def cut_lenght(image):
    # обрезка по длинне:
    roi_hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    y, x, c = roi_hsv.shape
    dy = int(y * 0.03)
    dx = int(y * 0.5)
    y_00 = int(y / 2 - dy)
    y_01 = int(y / 2 + dy)

    roi = roi_hsv[y_00:y_01, dx:]
    mask = cv2.inRange(roi, np.array([0, 35, 90]), np.array([30, 200, 255]))

    x_min = 0
    x_max = mask.shape[1]

    black_line = [0 for i in range(mask.shape[0])]
    # black_line = np.linspace(0, 0, mask.shape[0], dtype=np.int16)
    for x in range(mask.shape[1]):
        l = []
        for y in range(mask.shape[0]):
            l.append(mask[y][x])
        # if black_line != l and x_min == 0:
        #     x_min = x
        # elif black_line == l and x_min != 0 and x_max == mask.shape[1]:
        #     x_max = x
        #     break
        if black_line == l and x_max == mask.shape[1]:
            x_max = x + dx
            break

    if x_max <= (image.shape[1]-10):
        x_max = x_max+10


    # print(x_min, x_max)
    h = image.shape[0]
    # 1460:240 сообношение сторон
    y = int(240*(x_max-x_min)/1468)
    try:
        image = image[int(h / 2 - y / 2):int(h / 2 + y / 2), x_min:x_max]
    except Exception as e:
        print(e)
    return image, x_min, x_max



if __name__ == "__main__":
    # t1 = time.time()
    #
    # im01 = cv2.imread('c:/PYTHON/SORT_LINE/IMAGE_1/test1/3000.jpg')
    # im02 = cv2.imread('c:/PYTHON/SORT_LINE/IMAGE_1/test1/2999.jpg')
    #
    # img1 = cv2.imread('/home/ast/Desktop/1.jpg')
    # img2 = cv2.imread('C:/Users/Mikalai/Desktop/_IMAGE/2019-09-01/2.jpg')
    # img3 = cv2.imread('C:/Users/Mikalai/Desktop/_IMAGE/2019-09-01/1.jpg')
    #
    # # img = cv2.resize(img, (367, 200))
    # # img1 = cv2.resize(img1, (367, 200))
    #
    # # im1 = [img1, img2, img3]
    #
    # # im = dict(cam1=cv2.resize(img1, (1280, 720)), cam2=cv2.resize(img2, (1280, 720)), cam3=cv2.resize(img3, (1280, 720)))
    # im = dict(cam1=img1, cam2=img1, cam3=img1)
    #
    # # im = fishEyeDict(im)
    #
    # # vis, visP = createIMG(im)
    #
    # # vis = settingsMask(img3)
    #
    # # frame = img1
    # # frame = fishEyeCam(cv2.resize(frame, (1280, 720)))
    # # vis = color(frame)
    #
    # # vis = cutImg(img1)
    # # vis = cutImgMask(getMask(im01)[0], getMask(im01)[1])
    #
    # vis = np.concatenate((im['cam1'], im['cam2'], im['cam3']), axis=0)
    # cv2.imshow('qwe', vis)
    # print(time.time() - t1)
    # cv2.waitKey()
    # cv2.destroyAllWindows()
    # # # visP.show()

    im = cv2.imread('/home/ast/Desktop/_IMAGE/NET/13.jpg')
    settingsMask(im)