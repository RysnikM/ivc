from __future__ import with_statement

import cv2
import numpy as np
import threading
import time
import datetime

import MODULS.image as IMG
import MODULS.TCP as TCP
import MODULS.Sort as SORT
from MODULS.saveIMG import saveIMG
import  NET.NET_from_image as NET
import MODULS.IMPORT as IMPORT

# создаем лок для синхронизации работы потоков

mes3 = IMPORT.COLOR_TEXT_PRINT_MESS3
mes2 = IMPORT.COLOR_TEXT_PRINT_MESS2
lock = threading.Lock()
lock_t = threading.Lock()
lock_b = threading.Lock()

Locks = {
    'top': False,
    'bottom': False
}

URL = dict(cam1='rtsp://admin:z1x2c3v4b5@192.168.0.101:554', cam2='rtsp://admin:z1x2c3v4b5@192.168.0.102:554',
           cam3='rtsp://admin:z1x2c3v4b5@192.168.0.103:554', cam4='rtsp://admin:z1x2c3v4b5@192.168.0.104:554',
           cam5='rtsp://admin:z1x2c3v4b5@192.168.0.105:554', cam6='rtsp://admin:z1x2c3v4b5@192.168.0.106:554',
           cam7='rtsp://admin:z1x2c3v4b5@192.168.0.107:554', cam8='rtsp://admin:z1x2c3v4b5@192.168.0.108:554')

h = 0
w = 720
RoiCam = dict(cam1=(h, w, 0, 1280), cam2=(h, w, 150, 1280), cam3=(h, w, 150, 1200), cam4=(h, w, 280, 1280),
              cam5=(h, w, 0, 1280), cam6=(h, w, 0, 1280), cam7=(h, w, 0, 1280), cam8=(h, w, 0, 1280))

Ymin = dict(cam1=None, cam2=None, cam3=None, cam4=None, cam5=None, cam6=None, cam7=None, cam8=None)
Ymax = dict(cam1=None, cam2=None, cam3=None, cam4=None, cam5=None, cam6=None, cam7=None, cam8=None)

CAM = dict(cam1=None, cam2=None, cam3=None, cam4=None, cam5=None, cam6=None, cam7=None, cam8=None)

Frame = dict(cam1=None, cam2=None, cam3=None, cam4=None, cam5=None, cam6=None, cam7=None, cam8=None)
FrameTop = dict(cam1=None, cam2=None, cam3=None, cam4=None)
FrameBottom = dict(cam5=None, cam6=None, cam7=None, cam8=None)

# Mask = dict(cam1=None, cam2=None, cam3=None, cam4=None, cam5=None, cam6=None, cam7=None, cam8=None)
# _Frame = dict()
# _Frame = dict(cam1=None, cam2=None, cam3=None, cam4=None, cam5=None, cam6=None, cam7=None, cam8=None)
# Background = dict.fromkeys(position_camers)
# Background = dict(cam1=None, cam2=None, cam3=None, cam4=None, cam5=None, cam6=None, cam7=None, cam8=None)

# IMAGE = dict(top_original=None, buttom_original=None, top_original_from_net=None, buttom_original_from_net=None)

TOP_IMAGE_PATH = "/home/ast/Desktop/_IMAGE/NET/top.jpg"
BOTTOM_IMAGE_PATH = "/home/ast/Desktop/_IMAGE/NET/bottom.jpg"


def creatCam(url):
    try:
        cam = cv2.VideoCapture(url)
        return cam
    except:
        print('camera: ', url, ' not found')
        return False    

def camPreview(previewName, camID):
    cv2.namedWindow(previewName)
    cam = cv2.VideoCapture(camID)
    # if cam.isOpened():  # try to get the first frame
        # rval, frame = cam.read()
        # cv2.imshow(previewName, frame)
    # else:
        # rval = False
    while (True):
        _, frame = cam.read()
        frame = cv2.resize(frame,  (367, 200))
        cv2.imshow(previewName, frame)
        # print('---')
        if cv2.waitKey(1) & 0xFF == 27:
            break
        # print('--')

    cam.release()
    cv2.destroyWindow(previewName)
    # cv2.destroyAllWindows()

def camPreviewFromObject(previewName, camObj, position, _Frame):
    print(mes3, 'start thread: ', previewName, ', time', datetime.datetime.now(), mes3)
    _, frame = camObj.read()
    while True:
        _, frame = camObj.read()
        show = frame[:, :]
        # if not _lock.locked():
        if not Locks[position]:
            _Frame[previewName] = show

        # saveIMG(frame, path='/home/ast/Desktop/')
    #     cv2.imshow(previewName, frame)
    #     if cv2.waitKey(1) & 0xFF == 27:
    #         break
    # cv2.destroyAllWindows()


def createImg():
    print('start thread CreateImage',  ', time', datetime.datetime.now())
    while True:
        time.sleep(0.6)
        # _Frame = Frame.copy()
        lock.acquire()
        t1 = time.time()
        # v1, v2 = image.createIMG(_Frame)
        # формируем изображение
        IMG.deleteBackgroundDict(Frame, Background, Mask)
        # print('time_1: ', time.time()-t1)
        IMG.fishEyeDict(Frame)
        # print('time_2: ', time.time()-t1)
        IMG.roiCam(Frame, RoiCam)
        # print('time_3: ', time.time()-t1)
        IMG.getMaskDict(Frame, Mask)
        # print('time_4: ', time.time()-t1)
        IMG.cutImgMaskDict(Frame, Mask, Ymin, Ymax)
        # print('time_5: ', time.time()-t1)
        # print('Ymin: ', Ymin, '\nYmax: ', Ymax)
        IMG.lineUp(Frame, Ymin, Ymax)
        # v1, v2 = image.createIMG(Frame)
        # выводим изображение на экран
        v1 = np.concatenate((Frame['cam1'], Frame['cam2'], Frame['cam3'], Frame['cam4']), axis=1)
        v2 = np.concatenate((Mask['cam1'], Mask['cam2'], Mask['cam3'], Mask['cam4']), axis=1)
        lock.release()
        # v1 = v1[30:130, :]
        # print('time_6: ', time.time()-t1)
        # saveIMG(v1, path='/home/ast/Desktop/')
        cv2.imshow('qwe', v1)
        cv2.imshow('qweq', v2)
        print('time_10: ', time.time() - t1)
        print('=====================================')

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    cv2.destroyAllWindows()

def createImgSensorTopButtom(name, delay_sensor, _Frame, data):
    print('start thread: CREATE IMAGE ', name, ', time: ', datetime.datetime.now())
    try:
        while True:
            # SENSOR = TCP.Data_from_plc_siemens['sensor']
            if name == 'top':
                SENSOR = TCP.Data_from_plc_siemens['sensor_for_top']
                if SENSOR:
                    time.sleep(delay_sensor)
                    t1 = time.time()
                    # lock_t.acquire()
                    Locks['top'] = True
                    try:
                        TCP.Data_from_plc_siemens['write_sensor'] = False
                        for key in _Frame.keys():
                            frame = _Frame.get(key)
                            _Frame[key] = cv2.resize(frame, (1280, 720), interpolation=cv2.INTER_LINEAR)

                        IMG.fishEyeDict(_Frame)
                        # FOR TESTINGc==================================================
                        cam1 = _Frame.get('cam1')
                        cam2 = _Frame.get('cam2')
                        # cam3 = _Frame.get('cam3')
                        # cam4 = _Frame.get('cam4')
                        saveIMG(cam1, path='/home/ast/Desktop/FOR_TRAINING/')
                        saveIMG(cam2, path='/home/ast/Desktop/FOR_TRAINING/')
                        # saveIMG(cam3, path='/home/ast/Desktop/FOR_TRAINING/')
                        # saveIMG(cam4, path='/home/ast/Desktop/FOR_TRAINING/')
                        # ==============================================================
                        IMG.roiCam(_Frame, RoiCam)
                        IMG.find_board_region_dict(_Frame, Ymin, Ymax)
                        top_image = np.concatenate((_Frame['cam1'], _Frame['cam2'], _Frame['cam3'], _Frame['cam4']), axis=1)

                        top_image, x_min, x_max = IMG.cut_lenght(top_image)
                        SORT.check_boarf_param(_Frame, Ymin, Ymax, x_min, x_max, data)
                        # lock_t.release()
                        Locks['top'] = False


                        saveIMG(top_image, path='/home/ast/Desktop/')
                        cv2.imwrite(TOP_IMAGE_PATH, top_image)
                    except Exception as e:
                        print('top', e)
                    finally:
                        TCP.Data_from_plc_siemens['sensor_for_top'] = False
                        if data[0] != 1:
                            data[0] = 1
                        # if not NET.DATA_NET['start']:
                        #     NET.DATA_NET['start'] = True
                    print(mes2, name, ' time: ', (time.time() - t1).__round__(4), ', time:', datetime.datetime.now(), mes2)

            if name == 'bottom':
                SENSOR = TCP.Data_from_plc_siemens['sensor_for_bottom']
                if SENSOR:
                    time.sleep(delay_sensor)
                    t1 = time.time()
                    # lock_b.acquire()
                    TCP.Data_from_plc_siemens['sensor_for_bottom'] = False
                    Locks['bottom'] = True
                    try:
                        for key in _Frame.keys():
                            frame = _Frame.get(key)
                            _Frame[key] = cv2.resize(frame, (1280, 720), interpolation=cv2.INTER_LINEAR)

                        IMG.fishEyeDict(_Frame)
                        IMG.roiCam(_Frame, RoiCam)
                        IMG.find_board_region_dict(_Frame, Ymin, Ymax)
                        bottom_image = np.concatenate((_Frame['cam5'], _Frame['cam6'], _Frame['cam7'], _Frame['cam8']), axis=1)
                        Locks['bottom'] = False
                        # bottom_image, x_min, x_max = IMG.cut_lenght(bottom_image)
                        saveIMG(bottom_image, path='/home/ast/Desktop/')
                        cv2.imwrite(BOTTOM_IMAGE_PATH, bottom_image)
                    except Exception as e:
                        print('bottom', e)
                    finally:
                        TCP.Data_from_plc_siemens['sensor_for_bottom'] = False
                        # start object detection
                        if data[0] != 1:
                            data[0] = 1
                        # if not NET.DATA_NET['start']:
                        #     NET.DATA_NET['start'] = True
                    print(mes2, name, ' time: ', (time.time() - t1).__round__(4), ', time:', datetime.datetime.now(), mes2)


    except Exception as e:
        print('create object', e)
    finally:
        Locks['top'] = False
        Locks['bottom'] = False





if __name__ == "__main__":
    # верхние камеры
    t1 = threading.Thread(target=camPreviewFromObject, args=('cam1', creatCam(URL.get('cam1'))), daemon=True)
    t2 = threading.Thread(target=camPreviewFromObject, args=('cam2', creatCam(URL.get('cam2'))), daemon=True)
    t3 = threading.Thread(target=camPreviewFromObject, args=('cam3', creatCam(URL.get('cam3'))), daemon=True)
    t4 = threading.Thread(target=camPreviewFromObject, args=('cam4', creatCam(URL.get('cam4'))), daemon=True)
    # # нижние камеры
    # t5 = threading.Thread(target=camPreviewFromObject, args=('cam5', creatCam(URL.get('cam5'))), daemon=True)
    # t6 = threading.Thread(target=camPreviewFromObject, args=('cam6', creatCam(URL.get('cam6'))), daemon=True)
    # t7 = threading.Thread(target=camPreviewFromObject, args=('cam7', creatCam(URL.get('cam7'))), daemon=True)
    # t8 = threading.Thread(target=camPreviewFromObject, args=('cam8', creatCam(URL.get('cam8'))), daemon=True)
    # TCP
    conn = threading.Thread(target=tcp, daemon=True)
    # общее изображение
    # t00 = threading.Thread(target=createImgSensor)
    t00 = threading.Thread(target=createImg)
    #  старт потоков
    conn.start()

    t1.start()
    t2.start()
    t3.start()
    t4.start()
    # t5.start()
    # t6.start()
    # t7.start()
    # t8.start()
    t00.start()
    # t01.start()
    # завершение основных потоков
    t00.join()
    # t01.join()
    lock.release()
