from multiprocessing import Process
import socket
import time
from ssh2.session import Session
import os
import datetime
from termcolor import cprint
from threading import Thread

PATH_TO_SAVE_IMAGE = '/home/ast/Desktop/'


def send_file_scp(file):
    host = '192.168.0.121'
    user = 'ast'

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, 22))

    session = Session()
    session.handshake(sock)
    session.agent_auth(user)

    name = file.split('/').pop()
    fileinfo = os.stat(file)
    start_time = datetime.datetime.now()

    chan = session.scp_send('/home/ast/Desktop/%s' % name, fileinfo.st_mode & 0o777, fileinfo.st_size)

    with open(file, 'rb') as local_fh:
        for data in local_fh:
            chan.write(data)
    local_fh.close()
    res_time = datetime.datetime.now() - start_time
    cprint("copy file program on client server: %s" % res_time, 'yellow', attrs=['reverse'])


def server(name, port):
    file = PATH_TO_SAVE_IMAGE + name
    host = '0.0.0.0'
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    s.listen()
    # s.settimeout(3)
    cprint('wait connect %s' % name, 'yellow')
    cnt = 0
    while True:
        conn, addr = s.accept()
        ts = time.time()
        LEN = 0
        f = open(file, 'wb')
        while True:
            data = conn.recv(4096)
            LEN += len(data)
            f.write(data)
            if not data:
                cnt += 1
                f.close()
                print(cnt, ' ', name, 'LEN:', LEN, ': time: ', (time.time() - ts).__round__(4))
                break
        # print(data)
        time.sleep(0.5)


def server_sensor(port, flag):
    host = '0.0.0.0'
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    s.listen()
    # s.settimeout(3)
    print('wait connect SENSOR')
    cnt = 0
    while True:
        try:
            conn, addr = s.accept()
            ts = time.time()
            dt = None
            while True:
                message = dt
                dt = conn.recv(1024)
                if not dt:
                    cnt += 1
                    flag[0] = 1
                    cprint('%s SENSOR_mess: %s : time: %s' % (cnt, message, time.time() - ts), 'magenta', attrs=['reverse'])
                    break
        except Exception as e:
            cprint('ERROR server sensor: %s' % e, 'red', attrs=['reverse'])


def server_tcp(data):
    try:
        print('start.....')

        t00 = Thread(target=server_sensor, args=(10000, data))

        t1 = Thread(target=server, args=('cam1.jpg', 10001))
        t2 = Thread(target=server, args=('cam2.jpg', 10002))
        t3 = Thread(target=server, args=('cam3.jpg', 10003))
        t4 = Thread(target=server, args=('cam4.jpg', 10004))
        t5 = Thread(target=server, args=('cam5.jpg', 10005))
        t6 = Thread(target=server, args=('cam6.jpg', 10006))
        t7 = Thread(target=server, args=('cam7.jpg', 10007))
        t8 = Thread(target=server, args=('cam8.jpg', 10008))

        t11 = Thread(target=server, args=('cam1_b.jpg', 10011))
        t12 = Thread(target=server, args=('cam2_b.jpg', 10012))
        t13 = Thread(target=server, args=('cam3_b.jpg', 10013))
        t14 = Thread(target=server, args=('cam4_b.jpg', 10014))
        t15 = Thread(target=server, args=('cam5_b.jpg', 10015))
        t16 = Thread(target=server, args=('cam6_b.jpg', 10016))
        t17 = Thread(target=server, args=('cam7_b.jpg', 10017))
        t18 = Thread(target=server, args=('cam8_b.jpg', 10018))

        t00.start()
        t1.start()
        t2.start()
        t3.start()
        t4.start()
        t5.start()
        t6.start()
        t7.start()
        t8.start()
        t11.start()
        t12.start()
        t13.start()
        t14.start()
        t15.start()
        t16.start()
        t17.start()
        t18.start()

        t00.join()
        t1.join()
        t2.join()
        t3.join()
        t4.join()
        t5.join()
        t6.join()
        t7.join()
        t8.join()
        t11.join()
        t12.join()
        t13.join()
        t14.join()
        t15.join()
        t16.join()
        t17.join()
        t18.join()

        print('end.....')

    finally:
        os.system('killall -s 9 python3')
        print('fin def')


if __name__ == '__main__':
    data = [0, 0, 0, 0, 0]
    try:
        # programm = '/home/ast/Desktop/GIT/PYTHON/MODULS/client.py'
        programm = '/home/ast/Desktop/GIT/PYTHON/ServClient.py'
        send_file_scp(programm)
        # serv_1('/home/ast/Desktop/cam5.jpg', 9001)
        tcp_server = Process(target=server_tcp, args=(data,))
        tcp_server.start()
        tcp_server.join()
    except Exception as e:
        os.system('killall -s 9 python3')
        print('Exception')
    finally:
        os.system('killall -s 9 python3')
        print('fin main')