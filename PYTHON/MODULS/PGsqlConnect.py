import datetime
import time
import logging

import psycopg2
from psycopg2.extras import NamedTupleCursor

from MODULS.Sort import RECIPE as RECEPIES

Data_layer_from_plc = dict(Length=0, Width=0, Height=0, Sort=0, Volume=0, karman=0)
Data_board_from_plc = dict(Length=0, Width=0, Height=0, Sort=0, karman=0)
Data_palet_from_plc = dict(Length=0, Width=0, Height=0, Sort=0, Volume=0, karman=0)


def start_connection():
    global connection, cursor
    try:
        connection = psycopg2.connect(user="admin",
                                      password="z1x2c3v4b5",
                                      host="127.0.0.1",
                                      port="5432",
                                      database="ast")
        cursor = connection.cursor(cursor_factory=NamedTupleCursor)
        # cursor = connection.cursor()
        # Print PostgreSQL Connection properties
        # print(connection.get_dsn_parameters(), "\n")
        logging.debug(connection.get_dsn_parameters())
        # Print PostgreSQL version
        cursor.execute("SELECT version();")
        record = cursor.fetchone()
        # print("You are connected to - ", record, "\n")
    except (Exception, psycopg2.Error) as error:
        print("Error while connecting to PostgreSQL", error)

    return connection, cursor


def close_connection(connection, cursor):
    # #closing database connection.
    if connection:
        cursor.close()
        connection.close()
        # print("PostgreSQL connection is closed")


def status(connection, cursor):
    # write status in data base
    postgres_insert_query = """ UPDATE machine_infos SET status_connect=True WHERE status_connect=False"""
    cursor.execute(postgres_insert_query)
    try:
        connection.commit()
    except (Exception, psycopg2.Error) as error:
        print("Error status", error)


def read_recipe(connection, cursor):
    try:
        postgres_select = """ SELECT * FROM recipe_fir where selected=True"""

        cursor.execute(postgres_select)
        res = cursor.fetchone()

        #    print(res.healthyKnotsOnStrataSize0s)
        RECEPIES['suchki_type01']['enable'] = res.healthyKnotsEnable
        RECEPIES['suchki_type01']['plastevye']['OTB']['size'] = res.healthyKnotsOnStrataSize0s
        RECEPIES['suchki_type01']['plastevye']['OTB']['value'] = res.healthyKnotsOnStrataAmount0s
        RECEPIES['suchki_type01']['plastevye']['A']['size'] = res.healthyKnotsOnStrataSize1s
        RECEPIES['suchki_type01']['plastevye']['A']['value'] = res.healthyKnotsOnStrataAmount1s
        RECEPIES['suchki_type01']['plastevye']['B']['size'] = res.healthyKnotsOnStrataSize2s
        RECEPIES['suchki_type01']['plastevye']['B']['value'] = res.healthyKnotsOnStrataAmount2s
        RECEPIES['suchki_type01']['plastevye']['C']['size'] = res.healthyKnotsOnStrataSize3s
        RECEPIES['suchki_type01']['plastevye']['C']['value'] = res.healthyKnotsOnStrataAmount3s
        RECEPIES['suchki_type01']['plastevye']['D']['size'] = res.healthyKnotsOnStrataSize4s
        RECEPIES['suchki_type01']['plastevye']['D']['value'] = res.healthyKnotsOnStrataAmount4s

        RECEPIES['suchki_type01']['kromochnye_up40']['OTB']['size'] = res.healthyKnotsOnEdgeTo40Size0s
        RECEPIES['suchki_type01']['kromochnye_up40']['OTB']['value'] = res.healthyKnotsOnEdgeTo40Amount0s
        RECEPIES['suchki_type01']['kromochnye_up40']['A']['size'] = res.healthyKnotsOnEdgeTo40Size1s
        RECEPIES['suchki_type01']['kromochnye_up40']['A']['value'] = res.healthyKnotsOnEdgeTo40Amount1s
        RECEPIES['suchki_type01']['kromochnye_up40']['B']['size'] = res.healthyKnotsOnEdgeTo40Size2s
        RECEPIES['suchki_type01']['kromochnye_up40']['B']['value'] = res.healthyKnotsOnEdgeTo40Amount2s
        RECEPIES['suchki_type01']['kromochnye_up40']['C']['size'] = res.healthyKnotsOnEdgeTo40Size3s
        RECEPIES['suchki_type01']['kromochnye_up40']['C']['value'] = res.healthyKnotsOnEdgeTo40Amount3s
        RECEPIES['suchki_type01']['kromochnye_up40']['D']['size'] = res.healthyKnotsOnEdgeTo40Size4s
        RECEPIES['suchki_type01']['kromochnye_up40']['D']['value'] = res.healthyKnotsOnEdgeTo40Amount4s

        RECEPIES['suchki_type01']['kromochnye_over40']['OTB']['size'] = res.healthyKnotsOnEdgeFrom40Size0s
        RECEPIES['suchki_type01']['kromochnye_over40']['OTB']['value'] = res.healthyKnotsOnEdgeFrom40Amount0s
        RECEPIES['suchki_type01']['kromochnye_over40']['A']['size'] = res.healthyKnotsOnEdgeFrom40Size1s
        RECEPIES['suchki_type01']['kromochnye_over40']['A']['value'] = res.healthyKnotsOnEdgeFrom40Amount1s
        RECEPIES['suchki_type01']['kromochnye_over40']['B']['size'] = res.healthyKnotsOnEdgeFrom40Size2s
        RECEPIES['suchki_type01']['kromochnye_over40']['B']['value'] = res.healthyKnotsOnEdgeFrom40Amount2s
        RECEPIES['suchki_type01']['kromochnye_over40']['C']['size'] = res.healthyKnotsOnEdgeFrom40Size3s
        RECEPIES['suchki_type01']['kromochnye_over40']['C']['value'] = res.healthyKnotsOnEdgeFrom40Amount3s
        RECEPIES['suchki_type01']['kromochnye_over40']['D']['size'] = res.healthyKnotsOnEdgeFrom40Size4s
        RECEPIES['suchki_type01']['kromochnye_over40']['D']['value'] = res.healthyKnotsOnEdgeFrom40Amount4s

        #   suchki_type02
        RECEPIES['suchki_type02']['enable'] = res.partHealthyKnotsEnable
        RECEPIES['suchki_type02']['plastevye']['OTB']['size'] = res.partHealthyKnotsOnStrataSize0s
        RECEPIES['suchki_type02']['plastevye']['OTB']['value'] = res.partHealthyKnotsOnStrataAmount0s
        RECEPIES['suchki_type02']['plastevye']['A']['size'] = res.partHealthyKnotsOnStrataSize1s
        RECEPIES['suchki_type02']['plastevye']['A']['value'] = res.partHealthyKnotsOnStrataAmount1s
        RECEPIES['suchki_type02']['plastevye']['B']['size'] = res.partHealthyKnotsOnStrataSize2s
        RECEPIES['suchki_type02']['plastevye']['B']['value'] = res.partHealthyKnotsOnStrataAmount2s
        RECEPIES['suchki_type02']['plastevye']['C']['size'] = res.partHealthyKnotsOnStrataSize3s
        RECEPIES['suchki_type02']['plastevye']['C']['value'] = res.partHealthyKnotsOnStrataAmount3s
        RECEPIES['suchki_type02']['plastevye']['D']['size'] = res.partHealthyKnotsOnStrataSize4s
        RECEPIES['suchki_type02']['plastevye']['D']['value'] = res.partHealthyKnotsOnStrataAmount4s

        RECEPIES['suchki_type02']['kromochnye_up40']['OTB']['size'] = res.partHealthyKnotsOnEdgeTo40Size0s
        RECEPIES['suchki_type02']['kromochnye_up40']['OTB']['value'] = res.partHealthyKnotsOnEdgeTo40Amount0s
        RECEPIES['suchki_type02']['kromochnye_up40']['A']['size'] = res.partHealthyKnotsOnEdgeTo40Size1s
        RECEPIES['suchki_type02']['kromochnye_up40']['A']['value'] = res.partHealthyKnotsOnEdgeTo40Amount1s
        RECEPIES['suchki_type02']['kromochnye_up40']['B']['size'] = res.partHealthyKnotsOnEdgeTo40Size2s
        RECEPIES['suchki_type02']['kromochnye_up40']['B']['value'] = res.partHealthyKnotsOnEdgeTo40Amount2s
        RECEPIES['suchki_type02']['kromochnye_up40']['C']['size'] = res.partHealthyKnotsOnEdgeTo40Size3s
        RECEPIES['suchki_type02']['kromochnye_up40']['C']['value'] = res.partHealthyKnotsOnEdgeTo40Amount3s
        RECEPIES['suchki_type02']['kromochnye_up40']['D']['size'] = res.partHealthyKnotsOnEdgeTo40Size4s
        RECEPIES['suchki_type02']['kromochnye_up40']['D']['value'] = res.partHealthyKnotsOnEdgeTo40Amount4s

        RECEPIES['suchki_type02']['kromochnye_over40']['OTB']['size'] = res.partHealthyKnotsOnEdgeFrom40Size0s
        RECEPIES['suchki_type02']['kromochnye_over40']['OTB']['value'] = res.partHealthyKnotsOnEdgeFrom40Amount0s
        RECEPIES['suchki_type02']['kromochnye_over40']['A']['size'] = res.partHealthyKnotsOnEdgeFrom40Size1s
        RECEPIES['suchki_type02']['kromochnye_over40']['A']['value'] = res.partHealthyKnotsOnEdgeFrom40Amount1s
        RECEPIES['suchki_type02']['kromochnye_over40']['B']['size'] = res.partHealthyKnotsOnEdgeFrom40Size2s
        RECEPIES['suchki_type02']['kromochnye_over40']['B']['value'] = res.partHealthyKnotsOnEdgeFrom40Amount2s
        RECEPIES['suchki_type02']['kromochnye_over40']['C']['size'] = res.partHealthyKnotsOnEdgeFrom40Size3s
        RECEPIES['suchki_type02']['kromochnye_over40']['C']['value'] = res.partHealthyKnotsOnEdgeFrom40Amount3s
        RECEPIES['suchki_type02']['kromochnye_over40']['D']['size'] = res.partHealthyKnotsOnEdgeFrom40Size4s
        RECEPIES['suchki_type02']['kromochnye_over40']['D']['value'] = res.partHealthyKnotsOnEdgeFrom40Amount4s

        # suchki_type03
        RECEPIES['suchki_type03']['enable'] = res.badKnotsEnable
        RECEPIES['suchki_type03']['OTB']['size'] = res.badKnotsSize0s
        RECEPIES['suchki_type03']['OTB']['value'] = res.badKnotsAmount0s
        RECEPIES['suchki_type03']['A']['size'] = res.badKnotsSize1s
        RECEPIES['suchki_type03']['A']['value'] = res.badKnotsAmount1s
        RECEPIES['suchki_type03']['B']['size'] = res.badKnotsSize2s
        RECEPIES['suchki_type03']['B']['value'] = res.badKnotsAmount2s
        RECEPIES['suchki_type03']['C']['size'] = res.badKnotsSize3s
        RECEPIES['suchki_type03']['C']['value'] = res.badKnotsAmount3s
        RECEPIES['suchki_type03']['D']['size'] = res.badKnotsSize4s
        RECEPIES['suchki_type03']['D']['value'] = res.badKnotsAmount4s

        # treschina_type01
        RECEPIES['treschina_type01']['enable'] = res.strataEdgeOutOneEdgeCrackEnable
        RECEPIES['treschina_type01']['nonDeep']['OTB']['size'] = res.strataEdgeOutOneEdgeNonDeepCrackSize0s
        RECEPIES['treschina_type01']['nonDeep']['A']['size'] = res.strataEdgeOutOneEdgeNonDeepCrackSize1s
        RECEPIES['treschina_type01']['nonDeep']['B']['size'] = res.strataEdgeOutOneEdgeNonDeepCrackSize2s
        RECEPIES['treschina_type01']['nonDeep']['C']['size'] = res.strataEdgeOutOneEdgeNonDeepCrackSize3s
        RECEPIES['treschina_type01']['nonDeep']['D']['size'] = res.strataEdgeOutOneEdgeNonDeepCrackSize4s

        RECEPIES['treschina_type01']['deep']['OTB']['size'] = res.strataEdgeOutOneEdgeDeepCrackSize0s
        RECEPIES['treschina_type01']['deep']['A']['size'] = res.strataEdgeOutOneEdgeDeepCrackSize1s
        RECEPIES['treschina_type01']['deep']['B']['size'] = res.strataEdgeOutOneEdgeDeepCrackSize2s
        RECEPIES['treschina_type01']['deep']['C']['size'] = res.strataEdgeOutOneEdgeDeepCrackSize3s
        RECEPIES['treschina_type01']['deep']['D']['size'] = res.strataEdgeOutOneEdgeDeepCrackSize4s

        # treschina_type02
        RECEPIES['treschina_type02']['enable'] = res.strataEdgeThrouthCrackEnable
        RECEPIES['treschina_type02']['OTB']['size'] = res.strataEdgeThrouthCrackSize0s
        RECEPIES['treschina_type02']['A']['size'] = res.strataEdgeThrouthCrackSize1s
        RECEPIES['treschina_type02']['B']['size'] = res.strataEdgeThrouthCrackSize2s
        RECEPIES['treschina_type02']['C']['size'] = res.strataEdgeThrouthCrackSize3s
        RECEPIES['treschina_type02']['D']['size'] = res.strataEdgeThrouthCrackSize4s

        # prorost
        RECEPIES['prorost']['enable'] = res.sweepEnable
        RECEPIES['prorost']['width']['OTB']['size'] = res.sweepWidthSize0s
        RECEPIES['prorost']['width']['A']['size'] = res.sweepWidthSize1s
        RECEPIES['prorost']['width']['B']['size'] = res.sweepWidthSize2s
        RECEPIES['prorost']['width']['C']['size'] = res.sweepWidthSize3s
        RECEPIES['prorost']['width']['D']['size'] = res.sweepWidthSize4s
        RECEPIES['prorost']['length']['OTB']['size'] = res.sweepLengthSize0s
        RECEPIES['prorost']['length']['A']['size'] = res.sweepLengthSize1s
        RECEPIES['prorost']['length']['B']['size'] = res.sweepLengthSize2s
        RECEPIES['prorost']['length']['C']['size'] = res.sweepLengthSize3s
        RECEPIES['prorost']['length']['D']['size'] = res.sweepLengthSize4s

        # zabolon
        RECEPIES['zabolon']['enable'] = res.sapwoodEnable
        RECEPIES['zabolon']['OTB']['size'] = res.sapwoodSize0s
        RECEPIES['zabolon']['A']['size'] = res.sapwoodSize1s
        RECEPIES['zabolon']['B']['size'] = res.sapwoodSize2s
        RECEPIES['zabolon']['C']['size'] = res.sapwoodSize3s
        RECEPIES['zabolon']['D']['size'] = res.sapwoodSize4s

        # plesen
        RECEPIES['plesen']['enable'] = res.moldEnable
        RECEPIES['plesen']['OTB']['size'] = res.moldSize0s
        RECEPIES['plesen']['A']['size'] = res.moldSize1s
        RECEPIES['plesen']['B']['size'] = res.moldSize2s
        RECEPIES['plesen']['C']['size'] = res.moldSize3s
        RECEPIES['plesen']['D']['size'] = res.moldSize4s

        # mechanicDamage
        RECEPIES['mechanicDamage']['enable'] = res.mechanicalDamageEnable
        RECEPIES['mechanicDamage']['OTB']['size'] = res.mechanicalDamageSize0s
        RECEPIES['mechanicDamage']['A']['size'] = res.mechanicalDamageSize1s
        RECEPIES['mechanicDamage']['B']['size'] = res.mechanicalDamageSize2s
        RECEPIES['mechanicDamage']['C']['size'] = res.mechanicalDamageSize3s
        RECEPIES['mechanicDamage']['D']['size'] = res.mechanicalDamageSize4s

        # gnil
        RECEPIES['gnil']['enable'] = res.rotEnable
        RECEPIES['gnil']['OTB']['size'] = res.rotSize0s
        RECEPIES['gnil']['A']['size'] = res.rotSize1s
        RECEPIES['gnil']['B']['size'] = res.rotSize2s
        RECEPIES['gnil']['C']['size'] = res.rotSize3s
        RECEPIES['gnil']['D']['size'] = res.rotSize4s

        # sineva
        RECEPIES['sineva']['enable'] = res.blueEnable
        RECEPIES['sineva']['OTB']['size'] = res.blueSize0s
        RECEPIES['sineva']['A']['size'] = res.blueSize1s
        RECEPIES['sineva']['B']['size'] = res.blueSize2s
        RECEPIES['sineva']['C']['size'] = res.blueSize3s
        RECEPIES['sineva']['D']['size'] = res.blueSize4s

        # obzol
        RECEPIES['obzol']['enable'] = res.barkEnable
        RECEPIES['obzol']['widthBoard_up40']['width_type01']['OTB']['size'] = res.barkTo40FromEdgeSize0s
        RECEPIES['obzol']['widthBoard_up40']['width_type01']['A']['size'] = res.barkTo40FromEdgeSize1s
        RECEPIES['obzol']['widthBoard_up40']['width_type01']['B']['size'] = res.barkTo40FromEdgeSize2s
        RECEPIES['obzol']['widthBoard_up40']['width_type01']['C']['size'] = res.barkTo40FromEdgeSize3s
        RECEPIES['obzol']['widthBoard_up40']['width_type01']['D']['size'] = res.barkTo40FromEdgeSize4s

        RECEPIES['obzol']['widthBoard_up40']['width_type02']['OTB']['size'] = res.barkTo40OnEdgeSize0s
        RECEPIES['obzol']['widthBoard_up40']['width_type02']['A']['size'] = res.barkTo40OnEdgeSize1s
        RECEPIES['obzol']['widthBoard_up40']['width_type02']['B']['size'] = res.barkTo40OnEdgeSize2s
        RECEPIES['obzol']['widthBoard_up40']['width_type02']['C']['size'] = res.barkTo40OnEdgeSize3s
        RECEPIES['obzol']['widthBoard_up40']['width_type02']['D']['size'] = res.barkTo40OnEdgeSize4s

        RECEPIES['obzol']['widthBoard_up40']['length_type03']['OTB']['size'] = res.barkTo40ExtLengthSize0s
        RECEPIES['obzol']['widthBoard_up40']['length_type03']['A']['size'] = res.barkTo40ExtLengthSize1s
        RECEPIES['obzol']['widthBoard_up40']['length_type03']['B']['size'] = res.barkTo40ExtLengthSize2s
        RECEPIES['obzol']['widthBoard_up40']['length_type03']['C']['size'] = res.barkTo40ExtLengthSize3s
        RECEPIES['obzol']['widthBoard_up40']['length_type03']['D']['size'] = res.barkTo40ExtLengthSize4s

        RECEPIES['obzol']['widthBoard_over40']['width_type01']['OTB']['size'] = res.barkFrom40FromEdgeSize0s
        RECEPIES['obzol']['widthBoard_over40']['width_type01']['A']['size'] = res.barkFrom40FromEdgeSize1s
        RECEPIES['obzol']['widthBoard_over40']['width_type01']['B']['size'] = res.barkFrom40FromEdgeSize2s
        RECEPIES['obzol']['widthBoard_over40']['width_type01']['C']['size'] = res.barkFrom40FromEdgeSize3s
        RECEPIES['obzol']['widthBoard_over40']['width_type01']['D']['size'] = res.barkFrom40FromEdgeSize4s

        RECEPIES['obzol']['widthBoard_over40']['width_type02']['OTB']['size'] = res.barkFrom40OnEdgeSize0s
        RECEPIES['obzol']['widthBoard_over40']['width_type02']['A']['size'] = res.barkFrom40OnEdgeSize1s
        RECEPIES['obzol']['widthBoard_over40']['width_type02']['B']['size'] = res.barkFrom40OnEdgeSize2s
        RECEPIES['obzol']['widthBoard_over40']['width_type02']['C']['size'] = res.barkFrom40OnEdgeSize3s
        RECEPIES['obzol']['widthBoard_over40']['width_type02']['D']['size'] = res.barkFrom40OnEdgeSize4s

        RECEPIES['obzol']['widthBoard_over40']['length_type03']['OTB']['size'] = res.barkFrom40ExtLengthSize0s
        RECEPIES['obzol']['widthBoard_over40']['length_type03']['A']['size'] = res.barkFrom40ExtLengthSize1s
        RECEPIES['obzol']['widthBoard_over40']['length_type03']['B']['size'] = res.barkFrom40ExtLengthSize2s
        RECEPIES['obzol']['widthBoard_over40']['length_type03']['C']['size'] = res.barkFrom40ExtLengthSize3s
        RECEPIES['obzol']['widthBoard_over40']['length_type03']['D']['size'] = res.barkFrom40ExtLengthSize4s
    except Exception as e:
        pass
    finally:
        pass


def insert_input_volum(connection, cursor, flow1=0, flow2=0, sum=0, time=datetime.datetime.now()):
    postgres_insert_query = """ INSERT INTO input_volume ("flow1", "flow2", "SumVolume","Time") 
        VALUES (%s,%s,%s,%s)"""
    record_to_insert = (flow1, flow2, sum, time)
    cursor.execute(postgres_insert_query, record_to_insert)
    try:
        connection.commit()
        count = cursor.rowcount
        # print(count, "DB - Record insert_input_volum: ", datetime.datetime.now())
        logging.debug("DB - Record insert_input_volum")
        result = 1
    except (Exception, psycopg2.Error) as error:
        print("Error DB - Record insert_input_volum", error)
        result = 0
    return result


def insert_board(connection, cursor, length=0, width=0, height=0, time=datetime.datetime.now(), volume=0, sort=0,
                 type='Хвоя', suchki=0, treschiny=0, svievatist=0, prorost=0, zabolon=0, plesen=0, gnil=0,
                 sineva=0, obzol=0, mech=0, piatna=0, smena=0, priznak_size=0):
    postgres_insert_query = """ INSERT INTO data_boards ("Length", "Width", "Height","Time","Sort","Volume",\
        "Type","Suchki","Treschiny","Svievatist", "Prorost","Zabolon","Plesen","Gnil",\
        "Sineva","Obzol","Mech","Piatna", "Smena", "Priznak") 
        VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
    record_to_insert = (length, width, height, time, sort, volume, type, suchki,
                        treschiny, svievatist, prorost, zabolon, plesen, gnil, sineva, obzol, mech, piatna,
                        smena, priznak_size)
    cursor.execute(postgres_insert_query, record_to_insert)
    try:
        connection.commit()
        count = cursor.rowcount
        # print(count, "DB - Record data_boards: ", datetime.datetime.now())
        logging.debug("DB - Record data_boards")
        result = 1
    except (Exception, psycopg2.Error) as error:
        print("Error Record in data_boards", error)
        result = 0
    return result


def insert_layer(connection, cursor, length=0, width=0, height=0, time=datetime.datetime.now(),
                 sort=0, ColBoard=0, karman=0, priznak_size=0):
    postgres_insert_query = """ INSERT INTO statistic_politers
    ("Length", "Width", "Height","Time","sort","ColBoard", "karman", "Priznak") VALUES (%s,%s,%s,%s,%s,%s,%s, %s)"""
    record_to_insert = (length, width, height, time, sort, ColBoard, karman, priznak_size)
    cursor.execute(postgres_insert_query, record_to_insert)
    try:
        connection.commit()
        count = cursor.rowcount
        # print(count, "DB - Record politers: ", datetime.datetime.now())
        logging.debug("DB - Record politers")
        result = 1
    except (Exception, psycopg2.Error) as error:
        print("Error Record in statistic_politers", error)
        result = 0
    return result


def insert_polet(connection, cursor, length=0, width=0, height=0, time=datetime.datetime.now(), ColBoard=0):
    postgres_insert_query = """ INSERT INTO paletka_board
    ("Length", "Width", "Height","Time", "ColBoard") VALUES (%s,%s,%s,%s,%s)"""
    record_to_insert = (length, width, height, time, ColBoard)
    cursor.execute(postgres_insert_query, record_to_insert)
    try:
        connection.commit()
        count = cursor.rowcount
        # print(count, "DB - Record palet")
        logging.debug("DB - Record palet")
        return 1
    except (Exception, psycopg2.Error) as error:
        print("Error Record in palet_board", error)
        return 0


def smena():
    """
    :return:
        1 если 1 смена. работа с 8:00 до 16:30
        2 если 2 смена. работа с 16:30 до 01:00
    """
    t_hour = datetime.datetime.now().time().hour
    t_minute = datetime.datetime.now().time().minute

    if t_hour >= 16 and t_minute >= 30:
        return 2
    else:
        return 1


def priznak(h, w, l, sort):
    """
    Функция возвращает признак, относящий доску к стандартному ряду типоразмеров
    :param h: толщина
    :param w: ширина
    :param l: длинна
    :return: признак
    """
    global _h, _w

    _h = 0
    _w = 0
    H = [16, 19, 22, 25, 32, 40, 44, 50, 60]
    W = [50, 75, 100, 125, 150, 175, 200, 225]
    L = [3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0]
    S = [0, 1, 2, 3, 4]
    try:
        for i in range(len(H)):
            if h >= H[i] and h < H[i + 1]:
                _h = H[i]
        for j in range(len(W)):
            if w >= W[j] and h < W[j + 1]:
                _w = W[j]
    except Exception as e:
        print(e)
    K = [_h, _w, l, sort]

    # create array
    KEY = []
    for q in range(len(H)):
        for w in range(len(W)):
            for e in range(len(L)):
                for s in range(len(S)):
                    KEY.append([H[q], W[w], L[e], S[s]])

    # print(K, KEY[0])
    #
    for i in range(len(KEY)):
        if KEY[i] == K:
            return i
    return 999


def parse_date_layer(TCP_BUFER):
    Data_layer_from_plc['Length'] = TCP_BUFER[21] / 100
    Data_layer_from_plc['Width'] = TCP_BUFER[20] / 100
    Data_layer_from_plc['Height'] = TCP_BUFER[22] / 100
    Data_layer_from_plc['Sort'] = TCP_BUFER[19]
    Data_layer_from_plc['Volume'] = TCP_BUFER[23]
    Data_layer_from_plc['karman'] = TCP_BUFER[18]


def parse_date_board(TCP_BUFER):
    Data_board_from_plc['Length'] = TCP_BUFER[13] / 10
    Data_board_from_plc['Height'] = TCP_BUFER[14] / 100
    Data_board_from_plc['Width'] = TCP_BUFER[15] / 100
    Data_board_from_plc['Sort'] = TCP_BUFER[16]
    Data_board_from_plc['karman'] = TCP_BUFER[17]


def parse_date_palet(TCP_BUFER):
    Data_palet_from_plc['Length'] = TCP_BUFER[26] / 100
    Data_palet_from_plc['Height'] = TCP_BUFER[28] / 100
    Data_palet_from_plc['Width'] = TCP_BUFER[27] / 100
    Data_palet_from_plc['Sort'] = TCP_BUFER[25]
    Data_palet_from_plc['Volume'] = TCP_BUFER[29]
    Data_palet_from_plc['karman'] = TCP_BUFER[24]


def rw_data_base(tcp, flag, ftp):
    # tcp[1], flag[1] - rw_board
    # tcp[2], flag[2] - rw_layout
    # tcp[3], flag[3] - rw_palet
    #         flag[4] - rw_input
    global connection, cursor, rw_board, rw_layer, rw_palet, rw_input
    rw_board, rw_layer, rw_palet, rw_input = 0, 0, 0, 0

    print('start thread: DataBase', ', time', datetime.datetime.now())

    while True:
        # print('write1', rw_board, tcp[1], tcp[2], tcp[3])
        try:
            start_time = time.time()
            connection, cursor = start_connection()
            #  записываем значение доски
            if tcp[1] == 1 and rw_board == 0:
                # parse data
                parse_date_board(tcp)
                retValB = insert_board(
                    connection, cursor,
                    length=Data_board_from_plc['Length'],
                    width=Data_board_from_plc['Width'],
                    height=Data_board_from_plc['Height'],
                    time=datetime.datetime.now(),
                    sort=Data_board_from_plc['Sort'],
                    smena=smena(),
                    priznak_size=priznak(Data_board_from_plc['Height'],
                                         Data_board_from_plc['Width'],
                                         Data_board_from_plc['Length'],
                                         Data_board_from_plc['Sort']))
                if retValB:
                    rw_board = 1

            # записываем данные слоев
            if tcp[2] == 1 and rw_layer == 0:
                parse_date_layer(tcp)
                retVal = insert_layer(
                    connection, cursor,
                    length=Data_layer_from_plc['Length'],
                    width=Data_layer_from_plc['Width'],
                    height=Data_layer_from_plc['Height'],
                    time=datetime.datetime.now(),
                    sort=Data_layer_from_plc['Sort'],
                    ColBoard=Data_layer_from_plc['Volume'],
                    karman=Data_layer_from_plc['karman'],
                    priznak_size=priznak(Data_layer_from_plc['Height'],
                                         Data_layer_from_plc['Width'],
                                         Data_layer_from_plc['Length'] / 10,
                                         Data_layer_from_plc['Sort'])
                )
                if retVal != 0:
                    rw_layer = 1
            # write data polet
            if tcp[7] == 1 and rw_palet == 0:
                parse_date_palet(tcp)
                retValP = insert_polet(
                    connection, cursor,
                    length=Data_palet_from_plc['Length'],
                    width=Data_palet_from_plc['Width'],
                    height=Data_palet_from_plc['Height'],
                    time=datetime.datetime.now(),
                    # sort=Data_palet_from_plc['Sort'],
                    ColBoard=Data_palet_from_plc['Volume'],
                )
                if retValP != 0:
                    rw_palet = 1
            # ========================================================================================================
            # reset flag
            # ========================================================================================================
            if tcp[1] == 0:
                rw_board = 0
            if tcp[2] == 0:
                rw_layer = 0
            if tcp[7] == 0:
                rw_palet = 0
            # ========================================================================================================
            # input volum
            if flag[4]:
                result = insert_input_volum(connection, cursor,
                                            flow1=ftp[0],
                                            flow2=ftp[1],
                                            sum=ftp[2],
                                            time=datetime.datetime.now())
                if result != 0:
                    flag[4] = 0

            # ========================================================================================================
            # write status
            # ========================================================================================================
            status(connection, cursor)
            # ========================================================================================================
            # sensor(connection, cursor)
            # ========================================================================================================
            flag[1] = rw_board
            flag[2] = rw_layer
            flag[3] = rw_palet
            # print(flag[3])
            # ========================================================================================================
            # print('Cycle DB', time.time()-start_time)
        except Exception as e:
            print('rw_data base_error: ', e, '|time: ', datetime.datetime.now())
        finally:
            time.sleep(0.1)
            # закрываем соединение
            close_connection(connection, cursor)

if __name__ == '__main__':
    pass