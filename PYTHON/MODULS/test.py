from __future__ import print_function

import threading
from statistics import median

import cv2
import numpy as np
import time

from PIL import Image
from numba import jit

z        img_mask = img_mask[0]

    img_names = glob(img_mask)
    debug_dir = args.get('--debug')
    if not os.path.isdir(debug_dir):
        os.mkdir(debug_dir)
    square_size = float(args.get('--square_size'))

    pattern_size = (9, 6)
    # pattern_size = (19, 9)

    pattern_points = np.zeros((np.prod(pattern_size), 3), np.float32)
    pattern_points[:, :2] = np.indices(pattern_size).T.reshape(-1, 2)
    pattern_points *= square_size
    obj_points = []
    img_points = []
    h, w = 0, 0
    img_names_undistort = []
    for fn in img_names:
        print('processing %s... ' % fn, end='')
        img = cv2.imread(fn, 0)
        if img is None:
            print("Failed to load", fn)
            continue
        h, w = img.shape[:2]
        found, corners = cv2.findChessboardCorners(img, pattern_size)
        if found:
            term = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT, 30, 0.1)
            cv2.cornerSubPix(img, corners, (5, 5), (-1, -1), term)
        if not found:
            print('chessboard not found')
            continue
        img_points.append(corners.reshape(-1, 2))
        obj_points.append(pattern_points)
        print('ok')
    rms, camera_matrix, dist_coefs, rvecs, tvecs = cv2.calibrateCamera(obj_points, img_points, (w, h), None, None)
    dist_coefs = dist_coefs.ravel()
    print("\nRMS:", rms)
    print("camera matrix:\n", camera_matrix)
    print("distortion coefficients: ", dist_coefs)
    cv2.destroyAllWindows()
    return camera_matrix, dist_coefs


def calibration_v2():
    import numpy as np
    import cv2
    import glob

    n = 9
    m = 6
    # termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((9 * 6, 3), np.float32)
    objp[:, :2] = np.mgrid[0:6, 0:9].T.reshape(-1, 2)

    # Arrays to store object points and image points from all the images.
    objpoints = []  # 3d point in real world space
    imgpoints = []  # 2d points in image plane.

    images = glob.glob('C:/Users/Mikalai/Desktop/qwe/png_ivc/*.bmp')
    print(images)
    for fname in images:
        img = cv2.imread(fname)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # Find the chess board corners
        ret, corners = cv2.findChessboardCorners(gray, (6, 9), None)
        # print('2', ret, corners)
        # If found, add object points, image points (after refining them)
        if ret == True:
            objpoints.append(objp)
            cv2.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)
            imgpoints.append(corners)
            # Draw and display the corners
            cv2.drawChessboardCorners(img, (6, 9), corners, ret)
            cv2.imshow('img', img)
            print('__')
            cv2.waitKey()
    cv2.destroyAllWindows()


def get_points():
    import numpy as np
    import cv2
    import glob

    n = 9
    m = 6

    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((n * m, 3), np.float32)
    objp[:, :2] = np.mgrid[0:m, 0:n].T.reshape(-1, 2)
    # Arrays to store object points and image points from all the images.
    objpoints = []  # 3d points in real world space
    imgpoints = []  # 2d points in image plane.
    # Make a list of calibration images
    # images = glob.glob('C:/Users/Mikalai/Desktop/qwe/png/*.png')
    # images = glob.glob('C:/Users/Mikalai/Desktop/qwe/png_2/*.png')

    # images = glob.glob('C:/Users/Mikalai/Desktop/qwe/*.bmp')

    images = glob.glob('C:/Users/Mikalai/Desktop/qwe/png_ivc/*.bmp')

    # Step through the list and search for chessboard corners
    for fname in images:
        print(fname)
        img = cv2.imread(fname)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # Find the chess board corners
        ret, corners = cv2.findChessboardCorners(gray, (m, n), None)
        print('2', ret)
        # If found, add object points, image points (after refining them)
        if ret == True:
            objpoints.append(objp)
            cv2.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)
            imgpoints.append(corners)
            # Draw and display the corners
            cv2.drawChessboardCorners(img, (m, n), corners, ret)
            cv2.imshow('img', img)
            # # print('__')
            cv2.waitKey()
    cv2.destroyAllWindows()
    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)
    # print(ret, mtx, dist, rvecs, tvecs)

    img = cv2.imread('C:/Users/Mikalai/Desktop/qwe/png/q2.png')
    h, w = img.shape[:2]
    alpha = 0
    newcameramtx, roi = cv2.getOptimalNewCameraMatrix(mtx, dist, (w, h), alpha, (w, h))
    print('newcameramtx: ', newcameramtx, '\nroi', roi, '\n dist koef', dist.ravel())

    return newcameramtx, dist.ravel()
    # return objpoints, imgpoints


def fishEyeAddSettings():
    import numpy as np
    import cv2
    import time

    # FILENAME_IN = 'rtsp://admin:z1x2c3v4b5@192.168.0.102:554'
    # FILENAME_IN = 'rtsp://admin:z1x2c3v4b5@192.168.6.108:554'
    # FILENAME_IN = 'rtsp://admin:admin@192.168.0.65:554'

    FILENAME_IN = 'rtsp://admin:z1x2c3v4b5@192.168.0.101:554'

    cv2.namedWindow('settings')
    cv2.resizeWindow('settings', 600, 800)

    def nothing(x):
        pass

    par = 2000
    cv2.createTrackbar('H1', 'settings', 0, par, nothing)
    cv2.createTrackbar('H2', 'settings', 0, par, nothing)
    cv2.createTrackbar('S1', 'settings', 0, par, nothing)
    cv2.createTrackbar('S2', 'settings', 0, par, nothing)
    cv2.createTrackbar('V1', 'settings', 0, par, nothing)
    cv2.createTrackbar('V2', 'settings', 0, par, nothing)
    cv2.createTrackbar('Q1', 'settings', 0, par, nothing)
    cv2.createTrackbar('Q2', 'settings', 0, par, nothing)
    cv2.createTrackbar('W1', 'settings', 0, par, nothing)
    cv2.createTrackbar('W2', 'settings', 0, par, nothing)

    cv2.createTrackbar('p+', 'settings', 0, par, nothing)
    cv2.createTrackbar('p-', 'settings', 0, par, nothing)
    cv2.createTrackbar('s+', 'settings', 0, par, nothing)
    cv2.createTrackbar('s-', 'settings', 0, par, nothing)
    cv2.createTrackbar('a+', 'settings', 0, par, nothing)
    cv2.createTrackbar('a-', 'settings', 0, par, nothing)
    cv2.createTrackbar('x+', 'settings', 0, par, nothing)
    cv2.createTrackbar('x-', 'settings', 0, par, nothing)

    # camera_matrix = np.array([
    #     [832.46839736,      0,              644.95754444],
    #     [0,                 801.40931086,   344.10344623],
    #     [0.00000000e+00,    0.00000000e+00,    1.00000000e+00]]
    # )

    # dist_coefs = np.array(
    #     [-0.35883063,  0.13040111,  0.00144063, -0.00088184, -0.01557561]
    # )

    video = cv2.VideoCapture(FILENAME_IN)
    _, frame = video.read()
    size = frame.shape

    while True:
        t1 = time.time()
        h1 = cv2.getTrackbarPos('H1', 'settings')
        h2 = cv2.getTrackbarPos('H2', 'settings')
        h3 = cv2.getTrackbarPos('S1', 'settings')
        h4 = cv2.getTrackbarPos('S2', 'settings')
        h5 = cv2.getTrackbarPos('V1', 'settings')
        h6 = cv2.getTrackbarPos('V2', 'settings')
        h7 = cv2.getTrackbarPos('Q1', 'settings')
        h8 = cv2.getTrackbarPos('Q2', 'settings')
        h9 = cv2.getTrackbarPos('W1', 'settings')
        h10 = cv2.getTrackbarPos('W2', 'settings')

        x1 = cv2.getTrackbarPos('p+', 'settings')
        x2 = cv2.getTrackbarPos('p-', 'settings')
        x3 = cv2.getTrackbarPos('s+', 'settings')
        x4 = cv2.getTrackbarPos('s-', 'settings')
        x5 = cv2.getTrackbarPos('a+', 'settings')
        x6 = cv2.getTrackbarPos('a-', 'settings')
        x7 = cv2.getTrackbarPos('x+', 'settings')
        x8 = cv2.getTrackbarPos('x-', 'settings')

        # M = cv2.getRotationMatrix2D((size[0]/2, size[1]/2), 5, 1)
        _, frame = video.read()

        k0 = 0.1

        x1 = int(x1 * k0)
        x2 = int(x2 * k0)
        x3 = int(x3 * k0)
        x4 = int(x4 * k0)
        x5 = int(x5 * k0)
        x6 = int(x6 * k0)
        x7 = int(x7 * k0)
        x8 = int(x8 * k0)

        camera_matrix = np.array([
            [832 + x1 - x2, 0, 644 + x3 - x4],
            [0, 801 + x5 - x6, 344 + x7 - x8],
            [0, 0, 1]]
        )

        k1 = 0.00001
        dist_coefs = np.array(
            [-0.35883063 + (k1 * h1) - (k1 * h2), 0.13040111 + (k1 * h3) - (k1 * h4),
             0.00144063 + (k1 * h5) - (k1 * h6), -0.00088184 + (k1 * h7) - (k1 * h8),
             -0.01557561 + (k1 * h9) - (k1 * h10)]
        )

        # camera_matrix = np.array([
        #     [832 + 102.0, 0, 644 + 50.0],
        #     [0, 801 + 32.6, 344 + 46.5],
        #     [0, 0, 1]]
        # )
        # dist_coefs = np.array(
        #     [-0.35883063 + 0.00099, 0.13040111, 0.00144063 + 0.00781,
        #      -0.00088184, -0.01557561 - 0.00063]
        # )
        #

        newcameramtx, roi = cv2.getOptimalNewCameraMatrix(camera_matrix, dist_coefs, (size[0], size[1]), 1,
                                                          (size[0], size[1]))
        frame = cv2.undistort(frame, camera_matrix, dist_coefs, None, newcameramtx)
        h, w, c = frame.shape
        # frame = frame[y:y+h-50, x+70:x+w-20]

        # центр
        cv2.line(frame, (0, int(h / 2)), (w, int(h / 2)), (255, 0, 0), 1)
        cv2.line(frame, (int(w / 2), 0), (int(w / 2), h), (255, 0, 0), 1)

        # горизонтальные линии
        cv2.line(frame, (0, int(h / 3)), (w, int(h / 3)), (0, 200, 200), 1)
        cv2.line(frame, (0, int(h / 5)), (w, int(h / 5)), (0, 200, 200), 1)
        cv2.line(frame, (0, int(h / 10)), (w, int(h / 10)), (0, 200, 200), 1)
        cv2.line(frame, (0, int(h / 20)), (w, int(h / 20)), (0, 200, 200), 1)

        # cv2.line(frame, (0, int(2*h/3)),        (w, int(2 * h / 3)),    (0, 200, 200), 1)
        # cv2.line(frame, (0, int(4 * h / 5)),    (w, int(4 * h / 5)),    (0, 200, 200), 1)
        # cv2.line(frame, (0, int(9 * h / 10)),   (w, int(9 * h / 10)),   (0, 200, 200), 1)

        # вертикальные линии
        cv2.line(frame, (int(w / 4), 0), (int(w / 4), h), (200, 0, 0), 1)
        cv2.line(frame, (int(w / 5), 0), (int(w / 5), h), (200, 0, 0), 1)
        cv2.line(frame, (int(w / 10), 0), (int(w / 10), h), (200, 0, 0), 1)
        cv2.line(frame, (int(w / 20), 0), (int(w / 20), h), (200, 0, 0), 1)

        # cv2.line(frame, (int(3 * w / 4), 0),    (int(3 * w / 4), h),    (0, 255, 0), 1)
        # cv2.line(frame, (int(4 * w / 5), 0),    (int(4 * w / 5), h),    (0, 255, 0), 1)
        # cv2.line(frame, (int(9 * w / 10), 0),   (int(9 * w / 10), h),   (0, 255, 0), 1)

        cv2.imshow('image', frame)
        # print(time.time() - t1)
        if cv2.waitKey(1) & 0xFF == 27:
            break
    cv2.destroyAllWindows()
    video.release()


def fishEya(camera_matrix, dist_coefs):
    import numpy as np
    import cv2
    import time

    # FILENAME_IN = 'rtsp://admin:z1x2c3v4b5@192.168.0.102:554'
    # FILENAME_IN = 'rtsp://admin:z1x2c3v4b5@192.168.6.108:554'
    # FILENAME_IN = 'rtsp://admin:admin@192.168.0.65:554'
    FILENAME_IN = 'rtsp://admin:z1x2c3v4b5@192.168.0.102:554'
    video = cv2.VideoCapture(FILENAME_IN)
    _, frame = video.read()
    size = frame.shape
    newcameramtx, roi = cv2.getOptimalNewCameraMatrix(camera_matrix, dist_coefs, (size[0], size[1]), 1,
                                                      (size[0], size[1]))

    while True:
        t1 = time.time()

        # M = cv2.getRotationMatrix2D((size[0]/2, size[1]/2), 5, 1)
        _, frame = video.read()
        # frame = cv2.resize(frame, (640, 480))

        frame = cv2.undistort(frame, camera_matrix, dist_coefs, None, newcameramtx)
        h, w, c = frame.shape

        cv2.line(frame, (0, int(h / 2)), (w, int(h / 2)), (0, 200, 200), 2)
        cv2.line(frame, (0, int(h / 3)), (w, int(h / 3)), (0, 200, 200), 2)
        cv2.line(frame, (0, int(2 * h / 3)), (w, int(2 * h / 3)), (0, 200, 200), 2)
        cv2.line(frame, (0, int(h / 5)), (w, int(h / 5)), (0, 200, 200), 2)
        cv2.line(frame, (0, int(h / 10)), (w, int(h / 10)), (0, 200, 200), 2)

        cv2.line(frame, (0, int(4 * h / 5)), (w, int(4 * h / 5)), (0, 200, 200), 1)
        cv2.line(frame, (0, int(9 * h / 10)), (w, int(9 * h / 10)), (0, 200, 200), 1)

        cv2.line(frame, (int(w / 2), 0), (int(w / 2), h), (0, 25, 255), 1)
        cv2.line(frame, (int(w / 4), 0), (int(w / 4), h), (0, 25, 255), 1)
        cv2.line(frame, (int(3 * w / 4), 0), (int(3 * w / 4), h), (0, 25, 255), 1)
        cv2.line(frame, (int(4 * w / 5), 0), (int(4 * w / 5), h), (0, 25, 255), 1)
        cv2.line(frame, (int(9 * w / 10), 0), (int(9 * w / 10), h), (0, 25, 255), 1)

        cv2.imshow('image', frame)

        # print(time.time() - t1)
        if cv2.waitKey(1) & 0xFF == 27:
            break
    cv2.destroyAllWindows()
    video.release()


def showCam():
    import cv2

    FILENAME_IN = 'rtsp://admin:z1x2c3v4b5@192.168.0.101:554'
    video = cv2.VideoCapture(FILENAME_IN)
    while True:
        t1 = time.time()
        k = 3
        _, frame = video.read()
        h, w, c = frame.shape
        # frame = cv2.resize(frame, (int(w/k), int(h/k)))
        frame = cv2.resize(frame, (1280, 720), interpolation=cv2.INTER_LINEAR)

        # центр
        cv2.line(frame, (0, int(h / 2)), (w, int(h / 2)), (255, 0, 0), 1)
        cv2.line(frame, (int(w / 2), 0), (int(w / 2), h), (255, 0, 0), 1)

        # горизонтальные линии
        cv2.line(frame, (0, int(h / 3)), (w, int(h / 3)), (0, 200, 200), 1)
        cv2.line(frame, (0, int(h / 5)), (w, int(h / 5)), (0, 200, 200), 1)
        cv2.line(frame, (0, int(h / 10)), (w, int(h / 10)), (0, 200, 200), 1)
        cv2.line(frame, (0, int(h / 20)), (w, int(h / 20)), (0, 200, 200), 1)

        # cv2.line(frame, (0, int(2*h/3)),        (w, int(2 * h / 3)),    (0, 200, 200), 1)
        # cv2.line(frame, (0, int(4 * h / 5)),    (w, int(4 * h / 5)),    (0, 200, 200), 1)
        # cv2.line(frame, (0, int(9 * h / 10)),   (w, int(9 * h / 10)),   (0, 200, 200), 1)

        # вертикальные линии
        cv2.line(frame, (int(w / 4), 0), (int(w / 4), h), (200, 0, 0), 1)
        cv2.line(frame, (int(w / 5), 0), (int(w / 5), h), (200, 0, 0), 1)
        cv2.line(frame, (int(w / 10), 0), (int(w / 10), h), (200, 0, 0), 1)
        cv2.line(frame, (int(w / 20), 0), (int(w / 20), h), (200, 0, 0), 1)

        cv2.line(frame, (int(3 * w / 4), 0), (int(3 * w / 4), h), (0, 255, 0), 1)
        cv2.line(frame, (int(4 * w / 5), 0), (int(4 * w / 5), h), (0, 255, 0), 1)
        cv2.line(frame, (int(9 * w / 10), 0), (int(9 * w / 10), h), (0, 255, 0), 1)

        cv2.imshow('image', frame)
        print((time.time() - t1).__round__(4))
        if cv2.waitKey(1) & 0xFF == 27:
            break
    cv2.destroyAllWindows()
    video.release()


@jit
def applyMask(t1, t2, original_image, dlt):
    """

    :param t1: gray original image
    :param t2: gray background image
    :param original_image: origimal image
    :param dlt: delta
    :return:
    """
    for i in range(t1.shape[0]):
        for j in range(t1.shape[1]):
            if (t1[i][j] <= t2[i][j] + dlt) and (t1[i][j] >= t2[i][j] - dlt):
                t1[i][j] = 0
                original_image[i][j][0] = 0
                original_image[i][j][1] = 0
                original_image[i][j][2] = 0

    return t1, original_image


@jit
def convolution_min(frame, n, m):
    """
    изменение размера (свертка) изображения матрицей n,m по минимальному значению в матрице

    :param frame: image in BIN
    :param n: matrix hight
    :param m: matrix weight
    :return: image
    """
    b = np.array(n, m)

    for i in range(frame.shape[0]):
        for j in range(frame.shape[1], m):
            b[i][j] = min(frame[i][j:j + m])

    return frame


def deleteBackground(original_image, background_image):
    """
    Функиция позволяет сделать сравнение 2 изображений для удаления фона путем создания маски
    """
    gray_original = cv2.cvtColor(original_image, cv2.COLOR_BGR2GRAY)
    gray_background = cv2.cvtColor(background_image, cv2.COLOR_BGR2GRAY)

    dlt = 30

    t1, original_image = applyMask(gray_original, gray_background, original_image, dlt)

    _, t1 = cv2.threshold(t1, thresh=50, maxval=500, type=cv2.THRESH_BINARY)

    return t1, original_image


def getMask(roi):
    roi_copy = roi.copy()
    roi_hsv = cv2.cvtColor(roi, cv2.COLOR_RGB2HSV)
    # filter black color
    mask1 = cv2.inRange(roi_hsv, np.array([80, 100, 80]), np.array([255, 255, 255]))
    # mask1 = cv2.morphologyEx(mask1, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3)))
    mask1 = cv2.Canny(mask1, 100, 300)
    mask1 = cv2.GaussianBlur(mask1, (1, 1), 0)
    mask1 = cv2.Canny(mask1, 100, 300)
    mask1 = cv2.morphologyEx(mask1, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3)))
    return mask1


def cutImg(img):
    inputImg = img

    canny = cv2.Canny(img, 255, 255, 5)
    # kernel = np.ones((3, 2), np.uint8)
    # kernel = np.array([ [2, 4, 5, 4, 2],
    #                     [4, 9, 12, 9, 4],
    #                     [5, 12, 15, 12, 5],
    #                     [4, 9, 12, 9, 4],
    #                     [2, 4, 5, 4, 2]], np.uint8)/159
    kernel = np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]], np.uint8)
    canny = cv2.dilate(canny, kernel, iterations=5)

    roi_copy = inputImg.copy()
    roi_hsv = cv2.cvtColor(inputImg, cv2.COLOR_RGB2HSV)

    # filter black color
    mask1 = cv2.inRange(roi_hsv, np.array([80, 100, 80]), np.array([255, 255, 255]))
    # return mask1
    mask1 = cv2.morphologyEx(mask1, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3)))
    mask1 = cv2.Canny(mask1, 100, 300)
    mask1 = cv2.GaussianBlur(mask1, (1, 1), 0)
    mask1 = cv2.Canny(mask1, 100, 300)
    # mask1 = cv2.morphologyEx(mask1, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3)))

    lines = cv2.HoughLines(mask1, 1, np.pi / 2, 200)

    y_1 = 0
    y_2 = 10000
    try:
        for line in lines:
            for rho, theta in line:
                a = np.cos(theta)
                b = np.sin(theta)
                x0 = a * rho
                y0 = b * rho
                x1 = int(x0 + 1000 * (-b))
                y1 = int(y0 + 1000 * (a))
                x2 = int(x0 - 1000 * (-b))
                y2 = int(y0 - 1000 * (a))
                # if x1==x2:
                #     continue
                y_1 = max(y_1, y1)
                y_2 = min(y_2, y2)
                cv2.line(inputImg, (x1, y1), (x2, y2), (0, 255, 255), 1)
    except:
        # pass
        print('err')

    return inputImg



def cutImgMask(inputImg, mask):
    """
    :param inputImg:
    :param mask:
    :return:
    image, y_min, y_max
    """
    y_1 = 0
    y_2 = 10000
    delta = 10
    h, w = inputImg.shape[:2]

    y_min = 0
    y_max = h

    c = int(h / 2)
    cv2.line(inputImg, (0, c), (w, c), (0, 0, 200), 1)

    lines = cv2.HoughLines(mask, 1, np.pi / 2, 200)

    try:
        for line in lines:
            for rho, theta in line:
                a = np.cos(theta)
                b = np.sin(theta)
                x0 = a * rho
                y0 = b * rho
                x1 = int(x0 + w * (-b))
                y1 = int(y0 + w * (a))
                x2 = int(x0 - w * (-b))
                y2 = int(y0 - w * (a))
                # if x1 == x2:
                #     continue
                y_1 = max(y_1, y1)
                y_2 = min(y_2, y2)

                print('x1: ', x1, ' x2: ', x2)
                cv2.line(inputImg, (x1, 0), (x1, h), (255, 255, 0), 2)

        y_min = y_2 - delta
        if y_min < 0: y_min = 0

        y_max = y_1 + delta

        # print('area: ', y_min, y_max)

        cv2.line(inputImg, (0, y_min), (w, y_min), (0, 255, 255), 1)
        cv2.line(inputImg, (0, y_max), (w, y_max), (0, 255, 255), 1)
    except:
        # y_min = 150
        # y_max = 400
        # print('err: ',y_min, y_max)
        pass
    return inputImg, y_min, y_max





board_param = dict(
    y_min=0, y_max=0,
    hight=0,
    wiedth=100,
)


def check_boarf_param(Frame, Ymin, Ymax):
    y_min = []
    y_max = []
    for key in Frame.keys():
        try:
            y_min.append(Ymin[key])
            y_max.append(Ymax[key])
        except Exception as e:
            print(e)
            print('check_height')
    board_param['y_min'] = int(median(y_min) * 1)
    board_param['y_max'] = int(median(y_max) * 1)
    board_param['wiedth'] = int((board_param['y_max'] - board_param['y_min']) * 1)


def blacked(frame, y_min, y_max):
    """
    заполняем изображение черным цветом, исключая зону y_min:y_max
    :param frame:
    :param y_min:
    :param y_max:
    :return:
    """
    zero_line = np.zeros([frame.shape[1], frame.shape[2]])
    for y in range(frame.shape[0]):
        if y <= y_min or y >= y_max:
            frame[y] = zero_line
    return frame


import logging

logging.basicConfig(format=u'%(levelname)-8s [%(asctime)s] %(message)s', level=logging.INFO, filename='test_log.log')


def find_region():
    import os
    from MODULS.image import fishEyeCam
    # PATH = "/home/ast/Desktop/_IMAGE/FOR_TRAINING/obzol/"
    PATH = "/home/ast/Desktop/FOR_TRAINING/_IMAGE/2019-10-08/"
    # PATH = "/home/ast/Desktop/_IMAGE/NET/"

    lis = os.listdir(path=PATH)
    lis.sort()
    for file in lis:
        img = cv2.imread(os.path.join(PATH, file))
        t1 = time.time()
        # img = fishEyeCam(img)
        img, _, __ = find_board_region(img)
        # mask = cv2.inRange(img, np.array([10, 30, 90]), np.array([30, 50, 100]))

        # mask = color(img, 3)
        # mask1 = color(img, 2)

        # mask3 = np.zeros([mask.shape[0], mask.shape[1]])
        # for y in range(mask3.shape[0]):
        #     for x in range(mask3.shape[1]):
        #         if mask[y][x] == mask1[y][x]:
        #             mask3[y][x] = 255
        # y, x = mask3.shape
        # y1, x1 = mask1.shape
        #
        # k = 1
        # mask3 = cv2.resize(mask3, (int(x/k), int(y/k)), interpolation=cv2.INTER_LINEAR)
        # mask3 = cv2.resize(mask3, (x1, y1))

        cv2.imshow('file', img)
        # cv2.imshow('mask', mask)
        # cv2.imshow('mask1', mask1)
        # cv2.imshow('mask3', mask3)
        #
        cv2.imwrite(os.path.join("/home/ast/Desktop/_IMAGE/FOR_TRAINING/2019-10-08", file), img)
        logging.info('done write {}'.format(file))
        if cv2.waitKey(1) & 0xFF == 27:
            break
        # print('total time: ', time.time()-t1)
        # time.sleep(8)
    cv2.destroyAllWindows()


def cut(image):
    # обрезка по длинне:
    roi_hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    y, x, c = roi_hsv.shape
    dy = int(y * 0.025)
    y_00 = int(y / 2 - dy)
    y_01 = int(y / 2 + dy)
    roi = roi_hsv[y_00:y_01, :]
    mask = cv2.inRange(roi, np.array([0, 35, 90]), np.array([30, 200, 255]))

    x_min = 0
    x_max = mask.shape[1]

    black_line = [0 for i in range(mask.shape[0])]
    for x in range(mask.shape[1]):
        l = []
        for y in range(mask.shape[0]):
            l.append(mask[y][x])
        if black_line != l and x_min == 0:
            x_min = x
        elif black_line == l and x_min != 0 and x_max == mask.shape[1]:
            x_max = x
            break

    print(x_min, x_max)

    h = image.shape[0]
    # 1460:240 сообношение сторон
    y = int(240 * (x_max - x_min) / 1468)
    image = image[int(h / 2 - y / 2):int(h / 2 + y / 2), x_min:x_max]

    return image


from PIL import Image

from multiprocessing import Process

TTT = {
    'a': 0,
    'b': 0
}


def f(key):
    while True:
        TTT[key] += 1
        time.sleep(1)
        print(TTT)


def f2():
    while True:
        TTT['b'] += 1
        time.sleep(1)
        print(TTT)


from MAIN import main
import NET.NET_from_image as NE


def mask(a1=0, a2=0, a3=0, a4=0, a5=0, a6=0, a7=0, a8=0):
    a = '{}{}{}{}{}{}{}{}'.format(a1, a2, a3, a4, a5, a6, a7, a8)


def set_bit(v, index, x):
    mask = 1 << index
    v &= ~mask
    if x:
        v |= mask
        return v


def main():
    import numpy as np
    import subprocess as sp
    import pylab
    # backend = 'Agg'
    # pylab.use(backend)

    # ffmpeg - rtsp_transport
    # tcp - i
    # 'rtsp://admin:z1x2c3v4b5@192.168.0.101:554' - r
    # 1 - f
    # image2
    # image - % 3
    # d.jpeg - pix_fmt
    # rgb24 - vcodec
    # rawrvideo

    # video properties
    # path = RTSP['cam1']
    resolution = (3072, 1728)

    framesize = resolution[0] * resolution[1] * 3

    # set up pipe
    FFMPEG_BIN = "ffmpeg"
    # command = [FFMPEG_BIN,
    #            '-i', path]
    i = 0

    command = [FFMPEG_BIN,
               # '-rtsp_transport tcp',
               '-i', 'rtsp://admin:z1x2c3v4b5@192.168.0.101:554',
               # '-r', 'image2pipe',
               # '-pix_fmt', 'rgb24',
               # '-vcodec', 'rawvideo '

               '-vf', 'fps=1',
               'd{}.png'.format(i)

               ]

    pipe = sp.Popen(command, stdout=sp.PIPE, bufsize=10 ** 8)
    while i < 5:
        # read first frame and save as image
        raw_image = pipe.stdout.read(framesize)
        i += 1
    # image = np.fromstring(raw_image, dtype='uint8')
    # print(raw_image)
    # cv2.imwrite('f.jpg', image)
    # image = image.reshape(resolution[0], resolution[1], 3)
    # pylab.imshow(image)
    # pylab.savefig('first_frame_ffmpeg_only.png')
    # pipe.stdout.flush()
    pipe.terminate()


import numba


@numba.jit()
def test(roi):
    return cv2.inRange(roi, np.array([0, 35, 90]), np.array([30, 200, 255]))


def find_board_region(image):
    fr_copy = image.copy()
    k = 25
    y, x = image.shape[:-1]
    frame = cv2.resize(fr_copy, (int(x / k), int(y / k)), interpolation=cv2.INTER_LINEAR)
    Ymin = []
    Ymax = []
    # зададим точки в которых хотим делать проверку
    # X = []
    num_point = 30
    X = list(np.linspace(0, 1, num_point + 1))
    point_min = []
    point_max = []
    roi_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    for i in range(len(X) - 1):
        y_min = 0
        y_max = frame.shape[0]

        dx = X[i + 1] - X[i]
        x_00 = int(roi_hsv.shape[1] * X[i])
        x_01 = int(roi_hsv.shape[1] * (X[i] + dx))

        roi = roi_hsv[:, x_00:x_01]
        mask = cv2.inRange(roi, np.array([0, 35, 90]), np.array([30, 200, 255]))
        mask = cv2.medianBlur(mask, 5)
        # mask = cv2.GaussianBlur(mask, (1, 1), 71)
        # mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3)))

        # cv2.imshow('mask', mask)
        # cv2.waitKey()
        # cv2.destroyAllWindows()

        white_line = list(np.linspace(255, 255, roi.shape[1], dtype=np.int16))

        for y in range(roi.shape[0]):
            if all(mask[y] == white_line) and y_min == 0:
                y_min = y
            elif all(mask[y] != white_line) and y_min != 0 and y_max == frame.shape[0]:
                y_max = y
                break

        if y_min != 0 and y_min != y_max:
            Ymin.append(y_min)
            # add point
            point_min.append([X[i] * image.shape[1], y_min * k])

        if y_max != frame.shape[0] and y_min != y_max:
            Ymax.append(y_max)
            point_max.append([X[i] * image.shape[1], y_max * k])

    if Ymin.__len__() != 0:
        y_min = median(Ymin) * k
        y_min = int(y_min * 1)
    if Ymax.__len__() != 0:
        y_max = int(median(Ymax) * k * 1)

    if y_min == 0 and y_max == frame.shape[0]:
        y_min = 0
        y_max = 0

    # frame = blacked(image, y_min, y_max)
    frame = image
    y, x, c = frame.shape

    # Tx = int((y/2 - y_max)+(y_max - y_min)/2)
    # M = np.float32([[1, 0, 0], [0, 1, Tx]])
    # frame = cv2.warpAffine(frame, M, (x, y))

    # return image[y_min:y_max, :], y_min, y_max

    # print(Ymin, '\n', Ymax, '\n', X)
    points = np.array([point_min], dtype=np.int32)
    cv2.polylines(image, points, False, (255, 0, 255), thickness=5)
    points = np.array([point_max], dtype=np.int32)
    cv2.polylines(frame, points, False, (0, 255, 0), thickness=5)

    return image

    # return image, y_min, y_max


def fishEyeCam(frame):
    K = np.array(
        [[1987.2902013412322, 0.0, 1540.7521175856489], [0.0, 1914.5965846929532, 843.8461627100188], [0.0, 0.0, 1.0]])
    D = np.array([[-0.061326402914289105], [0.2306815273667478], [-1.002826819903716], [1.0982644076261452]])
    DIM = (3072, 1728)

    # img = cv2.imread(PATH_ROOT)
    img_dim = frame.shape[:2][::-1]
    balance = 0.0
    scaled_K = K * img_dim[0] / DIM[0]
    scaled_K[2][2] = 1.0
    new_K = cv2.fisheye.estimateNewCameraMatrixForUndistortRectify(scaled_K, D,
                                                                   img_dim, np.eye(3), balance=balance)

    map1, map2 = cv2.fisheye.initUndistortRectifyMap(scaled_K, D, np.eye(3),
                                                     new_K, img_dim, cv2.CV_16SC2)
    undist_image = cv2.remap(frame, map1, map2, interpolation=cv2.INTER_LINEAR,
                             borderMode=cv2.BORDER_CONSTANT)
    return undist_image



import time
from statistics import median
from numba import jit, njit, cuda


@jit()
def applyMask_v02(t1, t2, original_image, dlt, white_line, black_line):
    """
    :param t1: gray original image
    :param t2: gray background image
    :param original_image: origimal image
    :param dlt: delta
    :return:
    """
    mask = t1
    for i in range(t1.shape[0]):
        cnt = 0
        for j in range(t1.shape[1]):
            if (t1[i][j] <= t2[i][j] + dlt) and (t1[i][j] >= t2[i][j] - dlt):
                mask[i][j] = 0
                original_image[i][j] = [0, 0, 0]
            else:
                mask[i][j] = 255
                cnt += 1
        if cnt < 0.1 * t1.shape[1]:
            mask[i] = black_line
            original_image[i] = white_line
    return mask, original_image


def deleteBackground_v02(original_image, background_image):
    """
    Функиция позволяет сделать сравнение 2 изображений для удаления фона путем создания маски
    """
    gray_original = cv2.cvtColor(original_image, cv2.COLOR_BGR2GRAY)
    gray_background = cv2.cvtColor(background_image, cv2.COLOR_BGR2GRAY)

    original = cv2.medianBlur(gray_original, 19)
    # original = cv2.bilateralFilter(gray_original, 30, 80, 80)
    background = cv2.medianBlur(gray_background, 19)
    # background = cv2.bilateralFilter(gray_background, 30, 80, 80)

    dlt = 50
    # cv2.imshow('origiman', original)
    # cv2.imshow('background', background)
    white_line_3 = np.linspace([0, 0, 0], 0, original.shape[1], dtype=np.uint8)
    black_line_1 = np.linspace(0, 0, original.shape[1], dtype=np.uint8)
    mask, original_image = applyMask_v02(original, background, original_image, dlt, white_line_3, black_line_1)
    edg = cv2.Laplacian(mask, cv2.CV_64F)

    # cv2.imshow('img', mask)
    # cv2.imshow('CANNY', cv2.Canny(mask, 0, 200))
    # cv2.imshow('Lapl', cv2.Laplacian(mask, cv2.CV_64F))
    # cv2.imshow('img1', original_image)

    # cv2.waitKey()
    # cv2.destroyAllWindows()

    return mask, original_image, edg


def find_concat_param(image):
    fr_copy = image.copy()
    k = 2
    y, x = image.shape
    frame = cv2.resize(fr_copy, (int(x / k), int(y / k)), interpolation=cv2.INTER_LINEAR)
    Ymin = []
    Ymax = []
    W = []
    # зададим точки в которых хотим делать проверку
    X = [0.0, 0.98]
    # X = np.linspace(0, 0.99, 11)

    # roi_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    roi_hsv = frame
    for i in range(len(X)):
        y_min = 0
        y_max = frame.shape[0]

        x_00 = int(roi_hsv.shape[1] * X[i])
        x_01 = int(roi_hsv.shape[1] * (X[i] + 0.01))

        roi = roi_hsv[:, x_00:x_01]
        # mask = cv2.inRange(roi, np.array([0, 35, 90]), np.array([30, 200, 255]))
        mask = roi
        mask = cv2.medianBlur(mask, 5)

        # cv2.imshow('mask', mask)
        # cv2.waitKey()
        # cv2.destroyAllWindows()

        white_line = list(np.linspace(255, 255, roi.shape[1], dtype=np.int16))
        for y in range(roi.shape[0]):
            if all(mask[y] == white_line) and y_min == 0:
                y_min = y
            elif all(mask[y] != white_line) and y_min != 0 and y_max == frame.shape[0]:
                y_max = y
                break
        Ymin.append(y_min * k)
        Ymax.append(y_max * k)
        W.append(y_max * k - y_min * k)
    return Ymin, Ymax, W


def lineUp(Frame, dY):
    try:
        rows, cols, _ = Frame.shape
        M = np.float32([[1, 0, 0], [0, 1, dY]])
        return cv2.warpAffine(Frame, M, (cols, rows))
    except:
        return Frame


def histogramm(res):
    try:
        res = cv2.cvtColor(res, cv2.COLOR_BGR2YUV)
        res[0, :, :] = cv2.equalizeHist(res[0, :, :])
        res[:, 0, :] = cv2.equalizeHist(res[:, 0, :])
        return cv2.cvtColor(res, cv2.COLOR_YUV2BGR)
    except Exception as e:
        print('histogramm ', e)
        return None


def rio_map(img_o, shape=0):
    # img = img_o
    img = cv2.blur(img_o, (8, 3))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = cv2.Canny(img, 0, 200)
    # img = cv2.Laplacian(img, cv2.CV_8U)
    img = cv2.dilate(img, (3, 3), iterations=2)

    # minLineLength = 0
    # maxLineGap = 0

    lines = cv2.HoughLinesP(img,
                            rho=1,
                            theta=np.pi / 2,
                            threshold=20,
                            minLineLength=100,
                            maxLineGap=10)

    if lines is None:
        return None, None
    # for l in lines:
    #     x1, y1, x2, y2 = l[0]
    #     cv2.line(img_o, (x1, y1), (x2, y2), (255, 255, 0), 1)
    top, bottom = spline(lines, shape)

    # for l in top:
    #     x1, y1, x2, y2 = l
    #     cv2.line(img_o, (x1, y1), (x2, y2), (0, 255, 0), 2)
    # for l in bottom:
    #     x1, y1, x2, y2 = l
    #     cv2.line(img_o, (x1, y1), (x2, y2), (0, 0, 255), 2)

    res = cv2.vconcat([cv2.cvtColor(img, cv2.COLOR_GRAY2BGR), img_o])
    cv2.imshow('img', res)

    cv2.waitKey()
    cv2.destroyAllWindows()

    return top, bottom


def spline(lines, shape):
    if lines is None:
        return [0], [0]

    _L = []
    _L1 = []
    top_line = []
    bottom_line = []

    def check_degree(x1, y1, x2, y2):
        import math
        try:
            alpha_rad = math.tan((y2 - y1) / (x2 - x1))
            alpha_degree = (alpha_rad * 180 / math.pi).__round__(0)
            # print('alpha_degree: ', alpha_degree)
            if abs(alpha_degree) > 1:
                return True
            else:
                return False
        except:
            pass

    # sort list line
    for l in lines:
        x1, y1, x2, y2 = l[0]
        _L1.append([x1, y1, x2, y2])
        _L1.sort()

    cnt = 0
    add = True
    y = []
    for i in range(len(_L1)):
        x1, y1, x2, y2 = _L1[i]
        if cnt == 0: _L.append([x1, y1, x2, y2])
        cnt += 1
        y.append(y1)
        for l in _L:
            if abs(y1 - l[3]) <= 20:
                if not (x1 >= l[0] and x2 <= l[2]):
                    if (l[0] <= x1 <= l[2] and x2 > l[2]) or (x1 <= l[0] and x2 >= l[2]) or (
                            (x1 - l[2]) < 50 and x2 > l[2]):
                        l[2] = x2
                        l[3] = y2
                        add = False
                        break
                        add = False
                else:
                    add = False
            else:
                add = True
        if add:
            _L.append([x1, y1, x2, y2])
            add = True

    y_midl = (y[0] + y[-1]) / 2
    # separate lines
    for l in _L:
        if l[1] >= y_midl:
            bottom_line.append(l)
        else:
            top_line.append(l)

    _top_line = []
    _bottom_line = []
    # top line
    for t in range(len(top_line)):
        add = False
        x1, y1, x2, y2 = top_line[t]
        for l in top_line:
            if x1 > l[0] and x2 < l[2]:
                add = False
                break
            elif x1 > l[0] and x2 <= l[2]:
                add = False
                break
            elif x1 >= l[0] and x2 < l[2]:
                add = False
                break
            else:
                add = True
        if add:
            _top_line.append(top_line[t])
    # bottom line

    for t in range(len(bottom_line)):
        add = False
        x1, y1, x2, y2 = bottom_line[t]
        for l in bottom_line:
            if x1 > l[0] and x2 < l[2]:
                add = False
                break
            elif x1 > l[0] and x2 <= l[2]:
                add = False
                break
            elif x1 >= l[0] and x2 < l[2]:
                add = False
                break
            elif check_degree(x1, y1, x2, y2):
                add = False
                break
            else:
                add = True
        if add:
            _bottom_line.append(bottom_line[t])

    # add smeschenie:
    for l in _top_line:
        l[0] = l[0] + shape
        l[2] = l[2] + shape
    for l in _bottom_line:
        l[0] = l[0] + shape
        l[2] = l[2] + shape

    # print('L1', _L1)
    # print('L: ', _L)
    # print('top: ', top_line)
    # print('top: ', _top_line)
    # print('bottom: ', bottom_line)
    # print('bottom: ', _bottom_line)
    return _top_line, _bottom_line
    # return _L


def new_1():
    import MODULS.image as IMG
    # img_a = cv2.imread('/home/ast/Desktop/_IMAGE/296.jpg')
    img_1a = cv2.imread('/home/ast/Desktop/_IMAGE/Test/sort_1/cam1.jpg')
    img_1b = cv2.imread('/home/ast/Desktop/_IMAGE/Test/sort_1/cam1_b.jpg')
    img_2a = cv2.imread('/home/ast/Desktop/_IMAGE/Test/sort_1/cam2.jpg')
    img_2b = cv2.imread('/home/ast/Desktop/_IMAGE/Test/sort_1/cam2_b.jpg')
    img_3a = cv2.imread('/home/ast/Desktop/_IMAGE/Test/sort_1/cam3.jpg')
    img_3b = cv2.imread('/home/ast/Desktop/_IMAGE/Test/sort_1/cam3_b.jpg')
    #
    # img_1b = cv2.imread('/home/ast/Desktop/_IMAGE/Test/obzol_1/cam1.jpg')
    # img_1a = cv2.imread('/home/ast/Desktop/_IMAGE/Test/obzol_1/cam1_b.jpg')
    # img_2b = cv2.imread('/home/ast/Desktop/_IMAGE/Test/obzol_1/cam2.jpg')
    # img_2a = cv2.imread('/home/ast/Desktop/_IMAGE/Test/obzol_1/cam2_b.jpg')
    # img_3b = cv2.imread('/home/ast/Desktop/_IMAGE/Test/obzol_1/cam3.jpg')
    # img_3a = cv2.imread('/home/ast/Desktop/_IMAGE/Test/obzol_1/cam3_b.jpg')

    # img_1b = cv2.imread('/home/ast/Desktop/_IMAGE/Test/obzol_2/cam1.jpg')
    # img_1a = cv2.imread('/home/ast/Desktop/_IMAGE/Test/obzol_2/cam1_b.jpg')
    # img_2b = cv2.imread('/home/ast/Desktop/_IMAGE/Test/obzol_2/cam2.jpg')
    # img_2a = cv2.imread('/home/ast/Desktop/_IMAGE/Test/obzol_2/cam2_b.jpg')
    # img_3b = cv2.imread('/home/ast/Desktop/_IMAGE/Test/obzol_2/cam3.jpg')
    # img_3a = cv2.imread('/home/ast/Desktop/_IMAGE/Test/obzol_2/cam3_b.jpg')
    # # #
    # img_1a = cv2.imread('/home/ast/Desktop/_IMAGE/Test/obzol_3/cam1.jpg')
    # img_1b = cv2.imread('/home/ast/Desktop/_IMAGE/Test/obzol_3/cam1_b.jpg')
    # img_2a = cv2.imread('/home/ast/Desktop/_IMAGE/Test/obzol_3/cam2.jpg')
    # img_2b = cv2.imread('/home/ast/Desktop/_IMAGE/Test/obzol_3/cam2_b.jpg')
    # img_3a = cv2.imread('/home/ast/Desktop/_IMAGE/Test/obzol_3/cam3.jpg')
    # img_3b = cv2.imread('/home/ast/Desktop/_IMAGE/Test/obzol_3/cam3_b.jpg')

    # img_1a = cv2.imread('/home/ast/Desktop/_IMAGE/CAM/cam1.jpg')
    # img_1b = cv2.imread('/home/ast/Desktop/_IMAGE/CAM/cam1_b.jpg')
    # img_2a = cv2.imread('/home/ast/Desktop/_IMAGE/CAM/cam2.jpg')
    # img_2b = cv2.imread('/home/ast/Desktop/_IMAGE/CAM/cam2_b.jpg')
    # img_3a = cv2.imread('/home/ast/Desktop/_IMAGE/CAM/cam3.jpg')
    # img_3b = cv2.imread('/home/ast/Desktop/_IMAGE/CAM/cam3_b.jpg')

    img_1a = cv2.resize(img_1a, (1280, 720))
    img_1b = cv2.resize(img_1b, (1280, 720))
    img_2a = cv2.resize(img_2a, (1280, 720))
    img_2b = cv2.resize(img_2b, (1280, 720))
    img_3a = cv2.resize(img_3a, (1280, 720))
    img_3b = cv2.resize(img_3b, (1280, 720))

    img_1a = fishEyeCam(img_1a)
    img_1b = fishEyeCam(img_1b)
    img_2a = fishEyeCam(img_2a)
    img_2b = fishEyeCam(img_2b)
    img_3a = fishEyeCam(img_3a)
    img_3b = fishEyeCam(img_3b)

    # коррекция первой камеры
    # img_1a = cv2.resize(img_1a, (1280, 680))
    # img_1b = cv2.resize(img_1b, (1280, 680))

    time_start = time.time()

    # img, _, __ = find_board_region(img1)
    # img_a = img_a[700:1400, :]
    # img_b = img_b[700:1400, :]

    y00 = int(img_1a.shape[0] * 0.1)
    y01 = int(img_1a.shape[0] * 0.50)

    img_1a = img_1a[y00:y01, :img_1a.shape[1] - 17]
    img_1b = img_1b[y00:y01, :img_1b.shape[1] - 17]
    img_2a = img_2a[y00:y01, 17:img_2a.shape[1] - 17]
    img_2b = img_2b[y00:y01, 17:img_2b.shape[1] - 17]
    img_3a = img_3a[y00:y01, 17:]
    img_3b = img_3b[y00:y01, 17:]

    img1, _1img, edg1 = deleteBackground_v02(img_1b, img_1a)
    img2, _2img, edg2 = deleteBackground_v02(img_2b, img_2a)
    img3, _3img, edg3 = deleteBackground_v02(img_3b, img_3a)

    # ========================================================
    Y1 = find_concat_param(img1)
    Y2 = find_concat_param(img2)
    Y3 = find_concat_param(img3)
    # print(Y1, Y2)

    y10, y11 = Y1[0], Y1[1]
    y20, y21 = Y2[0], Y2[1]
    y30, y31 = Y3[0], Y3[1]

    dY = y10[-1] - y20[0]
    _2img = lineUp(_2img, dY)
    dY = y20[-1] - y30[0] + dY
    _3img = lineUp(_3img, dY)

    res = cv2.hconcat([_1img, _2img, _3img])

    # =========================================================
    res = cv2.imread('/home/ast/Desktop/_IMAGE/2019-11-05/113.jpg')
    roi_map_v3(res, 200, 250)

    # roi_map_v3(res, y10[-1], y11[-1])


def roi_map_v4(img_o, y_min, y_max, delta_y=0, add=False):

    img_t = img_o.copy()
    # определим центр доски
    y_mid = y_min + int((y_max - y_min) / 2)

    img_c = img_o.copy()
    img = cv2.blur(img_c, (16, 3))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = cv2.Canny(img, 0, 200)
    img = cv2.dilate(img, (3, 3), iterations=3)
    img = cv2.blur(img, (16, 3))
    top_img = img[:y_mid, :]
    btm_img = img[y_mid:, :]
    # найдем все линии
    top_lines = cv2.HoughLinesP(top_img,
                                rho=1,
                                theta=np.pi / 2,
                                threshold=20,
                                minLineLength=100,
                                maxLineGap=20)
    btm_lines = cv2.HoughLinesP(btm_img,
                                rho=1,
                                theta=np.pi / 2,
                                threshold=20,
                                minLineLength=100,
                                maxLineGap=20)
    _L1 = []
    _L1.clear()
    # переведем полученные данные в list и отсортируем по убыванию
    for l in top_lines:
        x1, y1, x2, y2 = l[0]
        _L1.append([x1, y1, x2, y2])
    _L1.sort(key=lambda i: i[2] - i[0], reverse=True)
    # линия контура верхней стороны
    x1, y1, x2, y2 = _L1[0][0], _L1[0][1], _L1[0][2], _L1[0][3]
    # line_contour_top = [0, _L1[0][1], 5120, _L1[0][3]]
    k_top = (y2 - y1) / (x2 - x1)
    b_top = (y1 * x2 - y2 * x1) / (x2 - x1)
    # top_line = k*x + b
    # нарисуем контурные линии
    x1 = 0
    y1 = int(x1 * k_top + b_top)
    x2 = img_t.shape[1]
    y2 = int(x2 * k_top + b_top)
    cv2.line(img_t, (x1, y1), (x2, y2), (200, 20, 0), 5)
    _L1.clear()
    for l in btm_lines:
        x1, y1, x2, y2 = l[0]
        _L1.append([x1, y1, x2, y2])
    _L1.sort(key=lambda i: i[2] - i[0], reverse=True)
    # линия контура нижней стороны
    # line_contour_btm = [0, _L1[0][1], 5120, _L1[0][3]]
    x1, y1, x2, y2 = _L1[0][0], _L1[0][1], _L1[0][2], _L1[0][3]
    # print(x1, y1, x2, y2)
    # line_contour_top = [0, _L1[0][1], 5120, _L1[0][3]]
    k_btm = (y2 - y1) / (x2 - x1)
    b_btm = (y1 * x2 - y2 * x1) / (x2 - x1) + y_mid
    # print(k_btm, b_btm)
    # нарисуем контурные линии
    x1 = 0
    y1 = int(x1 * k_btm + b_btm)
    x2 = img_t.shape[1]
    y2 = int(x2 * k_btm + b_btm)
    cv2.line(img_t, (x1, y1), (x2, y2), (200, 20, 0), 5)

    # ============================================================================
    img = cv2.blur(img_o.copy(), (11, 11))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    # закрасим зоны под рельсами
    if add:
        dy = y_mid
        for _y in range(5):
            y = _y + dy
            for x in range(img_t.shape[1]):
                img[y][x] = 255

    cv2.imwrite('/home/ast/Desktop/_IMAGE/test.jpg', img)

    # найдем контур доски
    edge_top, _ = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    # отсортируем контуры для выявления наибольшего
    edge_top.sort(key=lambda i: len(i), reverse=True)
    # нанесем контур
    cv2.drawContours(img_t, edge_top, 0, (0, 255, 255), 3)

    # # # нарисуем линии
    top_point = []
    btm_point = []
    i = 0
    while i < img_t.shape[1]:
        a = np.where(edge_top[0] == i)
        if not add:
            q = len(a[0])
        elif (i >= 0 and i <= 50) or (i >= 400 and i <= 500) \
                or (i >= 2050 and i <= 2150) or (i >= 3350 and i <= 3450):
            q = 0
        else:
            q = len(a[0])

        # print(q)
        if q != 0 and all(a[2] == 0):
            p0 = edge_top[0][a[0][0]][0][1]
            p1 = edge_top[0][a[0][1]][0][1]
            # print(i, ': ', a, ':', p0, p1)
            # x, y_p, y_l
            top_point.append([i, min(p0, p1), int(k_top * i + b_top)])
            btm_point.append([i, max(p0, p1), int(k_btm * i + b_btm)])
        i += 20
    for point in top_point:
        x1, y1, x2, y2 = point[0], point[1], point[0], 0
        cv2.line(img_t, (x1, y1), (x2, y2), (0, 255, 0), 1)
    for point in btm_point:
        x1, y1, x2, y2 = point[0], point[1], point[0], 400
        cv2.line(img_t, (x1, y1), (x2, y2), (0, 255, 0), 1)

    # выделим дефекты, которые будут учитываться
    # если длинна линии до донтура больше чем дельта, то считаем, что точка относится к дефекту
    def create_defect_array(points, delta):
        defekt = []
        key01 = 1
        xd1 = 0
        xd2 = 0
        x, y_p, y_l = 0, 0, 0
        for point in points:
            x, y_p, y_l = point[0], point[1], point[2]

            if abs(y_p - y_l) >= delta and key01:
                key01 = False
                xd1 = x
            elif abs(y_p - y_l) < delta and xd1 != 0:
                key01 = True
                xd2 = x
                defekt.append([xd1, y_l, xd2, y_l])
                xd1 = 0
                xd2 = 0
        if not key01:
            xd2 = img_t.shape[1]
            defekt.append([xd1, y_l, xd2, y_l])
        return defekt

    TOP_DEFEKT = create_defect_array(top_point, delta_y)
    BTM_DEFEKT = create_defect_array(btm_point, delta_y)

    for line in TOP_DEFEKT:
        x1, y1, x2, y2 = line
        cv2.line(img_t, (x1, y1 - 20), (x2, y2 - 20), (0, 0, 255), 5)
    for line in BTM_DEFEKT:
        x1, y1, x2, y2 = line
        cv2.line(img_t, (x1, y1 + 10), (x2, y2 + 10), (0, 0, 255), 5)

    cv2.imwrite('/home/ast/Desktop/_IMAGE/test1.jpg', img_t)

    return TOP_DEFEKT, BTM_DEFEKT













def roi_map_v3(img_o, y_min, y_max, shape=0, add=False):
    img_t = img_o.copy()
    img = cv2.blur(img_o.copy(), (16, 3))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = cv2.Canny(img, 0, 200)
    img = cv2.dilate(img, (3, 3), iterations=3)
    img = cv2.blur(img, (16, 3))
    # img = cv2.Canny(img, 0, 200)

    y_mid = y_min + int((y_max - y_min) / 2)
    cv2.imwrite('/home/ast/Desktop/_IMAGE/test.jpg', img)
    top_img = img[:y_mid, :]
    btm_img = img[y_mid:, :]

    top_lines = cv2.HoughLinesP(top_img,
                                rho=1,
                                theta=np.pi / 2,
                                threshold=20,
                                minLineLength=100,
                                maxLineGap=20)
    btm_lines = cv2.HoughLinesP(btm_img,
                                rho=1,
                                theta=np.pi / 2,
                                threshold=20,
                                minLineLength=100,
                                maxLineGap=20)
    for line in top_lines:
        x1, y1, x2, y2 = line[0]
        cv2.line(img_t, (x1, y1), (x2, y2), (255, 0, 25), 1)
    for line in btm_lines:
        x1, y1, x2, y2 = line[0]
        cv2.line(img_t, (x1, y1 + y_mid), (x2, y2 + y_mid), (255, 0, 25), 1)
    cv2.imwrite('/home/ast/Desktop/_IMAGE/testt.jpg', img_t)

    top = spline_v3(top_lines, add)
    bottom = spline_v3(btm_lines, add)
    for line in top:
        x1, y1, x2, y2 = line
        cv2.line(img_o, (x1, y1), (x2, y2), (255, 255, 25), 5)
    for line in bottom:
        x1, y1, x2, y2 = line
        cv2.line(img_o, (x1, y1 + y_mid), (x2, y2 + y_mid), (0, 255, 0), 2)
    # ======================================================================================
    # ======================================================================================

    cv2.imwrite('/home/ast/Desktop/_IMAGE/img_o.jpg', img_o)


def spline_v3(lines, add):
    result = []
    _L = []
    _L1 = []
    _line = []
    x_min = []
    x_max = []
    #  найдем максимальный прямой участок
    for l in lines:
        x1, y1, x2, y2 = l[0]
        _L1.append([x1, y1, x2, y2])
    _L1.sort(key=lambda i: i[2] - i[0], reverse=True)
    result.append([0, _L1[0][1], 5120, _L1[0][3]])
    return result


def cut_lenght(image):
    # обрезка по длинне:
    roi_hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    y, x, c = roi_hsv.shape
    dy = int(y * 0.10)
    dx = int(y * 0.5)
    y_00 = int(y / 2 - dy)
    y_01 = int(y / 2 + dy)

    roi = roi_hsv[y_00:y_01, dx:]
    mask = cv2.inRange(roi, np.array([0, 35, 90]), np.array([30, 200, 255]))

    x_min = 0
    x_max = mask.shape[1]

    black_line = [0 for i in range(mask.shape[0])]
    # black_line = np.linspace(0, 0, mask.shape[0], dtype=np.int16)
    for x in range(mask.shape[1]):
        l = []
        for y in range(mask.shape[0]):
            l.append(mask[y][x])
        # if black_line != l and x_min == 0:
        #     x_min = x
        # elif black_line == l and x_min != 0 and x_max == mask.shape[1]:
        #     x_max = x
        #     break
        if black_line == l and x_max == mask.shape[1]:
            x_max = x + dx
            break

    if x_max <= (image.shape[1] - 10):
        x_max = x_max + 10

    # print(x_min, x_max)
    h = image.shape[0]
    # 1460:240 сообношение сторон
    y = int(240 * (x_max - x_min) / 1468)
    try:
        # image = image[int(h / 2 - y / 2):int(h / 2 + y / 2), x_min:x_max]
        image = image[:, x_min:x_max]

    except Exception as e:
        print(e)
    return image, x_min, x_max


def check_brak(top, bottom, len_board, x_max):
    """
    :return:
        166 - optim 20%
        266 - optim 40%
        77 - Brak
        100 - sorting by neural netvork

    """
    defeck_top = []
    defeck_bottom = []

    def border_defekts(border, defekt_arr):
        # border.append([x_max])
        if len(border) >= 1:
            for i in range(len(border)):
                x1, y1, x2, y2 = border[i]
                if i <= len(border) - 2:
                    _x1, _y1, _x2, _y2 = border[i + 1]
                else:
                    _x1 = x_max

                # print((_x1 - x2)*100/len_board)
                # defekt
                if (_x1 - x2) * 100 / len_board >= 2:
                    defekt_arr.append([x2, _x1])

                # check_degree(x2, y2, _x1, _y1, 10)

    border_defekts(top, defeck_top)
    border_defekts(bottom, defeck_bottom)
    print('defeck_top: ', defeck_top)
    print('defeck_bottom: ', defeck_bottom)

    if len(defeck_bottom) != 0 or len(defeck_top) != 0:

        # =========================================================
        # BRAK
        sum_length_defekt_top = 0
        sum_length_defekt_bottom = 0

        for d in defeck_top:
            sum_length_defekt_top += (d[1] - d[0])
        for d in defeck_bottom:
            sum_length_defekt_bottom += (d[1] - d[0])
        # print(sum_length_defekt_top*1.21, sum_length_defekt_bottom*1.21)
        sum_defekt = max(sum_length_defekt_bottom, sum_length_defekt_top) * 1.21
        # print(sum_defekt)

        if (sum_defekt / x_max) * 100 > 40:
            return 77
        else:
            for d in defeck_top:
                if d[0] < x_max * 0.6:
                    return 77
            for d in defeck_bottom:
                if d[0] < x_max * 0.6:
                    return 77

        # =================================================================================
        # optimizacija
        opt = []
        for d in defeck_top:
            if d[0] > x_max * 0.8:
                opt.append(166)
            else:
                opt.append(266)
        for d in defeck_bottom:
            if d[0] > x_max * 0.8:
                opt.append(166)
            else:
                opt.append(266)
        if len(opt) != 0:
            opt.sort()
            return opt[0]

    else:
        return 100


def _new():
    time_start = time.time()

    img_1a = cv2.imread('/home/ast/Desktop/_IMAGE/CAM/cam1.jpg')
    img_1b = cv2.imread('/home/ast/Desktop/_IMAGE/CAM/cam1_b.jpg')

    q = 3
    img_1a = cv2.resize(img_1a, (int(img_1a.shape[1] / q), int(img_1a.shape[0] / q)), interpolation=cv2.INTER_LINEAR)
    img_1b = cv2.resize(img_1b, (int(img_1b.shape[1] / q), int(img_1b.shape[0] / q)), interpolation=cv2.INTER_LINEAR)

    # img_1b = cv2.cvtColor(img_1b, cv2.COLOR_BGR2YUV)
    #
    img_1a = fishEyeCam(img_1a)
    img_1b = fishEyeCam(img_1b)

    print(time.time() - time_start)

    y00 = int(img_1a.shape[0] * 0.15)
    y01 = int(img_1a.shape[0] * 0.50)

    img_1a = img_1a[y00:y01, :- 17]
    img_1b = img_1b[y00:y01, :- 17]

    res = deleteBackground_v02(img_1b, img_1a)[0]

    print(find_concat_param(res))
    print(time.time() - time_start)

    w = 1
    cv2.imshow('res', cv2.resize(res, (int(res.shape[1] / w), int(res.shape[0] / w))))
    cv2.waitKey()
    cv2.destroyAllWindows()


def create_sort_data_to_plc(x0=0, x1=0, x2=0):
    """
    XXX - код, передаваемый на ПЛК для далнейшей сортировки
    _ _ Х - сорт доски для толщины до 40мм
        1 - отборный сорт
        2 - 1-й сорт
        3 - 2-й сорт
        4 - 3-й сорт
        5 - 4-й сорт
        6 - доска для оптимизации
        7 - брак
    _ Х _ - сорт доски для толщины свыше 40мм
        код такой же как и для предыдущей позиции
    Х _ _ - положение обзола на доске:
        0 - нет обзола
        1 - обзол сконцентрировал слева в объеме 20% от длинны доски
        2 - обзол сконцентрировал слева в объеме 40% от длинны доски
    :return: код
    """
    return x0 + 10 * x1 + 100 * x2


def test_sorting():
    s_x0 = [7, 6, 5, 4, 3, 2, 1]
    s_x1 = [7, 6, 5, 4, 3, 2, 1]
    s_x2 = [2, 1, 0, 2, 1, 0, 2]
    for i in range(len(s_x0)):
        cod = create_sort_data_to_plc(s_x0.pop(), s_x1.pop(), s_x2.pop())
        time.sleep(1)
        print(cod)


def send_file_scp(file):
    from ssh2.session import Session
    import os
    import socket
    from termcolor import cprint
    from datetime import datetime
    import glob
    host = '192.168.0.121'
    user = 'ast'
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, 22))

    session = Session()
    session.handshake(sock)
    session.agent_auth(user)
    print('ok')

    name = file.split('/').pop()
    print(name)
    fileinfo = os.stat(file)
    # chan = session.scp_send64('/home/ast/Documents/ServClient/cam3.jpg', fileinfo.st_mode & 0o777, fileinfo.st_size,
    #                     fileinfo.st_mtime, fileinfo.st_atime)
    now = datetime.now()
    chan = session.scp_send('/home/ast/Documents/ServClient/%s' % name, fileinfo.st_mode & 0o777, fileinfo.st_size)

    with open(file, 'rb') as local_fh:
        for data in local_fh:
            chan.write(data)
    local_fh.close()
    # # read
    # channel, info = session.scp_recv('/home/ast/Documents/ServClient/cam3.jpg')
    # now = datetime.now()
    # buf = 1024
    # size, data = channel.read(buf)
    # f = open('/home/ast/Desktop/cam3.jpg', 'wb')
    # cnt = 0
    # while size == buf:
    #     cnt += 1
    #     f.write(data)
    #     # print('data %s, size: %s' % (cnt, size))
    #     size, data = channel.read(buf)
    # print('end')
    # f.close()
    # channel.close()

    taken = datetime.now() - now
    cprint("Time: %s" % taken, 'yellow', attrs=['reverse'])


def rr(n):
    import glob
    t1 = time.time()
    for f in glob.glob('/run/user/1000/gvfs/smb-share:server=ast-b360m-d3p.local,share=cam/*.jpg'):
        file = open(f, 'rb')
        name = (f.split('/')).pop()
        data = file.read()
        print(name)
        rw = open('/home/ast/Desktop/_IMAGE/CAM/%s' % name, 'wb')
        rw.write(data)
        rw.close()
        file.close()


def saveIMG(image, path='/home/ast/Desktop'):
    PATH_TO_FOLDER = path
    FOLDER_NAME = '_IMAGE/' + time.strftime("%Y-%m-%d")
    _PATH = os.path.join(PATH_TO_FOLDER, FOLDER_NAME)
    # проверка наличия пути, если нет, то создаем папку
    if os.path.exists(_PATH):
        pass
    # print('OK')
    else:
        try:
            os.makedirs(_PATH)
        except OSError:
            print("Creation of the directory %s failed" % _PATH)
        else:
            print("Successfully created the directory %s " % _PATH)

    # формируем имя файла
    FILE_NAME = '{}.jpg'.format(len(os.listdir(_PATH)) + 1)
    # print(FILE_NAME)
    # сохраняем файл в папку
    cv2.imwrite(_PATH + '/' + FILE_NAME, image)


import os


def r_data(port, num):
    # from termcolor import cprint
    # while True:
    # t1 = time.time()
    # file = []
    # os.system('nc -vl %s %s' % (port, num))

    os.system('nc -vl %s >/home/ast/Desktop/test/cam%s.jpg' % (port, num))
    # os.system('cp /home/ast/Desktop/test/cam%s.jpg /home/ast/Desktop/_IMAGE/CAM/cam%s.jpg' % (num, num))
    # i = cv2.imread('/home/ast/Desktop/_IMAGE/CAM/cam%s.jpg' % num)
    # saveIMG(i)

    # a = os.system('nc -vl 4444 >/home/ast/Desktop/test/arc.tar')
    # a = os.system('nc -vl 4444 | lzop -d | tar xvf -')
    # cprint(time.time() - t1, 'blue', attrs=['reverse'])


def r(port, num):
    import os
    import time
    from termcolor import cprint
    # while True:
    t1 = time.time()
    a = os.system('nc -N 192.168.0.40 %s </home/ast/Desktop/CAM/CAMR/cam%s.jpg' % (port, num))
    # a = os.system('nc -N 192.168.0.40 4444 </home/ast/Desktop/CAM/cam1.jpg')
    # a = os.system('(cd ./home/ast/Desktop/CAM/CAMR/; tar cf - .) | lzop | nc -N 192.168.0.40 4444')
    cprint(time.time() - t1, 'red')


def rt():
    import time
    from multiprocessing import Process
    tr1 = Process(target=r, args=('4000', '1'))
    tr2 = Process(target=r, args=('4010', '2'))
    tr3 = Process(target=r, args=('4020', '3'))
    tr4 = Process(target=r, args=('4030', '4'))
    tr5 = Process(target=r, args=('4040', '5'))
    tr6 = Process(target=r, args=('4050', '6'))
    tr7 = Process(target=r, args=('4060', '7'))
    tr8 = Process(target=r, args=('4070', '8'))
    tr11 = Process(target=r, args=('4001', '1_b'))
    tr21 = Process(target=r, args=('4011', '2_b'))
    tr31 = Process(target=r, args=('4021', '3_b'))
    tr41 = Process(target=r, args=('4031', '4_b'))
    tr51 = Process(target=r, args=('4041', '5_b'))
    tr61 = Process(target=r, args=('4051', '6_b'))
    tr71 = Process(target=r, args=('4061', '7_b'))
    tr81 = Process(target=r, args=('4071', '8_b'))
    t1 = time.time()
    tr1.start()
    tr2.start()
    tr3.start()
    tr4.start()
    tr5.start()
    tr6.start()
    tr7.start()
    tr8.start()
    tr11.start()
    tr21.start()
    tr31.start()
    tr41.start()
    tr51.start()
    tr61.start()
    tr71.start()
    tr81.start()
    tr1.join()
    tr2.join()
    tr3.join()
    tr4.join()
    tr5.join()
    tr6.join()
    tr7.join()
    tr8.join()
    tr11.join()
    tr21.join()
    tr31.join()
    tr41.join()
    tr51.join()
    tr61.join()
    tr71.join()
    tr81.join()
    print(time.time() - t1)


def read_data_from_scp():
    from multiprocessing import Process
    while True:
        tr1 = Process(target=r_data, args=('4000', '1'))
        tr2 = Process(target=r_data, args=('4001', '2'))
        tr3 = Process(target=r_data, args=('4002', '3'))
        tr4 = Process(target=r_data, args=('4003', '4'))
        tr5 = Process(target=r_data, args=('4004', '5'))
        tr6 = Process(target=r_data, args=('4005', '6'))
        tr7 = Process(target=r_data, args=('4006', '7'))
        tr8 = Process(target=r_data, args=('4007', '8'))
        tr11 = Process(target=r_data, args=('4010', '1_b'))
        tr21 = Process(target=r_data, args=('4011', '2_b'))
        tr31 = Process(target=r_data, args=('4012', '3_b'))
        tr41 = Process(target=r_data, args=('4013', '4_b'))
        tr51 = Process(target=r_data, args=('4014', '5_b'))
        tr61 = Process(target=r_data, args=('4015', '6_b'))
        tr71 = Process(target=r_data, args=('4016', '7_b'))
        tr81 = Process(target=r_data, args=('4017', '8_b'))

        tr1.start()
        tr2.start()
        tr3.start()
        tr4.start()
        tr5.start()
        tr6.start()
        tr7.start()
        tr8.start()
        tr11.start()
        tr21.start()
        tr31.start()
        tr41.start()
        tr51.start()
        tr61.start()
        tr71.start()
        tr81.start()
        t1 = time.time()
        tr1.join()
        tr2.join()
        tr3.join()
        tr4.join()
        tr5.join()
        tr6.join()
        tr7.join()
        tr8.join()
        tr11.join()
        tr21.join()
        tr31.join()
        tr41.join()
        tr51.join()
        tr61.join()
        tr71.join()
        tr81.join()
        # print(time.time() - t1)


def start_Server_Client():
    import os
    t1 = time.time()
    file = '/home/ast/Desktop/GIT/PYTHON/ServClient.py'
    send_file_scp(file)
    # start script
    try:
        print(os.system('ssh ast@192.168.0.121 python3 /home/ast/Documents/ServClient/ServClient.py'))
    finally:
        # stop script
        # os.system('ssh ast@192.168.0.121 cp -a /home/ast/Documents/CAM_RAW/. /home/ast/Documents/ServClient/')
        os.system('ssh ast@192.168.0.121 killall python3')

    # os.system(
    #     'cp -a /run/user/1000/gvfs/smb-share:server=ast-b360m-d3p.local,share=servclient/. /home/ast/Desktop/test/')

    print(time.time() - t1)


def serv():
    # Echo server program
    import socket
    cnt = 1
    # while True:
    try:
        HOST = ''  # Symbolic name meaning all available interfaces
        PORT = 8888  # Arbitrary non-privileged port
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((HOST, PORT))
        s.listen(10)
        conn, addr = s.accept()
        print(cnt, ': Connected by', addr)
        while True:
            data = conn.recv(10)
            # print(data)
            if not data:
                cnt += 1
                conn.close()
                s.close()
                print('break')
                break
            conn.send(data)
        conn.close()
    except Exception as e:
        pass
        # print('ERR socket: ', e)


import socket


def int_to_bytes(x: int) -> bytes:
    return x.to_bytes((x.bit_length() + 7) // 8, 'big')


def int_from_bytes(xbytes: bytes) -> int:
    return int.from_bytes(xbytes, 'big')


def serv_img(port, file):
    while True:
        try:
            print('serv_img')
            HOST = ''  # Symbolic name meaning all available interfaces
            PORT = port  # Arbitrary non-privileged port
            print('0')
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            print('1', s)
            s.bind((HOST, PORT))
            print('2')
            s.listen(20)
            print('3')
            conn, addr = s.accept()
            print('4', conn, addr)
            # fl = open(file, 'wb')
            size = conn.recv(1024)
            print('5')
            size_int = int_from_bytes(size)
            print('size: ', size_int)
            # # conn.send(size)
            # size_int = 1024
            # with open(file, 'wb') as ff:
            #     data = conn.recv(size_int)
            #     while len(data) == 1024:
            #         ff.write(data)
            #         data = conn.recv(size_int)
            #         print('data: ', len(data))
            #     ff.write(data)
            print(conn.send(b'done'))
            conn.close()
            print('done')
            break

        except Exception as e:
            print('ERR socket image: ', e)
            # conn.close()
            # s.close()
            time.sleep(2)


def perspektive():
    img = cv2.imread('/home/ast/Desktop/_IMAGE/CAM/cam1.jpg')
    rows, cols, ch = img.shape
    # pts1 = np.float32([[0, 100], [1800, 0], [400, 400]])
    # pts2 = np.float32([[0, 0],  [1920, 0], [400, 400]])
    #
    # M = cv2.getAffineTransform(pts1, pts2)
    # dst = cv2.warpAffine(img, M, (cols, rows))

    pts1 = np.float32([[0, 0], [400, 0], [0, 1920], [400, 1920]])
    pts2 = np.float32([[0, 0], [400, 0], [0, 1920], [400, 1920]])
    M = cv2.getPerspectiveTransform(pts1, pts2)
    dst = cv2.warpPerspective(img, M, (cols, rows))

    cv2.imshow('dst', cv2.vconcat([dst, img]))
    cv2.waitKey()
    cv2.destroyAllWindows()


def circle():
    import cv2
    import numpy as np

    img = cv2.imread('/home/ast/Desktop/_IMAGE/NET/top.jpg', 0)
    # img = cv2.medianBlur(img, 5)
    cimg = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

    circles = cv2.HoughCircles(img, cv2.HOUGH_GRADIENT, 1, 100,
                               param1=50, param2=50, minRadius=0, maxRadius=200)

    circles = np.uint16(np.around(circles))
    for i in circles[0, :]:
        # draw the outer circle
        cv2.circle(cimg, (i[0], i[1]), i[2], (0, 255, 0), 2)
        # # draw the center of the circle
        # cv2.circle(cimg, (i[0], i[1]), 2, (0, 0, 255), 3)

    cv2.imshow('detected circles', cimg)
    cv2.waitKey()
    cv2.destroyAllWindows()


# q = 5
def PespectivaChange(sourceimg, lup_1=[0, 0], rup_2=[0, 0], rdn_3=[0, 0], ldn_4=[0, 0]):
    """
    задается смещение в пикселях [x, y]
    точки распологаются по часовой стрелке
    1, 2,
    4, 3
    :param sourceimg:
    :param lup_1:
    :param rup_2:
    :param ldn_4:
    :param rdn_3:
    :return:
    """
    # cv2.imshow("img", sourseimg)
    y, x, _ = sourceimg.shape
    kX1 = 1280 / 373
    kY1 = 300 / 13
    kX2 = 1280 / 847
    kY2 = 300 / 14
    kX3 = 1280 / 851
    kY3 = 300 / 232
    kX4 = 1280 / 355
    kY4 = 300 / 227
    delta = 1280 / 16

    inpquad = np.float32([[x / kX1, y / kY1], [x / kX2, y / kY2], [x / kX3, y / kY3], [x / kX4, y / kY4]])
    outquad = np.float32([[x / kX1 - x / delta + lup_1[0], y / kY1 + lup_1[1]],
                          [x / kX3 + rup_2[0], y / kY1 + rup_2[1]],
                          [x / kX3 + rdn_3[0], y / kY4 + rdn_3[1]],
                          [x / kX4 + ldn_4[0], y / kY4 + ldn_4[1]]])
    m = cv2.getPerspectiveTransform(inpquad, outquad)

    return cv2.warpPerspective(sourceimg, m, dsize=(sourceimg.shape[1], sourceimg.shape[0]))


def setka(img, linex, liney):
    y, x, _ = img.shape
    stepX = x / linex
    curX = 0
    while curX <= x:
        cv2.line(img, (int(curX), 0), (int(curX), y), (245, 149, 149, 31), 1)
        curX = curX + stepX

    stepY = y / liney
    curY = 0
    while curY <= y:
        cv2.line(img, (0, int(curY)), (x, int(curY)), (245, 149, 149, 31), 1)
        curY = curY + stepX


def roi_map_v2(img_o, y_min, y_max, shape=0, add=False):
    img_t = img_o.copy()
    img = cv2.blur(img_o.copy(), (16, 3))

    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = cv2.Canny(img, 0, 200)
    img = cv2.dilate(img, (3, 3), iterations=3)
    img = cv2.blur(img, (16, 3))
    # img = cv2.Canny(img, 0, 200)

    y_mid = y_min + int((y_max - y_min) / 2)
    cv2.imwrite('/home/ast/Desktop/_IMAGE/test.jpg', img)
    top_img = img[:y_mid, :]
    btm_img = img[y_mid:, :]

    # cv2.imshow('detected', img)
    # cv2.waitKey()
    # cv2.destroyAllWindows()

    top_lines = cv2.HoughLinesP(top_img,
                                rho=1,
                                theta=np.pi / 2,
                                threshold=20,
                                minLineLength=300,
                                maxLineGap=20)
    btm_lines = cv2.HoughLinesP(btm_img,
                                rho=1,
                                theta=np.pi / 2,
                                threshold=20,
                                minLineLength=100,
                                maxLineGap=20)

    for line in top_lines:
        x1, y1, x2, y2 = line[0]
        cv2.line(img_t, (x1, y1), (x2, y2), (255, 0, 25), 1)
    for line in btm_lines:
        x1, y1, x2, y2 = line[0]
        cv2.line(img_t, (x1, y1 + y_mid), (x2, y2 + y_mid), (255, 0, 25), 1)
    cv2.imwrite('/home/ast/Desktop/_IMAGE/testt.jpg', img_t)

    top = spline_v2(top_lines, add)
    bottom = spline_v2(btm_lines, add)

    for line in top:
        x1, y1, x2, y2 = line
        cv2.line(img_o, (x1, y1), (x2, y2), (255, 0, 25), 2)
    for line in bottom:
        x1, y1, x2, y2 = line
        cv2.line(img_o, (x1, y1 + y_mid), (x2, y2 + y_mid), (0, 255, 0), 2)
    cv2.imwrite('/home/ast/Desktop/_IMAGE/img_o.jpg', img_o)


def spline_v2(lines, add):
    _L = []
    _L1 = []
    _line = []
    # sort list line
    for l in lines:
        x1, y1, x2, y2 = l[0]
        _L1.append([x1, y1, x2, y2])
    if add:
        yt = _L1[0][1]
        # cam5
        _L1.append([230, yt, 350, yt])
        # cam6
        _L1.append([1330, yt, 1450, yt])
        _L1.append([2220, yt, 2330, yt])
        # cam7
        _L1.append([2880, yt, 2970, yt])
    _L1.sort()
    cnt = 0
    add = True
    for i in range(len(_L1)):
        x1, y1, x2, y2 = _L1[i]
        # добавим первый элемент в список для того, чтобы можно было с ним сравнивать.
        # добавляем первый отсортированный элемент, чтобы с него начать сравнение
        if cnt == 0:
            _L.append([x1, y1, x2, y2])
        cnt += 1
        # для каэдого элемента списка
        for l in _L:
            # if True:
            # если конец линии из списка и новой линии находятся на расстоянии по у меньше чем хх пикселей, то
            if abs(y2 - l[1]) <= 10:
                # если конец новой линии находится дальше существующей, то
                if x2 > l[2]:
                    # если начало новой линии находися в существующей
                    # или новая линия поглащает существующую, то мы расширяем линию и выходим из цикла, иначе
                    if (l[0] <= x1 <= l[3]) \
                            or (x1 >= l[0] and x2 >= l[2]):
                        l[2] = x2
                        l[3] = y2
                        add = False
                        break
                    elif (l[0] <= x1 <= l[3]) \
                            or (x1 >= l[0] and x2 <= l[2]):
                        add = False
                        break
                    else:
                        # устанавливаем флаг на добавление линии в массив
                        add = True
                else:
                    add = False
            else:
                add = True
        # если линия не подошла для расширения ни в один из диапазонов, то мы ее добавляем
        if add:
            _L.append([x1, y1, x2, y2])
            add = True
    # return _L

    # фильтр линий line
    print("_L: ", _L)

    def filtr(line_array):
        new_line_array = []
        for t in range(len(line_array)):
            add = False
            x1, y1, x2, y2 = line_array[t]
            for l in line_array:
                # если рассматриваемая линия поглащается любой из массива,
                # то устанавливаем флаг в False и не добавляем линию
                if x1 > l[0] and x2 < l[2]:
                    add = False
                    break
                elif x1 > l[0] and x2 <= l[2]:
                    add = False
                    break
                elif x1 >= l[0] and x2 < l[2]:
                    add = False
                    break
                else:
                    add = True
            if add:
                new_line_array.append(_L[t])

        return new_line_array

    # return _L

    return filtr(_L)

_PATH_TO_WRITE_BOTTOM_RAW = "/var/www/html/your-project/storage/app/public/board/bottom-raw.jpg"
Roi = dict(
    cam1=[0.1, 0.5, 0, -17], cam2=[0.1, 0.5, 17, -17], cam3=[0.1, 0.5, 17, -17], cam4=[0.1, 0.5, 17, -1],
    cam1_b=[0.1, 0.5, 0, -17], cam2_b=[0.1, 0.5, 17, -17], cam3_b=[0.1, 0.5, 17, -17], cam4_b=[0.1, 0.5, 17, -1],

    cam5_b=[0.1, 0.5, 0, -1], cam6_b=[0.1, 0.5, 0, -1], cam7_b=[0.1, 0.5, 25, -1], cam8_b=[0.1, 0.5, 400, -1],
    cam5=[0.1, 0.5, 0, -1], cam6=[0.1, 0.5, 0, -1], cam7=[0.1, 0.5, 15, -1], cam8=[0.1, 0.5, 400, -1],
)
Frame = dict(cam1=None, cam2=None, cam3=None, cam4=None, cam5=None, cam6=None, cam7=None, cam8=None,
             cam1_b=None, cam2_b=None, cam3_b=None, cam4_b=None, cam5_b=None, cam6_b=None, cam7_b=None, cam8_b=None,
             flag_err=None)

def sepatare_btm_image():
    image = cv2.imread(_PATH_TO_WRITE_BOTTOM_RAW)
    Frame['cam5'] = image[:, :1280 - Roi['cam5'][3]]
    image = image[:, 1280 - Roi['cam5'][3]:]
    Frame['cam6'] = image[:, :1280 - Roi['cam6'][3]]
    image = image[:, 1280 - Roi['cam6'][3]:]
    Frame['cam7'] = image[:, :1280 - Roi['cam7'][3]]
    image = image[:, 1280 - Roi['cam7'][3]:]
    Frame['cam8'] = image

    cv2.imwrite('/home/ast/Desktop/_IMAGE/5.jpg', Frame['cam5'])
    cv2.imwrite('/home/ast/Desktop/_IMAGE/6.jpg', Frame['cam6'])
    cv2.imwrite('/home/ast/Desktop/_IMAGE/7.jpg', Frame['cam7'])
    cv2.imwrite('/home/ast/Desktop/_IMAGE/8.jpg', Frame['cam8'])

def draw_area_defect(img, X1, X2, step=10):
    # for i in range(int((x2 - x1)/step)):
    color = (100, 100, 250)
    y = 1
    x = X1

    while y < img.shape[0]:
        # ----горизонтальные линии------------------------------
        x1, y1, x2, y2 = X1, y, X2, y
        cv2.line(img, (x1, y1), (x2, y2), color, 1)
        y += step
    while x <= X2:
        # ----вертикальные линии------------------------------
        x1, y1, x2, y2 = x, 0, x, img.shape[0]
        cv2.line(img, (x1, y1), (x2, y2), color, 1)
        x += step


def deleteBackground_v03(original_image, background_image):
    """
    Функиция позволяет сделать сравнение 2 изображений для удаления фона путем создания маски
    """
    try:
        ts = time.time()

        delta_gray = 40
        delta_hsv = 40

        # ---------------------------------------------------------------------------------
        gray_original = cv2.cvtColor(original_image.copy(), cv2.COLOR_BGR2GRAY)
        gray_background = cv2.cvtColor(background_image.copy(), cv2.COLOR_BGR2GRAY)

        hsv_original = cv2.cvtColor(original_image.copy(), cv2.COLOR_BGR2HSV)
        hsv_background = cv2.cvtColor(background_image.copy(), cv2.COLOR_BGR2HSV)
        gray_hsv_original = cv2.cvtColor(hsv_original, cv2.COLOR_BGR2GRAY)
        gray_hsv_background = cv2.cvtColor(hsv_background, cv2.COLOR_BGR2GRAY)
        # ---------------------------------------------------------------------------------
        original = cv2.medianBlur(gray_original, 19)
        background = cv2.medianBlur(gray_background, 19)
        original_hsv = cv2.medianBlur(gray_hsv_original, 11)
        background_hsv = cv2.medianBlur(gray_hsv_background, 11)
        # ---------------------------------------------------------------------------------
        msk1 = original >= background + delta_gray
        msk2 = original_hsv >= background_hsv + delta_gray
        msk1b = original < background + delta_hsv
        msk2b = original_hsv < background_hsv + delta_hsv
        # ---------------------------------------------------------------------------------
        original[msk1] = 255
        original_hsv[msk2] = 255
        original[msk1b] = 0
        original = filter(original)

        cv2.imwrite('/home/ast/Desktop/_IMAGE/1.jpg', original)

        original_hsv[msk2b] = 0
        original_hsv = filter(original_hsv)

        msk3 = original_hsv != original
        original_hsv[msk3] = 255
        # ---------------------------------------------------------------------------------
        # original_hsv = filter(original_hsv)
        # filter(original_hsv)
        # ---------------------------------------------------------------------------------
        m = original_hsv == 0
        original_image[m] = [0, 0, 0]
        print(time.time() - ts)
        cv2.imwrite('/home/ast/Desktop/_IMAGE/2.jpg', original_hsv)

        return original_image
    except:
        import traceback
        traceback.print_exc()
        print('err')
        pass

from collections import Counter

def filter(mask):
    black_line = np.linspace(0, 0, mask.shape[1])
    # для каждой продолной линии:
    for i in range(mask.shape[0]):
        # если колличество вхождений 0 (черного цвета) больше заданного, то стираем
        if np.bincount(mask[i])[0] > 0.8 * mask.shape[1]:
            mask[i] = black_line
    return mask


def color(img, K=2):
    k = 3
    y, x = img.shape[:-1]
    # # kernel = np.array([[-0.627, 0.352, -0.627], [0.352, 2.323, 0.352], [-0.627, 0.352, -0.627]])
    # kernel = np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]])
    # img = cv2.filter2D(img, -1, kernel)

    img = cv2.resize(img, (int(x / k), int(y / k)))
    Z = img.reshape((-1, 3))
    # convert to np.float32
    Z = np.float32(Z)
    # define criteria, number of clusters(K) and apply kmeans()
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 2.0)
    # K = 2
    ret, label, center = cv2.kmeans(Z, K, None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)
    # Now convert back into uint8, and make original image
    center = np.uint8(center)
    res = center[label.flatten()]
    res2 = res.reshape((img.shape))
    # res2 = cv2.cvtColor(res2, cv2.COLOR_BGR2GRAY)
    # _, res2 = cv2.threshold(res2, 50, 255, cv2.THRESH_BINARY)

    return res2


import glob
if __name__ == '__main__':
    for f in glob.glob('/home/ast/Desktop/_IMAGE/2019-12-05/*.jpg'):
        # img = cv2.imread('/home/ast/Desktop/_IMAGE/2019-12-05/30.jpg')
        img = cv2.imread(f)
        # _img = cv2.cvtColor(img.copy(), cv2.COLOR_BGR2HSV)
        # _img = cv2.cvtColor(_img, cv2.COLOR_BGR2GRAY)
        img_g = cv2.cvtColor(img.copy(), cv2.COLOR_BGR2GRAY)
        _img = cv2.Canny(img_g.copy(), 100, 250)
        res = cv2.dilate(_img, (3, 3), iterations=1)
        edge_top, _ = cv2.findContours(res, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

        # edge_top.sort(key=lambda i: len(i), reverse=True)
        cv2.drawContours(img, edge_top, -1, (0, 255, 255), 2)

        # img = cv2.imread('/home/ast/Desktop/_IMAGE/2019-12-05/29.jpg')
        # res = color(img, 100)

        # img = cv2.resize(cv2.imread('/home/ast/Desktop/_IMAGE/btn5.jpg'), (1280, 720))
        # imgb = cv2.resize(cv2.imread('/home/ast/Desktop/_IMAGE/btn5b.jpg'), (1280, 720))

        # print(img.shape)
        # print(imgb.shape)

        # img[0:155, :] = 0
        # draw_area_defect(img, 200, 400)
        res = cv2.vconcat([img, cv2.cvtColor(img_g, cv2.COLOR_GRAY2BGR), cv2.cvtColor(res, cv2.COLOR_GRAY2BGR)])
        cv2.imshow('f', cv2.resize(res, (int(res.shape[1]/3), int(res.shape[0]/3))))
        # cv2.imshow('f', img_g)

        key = cv2.waitKey()
        if key == 27:
            cv2.destroyAllWindows()
            break


