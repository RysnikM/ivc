from lxml import etree
import numpy as np

def pars_data_from_net(image, boxes=[], scores=[], classes=[], min_score_thresh=0.5, num=300,
    _folder='', _filename='', _path='', 
    category_index={}):

    '''
    функция парсит данные с нейросети и создает xml файл для 
    возможности дальнейшей корректировки при помощи ПО labelMap и дообучения
    '''

    page = etree.Element('annotation')
    doc = etree.ElementTree(page)

    folder = etree.SubElement(page, 'folder')
    folder.text = _folder

    filename = etree.SubElement(page, 'filename')
    filename.text = _filename + '.jpg'

    path = etree.SubElement(page, 'path')
    path.text = _path

    source = etree.SubElement(page, 'source')
    database = etree.SubElement(source, 'database')
    database.text = 'Unknown'

    _height, _width, _channels = image.shape

    size = etree.SubElement(page, 'size')
    width = etree.SubElement(size, 'width')
    width.text = str(_width)
    height = etree.SubElement(size, 'height')
    height.text = str(_height)
    depth = etree.SubElement(size, 'depth')
    depth.text = str(_channels)

    # переберем очки, для того, чтобы не работать с ненужными данными
    for i in range(num):
        
        if scores[0][i] > min_score_thresh:
            obj = etree.SubElement(page, 'object')
            name = etree.SubElement(obj, 'name')           
            name.text = category_index[np.squeeze(classes).astype(np.int32)[i]].get('name')
            pose = etree.SubElement(obj, 'pose')
            pose.text = 'Unspecified'
            truncated = etree.SubElement(obj, 'truncated')
            truncated.text = '0'
            difficult = etree.SubElement(obj, 'difficult')
            difficult.text = '0'
            bndbox = etree.SubElement(obj, 'bndbox')
            xmin = etree.SubElement(bndbox, 'xmin')
            xmin.text = str((boxes[i][1]*_width).__round__(0))
            ymin = etree.SubElement(bndbox, 'ymin')
            ymin.text = str((boxes[i][0]*_height).__round__(0))
            xmax = etree.SubElement(bndbox, 'xmax')
            xmax.text = str((boxes[i][3]*_width).__round__(0))
            ymax = etree.SubElement(bndbox, 'ymax')
            ymax.text = str((boxes[i][2]*_height).__round__(0))
        else:
            break


    # print (etree.tostring(page, pretty_print=True).decode("utf-8"))  

    # записываем файл
    doc.write(_path +_filename + '.xml', pretty_print=True)



