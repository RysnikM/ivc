import time
from ftplib import FTP
import msoffcrypto
import os
import shutil
import glob
from pyxlsb import open_workbook as open_xlsb

#ftpInputVolume v3.0
#Функция забирает входной объем бревен с 2х линий
#dirpath - путь где храняться скаченные файлы
def ftpInputVolume(dirpath, flag, ftp_data):
    while True:
        # удаляем старые файлы
        for filename in os.listdir(dirpath):
            filepath = os.path.join(dirpath, filename)
            try:
                shutil.rmtree(filepath)
            except OSError:
                os.remove(filepath)

        path1file = dirpath
        path2file = dirpath + 'm1.xlsb'
        # подключаемся к FTP
        ftp = FTP('177.168.3.11')
        ftp.login('root', 'z1x2c3v4b5')

        # функция сохранения файлов входного объема на компьютере
        def loadXlsx(direct_listPath, pathLoad, rash):
            direct_list = ftp.nlst(direct_listPath)
            for dirr in direct_list:
                inName = 'merjenje' in dirr
                inExt = rash in dirr
                nameXlsx = dirr[dirr.rfind('merjenje'):len(dirr)]
                pathDir = pathLoad + nameXlsx
                if inName and inExt:
                    with open(pathDir, 'wb') as f:
                        ftp.retrbinary('RETR ' + dirr, f.write)

        loadXlsx('/doNotTouch/save1', path1file, '.xlsx')

        direct_list = ftp.nlst("/doNotTouch/save1/auto")

        # докачиваем остальные файлы
        for p in direct_list:
            if 'merjenje' in p:
                file_list = ftp.nlst(p)
                loadXlsx(p, path1file, '.xlsb')

        fileFolderXlsb = glob.glob(dirpath+'*.xlsb')
        fileFolderXlsb.sort(reverse=True)
        finder = fileFolderXlsb[0]

        file = msoffcrypto.OfficeFile(open(finder, "rb"))
        file.load_key(password="a444cda")
        file.decrypt(open(path2file, "wb"))

        df = []

        with open_xlsb(path2file) as wb:
            s = wb.get_sheet(11)
            cnt = 0
            for r in s.rows():
                cnt += 1
                potok = [r[1].v, r[13].v]
                if cnt >= 1:
                    break

        flag[4] = 1
        ftp_data[0] = potok[0]
        ftp_data[1] = potok[1]
        ftp_data[2] = potok[0] + potok[1]
        # print(ftp_data[:], flag[4])
        time.sleep(300)

def read_data(dirpath):
    # path1file = dirpath
    # path2file = dirpath + 'm1.xlsb'
    # подключаемся к FTP
    ftp = FTP('177.168.3.11')
    ftp.login('root', 'z1x2c3v4b5')
    ftp.cwd('/doNotTouch/')
    file = 'arch.tar'
    ftp.retrbinary('RETR ' + file, open('/home/ast/Desktop/arch.tar', 'wb').write)

if __name__ == '__main__':
    # pass
    t1 = time.time()
    # ftpInputVolume('/home/ast/Documents/ftpFiles/', [0, 0, 0, 0, 0], [0,0,0,0])
    read_data('/home/ast/Documents/ftpFiles/')
    print(time.time() - t1)
    #
    # print(g)


