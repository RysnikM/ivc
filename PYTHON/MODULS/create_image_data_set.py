import cv2
import numpy as np
import os
import time

PATH_ROOT = '/home/ast/Desktop/_IMAGE/2019-10-23/'
PATH_TO_SAVE = '/home/ast/Desktop/_IMAGE/FOR_TRAINING/'

def fishEyeCam(img):
    K = np.array(
        [[1987.2902013412322, 0.0, 1540.7521175856489], [0.0, 1914.5965846929532, 843.8461627100188], [0.0, 0.0, 1.0]])
    D = np.array([[-0.061326402914289105], [0.2306815273667478], [-1.002826819903716], [1.0982644076261452]])
    DIM = (3072, 1728)
    img_dim = img.shape[:2][::-1]
    balance = 0.0
    scaled_K = K * img_dim[0] / DIM[0]
    scaled_K[2][2] = 1.0
    new_K = cv2.fisheye.estimateNewCameraMatrixForUndistortRectify(scaled_K, D,
                                                                   img_dim, np.eye(3), balance=balance)

    map1, map2 = cv2.fisheye.initUndistortRectifyMap(scaled_K, D, np.eye(3),
                                                     new_K, img_dim, cv2.CV_16SC2)
    undist_image = cv2.remap(img, map1, map2, interpolation=cv2.INTER_LINEAR,
                             borderMode=cv2.BORDER_CONSTANT)
    # k = np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])
    k = np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]])

    undist_image = cv2.filter2D(undist_image, -1, kernel=k)

    return undist_image


def saveIMG(image, path='/home/ast/Desktop'):
    PATH_TO_FOLDER = path
    FOLDER_NAME = time.strftime("%Y-%m-%d")
    _PATH = os.path.join(PATH_TO_FOLDER, FOLDER_NAME)
    # проверка наличия пути, если нет, то создаем папку
    if os.path.exists(_PATH):
        pass
    # print('OK')
    else:
        try:
            os.makedirs(_PATH)
        except OSError:
            print("Creation of the directory %s failed" % _PATH)
        else:
            print("Successfully created the directory %s " % _PATH)
    # формируем имя файла
    FILE_NAME = 'img#{}#{}.jpg'.format(FOLDER_NAME, len(os.listdir(_PATH)) + 1)
    # сохраняем файл в папку
    cv2.imwrite(_PATH + '/' + FILE_NAME, image)

def main():
    list = os.listdir(PATH_ROOT)
    cnt = 0
    for f in list:
        cnt += 1
        image = cv2.imread(os.path.join(PATH_ROOT, f))
        image = fishEyeCam(image)
        saveIMG(image[800:1500, :], PATH_TO_SAVE)
        print(cnt)
        if cnt == 100:
            break
if __name__ == '__main__':
    main()