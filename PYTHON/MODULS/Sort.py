import datetime
import math
import time
from statistics import median
import numpy as np
import psycopg2
from psycopg2.extras import NamedTupleCursor
# ==================================================================================================================
# CREATE VAR
# геометрические параметры рассматриваемой доски
board_param = dict(
    x_min=0, x_max=0,
    y_min=0, y_max=0,
    hight=0,
    wiedth=100,
    lenght=4000)

# коэффициент трансформации из пикселей в миллиметры
# koeff_transform_px_to_mm = 1
koeff_transform_px_to_mm = 1.00

# Дефекты полученные из нейронки
# массив состоит из:
# sort - сорт к которому относится дефект
# meter - на коком метре находится дефект
_list_defects = {
    'suchki_type01': 0,
    'suchki_type02': 0,
    'suchki_type03': 0
}
DEFECTS = {
    'suchki_type01': {
        'plastevye': [],
        'kromochnye_up40': [],
        'kromochnye_over40': []
    },
    'suchki_type02': {
        'plastevye': [],
        'kromochnye_up40': [],
        'kromochnye_over40': []
    },
    'suchki_type03': [],
}
# DEFECTS = {
#     'suchki_type01': {
#         'plastevye': [],
#         'kromochnye_up40': [],
#         'kromochnye_over40': []
#     },
#     'suchki_type02': {
#         'plastevye': [],
#         'kromochnye_up40': [],
#         'kromochnye_over40': []
#     },
#     'suchki_type03': [],
#     'obzol': [],
#     'sineva': [],
#     'treschina_type02': []
# }
RECIPE = {
    'suchki_type01': {
        'enable': True,
        'plastevye': {
            'OTB': {
                'size': 1 / 5,
                'value': 2
            },
            'A': {
                'size': 1 / 4,
                'value': 3
            },
            'B': {
                'size': 1 / 3,
                'value': 4
            },
            'C': {
                'size': 1 / 2,
                'value': 4
            },
            'D': {
                'size': 999,
                'value': 999
            }
        },
        'kromochnye_up40': {
            'OTB': {
                'size': 1 / 5,
                'value': 2
            },
            'A': {
                'size': 1 / 4,
                'value': 3
            },
            'B': {
                'size': 1 / 3,
                'value': 4
            },
            'C': {
                'size': 1 / 2,
                'value': 4
            },
            'D': {
                'size': 999,
                'value': 999
            }
        },
        'kromochnye_over40': {
            'OTB': {
                'size': 1 / 5,
                'value': 2
            },
            'A': {
                'size': 1 / 4,
                'value': 3
            },
            'B': {
                'size': 1 / 3,
                'value': 4
            },
            'C': {
                'size': 1 / 2,
                'value': 4
            },
            'D': {
                'size': 999,
                'value': 999
            }
        },
    },
    'suchki_type02': {
        'enable': True,
        'plastevye': {
            'OTB': {
                'size': 1 / 5,
                'value': 2
            },
            'A': {
                'size': 1 / 4,
                'value': 3
            },
            'B': {
                'size': 1 / 3,
                'value': 4
            },
            'C': {
                'size': 1 / 2,
                'value': 4
            },
            'D': {
                'size': 999,
                'value': 999
            }
        },
        'kromochnye_up40': {
            'OTB': {
                'size': 1 / 5,
                'value': 2
            },
            'A': {
                'size': 1 / 4,
                'value': 3
            },
            'B': {
                'size': 1 / 3,
                'value': 4
            },
            'C': {
                'size': 1 / 2,
                'value': 4
            },
            'D': {
                'size': 999,
                'value': 999
            }
        },
        'kromochnye_over40': {
            'OTB': {
                'size': 1 / 5,
                'value': 2
            },
            'A': {
                'size': 1 / 4,
                'value': 3
            },
            'B': {
                'size': 1 / 3,
                'value': 4
            },
            'C': {
                'size': 1 / 2,
                'value': 4
            },
            'D': {
                'size': 999,
                'value': 999
            }
        },
    },
    'suchki_type03': {
        'enable': True,
        'OTB': {
            'size': 1 / 5,
            'value': 2
        },
        'A': {
            'size': 1 / 4,
            'value': 3
        },
        'B': {
            'size': 1 / 3,
            'value': 4
        },
        'C': {
            'size': 1 / 2,
            'value': 4
        },
        'D': {
            'size': 999,
            'value': 999
        }
    },
    'treschina_type01': {
        'enable': True,
        'nonDeep': {
            'OTB': {
                'size': 1 / 5
            },
            'A': {
                'size': 1 / 4
            },
            'B': {
                'size': 1 / 3
            },
            'C': {
                'size': 1 / 2
            },
            'D': {
                'size': 99
            }
        },
        'deep': {
            'OTB': {
                'size': 1 / 5
            },
            'A': {
                'size': 1 / 4
            },
            'B': {
                'size': 1 / 3
            },
            'C': {
                'size': 1 / 2
            },
            'D': {
                'size': 99
            }
        }
    },
    'treschina_type02': {
        'enable': True,
        'OTB': {
            'size': 1 / 5
        },
        'A': {
            'size': 1 / 4
        },
        'B': {
            'size': 1 / 3
        },
        'C': {
            'size': 1 / 2
        },
        'D': {
            'size': 99
        }
    },
    'prorost': {
        'enable': True,
        'width': {
            'OTB': {
                'size': 1 / 5
            },
            'A': {
                'size': 1 / 4
            },
            'B': {
                'size': 1 / 3
            },
            'C': {
                'size': 1 / 2
            },
            'D': {
                'size': 99
            }
        },
        'length': {
            'OTB': {
                'size': 1 / 5
            },
            'A': {
                'size': 1 / 4
            },
            'B': {
                'size': 1 / 3
            },
            'C': {
                'size': 1 / 2
            },
            'D': {
                'size': 99
            }
        }
    },
    'zabolon': {
        'enable': True,
        'OTB': {
            'size': 1 / 5
        },
        'A': {
            'size': 1 / 4
        },
        'B': {
            'size': 1 / 3
        },
        'C': {
            'size': 1 / 2
        },
        'D': {
            'size': 99
        }
    },
    'plesen': {
        'enable': True,
        'OTB': {
            'size': 1 / 5
        },
        'A': {
            'size': 1 / 4
        },
        'B': {
            'size': 1 / 3
        },
        'C': {
            'size': 1 / 2
        },
        'D': {
            'size': 99
        }
    },
    'mechanicDamage': {
        'enable': True,
        'OTB': {
            'size': 1 / 5
        },
        'A': {
            'size': 1 / 4
        },
        'B': {
            'size': 1 / 3
        },
        'C': {
            'size': 1 / 2
        },
        'D': {
            'size': 99
        }
    },
    'gnil': {
        'enable': True,
        'OTB': {
            'size': 1 / 5
        },
        'A': {
            'size': 1 / 4
        },
        'B': {
            'size': 1 / 3
        },
        'C': {
            'size': 1 / 2
        },
        'D': {
            'size': 99
        }
    },
    'obzol': {
        'enable': True,
        'widthBoard_up40': {
            'width_type01': {
                'OTB': {
                    'size': 1 / 5
                },
                'A': {
                    'size': 1 / 4
                },
                'B': {
                    'size': 1 / 3
                },
                'C': {
                    'size': 1 / 2
                },
                'D': {
                    'size': 99
                }
            },
            'width_type02': {
                'OTB': {
                    'size': 1 / 5
                },
                'A': {
                    'size': 1 / 4
                },
                'B': {
                    'size': 1 / 3
                },
                'C': {
                    'size': 1 / 2
                },
                'D': {
                    'size': 99
                }
            },
            'length_type03': {
                'OTB': {
                    'size': 1 / 5
                },
                'A': {
                    'size': 1 / 4
                },
                'B': {
                    'size': 1 / 3
                },
                'C': {
                    'size': 1 / 2
                },
                'D': {
                    'size': 99
                }
            }
        },
        'widthBoard_over40': {
            'width_type01': {
                'OTB': {
                    'size': 1 / 5
                },
                'A': {
                    'size': 1 / 4
                },
                'B': {
                    'size': 1 / 3
                },
                'C': {
                    'size': 1 / 2
                },
                'D': {
                    'size': 99
                }
            },
            'width_type02': {
                'OTB': {
                    'size': 1 / 5
                },
                'A': {
                    'size': 1 / 4
                },
                'B': {
                    'size': 1 / 3
                },
                'C': {
                    'size': 1 / 2
                },
                'D': {
                    'size': 99
                }
            },
            'length_type03': {
                'OTB': {
                    'size': 1 / 5
                },
                'A': {
                    'size': 1 / 4
                },
                'B': {
                    'size': 1 / 3
                },
                'C': {
                    'size': 1 / 2
                },
                'D': {
                    'size': 99
                }
            }
        },
    },
    'sineva': {
        'enable': True,
        'OTB': {
            'size': 1 / 5
        },
        'A': {
            'size': 1 / 4
        },
        'B': {
            'size': 1 / 3
        },
        'C': {
            'size': 1 / 2
        },
        'D': {
            'size': 99
        }
    }
}

# ИТОГОВЫЙ СОРТ ДОСКИ
SORT = {
    'top': 'D',
    'bottom': 'D',
    'result': 99
}

# вспомогательные переменные
list_d = []

# helper function
def read_recipe():
    try:
        connection = psycopg2.connect(user="admin",
                                      password="z1x2c3v4b5",
                                      host="127.0.0.1",
                                      port="5432",
                                      database="ast")
        cursor = connection.cursor(cursor_factory=NamedTupleCursor)
        # -------------------------------------------------------------------
        postgres_select = """ SELECT * FROM recipe_fir where selected=True"""
        # -------------------------------------------------------------------
        cursor.execute(postgres_select)
        res = cursor.fetchone()
        # -------------------------------------------------------------------
        RECIPE['suchki_type01']['enable'] = res.healthyKnotsEnable
        RECIPE['suchki_type01']['plastevye']['OTB']['size'] = res.healthyKnotsOnStrataSize0s
        RECIPE['suchki_type01']['plastevye']['OTB']['value'] = res.healthyKnotsOnStrataAmount0s
        RECIPE['suchki_type01']['plastevye']['A']['size'] = res.healthyKnotsOnStrataSize1s
        RECIPE['suchki_type01']['plastevye']['A']['value'] = res.healthyKnotsOnStrataAmount1s
        RECIPE['suchki_type01']['plastevye']['B']['size'] = res.healthyKnotsOnStrataSize2s
        RECIPE['suchki_type01']['plastevye']['B']['value'] = res.healthyKnotsOnStrataAmount2s
        RECIPE['suchki_type01']['plastevye']['C']['size'] = res.healthyKnotsOnStrataSize3s
        RECIPE['suchki_type01']['plastevye']['C']['value'] = res.healthyKnotsOnStrataAmount3s
        RECIPE['suchki_type01']['plastevye']['D']['size'] = res.healthyKnotsOnStrataSize4s
        RECIPE['suchki_type01']['plastevye']['D']['value'] = res.healthyKnotsOnStrataAmount4s

        RECIPE['suchki_type01']['kromochnye_up40']['OTB']['size'] = res.healthyKnotsOnEdgeTo40Size0s
        RECIPE['suchki_type01']['kromochnye_up40']['OTB']['value'] = res.healthyKnotsOnEdgeTo40Amount0s
        RECIPE['suchki_type01']['kromochnye_up40']['A']['size'] = res.healthyKnotsOnEdgeTo40Size1s
        RECIPE['suchki_type01']['kromochnye_up40']['A']['value'] = res.healthyKnotsOnEdgeTo40Amount1s
        RECIPE['suchki_type01']['kromochnye_up40']['B']['size'] = res.healthyKnotsOnEdgeTo40Size2s
        RECIPE['suchki_type01']['kromochnye_up40']['B']['value'] = res.healthyKnotsOnEdgeTo40Amount2s
        RECIPE['suchki_type01']['kromochnye_up40']['C']['size'] = res.healthyKnotsOnEdgeTo40Size3s
        RECIPE['suchki_type01']['kromochnye_up40']['C']['value'] = res.healthyKnotsOnEdgeTo40Amount3s
        RECIPE['suchki_type01']['kromochnye_up40']['D']['size'] = res.healthyKnotsOnEdgeTo40Size4s
        RECIPE['suchki_type01']['kromochnye_up40']['D']['value'] = res.healthyKnotsOnEdgeTo40Amount4s

        RECIPE['suchki_type01']['kromochnye_over40']['OTB']['size'] = res.healthyKnotsOnEdgeFrom40Size0s
        RECIPE['suchki_type01']['kromochnye_over40']['OTB']['value'] = res.healthyKnotsOnEdgeFrom40Amount0s
        RECIPE['suchki_type01']['kromochnye_over40']['A']['size'] = res.healthyKnotsOnEdgeFrom40Size1s
        RECIPE['suchki_type01']['kromochnye_over40']['A']['value'] = res.healthyKnotsOnEdgeFrom40Amount1s
        RECIPE['suchki_type01']['kromochnye_over40']['B']['size'] = res.healthyKnotsOnEdgeFrom40Size2s
        RECIPE['suchki_type01']['kromochnye_over40']['B']['value'] = res.healthyKnotsOnEdgeFrom40Amount2s
        RECIPE['suchki_type01']['kromochnye_over40']['C']['size'] = res.healthyKnotsOnEdgeFrom40Size3s
        RECIPE['suchki_type01']['kromochnye_over40']['C']['value'] = res.healthyKnotsOnEdgeFrom40Amount3s
        RECIPE['suchki_type01']['kromochnye_over40']['D']['size'] = res.healthyKnotsOnEdgeFrom40Size4s
        RECIPE['suchki_type01']['kromochnye_over40']['D']['value'] = res.healthyKnotsOnEdgeFrom40Amount4s

        #   suchki_type02
        RECIPE['suchki_type02']['enable'] = res.partHealthyKnotsEnable
        RECIPE['suchki_type02']['plastevye']['OTB']['size'] = res.partHealthyKnotsOnStrataSize0s
        RECIPE['suchki_type02']['plastevye']['OTB']['value'] = res.partHealthyKnotsOnStrataAmount0s
        RECIPE['suchki_type02']['plastevye']['A']['size'] = res.partHealthyKnotsOnStrataSize1s
        RECIPE['suchki_type02']['plastevye']['A']['value'] = res.partHealthyKnotsOnStrataAmount1s
        RECIPE['suchki_type02']['plastevye']['B']['size'] = res.partHealthyKnotsOnStrataSize2s
        RECIPE['suchki_type02']['plastevye']['B']['value'] = res.partHealthyKnotsOnStrataAmount2s
        RECIPE['suchki_type02']['plastevye']['C']['size'] = res.partHealthyKnotsOnStrataSize3s
        RECIPE['suchki_type02']['plastevye']['C']['value'] = res.partHealthyKnotsOnStrataAmount3s
        RECIPE['suchki_type02']['plastevye']['D']['size'] = res.partHealthyKnotsOnStrataSize4s
        RECIPE['suchki_type02']['plastevye']['D']['value'] = res.partHealthyKnotsOnStrataAmount4s

        RECIPE['suchki_type02']['kromochnye_up40']['OTB']['size'] = res.partHealthyKnotsOnEdgeTo40Size0s
        RECIPE['suchki_type02']['kromochnye_up40']['OTB']['value'] = res.partHealthyKnotsOnEdgeTo40Amount0s
        RECIPE['suchki_type02']['kromochnye_up40']['A']['size'] = res.partHealthyKnotsOnEdgeTo40Size1s
        RECIPE['suchki_type02']['kromochnye_up40']['A']['value'] = res.partHealthyKnotsOnEdgeTo40Amount1s
        RECIPE['suchki_type02']['kromochnye_up40']['B']['size'] = res.partHealthyKnotsOnEdgeTo40Size2s
        RECIPE['suchki_type02']['kromochnye_up40']['B']['value'] = res.partHealthyKnotsOnEdgeTo40Amount2s
        RECIPE['suchki_type02']['kromochnye_up40']['C']['size'] = res.partHealthyKnotsOnEdgeTo40Size3s
        RECIPE['suchki_type02']['kromochnye_up40']['C']['value'] = res.partHealthyKnotsOnEdgeTo40Amount3s
        RECIPE['suchki_type02']['kromochnye_up40']['D']['size'] = res.partHealthyKnotsOnEdgeTo40Size4s
        RECIPE['suchki_type02']['kromochnye_up40']['D']['value'] = res.partHealthyKnotsOnEdgeTo40Amount4s

        RECIPE['suchki_type02']['kromochnye_over40']['OTB']['size'] = res.partHealthyKnotsOnEdgeFrom40Size0s
        RECIPE['suchki_type02']['kromochnye_over40']['OTB']['value'] = res.partHealthyKnotsOnEdgeFrom40Amount0s
        RECIPE['suchki_type02']['kromochnye_over40']['A']['size'] = res.partHealthyKnotsOnEdgeFrom40Size1s
        RECIPE['suchki_type02']['kromochnye_over40']['A']['value'] = res.partHealthyKnotsOnEdgeFrom40Amount1s
        RECIPE['suchki_type02']['kromochnye_over40']['B']['size'] = res.partHealthyKnotsOnEdgeFrom40Size2s
        RECIPE['suchki_type02']['kromochnye_over40']['B']['value'] = res.partHealthyKnotsOnEdgeFrom40Amount2s
        RECIPE['suchki_type02']['kromochnye_over40']['C']['size'] = res.partHealthyKnotsOnEdgeFrom40Size3s
        RECIPE['suchki_type02']['kromochnye_over40']['C']['value'] = res.partHealthyKnotsOnEdgeFrom40Amount3s
        RECIPE['suchki_type02']['kromochnye_over40']['D']['size'] = res.partHealthyKnotsOnEdgeFrom40Size4s
        RECIPE['suchki_type02']['kromochnye_over40']['D']['value'] = res.partHealthyKnotsOnEdgeFrom40Amount4s

        # suchki_type03
        RECIPE['suchki_type03']['enable'] = res.badKnotsEnable
        RECIPE['suchki_type03']['OTB']['size'] = res.badKnotsSize0s
        RECIPE['suchki_type03']['OTB']['value'] = res.badKnotsAmount0s
        RECIPE['suchki_type03']['A']['size'] = res.badKnotsSize1s
        RECIPE['suchki_type03']['A']['value'] = res.badKnotsAmount1s
        RECIPE['suchki_type03']['B']['size'] = res.badKnotsSize2s
        RECIPE['suchki_type03']['B']['value'] = res.badKnotsAmount2s
        RECIPE['suchki_type03']['C']['size'] = res.badKnotsSize3s
        RECIPE['suchki_type03']['C']['value'] = res.badKnotsAmount3s
        RECIPE['suchki_type03']['D']['size'] = res.badKnotsSize4s
        RECIPE['suchki_type03']['D']['value'] = res.badKnotsAmount4s

        # treschina_type01
        RECIPE['treschina_type01']['enable'] = res.strataEdgeOutOneEdgeCrackEnable
        RECIPE['treschina_type01']['nonDeep']['OTB']['size'] = res.strataEdgeOutOneEdgeNonDeepCrackSize0s
        RECIPE['treschina_type01']['nonDeep']['A']['size'] = res.strataEdgeOutOneEdgeNonDeepCrackSize1s
        RECIPE['treschina_type01']['nonDeep']['B']['size'] = res.strataEdgeOutOneEdgeNonDeepCrackSize2s
        RECIPE['treschina_type01']['nonDeep']['C']['size'] = res.strataEdgeOutOneEdgeNonDeepCrackSize3s
        RECIPE['treschina_type01']['nonDeep']['D']['size'] = res.strataEdgeOutOneEdgeNonDeepCrackSize4s

        RECIPE['treschina_type01']['deep']['OTB']['size'] = res.strataEdgeOutOneEdgeDeepCrackSize0s
        RECIPE['treschina_type01']['deep']['A']['size'] = res.strataEdgeOutOneEdgeDeepCrackSize1s
        RECIPE['treschina_type01']['deep']['B']['size'] = res.strataEdgeOutOneEdgeDeepCrackSize2s
        RECIPE['treschina_type01']['deep']['C']['size'] = res.strataEdgeOutOneEdgeDeepCrackSize3s
        RECIPE['treschina_type01']['deep']['D']['size'] = res.strataEdgeOutOneEdgeDeepCrackSize4s

        # treschina_type02
        RECIPE['treschina_type02']['enable'] = res.strataEdgeThrouthCrackEnable
        RECIPE['treschina_type02']['OTB']['size'] = res.strataEdgeThrouthCrackSize0s
        RECIPE['treschina_type02']['A']['size'] = res.strataEdgeThrouthCrackSize1s
        RECIPE['treschina_type02']['B']['size'] = res.strataEdgeThrouthCrackSize2s
        RECIPE['treschina_type02']['C']['size'] = res.strataEdgeThrouthCrackSize3s
        RECIPE['treschina_type02']['D']['size'] = res.strataEdgeThrouthCrackSize4s

        # prorost
        RECIPE['prorost']['enable'] = res.sweepEnable
        RECIPE['prorost']['width']['OTB']['size'] = res.sweepWidthSize0s
        RECIPE['prorost']['width']['A']['size'] = res.sweepWidthSize1s
        RECIPE['prorost']['width']['B']['size'] = res.sweepWidthSize2s
        RECIPE['prorost']['width']['C']['size'] = res.sweepWidthSize3s
        RECIPE['prorost']['width']['D']['size'] = res.sweepWidthSize4s
        RECIPE['prorost']['length']['OTB']['size'] = res.sweepLengthSize0s
        RECIPE['prorost']['length']['A']['size'] = res.sweepLengthSize1s
        RECIPE['prorost']['length']['B']['size'] = res.sweepLengthSize2s
        RECIPE['prorost']['length']['C']['size'] = res.sweepLengthSize3s
        RECIPE['prorost']['length']['D']['size'] = res.sweepLengthSize4s

        # zabolon
        RECIPE['zabolon']['enable'] = res.sapwoodEnable
        RECIPE['zabolon']['OTB']['size'] = res.sapwoodSize0s
        RECIPE['zabolon']['A']['size'] = res.sapwoodSize1s
        RECIPE['zabolon']['B']['size'] = res.sapwoodSize2s
        RECIPE['zabolon']['C']['size'] = res.sapwoodSize3s
        RECIPE['zabolon']['D']['size'] = res.sapwoodSize4s

        # plesen
        RECIPE['plesen']['enable'] = res.moldEnable
        RECIPE['plesen']['OTB']['size'] = res.moldSize0s
        RECIPE['plesen']['A']['size'] = res.moldSize1s
        RECIPE['plesen']['B']['size'] = res.moldSize2s
        RECIPE['plesen']['C']['size'] = res.moldSize3s
        RECIPE['plesen']['D']['size'] = res.moldSize4s

        # mechanicDamage
        RECIPE['mechanicDamage']['enable'] = res.mechanicalDamageEnable
        RECIPE['mechanicDamage']['OTB']['size'] = res.mechanicalDamageSize0s
        RECIPE['mechanicDamage']['A']['size'] = res.mechanicalDamageSize1s
        RECIPE['mechanicDamage']['B']['size'] = res.mechanicalDamageSize2s
        RECIPE['mechanicDamage']['C']['size'] = res.mechanicalDamageSize3s
        RECIPE['mechanicDamage']['D']['size'] = res.mechanicalDamageSize4s

        # gnil
        RECIPE['gnil']['enable'] = res.rotEnable
        RECIPE['gnil']['OTB']['size'] = res.rotSize0s
        RECIPE['gnil']['A']['size'] = res.rotSize1s
        RECIPE['gnil']['B']['size'] = res.rotSize2s
        RECIPE['gnil']['C']['size'] = res.rotSize3s
        RECIPE['gnil']['D']['size'] = res.rotSize4s

        # sineva
        RECIPE['sineva']['enable'] = res.blueEnable
        RECIPE['sineva']['OTB']['size'] = res.blueSize0s
        RECIPE['sineva']['A']['size'] = res.blueSize1s
        RECIPE['sineva']['B']['size'] = res.blueSize2s
        RECIPE['sineva']['C']['size'] = res.blueSize3s
        RECIPE['sineva']['D']['size'] = res.blueSize4s

        # obzol
        RECIPE['obzol']['enable'] = res.barkEnable
        RECIPE['obzol']['widthBoard_up40']['width_type01']['OTB']['size'] = res.barkTo40FromEdgeSize0s
        RECIPE['obzol']['widthBoard_up40']['width_type01']['A']['size'] = res.barkTo40FromEdgeSize1s
        RECIPE['obzol']['widthBoard_up40']['width_type01']['B']['size'] = res.barkTo40FromEdgeSize2s
        RECIPE['obzol']['widthBoard_up40']['width_type01']['C']['size'] = res.barkTo40FromEdgeSize3s
        RECIPE['obzol']['widthBoard_up40']['width_type01']['D']['size'] = res.barkTo40FromEdgeSize4s

        RECIPE['obzol']['widthBoard_up40']['width_type02']['OTB']['size'] = res.barkTo40OnEdgeSize0s
        RECIPE['obzol']['widthBoard_up40']['width_type02']['A']['size'] = res.barkTo40OnEdgeSize1s
        RECIPE['obzol']['widthBoard_up40']['width_type02']['B']['size'] = res.barkTo40OnEdgeSize2s
        RECIPE['obzol']['widthBoard_up40']['width_type02']['C']['size'] = res.barkTo40OnEdgeSize3s
        RECIPE['obzol']['widthBoard_up40']['width_type02']['D']['size'] = res.barkTo40OnEdgeSize4s

        RECIPE['obzol']['widthBoard_up40']['length_type03']['OTB']['size'] = res.barkTo40ExtLengthSize0s
        RECIPE['obzol']['widthBoard_up40']['length_type03']['A']['size'] = res.barkTo40ExtLengthSize1s
        RECIPE['obzol']['widthBoard_up40']['length_type03']['B']['size'] = res.barkTo40ExtLengthSize2s
        RECIPE['obzol']['widthBoard_up40']['length_type03']['C']['size'] = res.barkTo40ExtLengthSize3s
        RECIPE['obzol']['widthBoard_up40']['length_type03']['D']['size'] = res.barkTo40ExtLengthSize4s

        RECIPE['obzol']['widthBoard_over40']['width_type01']['OTB']['size'] = res.barkFrom40FromEdgeSize0s
        RECIPE['obzol']['widthBoard_over40']['width_type01']['A']['size'] = res.barkFrom40FromEdgeSize1s
        RECIPE['obzol']['widthBoard_over40']['width_type01']['B']['size'] = res.barkFrom40FromEdgeSize2s
        RECIPE['obzol']['widthBoard_over40']['width_type01']['C']['size'] = res.barkFrom40FromEdgeSize3s
        RECIPE['obzol']['widthBoard_over40']['width_type01']['D']['size'] = res.barkFrom40FromEdgeSize4s

        RECIPE['obzol']['widthBoard_over40']['width_type02']['OTB']['size'] = res.barkFrom40OnEdgeSize0s
        RECIPE['obzol']['widthBoard_over40']['width_type02']['A']['size'] = res.barkFrom40OnEdgeSize1s
        RECIPE['obzol']['widthBoard_over40']['width_type02']['B']['size'] = res.barkFrom40OnEdgeSize2s
        RECIPE['obzol']['widthBoard_over40']['width_type02']['C']['size'] = res.barkFrom40OnEdgeSize3s
        RECIPE['obzol']['widthBoard_over40']['width_type02']['D']['size'] = res.barkFrom40OnEdgeSize4s

        RECIPE['obzol']['widthBoard_over40']['length_type03']['OTB']['size'] = res.barkFrom40ExtLengthSize0s
        RECIPE['obzol']['widthBoard_over40']['length_type03']['A']['size'] = res.barkFrom40ExtLengthSize1s
        RECIPE['obzol']['widthBoard_over40']['length_type03']['B']['size'] = res.barkFrom40ExtLengthSize2s
        RECIPE['obzol']['widthBoard_over40']['length_type03']['C']['size'] = res.barkFrom40ExtLengthSize3s
        RECIPE['obzol']['widthBoard_over40']['length_type03']['D']['size'] = res.barkFrom40ExtLengthSize4s
        # print('DONE RECIPE')
        # --------------------------------------------------------------------------------------------------------------
        cursor.close()
        connection.close()
        # --------------------------------------------------------------------------------------------------------------
    except Exception as e:
        cursor.close()
        connection.close()
    finally:
        cursor.close()
        connection.close()


def chesk_size(size):
    """
    метод позволяет прировнять текущую доску к ближайшему типоразмеру
    input:
    size = list(h,w)
    return:
    актуальный размер по госту
    """
    H = [16, 19, 22, 25, 32, 40, 44, 50, 60]
    W = [50, 75, 100, 125, 150, 175, 200]
    h = size[0]
    w = size[1]
    for i in range(len(H)):
        if h >= H[i] and h < H[i + 1]:
            _h = H[i]
    for j in range(len(W)):
        if w >= W[j] and h < W[j + 1]:
            _w = W[j]
    return (_h, _w)


def check_boarf_param(Frame, Ymin, Ymax, x_min, x_max, data):
    y_min = []
    y_max = []
    for key in Frame.keys():
        try:
            if Ymin[key] != 0:
                y_min.append(Ymin[key])
            if Ymax[key] != 0:
                y_max.append(Ymax[key])
        except Exception as e:
            print('check_height', e)

    # print(y_min, y_max)
    board_param['x_min'] = x_min
    board_param['x_max'] = x_max
    board_param['y_min'] = int(median(y_min))
    board_param['y_max'] = int(median(y_max))
    board_param['wiedth'] = int((board_param['y_max'] - board_param['y_min']) * koeff_transform_px_to_mm)
    board_param['lenght'] = int((x_max - x_min)*(koeff_transform_px_to_mm + 0.3))

    # data for exchange
    data[3] = board_param['x_min']
    data[4] = board_param['x_max']
    data[5] = board_param['y_min']
    data[6] = board_param['y_max']
    data[7] = board_param['wiedth']
    data[8] = board_param['lenght']

    # print(board_param)


def sort_obzol():
    try:
        # arrays to param
        l = []
        w = []
        # koeff transformation pix in mm
        k = 1
        # param defects
        L = 0
        W = 0
        # extract data
        if len(DEFECTS['obzol']) != 0:
            for obz in DEFECTS['obzol']:
                l.append((obz[1] - obz[0])*k)
                w.append((obz[3] - obz[2])*k)
            # clear
            DEFECTS['obzol'] = []
            # lenghr obzol
            for _l in l:
                L = L+_l
            # check recipe lenght
            sort_defect_type3 = 'D'
            RES = RECIPE['obzol']['widthBoard_up40']['length_type03']
            for key in RES:
                if int((L/board_param['lenght'])*100) <= RES[key]['size']:
                    sort_defect_type3 = key
                    break
            DEFECTS['obzol'].append(sort_defect_type3)
            # width obzol
            for _w in w:
                W = W + _w
            # check recipe width
            sort_defect_type2 = 'D'
            RES = RECIPE['obzol']['widthBoard_up40']['width_type02']
            for key in RES:
                if int((W / board_param['wiedth']) * 100) <= RES[key]['size']:
                    sort_defect_type2 = key
                    break
            DEFECTS['obzol'].append(sort_defect_type2)

            add_sort_index(DEFECTS['obzol'])
            DEFECTS['obzol'].sort(reverse=True)
            del_sort_index(DEFECTS['obzol'])

            sort_obzol_result = DEFECTS['obzol'][0]

            DEFECTS['obzol'] = []
            DEFECTS['obzol'] = sort_obzol_result

    except Exception as e:
        print('sort_obzol')
        print(e)


def get_data_from_net_for_sorting(frame, DEFECTS, boxes, scores, classes, data,  min_score_thresh=0.2, num=300,
                                  category_index={}):
    """
    остаем дефекты из нейровки, и на их основе сортируем доску

    доступные дефекты из нейронки:
        id: 1
        name: 'suchki_type01'
        id: 2
        name: 'suchki_type02'
        id: 3
        name: 'suchki_type03'
        id: 4
        name: 'camera'
        id: 5
        name: 'treschina'
        id: 6
        name: 'obzol'
        id: 7
        name: 'serdce'
        id: 8
        name: 'roll'
        id: 9
        name: 'karman'
        id: 10
        name: 'kren'
        id: 11
        name: 'no_name'
        id: 12
        name: 'gnil'
        id: 13
        name: 'no_name'
    """
    # ------------------------------------------------------------------------------------------------------------------
    # считываем актуальный рецепт
    read_recipe()
    # ------------------------------------------------------------------------------------------------------------------
    # получим данные о геомертии доски
    board_param['x_min'] = data[3]
    board_param['x_max'] = data[4]
    board_param['y_min'] = data[5]
    board_param['y_max'] = data[6]
    board_param['wiedth'] = data[7]
    board_param['lenght'] = data[8]
    # ------------------------------------------------------------------------------------------------------------------
    # кол-во дефектов которые попали в сортировку
    num_defects = 0
    # ------------------------------------------------------------------------------------------------------------------
    try:
        # очистим список полученных дефектов
        clearDictL(_list_defects)
        # размеры изображения
        _height, _width, _channels = frame.shape
        # положение дефектов
        boxes = np.squeeze(boxes)
        # очистим словарь дефектов
        clearDict(DEFECTS)
        # перебираем полученные дефекты
        for i in range(num):
            # если вероятность дефекта выше предела, то сортируем и добавляем его
            if np.squeeze(scores)[i] > min_score_thresh:
                # подсчитаем кол-во дефектов:
                num_defects += 1
                # название дефекта
                typeH = category_index[np.squeeze(classes).astype(np.int32)[i]].get('name')
                # print(typeH)
                # print('num: ', i, ' class: ', typeH, ' scores: ', np.squeeze(scores)[i])
                # если сортировка по данному дефекту активирована, то продолжаем
                if typeH in RECIPE and RECIPE[typeH]['enable']:
                    # позиции дефекта в пикселях
                    xmin_defect = (boxes[i][1] * _width).__round__(0) + 3
                    ymin_defect = (boxes[i][0] * _height).__round__(0)
                    xmax_defect = (boxes[i][3] * _width).__round__(0) - 3
                    ymax_defect = (boxes[i][2] * _height).__round__(0)
                    # максимальный размер дефекта по высоте
                    # size_defect = abs(ymax_defect - ymin_defect) * koeff_transform_px_to_mm * 1.0
                    size_defect = max(abs(xmax_defect - xmin_defect), abs(ymax_defect - ymin_defect)) * \
                                  koeff_transform_px_to_mm*0.95
                    # print(typeH, (size_defect/board_param['wiedth']).__round__(3))
                    # определяем подкласс дефекта
                    if typeH == 'suchki_type01' or typeH == 'suchki_type02':
                        _list_defects[typeH] += 1
                        # typeL = check_suchki_plast(board_param['y_min'], ymin_defect)
                        # подкласс определяем как пластевые, т.к. камера не видит сучки с торца доски
                        typeL = 'plastevye'
                        # if typeL == 'kromochnye':
                        #     typeL = typeL + check_suchki_H(board_param['hight'])
                        # проверим к какому наилучшему сотру относится дефект
                        # ----------------------------------------------------------------------------------------------
                        # отнесем дефект к сорту
                        for key in RECIPE[typeH][typeL].keys():
                            # # пропускаем ключ enable
                            if key == 'enable':
                                continue
                            # если не один из дефектов не прошел, считаем что дефект характеризует доску как худщего сорта
                            # для этого задаем стартовый сорт худшим
                            sort_defect = 'D'
                            if size_defect <= RECIPE[typeH][typeL][key]['size'] * board_param['wiedth']:
                                sort_defect = key
                                break
                        # определим к какому метру длинны относится дефект
                        meter = check_meter(xmax_defect)
                        # добавляем дефект в словарь для анализа распределения по длинне
                        DEFECTS[typeH][typeL].append([sort_defect, meter])
                    if typeH == 'suchki_type03':
                        _list_defects['suchki_type03'] += 1
                        # если не один из дефектов не прошел, считаем что дефект характеризует доску как худщего сорта
                        # для этого задаем стартовый сорт худшим
                        sort_defect = 'D'
                        for key in RECIPE[typeH].keys():
                            # пропускаем ключ enable
                            if key == 'enable':
                                continue
                            # если размер сучка меньше заданного в рецепте, то считаем, что дефект относится к
                            # исследуемому сорту и выходим из цикла
                            # print(size_defect, RECIPE[typeH][key]['size'])
                            if size_defect <= RECIPE[typeH][key]['size'] * board_param['wiedth']:
                                sort_defect = key
                                break
                        # определим к какому метру длинны относится дефект
                        meter = check_meter(xmax_defect)
                        # добавляем дефект в словарь для анализа распределения по длинне
                        DEFECTS[typeH].append([sort_defect, meter])
                    # if typeH == 'obzol':
                    #     add all in tupl
                    #     [xmin_defect, xmax_defect, ymin_defect, yman_defect]
                    #     DEFECTS[typeH].append([xmin_defect, ymin_defect, xmax_defect, ymax_defect])

        # sort_obzol()
        # print('DEFECTS:', DEFECTS)
        if num_defects != 0:
            # из списка дефектов выделяем худший, т.к. доска характеризуется по худшему дефекту
            getD(DEFECTS, RECIPE)
            # print('DEFECTS: ', DEFECTS)
            # определим из списка полученныех дефектов сорт исследуемой пласти
            result_sort(list_d)
            # print('RECIPE: ', RECIPE)
        else:
            # если дефектов не найдено, то считаем пласть отборной
            SORT['result'] = 1

    except Exception as e:
        print('get data from  net: ', e, " | time: ", datetime.datetime.now())


def clearDict(Dict):
    for key in Dict.keys():
        if str(type(Dict[key])) == "<class 'dict'>":
            clearDict(Dict[key])
        else:
            Dict[key] = []
    return Dict


def clearDictL(Dict):
    for key in Dict.keys():
        if str(type(Dict[key])) == "<class 'dict'>":
            clearDictL(Dict[key])
        else:
            Dict[key] = 0
    return Dict


def check_suchki_H(h):
    """
    Проверяем куда относится сучек
    кромчатые до 40мм или кромчатые свыше 40мм
    """
    if h <= 40:
        return '_up40'
    else:
        return '_over40'


def check_suchki_plast(y_min_board, y_min_defect):
    """
    Метод определяет относится ли сучек к подклассу пластевых
    """
    if y_min_defect <= y_min_board:
        return 'plastevye'
    else:
        return 'kromochnye'


def check_sort_bord():
    """
    метод определяет сорт доски по определенным параметрам
    :return:
    """
    sort = 'D'
    return sort


def check_meter(x_max_defect):
    """
    метод определяет к какому метру доски относится дефект
    :return:
    """
    # ceil - округляет до большего числа
    # example: ceil(1,2) = 2
    return math.ceil(x_max_defect / (koeff_transform_px_to_mm * 1000))


def getD(Dict, RECIPE):
    for key in Dict.keys():
        if str(type(Dict[key])) == "<class 'dict'>":
            getD(Dict[key], RECIPE[key])
        else:
            if key != 'obzol':
                Dict[key] = sorted_board_knots(Dict[key], RECIPE[key])


def sorted_board_knots(D, RECIPE):
    """
    отнесем доску к какому-либо сорту на основе сучуков.
    """
    if len(D) != 0:
        # для сортировки переберем список и определим худшие сорта
        D = add_l_sort_index(D)
        D.sort(reverse=True)
        D = del_l_sort_index(D)
        minimal_sort = D[0][0]
        # выделим наиольшую концентрацию сучков на 1м доски
        _defect = []
        _defect_uniq = []
        value = 0
        for dfct in D:
            if dfct[0] == minimal_sort:
                value += 1
                _defect.append(dfct)
        for d in _defect:
            if d not in _defect_uniq:
                _defect_uniq.append(d)
        for d in _defect_uniq:
            d.append(_defect.count(d))
        # мы получили дефекты в виде [sort, meter, value]
        # выделим наихудшую концентрацию дефектов
        # отсортировав список по колонке value
        _defect_uniq_bad = sorted(_defect_uniq, key=lambda tup: tup[2], reverse=True)[0]
        # если колличество обнаруженных дефектов больше определенного,
        # то понижаем сорт и делаем проверку повторно
        # для этого сравним содержимое колонок "value"
        if RECIPE[_defect_uniq_bad[0]]['value'] > _defect_uniq_bad[2]:
            _defect_uniq_bad = _defect_uniq_bad[0]
        else:
            list_Recipe = list(RECIPE)
            defect_index = list_Recipe.index(_defect_uniq_bad[0])
            # print(len(list_Recipe))
            if defect_index < (len(list_Recipe) - 1):
                defect_index += 1
                _defect_uniq_bad = list_Recipe[defect_index]
            else:
                _defect_uniq_bad = _defect_uniq_bad[0]

        return _defect_uniq_bad
        # RECIPE[_defect_uniq[0]]
    else:
        return D


def get_date_defects(Dict):
    for key in Dict.keys():
        if str(type(Dict[key])) == "<class 'dict'>":
            get_date_defects(Dict[key])
        else:
            if str(type(Dict[key])) != "<class 'list'>":
                list_d.append(Dict[key])


def add_sort_index(l):
    for i in range(len(l)):
        if l[i] == 'A':
            l[i] = '1_' + l[i]
        if l[i] == 'B':
            l[i] = '2_' + l[i]
        if l[i] == 'C':
            l[i] = '3_' + l[i]
        if l[i] == 'D':
            l[i] = '4_' + l[i]
        if l[i] == 'OTB':
            l[i] = '0_' + l[i]
    return l


def add_l_sort_index(l):
    for i in range(len(l)):
        if l[i][0] == 'A':
            l[i][0] = '1_' + l[i][0]
        if l[i][0] == 'B':
            l[i][0] = '2_' + l[i][0]
        if l[i][0] == 'C':
            l[i][0] = '3_' + l[i][0]
        if l[i][0] == 'D':
            l[i][0] = '4_' + l[i][0]
        if l[i][0] == 'OTB':
            l[i][0] = '0_' + l[i][0]
    return l


def del_sort_index(l):
    for i in range(len(l)):
        l[i] = l[i][2:]
    return l


def del_l_sort_index(l):
    for i in range(len(l)):
        l[i][0] = l[i][0][2:]
    return l


def result_sort(list_d):
    list_d.clear()
    get_date_defects(DEFECTS)
    if len(list_d) != 0:
        list_d = add_sort_index(list_d)
        list_d.sort(reverse=True)
        list_d = del_sort_index(list_d)
        SORT['top'] = list_d[0]

    # переведем в цифры для отправки в ПЛК
    if SORT['top'] == "D":
        SORT['result'] = 5
    if SORT['top'] == "C":
        SORT['result'] = 4
    if SORT['top'] == "B":
        SORT['result'] = 3
    if SORT['top'] == "A":
        SORT['result'] = 2
    if SORT['top'] == "OTB":
        SORT['result'] = 1
    if SORT['top'] == "BRAK" or SORT['top'] == None:
        SORT['result'] = 77




def sort_knots(DEFECTS, boxes, classes, data, category_index):
    """
    остаем сорт по сучкам
        id: 1
        name: 'suchki_type01'
        id: 2
        name: 'suchki_type02'
        id: 3
        name: 'suchki_type03'

    """
    # ------------------------------------------------------------------------------------------------------------------
    # считываем актуальный рецепт
    read_recipe()
    # ------------------------------------------------------------------------------------------------------------------
    # для функции повышения сорта
    knot3 = 0
    from collections import Counter
    c = Counter(classes)
    knot3 = c[3]
    # ------------------------------------------------------------------------------------------------------------------

    # получим данные о геомертии доски
    board_param['wiedth'] = data[7]
    board_param['lenght'] = data[8]
    # ------------------------------------------------------------------------------------------------------------------
    # кол-во дефектов которые попали в сортировку
    num_defects = 0
    # ------------------------------------------------------------------------------------------------------------------
    try:
        # очистим список полученных дефектов
        clearDictL(_list_defects)
        # положение дефектов
        clearDict(DEFECTS)
        # перебираем полученные дефекты
        # print(len(classes))
        for i in range(len(classes)):
            # подсчитаем кол-во дефектов:
            num_defects += 1
            # название дефекта
            typeH = category_index[classes[i]].get('name')
            # print(typeH)
            # если сортировка по данному дефекту активирована, то продолжаем
            if RECIPE[typeH]['enable']:
                # максимальный размер дефекта по высоте
                size_defect = boxes[i][3]*1.2
                # определяем подкласс дефекта
                # print(typeH, boxes[i][2], boxes[i][3])
                # ------------------------------------------------------------------------------------------------------
                if typeH == 'suchki_type01' or typeH == 'suchki_type02':
                    typeL = 'plastevye'
                    # проверим к какому наилучшему сотру относится дефект
                    # print(size_defect, RECIPE[typeH][typeL]['OTB']['size'] * board_param['wiedth']/2)
                    # ----------------------------------------------------------------------------------------------
                    # если размер дуфекта меньше половины от минимального размера сучка по рецепту, то он не учитывается
                    if size_defect < RECIPE[typeH][typeL]['OTB']['size'] * board_param['wiedth']/2:
                        # print(typeH, RECIPE[typeH][typeL]['OTB']['size'] * board_param['wiedth'], boxes[i][3])
                        continue
                    # ----------------------------------------------------------------------------------------------
                    # отнесем дефект к сорту
                    for key in RECIPE[typeH][typeL].keys():
                        # # пропускаем ключ enable
                        if key == 'enable':
                            continue
                        # если не один из дефектов не прошел, считаем что дефект характеризует доску как худщего сорта
                        # для этого задаем стартовый сорт худшим
                        sort_defect = 'D'
                        # print(typeH, size_defect, RECIPE[typeH][typeL][key]['size'] * board_param['wiedth'])
                        if size_defect <= RECIPE[typeH][typeL][key]['size'] * board_param['wiedth']:
                            sort_defect = key
                            break
                    # определим к какому метру длинны относится дефект
                    meter = check_meter(boxes[i][0])
                    # добавляем дефект в словарь для анализа распределения по длинне
                    DEFECTS[typeH][typeL].append([sort_defect, meter])
                # ------------------------------------------------------------------------------------------------------
                if typeH == 'suchki_type03':
                    # ----------------------------------------------------------------------------------------------
                    # если размер дефекта меньше половины от минимального размера сучка по рецепту, то он не учитывается
                    # if size_defect < RECIPE[typeH]['OTB']['size'] * board_param['wiedth']/2:
                    #     # print(typeH, RECIPE[typeH]['OTB']['size'] * board_param['wiedth'], boxes[i][3])
                    #     continue
                    # ----------------------------------------------------------------------------------------------
                    # если не один из дефектов не прошел, считаем что дефект характеризует доску как худщего сорта
                    # для этого задаем стартовый сорт худшим
                    # print(typeH, RECIPE[typeH]['size'] * board_param['wiedth'], boxes[i][3])
                    sort_defect = 'D'
                    for key in RECIPE[typeH].keys():
                        # пропускаем ключ enable
                        if key == 'enable':
                            continue
                        # если размер сучка меньше заданного в рецепте, то считаем, что дефект относится к
                        # исследуемому сорту и выходим из цикла
                        # print(size_defect, RECIPE[typeH][key]['size'])
                        if size_defect <= RECIPE[typeH][key]['size'] * board_param['wiedth']:
                            # print(knot3, size_defect, RECIPE[typeH][key]['size'] * board_param['wiedth'])
                            sort_defect = key
                            break
                    # определим к какому метру длинны относится дефект
                    meter = check_meter(boxes[i][0])
                    # добавляем дефект в словарь для анализа распределения по длинне
                    # ----------------------------------------------------------------------------------------------
                    # в случае, если у нас один дефект 3 типа, то повышаем сортность
                    if knot3 == 1:
                        # понижаем сорт
                        if sort_defect == 'B' and RECIPE[typeH]['A']['size'] != 0:
                            sort_defect = 'A'
                        if sort_defect == 'C' and RECIPE[typeH]['B']['size'] != 0:
                            sort_defect = 'B'
                        if sort_defect == 'D' and RECIPE[typeH]['C']['size'] != 0:
                            sort_defect = 'C'


                    DEFECTS[typeH].append([sort_defect, meter])
      # --------------------------------------------------------------------------------------------------------------
        if num_defects != 0:
            from termcolor import cprint
            # cprint('DEFECTS[suchki_type01] %s'% DEFECTS['suchki_type01'], 'blue')
            # cprint('DEFECTS[suchki_type02] %s'% DEFECTS['suchki_type02'], 'yellow')
            # cprint('DEFECTS[suchki_type01] %s'% DEFECTS['suchki_type03'], 'red')
            # из списка дефектов выделяем худший, т.к. доска характеризуется по худшему дефекту
            getD(DEFECTS, RECIPE)
            # print('DEFECTS: ', DEFECTS)
    #         # определим из списка полученныех дефектов сорт исследуемой пласти
            result_sort(list_d)
    #         # print('RECIPE: ', RECIPE)
        else:
            # если дефектов не найдено, то считаем пласть отборной
            SORT['top'] = 'OTB'
            SORT['result'] = 1
    #
    except Exception as e:
        print('get data from  net: ', e, " | time: ", datetime.datetime.now())





def sort_obzol(obzol, data):
    """ Выведем сорт по обзолу
        Возвратим код сорта
    """
    # ------------------------------------------------------------------------------------------------------------------
    read_recipe()
    # ------------------------------------------------------------------------------------------------------------------
    board_param['wiedth'] = data[7]
    board_param['lenght'] = data[8]
    # print(RECIPE['obzol']['enable'])
    if len(obzol) == 0:
       return 0
    # ------------------------------------------------------------------------------------------------------------------
    if RECIPE['obzol']['enable']:
        # --------------------------------------------------------------------------------------------------------------
        # отсортируем по y, для того, чтобы узнать есть ли обзол с 2 сторон:
        if len(obzol) > 1:
            obzol.sort(key=lambda x: x[1])
            if abs(obzol[0][1] + obzol[0][3] - obzol[-1][1]) > 40:
                # имеем обзол с 2 стоорон, соответственно доска 4 сорта
                return 5
        # --------------------------------------------------------------------------------------------------------------
        # если обзол односторонний, то подсчитаем его длинну по кромке
        len_obzol = 0
        for i in obzol:
            x, y, w, h = i
            len_obzol += w
        # сравним данные с рецептом
        # подсчитаем толщину обзола
        obzol.sort(key=lambda x: x[3])
        hight_obzol = obzol[-1][3]
        # для пиломатериалов до 40мм:
        # проверим длинну
        sort = 'D'
        for key in RECIPE['obzol']['widthBoard_over40']['length_type03'].keys():
            if len_obzol * 6 <= \
                    RECIPE['obzol']['widthBoard_over40']['length_type03'][key]['size'] * board_param['lenght']/100:
                sort = key
                break
        # проверим ширину
        sort1 = 'D'
        for key in RECIPE['obzol']['widthBoard_over40']['width_type01'].keys():
            if hight_obzol*5 <= \
                    RECIPE['obzol']['widthBoard_over40']['length_type03'][key]['size'] * board_param['wiedth']/100:
                sort1 = key
                break

        # вывежим худший сорт по обзолу
        s_cod_1 = sort_cod(sort)
        s_cod_2 = sort_cod(sort1)
        # s_cod_2 = 0
        # ---------------------------------
        return s_cod_1 if s_cod_1 > s_cod_2 else s_cod_2
    else:
        return 0

def sort_cod(sort):
    if sort == "D":
        return 5
    if sort == "C":
        return 4
    if sort == "B":
        return 3
    if sort == "A":
        return 2
    if sort == "OTB":
        return 1







if __name__ == '__main__':
    t1 = time.time()
    # size = [22.3, 102.2, 4011]
    # print(chesk_size(size))
    # # print(DEFECTS)
    DEFECTS['suchki_type01']['plastevye'].append(['OTB', 1])
    DEFECTS['suchki_type01']['plastevye'].append(['B', 1])
    DEFECTS['suchki_type01']['plastevye'].append(['C', 1])

    DEFECTS['suchki_type01']['plastevye'].append(['D', 1])
    DEFECTS['suchki_type01']['plastevye'].append(['D', 1])
    DEFECTS['suchki_type02']['plastevye'].append(['D', 2])
    DEFECTS['suchki_type01']['plastevye'].append(['D', 3])
    DEFECTS['suchki_type01']['plastevye'].append(['B', 1])
    DEFECTS['suchki_type01']['plastevye'].append(['D', 1])

    DEFECTS['suchki_type01']['plastevye'].append(['A', 2])
    DEFECTS['suchki_type03'].append(['OTB', 3])
    DEFECTS['suchki_type01']['plastevye'].append(['A', 4])
    DEFECTS['suchki_type01']['plastevye'].append(['A', 1])
    DEFECTS['suchki_type01']['plastevye'].append(['B', 1])
    DEFECTS['suchki_type01']['plastevye'].append(['A', 10])

    DEFECTS['suchki_type01']['kromochnye_up40'].append(['B', 1])
    DEFECTS['suchki_type01']['kromochnye_up40'].append(['B', 1])

    DEFECTS['obzol'].append([20, 45, 45, 49])

    read_recipe()
    print(RECIPE)
    sort_obzol()
    # print(DEFECTS)
    getD(DEFECTS, RECIPE)
    print(DEFECTS)
    result_sort(list_d)
    print(SORT['result'])
    # sorted()
    print('time: ', time.time() - t1)
